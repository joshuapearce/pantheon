Pantheon Help: Asset Tracker / Asset Detective

Important: Pantheon *never* uploads any of your data outside of your direct control, and we store none of it outside of your computer. There should be no security or privacy concerns since none of your files or information ever leave your system.

  - The Asset Tracker is used to track what file name matched what asset GUID, so that if a file is later lost or deleted, it is possible for Pantheon to give you hints about that file. Instead of "missing script", you will get everything pantheon knew about that file.

  - All this information is stored locally, on your computer. By default, this information will be shared with other Unity projects you are running.




The Asset Detective panel will give you hints and links to objects in your scene which have missing components or references. It will not detect null references, only references which lead to invalid assets or objects.

The Asset Detective will similarly locate instances of missing prefabs, in the active scene.





The inspector panel for missing components has been replaced/improved from the default version. It now provides similar information as the Asset Detective panel does.

