﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

using UnityEditor;
using UnityEngine;

namespace Pantheon {
	[InitializeOnLoad]
	public class AutosaveSettings:
		ModuleSettings
	{
		static AutosaveSettings instance_;
		[SelfInitializing] public static AutosaveSettings instance {
			get {
				if(instance_ == null) instance_= ModuleSettings.LoadOrCreate<AutosaveSettings>("Autosave.settings");
				return instance_;
			}
		}

		#if UNITY_2018_3_OR_NEWER
		[SettingsProvider]
    private static SettingsProvider InitializeSettingsProvider_static() {
			return instance.InitializeSettingsProvider();
		}
		#endif

		new void OnEnable() {
			base.OnEnable();
			instance_= this;
			displayName= "Autosaving";
		}

		//--------------------------------
		[HideInInspector] public double timeOfLastSave;
		[HideInInspector] public Autosave.State state;

		public string AutosavePath {
			get {
				if(!string.IsNullOrEmpty(AutosavePathOverride)) return AutosavePathOverride;
				else return EditorManager.projectPath + "Pantheon/Autosave/";
			}
		}

		[SettingsField("Autosave Path Override", "", SettingsFieldAttribute.Flags.Advanced, 100)]
		public string AutosavePathOverride;

		[SettingsField(
			"Autosave Frequency (seconds)",
			"",
			SettingsFieldAttribute.Flags.SystemWide,
			200
		)]
		public int AutosaveFrequency {
			get { return EditorPrefs.GetInt("Pantheon.AutosaveFrequency", 300); }
			set { EditorPrefs.SetInt("Pantheon.AutosaveFrequency", value); }
		}

		[SettingsField(
			"Max Backups Per Scene",
			"0 for unlimited backups",
			SettingsFieldAttribute.Flags.SystemWide,
			300
		)]
		public int MaxBackupsPerScene {
			get { return EditorPrefs.GetInt("Pantheon.MaxBackupsPerScene", 10); }
			set { EditorPrefs.SetInt("Pantheon.MaxBackupsPerScene", value); }
		}

		[SettingsField(
			"Autosave only if scene is dirty",
			"Safe to leave this enabled. Disable if you don't trust the system. Either way, autosaving will still be throttled while Unity does not have focus.",
			SettingsFieldAttribute.Flags.SystemWide,
			400
		)]
		public bool AutosaveOnlyIfDirty {
			get { return EditorPrefs.GetBool("Pantheon.AutoSave.AutosaveOnlyIfDirty", true); }
			set { EditorPrefs.SetBool("Pantheon.AutoSave.AutosaveOnlyIfDirty", value); }
		}

		[SettingsField(
			"Autosave on every play",
			null,
			SettingsFieldAttribute.Flags.SystemWide,
			500
		)]
		public bool AutosaveOnPlay {
			get { return EditorPrefs.GetBool("Pantheon.AutoSave.AutosaveOnPlay", true); }
			set { EditorPrefs.SetBool("Pantheon.AutoSave.AutosaveOnPlay", value); }
		}

		[SettingsField(
			"Log saving to console",
			null,
			SettingsFieldAttribute.Flags.SystemWide,
			550
		)]
		public bool DoDebugLog {
			get { return EditorPrefs.GetBool("Pantheon.AutoSave.DoDebugLog", true); }
			set { EditorPrefs.SetBool("Pantheon.AutoSave.DoDebugLog", value); }
		}

		[SettingsField(
			"Display notification on save",
			null,
			SettingsFieldAttribute.Flags.SystemWide,
			551
		)]
		public bool DoNotify {
			get { return EditorPrefs.GetBool("Pantheon.AutoSave.DoNotify", true); }
			set { EditorPrefs.SetBool("Pantheon.AutoSave.DoNotify", value); }
		}

		[SettingsField(
			"Use Unity temp saves",
			"Unity occasionally temporarily saves the scene to disk. This setting will look for that temp file, instead of creating a new one. Leave enabled unless there's a problem.",
			SettingsFieldAttribute.Flags.Advanced,
			600
		)]
		public bool CopyNormalSavesWhenPossible {
			get { return EditorPrefs.GetBool("Pantheon.AutoSave.CopyNormalSavesWhenPossible", true); }
			set { EditorPrefs.SetBool("Pantheon.AutoSave.CopyNormalSavesWhenPossible", value); }
		}

		[SettingsField(
			"Prevent Duplicate Backups",
			"The new backup will be compared to the most recent backup, and ignored if the recent file is exactly the same",
			SettingsFieldAttribute.Flags.Advanced,
			700
		)]
		public bool PreventDuplicateBackups {
			get { return EditorPrefs.GetBool("Pantheon.AutoSave.PreventDuplicateBackups", true); }
			set { EditorPrefs.SetBool("Pantheon.AutoSave.PreventDuplicateBackups", value); }
		}

		[SettingsField(
			"Require User Inactivity for",
			"An autosave will not be triggered unless no keyboard/mouse activity is detected in Unity for this many seconds in a row. Detection is not always 100%, so an autosave may sometimes happen anyways.",
			SettingsFieldAttribute.Flags.Advanced,
			800
		)]
		public int RequireUserInactivitySeconds {
			get { return EditorPrefs.GetInt("Pantheon.AutoSave.RequireUserInactivitySeconds", 5); }
			set { EditorPrefs.SetInt("Pantheon.AutoSave.RequireUserInactivitySeconds", value); }
		}

		[SettingsField(
			"Ignore User Inactivity after",
			"If user activity prevents an autosave for this many seconds, it will save no matter what",
			SettingsFieldAttribute.Flags.Advanced,
			900
		)]
		public int IgnoreRequireUserInactivity {
			get { return EditorPrefs.GetInt("Pantheon.AutoSave.IgnoreRequireUserInactivity", 60); }
			set { EditorPrefs.SetInt("Pantheon.AutoSave.IgnoreRequireUserInactivity", value); }
		}

		[SettingsField(
			"Save like I'm Paranoid",
			"Most settings/conditions will be ignored, and the scene will save as often as Autosave Frequency allows",
			SettingsFieldAttribute.Flags.Advanced,
			1000
		)]
		public bool SaveLikeImParanoid {
			get { return EditorPrefs.GetBool("Pantheon.AutoSave.SaveLikeImParanoid", false); }
			set { EditorPrefs.SetBool("Pantheon.AutoSave.SaveLikeImParanoid", value); }
		}

		[SettingsField(
			"Save screenshot with scene",
			"It's possible some of your desktop or other windows could be accidentally saved in the screenshot, so don't use it if security/privacy is paramount",
			SettingsFieldAttribute.Flags.Default,
			1100
		)]
		public bool SaveSceneScreenshot {
			get { return EditorPrefs.GetBool("Pantheon.AutoSave.SaveSceneScreenshot", false); }
			set { EditorPrefs.SetBool("Pantheon.AutoSave.SaveSceneScreenshot", value); }
		}
	
		//----------------------------------
		public override void DrawContents(float contentWidth) {
			GUILayout.BeginVertical(defaultMargins);
			Styles.DrawLink("Open autosaves directory", AutosavePath);
			GUILayout.EndVertical();

			base.DrawContents(contentWidth);
		}

		public override string ConfigHeaderInfo() {
			if(double.IsNaN(Autosave.timeSinceLastSave)) return "Never autosaved";

			int minutes= (int)Math.Floor(Autosave.timeSinceLastSave / 60.0);
			int seconds= (int)Math.Floor(Autosave.timeSinceLastSave % 60.0);

			string str= "";

			if(!Autosave.enabled) str += "This module is disabled by your settings!\n\n";

			str += "Time since current scene saved, loaded, or backed up: ";
			if(minutes > 1) str += minutes + " minutes";
			else if(minutes == 1) str += "1 minute";
			else if(seconds != 1) str += seconds + " seconds";
			else str += "1 second";
			
			return str;
		}
	}
}