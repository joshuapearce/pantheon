﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

using UnityEditor;
using UnityEngine;
using UnityEditor.SceneManagement;
using UnityEngine.SceneManagement;
using UnityEditor.Events;

namespace Pantheon {
	[InitializeOnLoad]
	public class Autosave {
		static AutosaveSettings settings { get { return AutosaveSettings.instance; } }
		public static bool enabled { get; private set; }

		public static EditorManager.StringFunction onSaved;

		[MenuItem("Tools/Pantheon/Open autosave folder", false, ToolsMenu.MenuPriority + 60)]
		public static void OpenAutosaveFolder() {
			System.Diagnostics.Process.Start(Autosave.settings.AutosavePath);
		}

		static Autosave() {
			EditorManager.RegisterModule("Autosave", (state) => {
				enabled= state;
			});

			Manager.editorRunOnce += Initialize;

			if(EditorManager.isFirstEditorInstance) {
				settings.timeOfLastSave= float.NaN;
			}

			//This accessor needs to be added here, so that it is present when play mode initializes
			EditorApplication.playModeStateChanged += (playstate) => {
				if(!settings.AutosaveOnPlay) return;

				if(playstate == PlayModeStateChange.ExitingEditMode) {
					if(!settings.CopyNormalSavesWhenPossible) {
						state= State.SavedBeforePlaying;
						SaveNow("Play mode activating");
					}

				}else	if(playstate == PlayModeStateChange.EnteredPlayMode) {
					if(settings.CopyNormalSavesWhenPossible) {
						state= State.SavedBeforePlaying;
						foreach(var scene in activeScenes) {
							//Manager.Log(scene.name + ((scene.isDirty) ? " is dirty" : " is not dirty"));
							if(scene.isDirty || settings.SaveLikeImParanoid) {
								DoBackup(scene, FindPlayModeSceneAsset(scene), "Play mode activated");
							}
						}
					}
				}
			};
		}

		static bool Initialized;

		public enum State {
			Error= -1,
			Initializing= 0,
			SavedNormally,
			SavedBeforePlaying,
			SavedWhileInactive
		};
		static State state { get { return settings.state; } set { settings.state= value; } }

		public static double timeSinceLastSave {
			get { return EditorApplication.timeSinceStartup - settings.timeOfLastSave; }
			set { settings.timeOfLastSave= EditorApplication.timeSinceStartup - value; }
		}
		//static double timeOfLastSave { get { return settings.timeOfLastSave; } }
		static double timeOfLastUpdate;
		static double timeSinceLastUpdate {
			get { return EditorApplication.timeSinceStartup - timeOfLastUpdate; }
			set { timeOfLastUpdate= EditorApplication.timeSinceStartup - value; }
		}

		static double timeOfUserActivity= 0.0f;
		static double timeSinceLastUserActivity {
			get { return EditorApplication.timeSinceStartup - timeOfUserActivity; }
			set { timeOfUserActivity= EditorApplication.timeSinceStartup - value; }
		}

		static IEnumerable<Scene> activeScenes {
			get {
				int count= SceneManager.sceneCount;
				for(int i= 0; i<count; i++) {
					yield return SceneManager.GetSceneAt(i);
				}
			}
		}

		static void Initialize() {
			if(Initialized) return;
			Initialized= true;

			try {
				if(!Directory.Exists(settings.AutosavePath)) {
					Directory.CreateDirectory(settings.AutosavePath);
					//File.WriteAllText(settings.AutosavePath + ".gitignore", "*");
					//File.WriteAllText(settings.AutosavePath + ".collabignore", "*");
				}

			}catch(Exception e) {
				state= State.Error;
				Debug.LogError("There was an error trying to create the Pantheon autosave directory at '" + settings.AutosavePath + "'");
				Debug.LogException(e);
				return;
			}

			#if UNITY_2018_2_OR_NEWER
			EditorSceneManager.activeSceneChangedInEditMode += (oldScene, newScene) => {
				timeSinceLastSave= 0;
			};
			#endif

			EditorSceneManager.sceneSaved += (Scene scene) => {
				if(string.IsNullOrEmpty(scene.path)) return;

				if(scene.isDirty) {
					//This means (as of v2019.2.0) the scene was probably being saved to a temporary location
					//If it were updating the original asset file, it would not still be flagged as dirty
					//Because we don't want temporary saves to trigger a backup, we ignore it
					return;
				}

				timeSinceLastSave= 0;
				state= State.SavedNormally;
				DoBackup(scene, GetOrCreateSceneAsset(scene), "Saved through editor");
			};

			EditorApplication.update += StaticUpdate;

			#if UNITY_2019_OR_NEWER
				Manager.globalKeyEventListener += (e)=> {
					timeSinceLastUserActivity= 0.0f;
				};

				//SceneView.onSceneGUIDelegate += (sceneView) {
				SceneView.duringSceneGui += (sceneView)=> {
					timeSinceLastUserActivity= 0.0f;
				};
			#else
				timeSinceLastUserActivity= float.PositiveInfinity;
			#endif
		}

		static void StaticUpdate() {
			if(state == State.Error) return;

			if(EditorApplication.timeSinceStartup < 30.0f) {
				//This prevents the autosave from triggering right after Unity launches for whatever reason
				//It should probably never be triggered
				state= State.Initializing;
				return;
			}

			double timeDelta= timeSinceLastUpdate;
			timeSinceLastUpdate= 0.0f;

			//bool isPlaying= Application.isPlaying;
			bool hasFocus= UnityEditorInternal.InternalEditorUtility.isApplicationActive;
			hasFocus &= !Application.isPlaying;

			if(!enabled) return;
			if((settings.AutosaveFrequency <= 0) || (timeSinceLastSave < settings.AutosaveFrequency)) return;
			bool requestSave= true;

			if(!hasFocus) {
				if(state == State.SavedWhileInactive) {
					//Keep resetting the timer until the editor has focus again,
					//We'll still continue the normal save logic (which will usually result in nothing)
					timeSinceLastSave= 0;
					requestSave= false;
				}
			}

			if(
				hasFocus &&
				(timeSinceLastUserActivity < settings.RequireUserInactivitySeconds) &&
				(timeSinceLastSave < settings.IgnoreRequireUserInactivity)
			) {
				//This avoids interupting the user while they're interacting
				requestSave= false;
			}

			if(hasFocus) state= State.SavedNormally;
			else state= State.SavedWhileInactive;

			if(!Application.isPlaying) {
				if(requestSave) {
					timeSinceLastSave= 0;
					SaveNow("Automatic");
				}else if(settings.SaveLikeImParanoid) {
					timeSinceLastSave= 0;
					SaveNow("Paranoid");
				}
			}
		}

		//--------------------------------
		[MenuItem("Tools/Pantheon/Advanced/Backup open scenes now", false, ToolsMenu.MenuPriority + 1000)]
		static void SaveNowMenu() {
			SaveNow("User requested", false);
		}

		public static bool SaveNow(string reasonMessage, bool onlyIfDirty= true) {
			bool state= true;

			foreach(var scene in activeScenes) {
				string path= null;

				if(scene.isDirty) {
					path= DoBackup(scene, CreateSceneTempAsset(scene), reasonMessage);
				}else if(settings.SaveLikeImParanoid || !onlyIfDirty) {
					path= DoBackup(scene, GetOrCreateSceneAsset(scene), reasonMessage);
				}

				if(path != null) {
					state= true;
				}
			}

			timeSinceLastSave= 0.0f;
			return state;
		}
		
		static string CreateSceneTempAsset(Scene scene) {
			string temppath= EditorManager.tempDataPath + "SceneInstance.unity";
			EditorSceneManager.SaveScene(scene, temppath, true);
			return temppath;
		}

		static string FindPlayModeSceneAsset(Scene scene, bool relative= false) {
			if(!Application.isPlaying) {
				throw new InvalidOperationException("Can't use this function form edit mode");
			}

			int index= 0;
			foreach(var scene_i in activeScenes) {
				if(scene_i == scene) {
					var str= relative ? "" : EditorManager.projectPath;
					str += "temp/__Backupscenes/" + index + ".backup";
					return str;
				}
			}

			Debug.LogError("Scene '" + scene.name + "' was not found in the temp backups");
			return null;
		}

		static string GetOrCreateSceneAsset(Scene scene) {
			if(string.IsNullOrEmpty(scene.path) || scene.isDirty) {
				if(Application.isPlaying) {
					return FindPlayModeSceneAsset(scene);
				}else{
					return CreateSceneTempAsset(scene);
				}
			}else{
				return scene.path;
			}
		}

		static void CopyAsset(string sourcePath, string destinationPath) {
			if(sourcePath == EditorManager.tempDataPath + "SceneInstance.unity") {
				File.Move(sourcePath, destinationPath);
			}else{
				File.Copy(sourcePath, destinationPath);
			}
		}

		static string DoBackup(Scene scene, string sourcePath, string reasonMessage) {
			bool isTemp= string.IsNullOrEmpty(scene.path);
			string GUID;
			string filename;
			string originalPath;

			if(isTemp) {
				GUID= "null";
				originalPath= "Untitled Scene.unity";
				filename= "Untitled Scene.unity";
			}else{
				originalPath= scene.path;
				GUID= AssetDatabase.AssetPathToGUID(scene.path);
				filename= Path.GetFileName(scene.path);
			}

			var IDs= GetExistingIDs(GUID);
			int prevIndex=(IDs.Count > 0) ? IDs[0] : 0;
			string previousOutputPath= null;
			int ID= prevIndex + 1;
			IDs.Insert(0, ID);

			EditorManager.Log("Backup requested for '" + originalPath + "'" + ": " + reasonMessage);
			
			bool deletePrevious= false;
			if(settings.PreventDuplicateBackups && (prevIndex > 0)) {
				previousOutputPath= GetDirectoryPathFromIndex(GUID, prevIndex) + filename;
				if(CompareFiles(previousOutputPath, sourcePath)) {
					EditorManager.Log("Previous backup will be deleted because it's identical", true);
					deletePrevious= true;
				}
			}

			string outputDir= GetDirectoryPathFromIndex(GUID, ID);
			string outputPath= outputDir + filename;
			Directory.CreateDirectory(outputDir);

			if(deletePrevious) {
				//Moving the old scene file, before deleting the directory it's in, instead of copying an identical file
				File.Move(previousOutputPath, outputPath);
				IDs.Remove(prevIndex);
				DeleteBackup(GUID, prevIndex);
			}else{
				CopyAsset(sourcePath, outputPath);
			}

			if(!isTemp) {
				File.Copy(originalPath + ".meta", outputPath + ".meta");
			}

			File.WriteAllText(outputPath + ".nfo",
				"Original file path = " + originalPath + "\n" +
				"Time of autobackup (UTC)= " + DateTime.UtcNow.ToString() + "\n" +
				"Time of autobackup (Local)= " + DateTime.Now.ToString() + "\n" +
				"Unity version = " + UnityEditorInternal.InternalEditorUtility.GetFullUnityVersion() + "\n" +
				"Editor uptime = " + (int)EditorApplication.timeSinceStartup + "s\n" +
				"Reason for backup = " + reasonMessage + "\n"
			);

			if(settings.SaveSceneScreenshot) {
				var texture= ScreenshotTool.CaptureEditorNow(SceneView.lastActiveSceneView);
				if(texture != null) ScreenshotTool.WriteTexture(texture, outputPath + ".png");
			}

			if(settings.DoDebugLog) {
				Debug.Log("Scene '" + scene.name + "' backed up: " + reasonMessage);
			}

			if(settings.DoNotify) {
				EditorManager.Notify("Scene backed up");
			}

			EditorManager.Log("Done backing up asset '" + sourcePath + "'");

			if(settings.MaxBackupsPerScene > 0) {
				while(IDs.Count > settings.MaxBackupsPerScene) {
					int i= IDs.Count - 1;
					DeleteBackup(GUID, IDs[i]);
					IDs.RemoveAt(i);
				}
			}

			if(onSaved != null) {
				onSaved(outputPath);
			}

			return outputPath;
		}
		
		//--------------------------------
		static bool CompareFiles(string path1, string path2) {
			if(string.IsNullOrEmpty(path1)) return false;
			if(string.IsNullOrEmpty(path2)) return false;
			if(!File.Exists(path1)) return false;
			if(!File.Exists(path2)) return false;

			if(path1 == path2) return true;

			using(FileStream file1 = File.OpenRead(path1), file2 = File.OpenRead(path2)) {
				if(file1.Length != file2.Length) {
					return false;
				}else{
					var buffer1 = new byte[32768];
					var buffer2 = new byte[buffer1.Length];

					for(int pos= 0; pos<file1.Length; pos += buffer1.Length) {
						int bytesRead = file1.Read(buffer1, 0, buffer1.Length);
						file2.Read(buffer2, 0, buffer1.Length);

						for(int b = 0; b<bytesRead; b++) {
							if(buffer1[b] != buffer2[b]) {
								return false;
							}
						}
					}
				}
			}

			return true;
		}

		static void DeleteBackup(string GUID, int ID) {
			string filepath= GetDirectoryPathFromIndex(GUID, ID);

			EditorManager.Log("Deleting backup '" + filepath + "'");

			Directory.Delete(filepath, true);

			if(
				(Directory.GetFiles(GetDirectoryPath(GUID)).Length == 0) &&
				(Directory.GetDirectories(GetDirectoryPath(GUID)).Length == 0)
			) {				
				Directory.Delete(GetDirectoryPath(GUID));
			}
		}

		public static string GetDirectoryPathFromIndex(string GUID, int ID) {
			return GetDirectoryPath(GUID) + ID + "/";
		}

		static string CreateNewFilePath(string GUID) {
			var IDs= GetExistingIDs(GUID);
			int prevIndex=(IDs.Count > 0) ? IDs[0] : 0;
			int index= prevIndex + 1;

			string path= GetDirectoryPathFromIndex(GUID, index);
			Directory.CreateDirectory(path);
			return path;
		}

		static string GetDirectoryPath(string GUID) {
			return settings.AutosavePath + GUID + "/";
		}

		public static List<int> GetExistingIDs(string GUID) {
			var output= new List<int>();
			var path= GetDirectoryPath(GUID);

			if(Directory.Exists(path)) {
				foreach(var subdir in Directory.GetDirectories(path)) {
					var match= Regex.Match(subdir, "[/\\\\](\\d+)$");
					if(match.Success) {
						output.Add(int.Parse(match.Groups[1].Value));
					}
				}

				output.Sort((a,b) => b.CompareTo(a));
			}

			return output;
		}

		public static string GetSceneBackupInfo(string GUID, int index) {
			string path= GetDirectoryPathFromIndex(GUID, index);
			if(path == null) return null;

			foreach(var nfofile in Directory.GetFiles(path, "*.nfo")) {
				return File.ReadAllText(nfofile);
			}

			return null;
		}
	}
}