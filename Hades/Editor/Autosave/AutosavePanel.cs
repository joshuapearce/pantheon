using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;

using UnityEditor;
using UnityEngine;

namespace Pantheon {
	[InitializeOnLoad]
	public class AutosavePanel:
		VulcanWindow
	{
		[ConditionalMenuItem("Tools/Pantheon/Autosave Viewer", ToolsMenu.MenuPriority)]
		[ConditionalMenuItem("Window/General/Autosave Viewer", ToolsMenu.GeneralMenuPriority)]
		public static void Create() {
			var instance= VulcanWindowManager.OpenWindow<AutosavePanel>();
			instance.Focus();			
		}

		static AutosavePanel() {

		}

		protected override ModuleSettings settingsModule { get { return AutosaveSettings.instance; } }

		class SceneInfo {
			public struct VersionInfo {
				Texture thumbnail_;
				bool thumbnail_error;
				public Texture thumbnail {
					get {
						if(!thumbnail_error && (thumbnail_ == null)) {
							thumbnail_= ThumbnailManager.GenerateThumbnail(screenshot_path);
							thumbnail_error= (thumbnail_ != null);
						}
						return thumbnail_;
					}
				}
				public string screenshot_path { get { return directory + filename + ".png"; } }

				public int id;
				public DateTime dateTime;
				public string reason;
				public string filename;
				public string assetPath;
				public string directory;

				public string filePath { get { return directory + filename; } }
			}

			public List<VersionInfo> versions= new List<VersionInfo>();
		}
		[NonSerialized] List<SceneInfo> scenes= new List<SceneInfo>();
		[NonSerialized] SceneInfo overScene;
		[NonSerialized] bool overThumbnail;

		//--------------------------------
		protected override void InitializePanel() {
			repaintOccasionallyInEdit= true;
			repaintOccasionallyInPlay= true;
			hasContextMenu= true;

			defaultTitle= "Autosave Viewer";
			defaultIcon= EditorGUIUtility.IconContent("SavePassive").image;

			Autosave.onSaved += onSaved;
			EditorManager.onEditorFocus += onEditorFocus;
		}

		protected override void OnDestroy() {
			Autosave.onSaved -= onSaved;
			EditorManager.onEditorFocus -= onEditorFocus;
			base.OnDestroy();
		}

		void onEditorFocus(bool state) {
			if(state) RefreshAssets();
		}

		protected override void WindowUpdate() {
			
		}

		//--------------------------------
		void onSaved(string path) {
			RefreshAssets();
		}

		protected override void RefreshAssets() {
			scenes.Clear();

			if(Directory.Exists(AutosaveSettings.instance.AutosavePath)) {
				foreach(var dir in Directory.GetDirectories(AutosaveSettings.instance.AutosavePath)) {
					string GUID= Path.GetFileName(dir.TrimEnd(Path.DirectorySeparatorChar));
					var scene= new SceneInfo();

					foreach(var id in Autosave.GetExistingIDs(GUID)) {
						var v= new SceneInfo.VersionInfo();
						v.id= id;
						v.directory= Autosave.GetDirectoryPathFromIndex(GUID, id);
						string info= Autosave.GetSceneBackupInfo(GUID, id);

						if(string.IsNullOrEmpty(info)) continue;

						int counter= 0;
						foreach(var line in info.Split('\n')) {
							if(string.IsNullOrEmpty(line) || (line.Trim() == "")) continue;

							//string name= line.Substring(0, line.IndexOf('=')).Trim();
							string value= line.Substring(line.IndexOf('=')+1).Trim();

							switch(counter++) {
							case 0: v.assetPath= value; v.filename= Path.GetFileName(value); break;
							case 1: v.dateTime= DateTime.Parse(value); break;
							case 5: v.reason= value; break;
							}
						}

						scene.versions.Add(v);
					}

					if(scene.versions.Count > 0) {
						scenes.Add(scene);
					}
				}

			}

			scenes.Sort((a,b)=> DateTime.Compare(b.versions[0].dateTime, a.versions[0].dateTime));

			Refresh();
		}

		protected override void onDoubleClicked() {
			var scene= overScene;
			if(scene == null) return;
			var info= scene.versions[0];

			if(overThumbnail && File.Exists(info.screenshot_path)) {
				EditorUtility.OpenWithDefaultApp(info.screenshot_path);
			}else{
				ShowSceneFile(info);
			}
		}

		protected override GenericMenu CreateContextMenu() {
			var scene= overScene;
			if(scene == null) return null;

			var menu= new GenericMenu();
			foreach(var info in scene.versions) {
				menu.AddItem(new GUIContent(info.dateTime.ToLocalTime().ToString()), false, ()=> {
					ShowSceneFile(info);
				});
			}

			return menu;
		}

		static void ShowSceneFile(SceneInfo.VersionInfo info) {
			var path= info.filePath;

			if(File.Exists(path)) {
				EditorUtility.RevealInFinder(info.filePath);
			}else{
				EditorUtility.OpenWithDefaultApp(Path.GetDirectoryName(info.filePath) + Path.DirectorySeparatorChar);
			}
		}

		protected override void DrawToolbar(Rect rect) {
			if(GUILayout.Button("Backup Now", EditorStyles.toolbarButton)) {
				Autosave.SaveNow("User choice", false);
			}

			if(GUILayout.Button("Open Folder", EditorStyles.toolbarButton)) {
				Autosave.OpenAutosaveFolder();
			}

			GUILayout.FlexibleSpace();
		}

		protected override Rect DrawContents(Rect rect) {
			EditorGUILayout.HelpBox(settingsModule.ConfigHeaderInfo(), MessageType.Info, true);

			SceneInfo overScene= null;
			bool overThumbnail= false;

			GUILayout.BeginVertical();

			foreach(var scene in scenes) {
				GUILayout.Space(10);
				GUILayout.BeginHorizontal();
				GUILayout.Space(10);

				var version= scene.versions[0];
				Rect thumbnailRect;
				if(version.thumbnail != null) {
					thumbnailRect= ThumbnailManager.GUI_DrawThumbnail(version.thumbnail, 64, 64, 2);
					//let onDoubleClicked() handle it
				}else{
					GUILayout.Label(EditorGUIUtility.IconContent("SceneAsset Icon").image, GUILayout.Width(64), GUILayout.Height(64));
					thumbnailRect= GUILayoutUtility.GetLastRect();
				}

				if(thumbnailRect.Contains(Event.current.mousePosition)) {
					overScene= scene;
					overThumbnail= true;
				}

				//let onDoubleClicked() handle it

				string path= Path.GetDirectoryName(version.assetPath).Replace('\\','/');
				if(path != "") path += '/';
				path += Path.GetFileNameWithoutExtension(version.assetPath);

				GUILayout.Label(
					new GUIContent(
						path +
						"\nMost recent version: " + version.dateTime.ToLocalTime().ToString() +
						"\nNumber of versions: " + scene.versions.Count + 
						"\nNote: " + version.reason,

						"Double click to get the file\nRight click to see more versions"
					),
					Styles.text
				);

				if(GUILayoutUtility.GetLastRect().Contains(Event.current.mousePosition)) {
					overScene= scene;
				}
				
				GUILayout.EndHorizontal();
			}

			if(Event.current.type == EventType.Repaint) {
				this.overScene= overScene;
				this.overThumbnail= overThumbnail;
			}

			GUILayout.EndVertical();

			return rect;
		}
	}
}