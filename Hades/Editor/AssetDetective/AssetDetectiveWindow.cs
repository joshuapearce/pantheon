﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

#pragma warning disable 0219

namespace Pantheon {
	[InitializeOnLoad]
	public class AssetDetectiveWindow:
		VulcanWindow
	{
		static AssetDetectiveWindow() {
			EditorManager.RegisterModule("Asset Detective");
		}

		protected override ModuleSettings settingsModule { get { return AssetTrackerSettings.instance; } }
		AssetTrackerSettings settings { get { return AssetTrackerSettings.instance; } }

		[ConditionalMenuItem("Tools/Pantheon/Asset Detective", ToolsMenu.MenuPriority)]
		[ConditionalMenuItem("Window/General/Asset Detective", ToolsMenu.GeneralMenuPriority)]
		public static void ShowWindow() {
			var instance= VulcanWindowManager.OpenWindow<AssetDetectiveWindow>();
		}

		static bool ValidateSerializationSettings(bool SetValues=false) {
		  if(EditorSettings.serializationMode != SerializationMode.ForceText) {
				if(SetValues) {
					Debug.Log("Changing EditorSettings.serializationMode from '" + EditorSettings.serializationMode + "' to 'ForceText'");
					EditorSettings.serializationMode= SerializationMode.ForceText;
				}else{
					return false;
				}
			}

			/*
			if(EditorSettings.externalVersionControl != "Visible Meta Files") {
				if(SetValues) {
					EditorSettings.externalVersionControl= "Visible Meta Files";
				}else{
					return false;
				}
			}
			*/
			return true;
		}


		class LocationInfo {
			public string GUID;
			public AssetTrackerData.Entry localLocation;
			public List<AssetTrackerData.Entry> locations= new List<AssetTrackerData.Entry>();

			public void Refresh() {
				localLocation= null;
				locations.Clear();

				if(!string.IsNullOrEmpty(GUID)) {
					foreach(var data in AssetTracker.allData.Values) {
						var entry= data.GetEntry(GUID);
						if(entry != null) {
							locations.Add(entry);
							if(data == AssetTracker.data) localLocation= entry;
						}
					}

					locations.Sort((a, b)=> {
						return a.filetime_UTC.CompareTo(b.filetime_UTC);
					});

					if(localLocation != null) {
						locations.Remove(localLocation);
						locations.Insert(0, localLocation);
					}
				}
			}
		} 

		class MissingReferenceInfo {
			public string GUID;
			public Component component;
			public Type fieldType;
			public string fieldName; //will include parents
			public GameObject gameObject { get { if(component == null) return null; return component.gameObject; } }
			public bool isAsset;
		}

		class MissingPrefabInfo {
			public string GUID;
			public GameObject gameObject;
		}

		class MissingComponentInfo {
			public string GUID;
			public GameObject gameObject;
			public int index;
			public string componentData;
		}

		[NonSerialized] Dictionary<string, LocationInfo> lookup= new Dictionary<string, LocationInfo>();
		[NonSerialized] List<MissingComponentInfo> missingComponents= new List<MissingComponentInfo>();
		[NonSerialized] List<MissingReferenceInfo> missingReferences= new List<MissingReferenceInfo>();
		[NonSerialized] List<MissingPrefabInfo> missingPrefabObjects= new List<MissingPrefabInfo>();

		public enum ModeEnum {
			References,
			Components,
			Prefabs
		}

		[SerializeField] ModeEnum mode;
		[NonSerialized] double timeOfLastRefresh;
		[NonSerialized] bool requestRefresh;
		[NonSerialized] bool contentsDirty= true;

		string expandedInfo_hash= null;

		//--------------------------------
		protected override void InitializePanel() {
			hasScrollingH= false;
			hasScrollingV= true;
			hasContextMenu= false;

			contentsDirty= true;
			repaintConstantlyInEdit= false;
			repaintConstantlyInPlay= false;
			requestRefresh |= (settings.refreshOption == AssetTrackerSettings.RefreshOptions.OnAssetChange);

			//EditorManager.onEditorFocus += (state) => {
			//	if(state && (refreshOption == RefreshOptions.OnModeChange)) requestRefresh= true;
			//};

			//EditorApplication.playModeStateChanged += (state) => {
			//	if((state == PlayModeStateChange.EnteredEditMode) && (refreshOption == RefreshOptions.OnAssetChange)) requestRefresh= true;
			//};

			SceneManager.sceneLoaded += (scene, mode) => {
				if(settings.refreshOption == AssetTrackerSettings.RefreshOptions.OnAssetChange) requestRefresh= true;
			};

			AssetTracker.onAssetsCreatedOrRemoved += onAssetsCreatedOrRemoved;

			#if UNITY_2018_1_OR_NEWER
			EditorApplication.hierarchyChanged += ()=> { contentsDirty= true; };
			#else
			EditorApplication.hierarchyWindowChanged += ()=> { contentsDirty= true; };
			#endif

			defaultTitle= "Asset Detective";
			defaultIcon= EditorGUIUtility.IconContent("d_Profiler.GPU").image;
		}

		protected override void OnDestroy() {
			AssetTracker.onAssetsCreatedOrRemoved -= onAssetsCreatedOrRemoved;
			base.OnDestroy();
		}

		protected override void WindowUpdate() {
			switch(settings.refreshOption) {
				case AssetTrackerSettings.RefreshOptions.EverySecond:
					requestRefresh |= ((EditorApplication.timeSinceStartup - timeOfLastRefresh) >= 1.0);
					break;

				case AssetTrackerSettings.RefreshOptions.EveryThirtySeconds:
					requestRefresh |= ((EditorApplication.timeSinceStartup - timeOfLastRefresh) >= 30.0);
					break;
			}

			if(contentsDirty || requestRefresh) {
				RefreshMissingAssets();
				contentsDirty= false;
				requestRefresh= false;
			}
		}

		void onAssetsCreatedOrRemoved() {
			contentsDirty= true;
			if(settings.refreshOption == AssetTrackerSettings.RefreshOptions.OnAssetChange) requestRefresh= true;
		}

		protected override void RefreshAssets() {
			contentsDirty= true;
			requestRefresh= true;
		}

		protected override void RefreshContents() {

		}

		void RefreshMissingAssets() {
			requestRefresh= false;
			contentsDirty= false;
			//Repaint();

			timeOfLastRefresh= EditorApplication.timeSinceStartup;
			missingComponents.Clear();
			missingPrefabObjects.Clear();
			missingReferences.Clear();

			var GUIDs= new HashSet<string>();

			var scene= SceneManager.GetActiveScene();
			foreach(var obj in scene.GetRootGameObjects()) {
				IterateRefreshSceneObject(obj);
			}

			if((missingReferences.Count > 0) || (missingPrefabObjects.Count > 0) || (missingComponents.Count > 0)) {
				//AssetTracker.ImportExternalData();
				var sceneData= AssetDetective.GetSceneContents();

				foreach(var value in missingReferences) {
					var info= AssetDetective.GetComponentInfo(sceneData, value.component);
					if(info != null) {
						var componentData= info.data;

						var match= Regex.Match(componentData, @"[\n\r]+ *" + value.fieldName + "[: ]+.*[{, ]+guid[: ]+([A-Fa-f0-9]+)");
						if(match.Success) value.GUID= match.Groups[1].Value;

						if(value.GUID != null) GUIDs.Add(value.GUID);
					}
				}

				foreach(var value in missingPrefabObjects) {
					var info= AssetDetective.GetObjectInfo(sceneData, value.gameObject);
					if(info != null) {
						var data= info.data;

						var match= Regex.Match(data, @"[\n\r]+ *m_CorrespondingSourceObject[: ]+.*[{, ]+guid[: ]+([A-Fa-f0-9]+)");
						if(match.Success) value.GUID= match.Groups[1].Value;

						if(value.GUID != null) GUIDs.Add(value.GUID);
					}
				}

				foreach(var value in missingComponents) {
					var info= AssetDetective.GetObjectInfo(sceneData, value.gameObject);
					if(info != null) {
						var data= info.data;

						var match= Regex.Matches(data, @"component[: ]+[^A-Za-z]+fileID[: ]+(\d+)");
						if(match.Count > value.index) {
							var fileID= long.Parse(match[value.index].Groups[1].Value);
							string componentData;
							if(sceneData.items.TryGetValue(fileID, out componentData)) {
								value.componentData= componentData.Substring(componentData.IndexOf('\n', componentData.IndexOf('\n') + 1) + 1);

								var match2= Regex.Match(componentData, @"[\n\r]+ *m_Script[: ]+.*[{, ]+guid[: ]+([A-Fa-f0-9]+)");
								if(match2.Success) {
									value.GUID= match2.Groups[1].Value;
								}
							}
						}

						if(value.GUID != null) GUIDs.Add(value.GUID);
					}
				}
			}

			lookup.Clear();
			foreach(var GUID in GUIDs) {
				if((GUID != null) && (GUID != "")) {
					var value= new LocationInfo();
					value.GUID= GUID;
					value.Refresh();
					lookup.Add(GUID, value);
				}
			}
		}

		void IterateRefreshSceneObject(GameObject obj) {
			if(obj == null) return;

			if(PrefabHelper.IsPrefabAssetMissing(obj)) {
				var item= new MissingPrefabInfo();
				item.gameObject= obj;
				missingPrefabObjects.Add(item);
			}

			int index= 0;
			foreach(var component in obj.GetComponents(typeof(Component))) {
				if(component == null) {
					var item= new MissingComponentInfo();
					item.gameObject= obj;
					item.index= index;
					missingComponents.Add(item);
				}else{
					ParseObjectFields(new HashSet<object>(), component, component);
				}
				++index;
			}

			foreach(var child in obj.GetChildren()) {
				IterateRefreshSceneObject(child);
			}
		}

		void ParseObjectFields(HashSet<object> set, Component component, object obj, string location="", Type type=null) {
			if(set.Contains(obj)) {
				return;
			}
			set.Add(obj);

			if(type != null) {
				if(!IsInterestingType(type)) return;

				if(IsMissingReference(obj)) {
					//Debug.Log("Missing " + location + "; " + obj);

					var info= new MissingReferenceInfo();
					info.component= component;
					info.fieldName= location.TrimEnd('.');
					info.fieldType= type;
					info.isAsset= !type.IsAssignableFrom(typeof(Component)) && !type.IsAssignableFrom(typeof(GameObject));

					missingReferences.Add(info);
					return;
				}else if(obj == null) {
					//Just a regular null value
					return;
				}
			}else{
				type= obj.GetType();
			}

			foreach(var field in type.GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance)) {
				if(!IsInterestingType(field.FieldType)) continue;

				object value= null;

				try {
					value= field.GetValue(obj);
				}catch(Exception e) {
					//nothing to do, just don't crash
					//Better to parse incompletely than to have the whole thing crash with some weird object

					if(EditorManager.debugPantheon) {
						throw e;
					}else{
						continue;
					}
				}

				if(value == null) {
					//nothing to do

				}else if(typeof(UnityEngine.Object).IsAssignableFrom(field.FieldType)) {
					ParseObjectFields(set, component, value, location + field.Name + ".", field.FieldType);

				}else if(field.FieldType.IsArray) {
					var array= (Array)field.GetValue(obj);
					var subtype= field.FieldType.GetElementType();
					if(!IsInterestingType(subtype)) continue;

					int counter= 0;
					foreach(var subobj in array) {
						ParseObjectFields(set, component, subobj, location + field.Name + "[" + counter + "].", subtype);
						++counter;
					}

				}else if(typeof(System.Collections.IList).IsAssignableFrom(field.FieldType)) {
					var list= field.GetValue(obj) as System.Collections.IList;
					int counter= 0;
					foreach(var subobj in list) {
						//ParseObjectFields(component, subobj, location + field.Name + "[" + counter + "].", field.FieldType.GenericTypeArguments[0]);
						++counter;
					}

				}else if(field.FieldType.IsSerializable) {
					//Debug.Log(field.Name + "= " + value);
					ParseObjectFields(set, component, value, location + field.Name + ".", field.FieldType);
				}
			}
		}

		static bool IsInterestingType(Type type) {
			if(type == null) return false;
			if(type.IsPrimitive) return false;
			if(type == typeof(string)) return false;
			if(type.IsEnum) return false;

			if(type.IsArray) {
				return IsInterestingType(type.GetElementType());
			}

			if(typeof(System.Collections.IList).IsAssignableFrom(type)) {
				var subtypes= type.GetGenericArguments();
				if((subtypes == null) || (subtypes.Length != 1)) return false;
				return IsInterestingType(subtypes[0]);
			}

			if(type.IsSerializable) return true;
			if(typeof(UnityEngine.Object).IsAssignableFrom(type)) return true;

			return false;
		}

		static bool IsMissingReference(object value) {
			if(value == null) return false; //it's just null

			if(!typeof(UnityEngine.Object).IsAssignableFrom(value.GetType())) return false; //not a Unity reference
			if((UnityEngine.Object)value != null) return false; //Unity doesn't consider the value null, so it's not missing

			//The above test isn't enough to differentiate between actually-null and null-because-invalid
			if((value as UnityEngine.Object).GetInstanceID() == 0) return false; //works in Unity 2019.3

			return true;
		}

		//--------------------------------
		protected override void AddItemsToMenu_(GenericMenu menu) {
			menu.AddItem(
				new GUIContent("Refresh now\tF5"),
				false, 
				()=> { contentsDirty= true; requestRefresh= true; }
			);

			menu.AddItem(
				new GUIContent("Refresh mode/Every second"),
				settings.refreshOption == AssetTrackerSettings.RefreshOptions.EverySecond, 
				()=> { settings.refreshOption= AssetTrackerSettings.RefreshOptions.EverySecond; }
			);

			menu.AddItem(
				new GUIContent("Refresh mode/Thirty seconds"),
				settings.refreshOption == AssetTrackerSettings.RefreshOptions.EveryThirtySeconds, 
				()=> { settings.refreshOption= AssetTrackerSettings.RefreshOptions.EveryThirtySeconds; }
			);

			menu.AddItem(
				new GUIContent("Refresh mode/When assets change"),
				settings.refreshOption == AssetTrackerSettings.RefreshOptions.OnAssetChange, 
				()=> { settings.refreshOption= AssetTrackerSettings.RefreshOptions.OnAssetChange; }
			);

			menu.AddItem(
				new GUIContent("Refresh mode/Manual only"),
				settings.refreshOption == AssetTrackerSettings.RefreshOptions.ManualOnly, 
				()=> { settings.refreshOption= AssetTrackerSettings.RefreshOptions.ManualOnly; }
			);

			menu.AddItem(
				new GUIContent("Raw field names"),
				settings.RawFieldNames,
				()=> { settings.RawFieldNames= !settings.RawFieldNames; }
			);

			base.AddItemsToMenu_(menu);
		}

		//--------------------------------
		private void DrawToolbarTab(string name, ModeEnum mode, int count) {
			var content= new GUIContent(" " + name);
			if(count == 0) {
				#if UNITY_2018_1_OR_NEWER
				content.image= EditorGUIUtility.IconContent("d_FilterSelectedOnly").image;
				#else
				content.image= EditorGUIUtility.IconContent("Collab").image;
				#endif
			}else{
				content.image= EditorGUIUtility.IconContent("console.warnicon.sml").image;
				content.text += " - " + count;
			}

			if(base.DrawToolbarTab(content, this.mode == mode)) {
				this.mode= mode;
			}
		}

		protected override void DrawToolbar(Rect rect) {
			bool change= false;
			var prevMode= mode;

			GUILayout.Space(6);
			DrawToolbarTab("References", ModeEnum.References, missingReferences.Count);
			DrawToolbarTab("Components", ModeEnum.Components, missingComponents.Count);
			DrawToolbarTab("Prefabs", ModeEnum.Prefabs, missingPrefabObjects.Count);

			GUILayout.FlexibleSpace();

			if(GUILayout.Button("Refresh", Styles.toolbarButton)) {
				contentsDirty= true;
				requestRefresh= true;
				change= true;
			}

			base.DrawToolbar(rect);

			if(change) {
				GUIUtility.hotControl= 0;
				GUIUtility.keyboardControl= 0;
			}
		}

		void DrawItem(UnityEngine.Object obj, Type valueType, string GUID, string title, Texture icon, string moreInfo= null) {
			if(GUID == null) GUID= "";

			GameObject gameObj= null;

			string text= "";
			if(obj == null) {
				text += "\nThis object was deleted from the scene. Refresh to clear it.";
				title= "Deleted object." + title;
			}else{
				if(obj is GameObject) gameObj= obj as GameObject;
				else if(obj is Component) gameObj= (obj as Component).gameObject;
			}

			if(gameObj != null) {
				title= gameObj.FullName() + "." + title;
				title= title.TrimEnd('.',' ');
			}

			if(GUILayout.Button(new GUIContent(title, icon), EditorStyles.boldLabel, GUILayout.Height(32))) {
				if(gameObj != null) {
					Selection.activeGameObject= gameObj;
					EditorGUIUtility.PingObject(obj);
					EditorManager.OpenInspector();
				}
			}

			bool isGameEntity= (typeof(UnityEngine.Component).IsAssignableFrom(valueType) || typeof(UnityEngine.GameObject).IsAssignableFrom(valueType));
			bool hasGUID= string.IsNullOrEmpty(GUID);

			if((obj != null) && (typeof(Component).IsAssignableFrom(obj.GetType()))) {
				text += "\nComponent type: " + obj.GetType().Name;
			}

			text += "\nMissing type: " + valueType.ToString();
			if(!isGameEntity) text += "\nGUID: " + (hasGUID ? "Unknown" : GUID);
			if((obj != null) && string.IsNullOrEmpty(GUID) && (AssetDetective.GetFileIdentifier(obj) == 0)) {
				text += "\nThe file identifier for this object was null. Try saving the scene.";
			}
			text= text.Trim();

			{
				var style= EditorStyles.inspectorDefaultMargins;
				var content= new GUIContent(text);
				var height= style.CalcHeight(content, position.width - 20);
				EditorGUILayout.SelectableLabel(content.text, style, GUILayout.Height(height));
			}

			LocationInfo info= null;
			if(GUID != null) lookup.TryGetValue(GUID, out info);
			List<AssetTrackerData.Entry> locations= (info ==  null) ? null :  info.locations;

			if(isGameEntity) {
				//nothing
			}else	if((info == null) || (locations.Count == 0)) {
				GUILayout.Label("No file locations found by Pantheon's asset tracker", EditorStyles.inspectorDefaultMargins);
			}else{
				if(info.localLocation == null) {
					GUILayout.Button(
						new GUIContent("Original location unknown", EditorGUIUtility.IconContent("console.warnicon.sml").image),
						EditorStyles.inspectorDefaultMargins,
						GUILayout.Height(16)
					);
				}

				foreach(var entry in locations) {
					string state= null;
					bool fileExists= File.Exists(entry.filePath);
					bool dirExists= fileExists || Directory.Exists(Path.GetDirectoryName(entry.filePath));
					var line= new GUIContent();

					if(fileExists) {
						line.image= EditorGUIUtility.IconContent("greenLight").image;
						line.tooltip= "File available";

						bool mismatch= false;
						string fileGUID= null;
						if(entry.isLocal) fileGUID= AssetDatabase.AssetPathToGUID(entry.assetPath);
						else fileGUID= AssetTracker.FilePathToGUID(entry.filePath);
						if((fileGUID != null) && (fileGUID != entry.GUID)) line.tooltip += " but the GUID does not match";

					}else if(!dirExists) {
						line.image= EditorGUIUtility.IconContent("console.warnicon.sml").image;
						line.tooltip= "Directory missing";
						line.tooltip += "\nLast seen " + entry.dateTime.ToLocalTime().ToString();
					}else{
						line.image= EditorGUIUtility.IconContent("console.warnicon.sml").image;
						line.tooltip= "File missing";
						line.tooltip += "\nLast seen " + entry.dateTime.ToLocalTime().ToString();
					}

					line.text= (entry.owner == AssetTracker.data) ? entry.assetPath : entry.filePath;

					if(GUILayout.Button(line, EditorStyles.inspectorDefaultMargins, GUILayout.Height(16))) {
						Debug.Log(entry.filePath);
						if(fileExists) EditorUtility.RevealInFinder(entry.filePath);
						else if(dirExists) System.Diagnostics.Process.Start(Path.GetDirectoryName(entry.filePath));
					}
				}
			}

			if(moreInfo != null) {
				GUILayout.Space(10);
				string hash= GUID + title;
				bool expanded= (expandedInfo_hash == hash);
				if(EditorGUILayout.Foldout(expanded, "More info")) {
					expandedInfo_hash= hash;
					GUILayout.Label(moreInfo, EditorStyles.inspectorDefaultMargins);
				}else{
					if(expandedInfo_hash == hash) expandedInfo_hash= null;
				}
			}

			GUILayout.Space(10);
			EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);
		}

		protected override Rect DrawContents(Rect rect) {
			GUILayout.BeginVertical(EditorStyles.inspectorFullWidthMargins);
			if(!ValidateSerializationSettings()) {
				EditorGUILayout.HelpBox(
					"For this tool to work, Unity must be configured to serialize using text, instead of binary" +
					"\n\nClick here to change this setting automatically, or change it manually in \"ProjectSettings/Editor/Asset Serialization\""
					,
					MessageType.Error,
					true
				);
				if(GUI.Button(GUILayoutUtility.GetLastRect(), "", GUIStyle.none)) {
					ValidateSerializationSettings(true);
				}
			}

			Action<string> DrawHeader= (str)=> {
				EditorGUILayout.HelpBox(str, MessageType.Info, true);
				EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);
			};
			GUILayout.Space(10);
			if(mode == ModeEnum.References) {
				var count= missingReferences.Count;
				if(count == 0) DrawHeader("No missing references detected in this scene.\nNote that this detection is not perfect");
				else if(count == 1) DrawHeader("There is one invalid reference in the scene");
				else DrawHeader("There are " + count + " invalid references in the scene");

				foreach(var item in missingReferences) {
					DrawItem(
						item.component,
						item.fieldType,
						item.GUID,
						ObjectNames.NicifyVariableName(item.fieldName),
						AssetPreview.GetMiniTypeThumbnail(item.fieldType),
						null
					);
				}

			}else if(mode == ModeEnum.Components) {
				var set= new HashSet<GameObject>();
				foreach(var item in missingComponents) set.Add(item.gameObject);
				int count= set.Count;

				if(count == 0) DrawHeader("No missing scripts in this scene.");
				else if(count == 1) DrawHeader("There is one GameObject with missing script(s)");
				else DrawHeader("There are " + count + " GameObjects with missing scripts");

					//selected.Add(missingComponents.IndexOf(Selection.activeGameObject));

				foreach(var item in missingComponents) {
					DrawItem(
						item.gameObject,
						typeof(UnityEditor.MonoScript),
						item.GUID,
						"Component #" + item.index,
						EditorGUIUtility.IconContent("cs Script Icon").image,
						item.componentData
					);
				}

			}else if(mode == ModeEnum.Prefabs) {
				int count= missingPrefabObjects.Count;
				if(count == 0) DrawHeader("No missing prefabs in this scene");
				else if(count == 1) DrawHeader("There is one instance of a missing prefab");
				else DrawHeader("There are " + count + " instances of missing prefabs");

				foreach(var item in missingPrefabObjects) {
					DrawItem(
						item.gameObject,
						typeof(UnityEngine.Object),
						item.GUID,
						null,
						EditorGUIUtility.IconContent("Prefab Icon").image,
						null
					);
				}
			}
			GUILayout.Space(50);
			GUILayout.EndVertical();
			rect.yMax= GUILayoutUtility.GetLastRect().yMax;

			return rect;
		}
	}
}