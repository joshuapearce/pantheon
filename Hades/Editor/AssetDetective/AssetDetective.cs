﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;

using UnityEditor;
using UnityEditor.SceneManagement;
//using UnityEditor.Build.Content;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Pantheon {
	[InitializeOnLoad]
	public static class AssetDetective {
		static AssetDetective() {
			//Manager.RegisterClass<AssetDetective>();
		}

		public class SceneData {
			public SceneData(string fileContents, string path= null) {
				this.fileContents= fileContents;
				this.path= path;

				bool eof= false;
				int index= 0;
				while(!eof) {
					int nextResult= fileContents.IndexOf("\n--- !u!", index);
					if(nextResult < 0) {
						nextResult= fileContents.Length;
						eof= true;
					}

					if(index > 0) {
						string item= fileContents.Substring(index, nextResult - index);
						var match= Regex.Match(item, "^---[^&]*&(\\d+)\n");
						if(match.Success) {
							string ID= match.Groups[1].Value;
							if(!string.IsNullOrEmpty(ID) && (ID.Trim() != "")) {
								items[long.Parse(ID)]= item;
							}
						}
					}
					index= nextResult + 1;
				}
			}

			public string path;
			public string fileContents;
			public Dictionary<long, string> items= new Dictionary<long, string>();
		}

		public static long GetFileIdentifier(UnityEngine.Object obj) {
			if(obj == null) return 0;

			var serializedObject= new SerializedObject(obj);
			PropertyInfo inspectorModeInfo= typeof(SerializedObject).GetProperty("inspectorMode", BindingFlags.NonPublic | BindingFlags.Instance);
			inspectorModeInfo.SetValue(serializedObject, InspectorMode.Debug, null);

			SerializedProperty localIdProp= serializedObject.FindProperty("m_LocalIdentfierInFile"); //"Identfier" is correctly spelled wrong
			return localIdProp.longValue;
		}

		public static SceneData GetSceneContents() {
			return GetSceneContents(SceneManager.GetActiveScene()); 
		}

		public static SceneData GetSceneContents(Scene scene) {
			string path;
			bool deleteFile;

			if(scene.isDirty && !Application.isPlaying) {
				path= FileUtil.GetUniqueTempPathInProject() + ".unity";
				EditorSceneManager.SaveScene(scene, path, true);
				deleteFile= true;
			}else{
				path= scene.path;
				deleteFile= false;
			}

			var data= File.ReadAllText(path);

			if(deleteFile) {
				File.Delete(path);
			}

			return new SceneData(data, path);
		}

		public static GameObjectInfo GetObjectInfo(SceneData sceneData, GameObject obj) {
			if(obj == null) return null;

			long fileID= GetFileIdentifier(obj);

			if(fileID == 0) {
				if(EditorManager.debugPantheon) {
					Debug.LogWarning("Could not get file ID for object named '" + obj.name + "'; Some Pantheon features won't work.");
				}
				return null;
			}

			GameObjectInfo v= new GameObjectInfo();
			v.obj= obj;
			v.fileID= fileID;

			if(!sceneData.items.TryGetValue(fileID, out v.data)) {
				if(EditorManager.debugPantheon) {
					Debug.LogError("Could not find object named '" + obj.name + "' with fileID " + fileID + " in the scene asset " + sceneData.path);
				}
				return null;
			}
			v.typeName= Regex.Match(v.data, "^.*\\n([^:\\n]*)").Groups[1].Value;

			var components= obj.GetComponents<Component>();
			v.components= new ObjectInfo[components.Length];
			for(int i= 0; i<v.components.Length; i++) v.components[i]= new ObjectInfo();

			{
				var lines= v.data.Split('\n');

				for(int i= 0; i<lines.Length; i++) {
					if(lines[i] == "  m_Component:") {
						++i;
						int c= 0;
						while(lines[i+c].StartsWith("  - component:")) {
							var match= Regex.Match(lines[i+c], "[^A-Za-z]fileID: (\\d+)");
							v.components[c].fileID= long.Parse(match.Groups[1].Value);
							++c;
						}
						break;
					}
				}
			}
			
			for(int i= 0; i<components.Length; i++) {
				var c= v.components[i];
				c.obj= components[i];

				if(c.fileID != 0) {
					c.data= sceneData.items[c.fileID];
					c.assetGUID= Regex.Match(c.data, "m_Script: {.*guid:\\s([^, }]+).*}").Groups[1].Value;
					c.typeName= Regex.Match(c.data, "^.*\\n([^:\\n]*)").Groups[1].Value;

					//Debug.Log(c.fileID + " : " + c.assetGUID + "; " + c.tryGetAssetPath());
				}
			}

			return v;
		}

		public static ObjectInfo GetComponentInfo(SceneData sceneData, Component v) {
			if(v == null) return null;
			var objInfo= GetObjectInfo(sceneData, v.gameObject);
			if(objInfo == null) return null;

			foreach(var c in objInfo.components) {
				if(c.obj == v) return c;
			}

			return null;
		}


		//--------------------------------
		public class GameObjectInfo:
			ObjectInfo
		{
			public ObjectInfo[] components;
		}

		public class ObjectInfo {
			public UnityEngine.Object obj;
			public long fileID;
			public string assetGUID;
			public string typeName;

			public string data;

			public string tryGetAssetPath() {
				if(string.IsNullOrEmpty(assetGUID)) return null;

				var result= AssetDatabase.GUIDToAssetPath(assetGUID);
				//if(string.IsNullOrEmpty(result)) result= AssetTracker.GetRelativePath(assetGUID);
				return result;
			}
		}

	}
}