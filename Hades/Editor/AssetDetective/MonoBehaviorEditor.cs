﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

using UnityEditor;
using UnityEngine;

namespace Pantheon {
	[CustomEditor(typeof(MonoBehaviour))]
	[CanEditMultipleObjects]
	public class MonoBehaviorEditor:
		Editor
	{
		static GameObject gameObject;
		static string[] componentGUIDs;
		static AssetDetective.SceneData sceneData;
		static AssetDetective.GameObjectInfo objectInfo;
		AssetDetective.ObjectInfo componentInfo {
			get {
				if(componentIndex < 0) return null;
				if(objectInfo == null) return null;
				if(objectInfo.components == null) return null;
				if(objectInfo.components.Length <= componentIndex) return null;
				return objectInfo.components[componentIndex];
			}
		}

		int indexOfEditorInstance; //relative to other missing scripts in this object
		int componentIndex= -1; //relative to all components (valid or invalid) in this object
		bool isPrefab;

		bool foldoutProperties;
		bool foldoutData;
		float width;
		
		static long refresh_Tick;
		static int refresh_Counter= 0;

		public void RefreshObject() {
			if((object)target == null) {
				Debug.Log("Null target, wtf");
				return;
			}
			gameObject= (target as MonoBehaviour).gameObject;

			isPrefab= PrefabHelper.IsPartOfAnyPrefab(gameObject);
			if(!isPrefab) {
				sceneData= AssetDetective.GetSceneContents();
				objectInfo= AssetDetective.GetObjectInfo(sceneData, gameObject);
			}
		}
		 
		public void RefreshComponent() {
			if(refresh_Tick != EditorManager.tick) {
				refresh_Tick= EditorManager.tick;
				refresh_Counter= 0;
			}
			indexOfEditorInstance= refresh_Counter++;

			int nullCounter= 0;

			var components= gameObject.GetComponents<Component>();
			for(int i= 0; i<objectInfo.components.Length; i++) {
				if(components[i] != null) continue;

				if(nullCounter++ == indexOfEditorInstance) {
					componentIndex= i;
					break;
				}
			}
		}

		public override void OnInspectorGUI() {
			if(!EditorManager.Initialized) return;

			if(targets.Length > 1) OnInspectorGUI_Multi();
			else if(isPrefab) OnInspectorGUI_Prefab();
			else OnInspectorGUI_Single();
		}

		public void OnInspectorGUI_Single() {
			serializedObject.Update();
			var style= new GUIStyle(GUIStyle.none);
			style.wordWrap= true;

			EditorGUI.EndDisabledGroup();
			EditorGUI.BeginDisabledGroup(false);
			EditorGUILayout.BeginVertical();
			SerializedProperty scriptProperty= serializedObject.FindProperty("m_Script");
			EditorGUILayout.PropertyField(scriptProperty, false);

			bool hasModifiedProperties;
			#if UNITY_2018_3_OR_NEWER
			hasModifiedProperties= serializedObject.hasModifiedProperties;
			#else
			{
				var property= serializedObject.GetType().GetProperty("hasModifiedProperties", BindingFlags.NonPublic | BindingFlags.Instance);
				hasModifiedProperties= (bool)property.GetValue(serializedObject, null);
			}
			#endif

			if(hasModifiedProperties) {
				serializedObject.ApplyModifiedProperties();
				typeof(EditorUtility).GetMethod("ForceRebuildInspectors", BindingFlags.Static | BindingFlags.NonPublic).Invoke(null, null);
			}

			{
				//This can't be the only way to get the inspector width, right?...
				EditorGUILayout.BeginHorizontal();
				GUILayout.FlexibleSpace();
				EditorGUILayout.EndHorizontal();
				if(Event.current.type == EventType.Repaint) {
					width= GUILayoutUtility.GetLastRect().width;
				}
			}

			if(objectInfo == null) {
				RefreshObject();
				if(objectInfo == null) {

					return;
				}
			}
			if(componentIndex < 0) RefreshComponent();

			//serializedObject.Update();

			Action<string, string> DrawField=(name, value)=> {
				if(value == null) {
					EditorGUILayout.LabelField(name);
				}else{
					var label= new GUIContent(" " + name);
					var labelWidth= EditorGUIUtility.labelWidth + 4; //style.CalcSize(label).x;
					var content= new GUIContent(value);
					var contentWidth= width - labelWidth;
					var contentHeight= style.CalcHeight(content, contentWidth);

					EditorGUILayout.BeginHorizontal();
					EditorGUILayout.LabelField(label, style, GUILayout.Width(labelWidth));
					EditorGUILayout.SelectableLabel(value, style, GUILayout.Height(contentHeight), GUILayout.Width(contentWidth));
					EditorGUILayout.EndHorizontal();
				}
			};

			if(EditorManager.debugPantheon) {
				GUILayout.Label("objectInfo= " + ((objectInfo == null) ? "null" : "not null"));
				GUILayout.Label("componentIndex= " + componentIndex);
				GUILayout.Label("index= " + indexOfEditorInstance);
			}

			if(componentInfo == null) {
				GUILayout.Label("Component info unavailable");

			}else{
				string assetPath= componentInfo.tryGetAssetPath();
				DrawField("Original script path", assetPath ?? "Unknown");

				DrawField("Script GUID", componentInfo.assetGUID);
				DrawField("FileID in scene", ""+componentInfo.fileID);

				//--------------------------------		
				ShowScriptNotLoadedWarning();
				ShowExistingProperties();

				//--------------------------------		
				var valueData= componentInfo.data;
				if(string.IsNullOrEmpty(valueData) || (valueData.Trim() == "")) {
					EditorGUI.BeginDisabledGroup(true);
					EditorGUILayout.Foldout(false, "Extracted scene data: <None>", true);
					EditorGUI.EndDisabledGroup();
				}else{
					foldoutData= EditorGUILayout.Foldout(foldoutData, "Extracted scene data", true);
					if(foldoutData) {
						EditorGUI.BeginDisabledGroup(true);
						var valuesContent= new GUIContent(valueData);
						var height= style.CalcHeight(valuesContent, width);
						EditorGUILayout.LabelField(valuesContent, style, GUILayout.Width(width), GUILayout.Height(height));
						EditorGUI.EndDisabledGroup();
					}
				}
			}			

			EditorGUILayout.EndVertical();
		}

		void ShowExistingProperties() {
			var property_i= serializedObject.GetIterator();
			var properties= new List<SerializedProperty>();
			if(property_i.NextVisible(true)) {
				do {
					if(property_i.name == "m_Script") continue;
					properties.Add(property_i.Copy());
				}while(property_i.NextVisible(false));
			}
			EditorGUI.BeginDisabledGroup(properties.Count == 0);

			foldoutProperties= EditorGUILayout.Foldout(foldoutProperties, "Serialized script values", true);
			if(foldoutProperties) {
				EditorGUI.BeginDisabledGroup(true);
				foreach(var p in properties) {
					EditorGUILayout.PropertyField(p, true);
				}
				EditorGUI.EndDisabledGroup();
			}
			EditorGUI.EndDisabledGroup();
		}

		public void OnInspectorGUI_Prefab() {
			base.OnInspectorGUI();

			EditorGUILayout.HelpBox(
				"The routines used to detect missing script names don't (currently) work on prefabs."+
				"If you unpack the object completely (by right clicking on it), more details of the missing script may be revealed."+
				"You can also duplicate the object before unpacking it, and the information will be preserved.",
				MessageType.Info
			);
			//--------------------------------		
			ShowScriptNotLoadedWarning();
			ShowExistingProperties();
		}

		public void OnInspectorGUI_Multi() {
			base.OnInspectorGUI();
			 
			EditorGUILayout.LabelField("To inspect details of missing scripts, select an individual object");
			//--------------------------------		
			ShowScriptNotLoadedWarning();
		}

		private static void ShowScriptNotLoadedWarning() {
			var t_GenericInspector= typeof(Editor).Assembly.GetType("GenericInspector");
			if(t_GenericInspector != null) {
				var m= t_GenericInspector.GetMethod("ShowScriptNotLoadedWarning", BindingFlags.Static | BindingFlags.NonPublic);
				if(m != null) {
					m.Invoke(null, null);
					return;
				}
			}

      var text= ("The associated script can not be loaded.\nPlease fix any compile errors\nand assign a valid script.");
      EditorGUILayout.HelpBox(text, MessageType.Warning, true);
    }
	}
}