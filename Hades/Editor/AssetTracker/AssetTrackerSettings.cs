﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

using UnityEditor;
using UnityEngine;

namespace Pantheon {
	[InitializeOnLoad]
	public class AssetTrackerSettings:
		ModuleSettings
	{
		static AssetTrackerSettings instance_;
		[SelfInitializing] public static AssetTrackerSettings instance {
			get {
				if(instance_ == null) instance_= ModuleSettings.LoadOrCreate<AssetTrackerSettings>("AssetTrackerSettings.settings");
				return instance_;
			}
		}

		#if UNITY_2018_3_OR_NEWER
		[SettingsProvider]
    private static SettingsProvider InitializeSettingsProvider_static() {
			return instance.InitializeSettingsProvider();
		}
		#endif
		
		new void OnEnable() {
			base.OnEnable();
			showInConfigPanel= true;
			displayName= "Asset Tracker / Detective";
			helpFile= "AssetTracker.txt";
		}

		//--------------------------------
		//Values relevant to this project
		[SettingsField(
			"Disable asset tracking entirely",
			"Some Pantheon features will not work without this option.\nNone of this data is uploaded or used outside of Unity",
			SettingsFieldAttribute.Flags.Default,
			300
		)]
		public bool DisableAllTracking;

		[SettingsField(
			"Allow asset data exporting",
			"Exporting creates a shared file in your system appdata which contains a GUID and file path for each asset. This data makes it easier to determine when an asset is mistakenly removed.",
			SettingsFieldAttribute.Flags.Default,
			100
		)]
		public bool AllowExporting= true;

		//[SettingsField(
		//	"Allow asset data importing",
		//	"This allows data about the assets of other projects to be imported to this project, for use when trying to find a missing asset",
		//	SettingsFieldAttribute.Flags.Default,
		//	200
		//)]
		public bool AllowImporting { get { return true; } }
		public bool AutoImporting { get { return false; } }

		//--------------------------------
		public override void DrawContents(float contentWidth) {
			if(SettingsWindow.showAdvanced) {
				GUILayout.BeginVertical(defaultMargins);
				Styles.DrawLink("Open exported data directory", EditorManager.appDataPath + "AssetTracking/");
				GUILayout.EndVertical();
			}
			base.DrawContents(contentWidth);
		}

		public enum RefreshOptions {
			EverySecond,
			EveryThirtySeconds,
			OnAssetChange,
			ManualOnly
		}

		[SettingsField(
			"Detective refresh mode",
			null,
			SettingsFieldAttribute.Flags.Default,
			320
		)]
		public RefreshOptions refreshOption= RefreshOptions.OnAssetChange;

		[SettingsField(
			"Display raw field names",
			null,
			SettingsFieldAttribute.Flags.Default,
			330
		)]
		public bool RawFieldNames;
	}
}