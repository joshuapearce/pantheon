﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;

using UnityEditor;
using UnityEngine;

namespace Pantheon {
	[InitializeOnLoad]
	public class AssetTracker:
		UnityEditor.AssetModificationProcessor
	{
		static AssetTrackerSettings settings { get { return AssetTrackerSettings.instance; } }
		public static AssetTrackerData data;
		public static Dictionary<string, AssetTrackerData> allData= new Dictionary<string, AssetTrackerData>();

		static string localDataPath { get { return EditorManager.projectPath + "Pantheon/AssetDatabase.txt"; } }
		static string exportDataPath { get { return exportDataDir + data.key + ".txt"; } }
		static string exportDataDir { get { return EditorManager.appDataPath + "AssetTracking/"; } }

		static bool flagAssetsCreatedOrRemoved;
		public static EditorManager.VoidFunction onAssetsCreatedOrRemoved;
		static Queue<string> updateQueue= new Queue<string>();
		static bool needsExporting;

		static AssetTracker() {
			EditorManager.RegisterModule("Asset Tracker");
			Manager.update += StaticUpdate;

			#if UNITY_2018_1_OR_NEWER
			EditorApplication.quitting += EditorQuit;
			#endif

			EditorManager.onEditorFocus += (state)=> { 
				if(!state) ExportData();
				else if(AssetTrackerSettings.instance.AutoImporting) ImportExternalData();
			};
		}

		class ImportMonitor: UnityEditor.AssetPostprocessor {
			void OnPreprocessAsset() {
				AssetTracker.OnPreprocessAsset(assetPath);
			}
		}

		private static void OnPreprocessAsset(string assetPath) {
			updateQueue.Enqueue(assetPath);
			flagAssetsCreatedOrRemoved= true;
		}

		private static AssetMoveResult OnWillMoveAsset(string sourcePath, string destinationPath) {
			if(sourcePath.StartsWith(EditorManager.tempAssetPath, true, CultureInfo.CurrentCulture)) return AssetMoveResult.DidNotMove;
			updateQueue.Enqueue(destinationPath);
			return AssetMoveResult.DidNotMove;
		}

		private static void OnWillCreateAsset(string assetName) {
			if(assetName.StartsWith(EditorManager.tempAssetPath, true, CultureInfo.CurrentCulture)) return;

			if(Path.GetExtension(assetName) == ".meta") {
				assetName= assetName.Substring(0, assetName.Length - ".meta".Length);
			}
			updateQueue.Enqueue(assetName);
			flagAssetsCreatedOrRemoved= true;
		}

		private static AssetDeleteResult OnWillDeleteAsset(string assetName, RemoveAssetOptions options) {
			if(assetName.StartsWith(EditorManager.tempAssetPath, true, CultureInfo.CurrentCulture)) return AssetDeleteResult.DidNotDelete;

			flagAssetsCreatedOrRemoved= true;
			return AssetDeleteResult.DidNotDelete;
		}

		static void EditorQuit() {
			ExportData();
		}
		 
		public static bool Initialized { get; private set; }
		static void Initialize() {
			if(Initialized) return;
			Initialized= true;

			data= new AssetTrackerData();
			data.GUID= settings.GUID;
			data.projectPath= EditorManager.projectPath;
			allData.Add(data.key, data);
			
			data.ImportFile(localDataPath);
			var prevTime= data.filetime;
			data.UpdateExistingAssets();
			if(data.filetime != prevTime) needsExporting= true;

			if(AssetTrackerSettings.instance.AutoImporting) ImportExternalData();
		}

		static void ExportData(bool force= false) {
			Initialize();

			if(!force && !needsExporting) return;
			needsExporting= false;

			if(EditorManager.debugPantheon) Debug.Log("Exporting asset list");

			string str= data.Export();
			File.WriteAllText(localDataPath, str);
			if(settings.AllowExporting) {
				Directory.CreateDirectory(exportDataDir); 
				File.WriteAllText(exportDataPath, str);
			}
		}

		public static void ImportExternalData(bool force= false) {
			Initialize();
			if(!AssetTrackerSettings.instance.AllowImporting) return;
			
			string sourceDir= EditorManager.appDataPath + "AssetTracking/";
			if(!Directory.Exists(sourceDir)) return;

			foreach(var path in Directory.GetFiles(sourceDir, "*.txt")) {
				string key= Path.GetFileNameWithoutExtension(path);
				AssetTrackerData data;
				if(!allData.TryGetValue(key, out data)) {
					data= new AssetTrackerData();
					data.key= key;
				}
				var filetime= data.filetime;
				data.ImportFile(path);
				allData[data.key]= data; //sometimes redundant
				needsExporting |= (data.filetime != filetime);
			}
		}

		static void StaticUpdate() {
			if(!Initialized) Initialize();

			needsExporting |= (updateQueue.Count > 0);

			while(updateQueue.Count > 0) {
				var path= updateQueue.Dequeue();
				//Ignoring directories 
				data.UpdateEntry(null, path);
			}

			if(flagAssetsCreatedOrRemoved) {
				flagAssetsCreatedOrRemoved= false;
				if(onAssetsCreatedOrRemoved != null) onAssetsCreatedOrRemoved();
			}
		}

		public static bool CheckGUID(string GUID) {
			foreach(var data in allData.Values) {
				if(data.entries.ContainsKey(GUID)) return true;
			}
			return false;
		}

		public static string FilePathToGUID(string filePath) {
			if(string.IsNullOrEmpty(filePath)) return null;

			foreach(var data in allData.Values) {
				if(!filePath.StartsWith(data.projectPath, true, CultureInfo.CurrentCulture)) continue;

				var entry= data.GetEntryFromAssetPath(filePath.Substring(data.projectPath.Length));
				if(entry != null) {
					return entry.GUID;
				}
			}

			return null;
		}
	}
}