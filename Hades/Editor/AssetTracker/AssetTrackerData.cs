﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEditor;
using UnityEngine;

namespace Pantheon {
	public class AssetTrackerData {
		public string projectPath;
		public string GUID; //copied from AssetTrackerSettings.GUID
		string key_;
		public string key {
			get {
				if(key_ == null) key_= GUID + "-" + projectPath.GetHashCode().ToString("X");
				return key_;
			}

			set {
				key_= value;
			}
		}
		public long filetime= -1;

		public Dictionary<string, Entry> entries= new Dictionary<string, Entry>();
		public Dictionary<string, Entry> entries_filepaths= new Dictionary<string, Entry>();

		public Entry GetEntry(string GUID) {
			Entry e= null;
			entries.TryGetValue(GUID, out e);
			return e;
		}

		public Entry GetEntryFromAssetPath(string assetPath) {
			if(string.IsNullOrEmpty(assetPath)) return null;

			Entry entry= null;
			entries_filepaths.TryGetValue(assetPath, out entry);
			return entry;
		}

		//--------------------------------
		public class Entry {
			public readonly AssetTrackerData owner;
			public bool isLocal { get { return owner == AssetTracker.data; } }

			public string GUID;
			//the relative path in whatever project defined it
			public string assetPath;
			public long filetime_UTC;

			public DateTime dateTime {
				get { return DateTime.FromFileTimeUtc(filetime_UTC); }
				set { filetime_UTC= value.ToFileTimeUtc(); }
			}

			public string filePath {
				get { return Path.GetFullPath(Path.Combine(owner.projectPath, assetPath)); }
			}

			public Entry(AssetTrackerData owner) {
				this.owner= owner;
				ModifyTime();
			}

			public bool FileExists() {
				return File.Exists(filePath);
			}

			public bool VerifyGUID() {
				var systemGUID= AssetDatabase.AssetPathToGUID(assetPath);
				return systemGUID == GUID;
			}

			public void ModifyTime() {
				dateTime= DateTime.UtcNow;
			}
		}

		//--------------------------------
		public void UpdateExistingAssets() {
			int prevCount= entries.Count; 

			foreach(var assetPath in AssetDatabase.GetAllAssetPaths()) {
				if(assetPath.Trim() == "") continue;

				//We don't want to track directories. They're useless to us and just clutter up the "is file missing?" checks.
				if(AssetDatabase.IsValidFolder(assetPath)) continue;

				//It's a corner case that we'd ever get a meta file, but we don't want it
				if(assetPath.EndsWith(".meta")) continue;

				if(assetPath.StartsWith(EditorManager.tempAssetPath)) continue;

				if(!assetPath.StartsWith("Assets/")) continue;

				var guid= AssetDatabase.AssetPathToGUID(assetPath);
				UpdateEntry(guid, assetPath);
			}

			if(EditorManager.debugPantheon) {
				Debug.Log("Added " + (entries.Count - prevCount) + " entries from project");
			}
		}

		public void UpdateEntry(string guid, string path) {
			if(string.IsNullOrEmpty(guid)) guid= AssetDatabase.AssetPathToGUID(path);
			if(string.IsNullOrEmpty(path)) path= AssetDatabase.GUIDToAssetPath(guid);

			UpdateEntry(guid, path, DateTime.UtcNow.ToFileTimeUtc());
		}

		public void UpdateEntry(string guid, string assetPath, long filetime) {
			if(string.IsNullOrEmpty(assetPath)) return;

			Entry entry;
			if(!entries.TryGetValue(guid, out entry)) {
				entry= new Entry(this);
				entry.GUID= guid;
				entry.filetime_UTC= -1;
				entries.Add(guid, entry);
			} 

			if(entry.filetime_UTC <= filetime) {
				if(!string.IsNullOrEmpty(entry.assetPath)) {
					Entry existingEntry;
					if(entries_filepaths.TryGetValue(assetPath, out existingEntry) && (existingEntry == entry)) {
						entries_filepaths.Remove(assetPath);
					}
				}

				entry.filetime_UTC= filetime;
				entry.assetPath= assetPath;
				if(this.filetime < filetime) this.filetime= filetime;
				entries_filepaths[assetPath]= entry;
			}

		}

		public bool ImportFile(string filepath) {
			if(string.IsNullOrEmpty(filepath)) return false;
			if(!File.Exists(filepath)) return false;

			var filecontents= File.ReadAllText(filepath);
			if((filecontents == null) || (filecontents == "")) return false;

			var lines= filecontents.Split('\n');
			var values= new Dictionary<string, string>();
			int prevCount= entries.Count;

			int lineIndex= 0;
			while(lineIndex < lines.Length) {
				var line= lines[lineIndex++];
				if(line == "data") break;

				if(line.Trim() == "") continue;
				var field= line.Split('=');
				values.Add(field[0].Trim(), field[1].Trim());
			}

			if(values.ContainsKey("ExportTime")) {
				var filetime= long.Parse(values["ExportTime"]);
				if(filetime == this.filetime) {
					//Same file (probably, unless something weird happens)
					return true;
				}
				this.filetime= filetime;
			}

			values.TryGetValue("ProjectPath", out projectPath);

			bool invalid= false;

			while(lineIndex < lines.Length) {
				var line= lines[lineIndex++];
				if(line.Trim() == "") continue;
				
				int i1= line.IndexOf('\t');
				int i2= line.IndexOf('\t', i1+1);

				if((i1 <= 0) || (i2 <= i1)) {
					invalid= true;
					continue; 
				}

				string guid= line.Substring(0, i1);
				long filetime= long.Parse(line.Substring(i1 + 1, i2 - i1 - 1));
				string assetPath= line.Substring(i2 + 1);

				UpdateEntry(guid, assetPath, filetime);
			}

			if(invalid) {
				EditorManager.Log("Some invalid data was found in '" + filepath + "'", true);
			}

			if(EditorManager.debugPantheon) {
				Debug.Log("Added " + (entries.Count - prevCount) + " entries from " + filepath);
			}

			return true;
		}

		public string Export() {
			StringBuilder str= new StringBuilder();

			var entries= new List<Entry>(this.entries.Count);
			foreach(var e in this.entries.Values) {
				if(!string.IsNullOrEmpty(e.assetPath)) {
					entries.Add(e);
				}
			}

			str.Append("ProjectPath = " + projectPath + "\n");
			str.Append("ExportTime = " + DateTime.UtcNow.ToFileTimeUtc() + "\n");
			str.Append("ExportTimeStr = " + DateTime.UtcNow.ToString() + "\n");
			str.Append("PantheonVersion = " +  Manager.PantheonVersionString + "\n");
			str.Append("EntryCount = " + entries.Count + "\n");
			str.Append("\n");
			str.Append("data\n");

			foreach(var item in entries) {
				str.Append(item.GUID);
				str.Append('\t');
				str.Append(item.filetime_UTC);
				str.Append('\t');
				str.Append(item.assetPath);
				str.Append('\n');
			}

			return str.ToString();
		}

		public void Export(string filepath) {
			File.WriteAllText(filepath, Export());
		}
	}
}