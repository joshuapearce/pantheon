﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

using UnityEditor;
using UnityEngine;

namespace Pantheon {
	public class PrefabHelper {

		//--------------------------------

		//Returns true if the obj is the original root asset
		public static bool isPrefabAsset(UnityEngine.Object obj) {
			//Unity 2018.3 added some more PrefabUtility functions for this purpose, but the API is arcane and the names chosen
			//are often ambiguous. For that reason, we'll do it the simple, reliable (backwards compatible) way
			return (obj != null) && (getPrefabAsset(obj) == obj);
		}

		//Returns true if the obj is the original root asset, or an instance of that object (not a child)
		public static bool isPrefabOrInstance(UnityEngine.Object obj) {
			if(isPrefabAsset(obj)) return true;

			return IsAnyPrefabInstanceRoot(obj as GameObject);
		}

		//returns true if obj is NOT the prefab asset, but is an instance of it. Children not included.
		public static bool IsAnyPrefabInstanceRoot(GameObject obj) {
			if(obj == null) return false;

			#if UNITY_2018_3_OR_NEWER
			return PrefabUtility.IsAnyPrefabInstanceRoot(obj);
			#else

			if(!IsPartOfAnyPrefab(obj)) return false;
			return (PrefabUtility.FindPrefabRoot(obj) == obj); //FindPrefabRoot has some annoying behavior, when objects are not part of prefabs
			#endif
		}

		//Returns true if the object is  part of a prefab asset or part of an instance of a prefab
		public static bool IsPartOfAnyPrefab(GameObject obj) {
			#if UNITY_2018_3_OR_NEWER
			return !string.IsNullOrEmpty(PrefabUtility.GetPrefabAssetPathOfNearestInstanceRoot(obj));
			#else
			return (PrefabUtility.GetPrefabObject(obj) != null);
			#endif
		}

		public static bool IsPrefabAssetMissing(GameObject obj) {
			#if UNITY_2018_3_OR_NEWER
			return PrefabUtility.IsPrefabAssetMissing(obj);
			#else
			if(!IsPartOfAnyPrefab(obj)) return false;
			var asset= getPrefabAsset(obj);
			if(asset == null) return true;
			return false;
			#endif
		}

		static string GetPrefabAssetPathOfNearestInstanceRoot(UnityEngine.Object obj) {
			#if UNITY_2018_3_OR_NEWER
			return PrefabUtility.GetPrefabAssetPathOfNearestInstanceRoot(obj);
			#else
			return AssetDatabase.GetAssetPath(getPrefabAsset(obj));
			#endif
		}

		public static UnityEngine.Object getPrefabAsset(string guid) {
			if(string.IsNullOrEmpty(guid)) return null;
			var path= AssetDatabase.GUIDToAssetPath(guid);
			if(string.IsNullOrEmpty(path)) return null;

			return AssetDatabase.LoadAssetAtPath<UnityEngine.Object>(path);
		}

		//Returns the actual asset resource which this object is related to
		//Child objects are included
		public static UnityEngine.Object getPrefabAsset(UnityEngine.Object obj) {
			if(obj == null) return null;
#if UNITY_2018_3_OR_NEWER
			return PrefabUtility.GetCorrespondingObjectFromOriginalSource(obj);
#else
			if(obj is GameObject) {
				//if(!IsAnyPrefabInstanceRoot(obj as GameObject)) return null;
				if(!string.IsNullOrEmpty(AssetDatabase.GetAssetPath(obj))) return obj;
				return PrefabUtility.GetPrefabParent(obj);
			}

			return null;
#endif
		}

		//--------------------------------
		public static void GetReferencedAssets(
			UnityEngine.Object obj,
			HashSet<UnityEngine.Object> output,
			HashSet<UnityEngine.Object> tested=null
		) {
			GetReferencedAssets<UnityEngine.Object>(obj, output, tested);
		}

		public static void GetReferencedAssets<T>(
			UnityEngine.Object obj,
			HashSet<T> output,
			HashSet<UnityEngine.Object> tested=null
		)
			where T:UnityEngine.Object
		{
			if(obj == null) return;
			if(tested == null) tested= new HashSet<UnityEngine.Object>();

			var newOutput= new HashSet<UnityEngine.Object>();
			GetObjectReferences(obj, newOutput, tested, true);

			foreach(var i in newOutput) {
				UnityEngine.Object asset= null;

				var prefab= getPrefabAsset(i);
				if(prefab != null) {
					asset= prefab;
					GetReferencedAssets<T>(prefab, output, tested); //recursing
				}else	if(AssetDatabase.Contains(i)) {
					asset= i;
				}

				if(asset != null) {
					if(asset is T) output.Add(asset as T);
				}
			}
		}

		public static void GetObjectReferences(
			UnityEngine.Object obj,
			HashSet<UnityEngine.Object> output,
			HashSet<UnityEngine.Object> tested=null,
			bool recursive= false
		) {
			if(obj == null) return;
			if(tested == null) tested= output;
			if(obj is Transform) obj= (obj as Transform).gameObject;

			if(tested.Contains(obj)) return;
			tested.Add(obj);

			var gameObj= obj as GameObject;
			if(gameObj != null) {
				foreach(var c in gameObj.GetComponents<MonoBehaviour>()) {
					GetObjectReferences(c, output);
				}

				if(recursive) {
					var transform= gameObj.transform;
					if(transform != null) {
						for(int i= transform.childCount; i-- > 0;) {
							GetObjectReferences(transform.GetChild(i).gameObject, output);
						}
					}
				}

			}else{
				var s= new SerializedObject(obj);
				var i= s.GetIterator();

				while(i.Next(true)) {
					UnityEngine.Object reference= null;
					switch(i.propertyType) {
						case SerializedPropertyType.ExposedReference:	reference= i.exposedReferenceValue;	break;
						case SerializedPropertyType.ObjectReference:	reference= i.objectReferenceValue;	break;
					}

					if(reference != null) {
						output.Add(reference);
						if(recursive) {
							GetObjectReferences(i.objectReferenceValue, output, tested, true);
						}
					}
				}
			}
		}
	}
}