﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;

using UnityEngine;
using UnityEditor;

#pragma warning disable 0219

using System.Reflection;

namespace Pantheon {
	[InitializeOnLoad]
	public class SceneBrowser_Temp:
		VulcanAssetListWindow<UnityEngine.Object>
	{
		public override bool isDynamic { get { return true; } }

		public static AssetCollection staticassets { get { return SceneBrowser.staticassets; } }
		protected override AssetCollection assets { get { return staticassets; } }

		public static AssetCollection.Category recentCategory { get { return SceneBrowser.recentCategory; } }

		static SceneBrowser_Temp() {

		}

		public static void Open(string categoryName) {
			var viewRect= SceneView.lastActiveSceneView.position;

			var position= new Vector2(viewRect.xMin + 10, viewRect.yMin);
			var rect= new Rect(position.x, position.y, 350, 500);

			//var instance= VulcanWindow.OpenSpecialWindow<SceneBrowser_Temp>();
			var instance= VulcanWindowManager.OpenMenuWindow<SceneBrowser_Temp>(rect, 0.5f);

			instance.activeCategory= staticassets.GetCategory(categoryName);
			instance.hideToolbar= (categoryName != "All") && (categoryName != "Recent");
		}

		protected override void InitializePanel() {
			hasPictures= true;
			hasFavorites= true;
			hasCategoriesMenu= false;
			hasCustomCategories= false;
			hideFileExtension= true;
			hasMultiSelection= false;
			hasContextMenu= false;
			hasSpecialCloseButton= true;

			defaultTitle= "Scenes";
			defaultIcon= EditorGUIUtility.IconContent("BuildSettings.Editor.Small").image;

			base.InitializePanel();
		}

		//----------------------------------
		[ConditionalMenuItem("File/Open Local Scene %#o", true)] 
		static ConditionalMenuItem.ValidateResult OpenLocalScene_Validator() {
			if(!PantheonSettings.instance.EnableMenuOpenLocalScenes) return ConditionalMenuItem.ValidateResult.Hidden;
			return ConditionalMenuItem.ValidateResult.Enabled;
		}

		[ConditionalMenuItem("File/Open Local Scene %#o", false, 151)] 
		public static void OpenLocalScene() {
			SceneBrowser.RefreshAssets_static();
			SceneBrowser_Temp.Open("All");
		}

		[ConditionalMenuItem("File/Open Recent Scene %#o", true)] 
		static ConditionalMenuItem.ValidateResult OpenRecentScene_Validator() {
			if(!PantheonSettings.instance.EnableMenuOpenRecentScenes) return ConditionalMenuItem.ValidateResult.Hidden;
			if(SceneBrowser_Temp.recentCategory.Count <= 1) return ConditionalMenuItem.ValidateResult.Disabled;
			return ConditionalMenuItem.ValidateResult.Enabled;
		}

		[ConditionalMenuItem("File/Open Recent Scene %#o", false, 152)] 
		public static void OpenRecentScene() {
			SceneBrowser.RefreshAssets_static();
			SceneBrowser_Temp.Open("Recent");
		}

		//----------------------------------
		protected override void ProcessEvents(Event e) {
			base.ProcessEvents(e);

			switch(e.type) {
			case EventType.KeyUp:
				if(e.keyCode == KeyCode.Escape) Close();
				break;
			}

		}

		void OnLostFocus() {
			if(EditorManager.debugPantheon && (Selection.activeObject == this)) return;
			Close();
		}

		protected override void DrawToolbar(Rect rect) {
			//base.DrawToolbar(rect);
			if(GUILayout.Toggle(activeCategory.name == "All", "All", EditorStyles.toolbarButton)) {
				activeCategory= assets.GetCategory("All");
				RefreshContents();
			}
			
			if(GUILayout.Toggle(activeCategory.name == "Recent", "Recent", EditorStyles.toolbarButton)) {
				activeCategory= assets.GetCategory("Recent");
				RefreshContents();
			}

			if(GUILayout.Toggle(showOnlyFavorites, "Favorites", EditorStyles.toolbarButton) != showOnlyFavorites) {
				showOnlyFavorites= !showOnlyFavorites;
				RefreshContents();
			}
		}

		protected override void RefreshAssets() {

		}

		public override bool isObjectRelevant(UnityEngine.Object obj) {
			return obj is SceneAsset;
		}
		
 		protected override UnityEngine.Object GetRelevantAsset(UnityEngine.Object obj) {
			return isObjectRelevant(obj) ? obj : null;
		}

		protected override string GetGUID(UnityEngine.Object obj) {
			if(obj == null) return null;
			var path= AssetDatabase.GetAssetPath(obj);
			if(string.IsNullOrEmpty(path)) return null;
			return AssetDatabase.AssetPathToGUID(path);
		}

		protected override void onDoubleClickedItem(string guid) {
			AssetDatabase.OpenAsset(GetAsset(guid));
			Close();
		}
	}
}