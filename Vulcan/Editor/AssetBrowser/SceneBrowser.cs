﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;

using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;

#pragma warning disable 0219

namespace Pantheon {
	[InitializeOnLoad]
	public class SceneBrowser:
		VulcanAssetListWindow<UnityEngine.Object>
	{
		private static AssetCollection staticassets_;
		public static AssetCollection staticassets {
			get { return staticassets_ ?? (staticassets_= EditorManager.GetSettings<AssetCollection>("SceneCollection.settings")); }
		}
		protected override AssetCollection assets { get { return staticassets; } }

		[SelfInitializing] public static AssetCollection.Category recentCategory {
			get {
				var c= staticassets.GetCategory("Recent");
				if(c == null) {
					c= staticassets.GetOrCreateCategory("Recent", true);
					c.disableUserChanges= true;
					c.itemsAddedToStartOfList= true;
					c.sortable= false;
				}
				return c;
			}
		}

		static IEnumerable<string> recentScenes {
			get {
				foreach(var guid in recentCategory) {
					if(!string.IsNullOrEmpty(AssetDatabase.GUIDToAssetPath(guid))) {
						yield return guid;
					}
				}
			}
		}

		public bool showRecent;

		static SceneBrowser() {
			EditorManager.RegisterModule("Scene Browser");

			//It's harder to track scenes in versions of unity prior to 2018.3, so we just do it manually now
			Manager.editorUpdate += CheckForSceneChange;

			/*
			#if UNITY_2018_3_OR_NEWER
			EditorSceneManager.activeSceneChangedInEditMode += onSceneChanged;
			#endif
			EditorSceneManager.activeSceneChanged += onSceneChanged;
			*/
		}

		static UnityEngine.SceneManagement.Scene scene_previous;
		static void CheckForSceneChange() {
			var scene= EditorSceneManager.GetActiveScene();
			if(scene != scene_previous) {
				AddSceneToRecent(scene);
				scene_previous= scene;
			}
		}

		static void AddSceneToRecent(UnityEngine.SceneManagement.Scene scene) {
			string path= scene.path;
			if(string.IsNullOrEmpty(path)) return;
			var guid= AssetDatabase.AssetPathToGUID(path);
			if(string.IsNullOrEmpty(guid)) return;

			recentCategory.Remove(guid);
			recentCategory.Add(guid);

			while(recentCategory.Count > 10) {
				recentCategory.RemoveAt(10);
			}
		}

		//No longer used, due to not being super backwards compatible
		/*
		static void onSceneChanged(UnityEngine.SceneManagement.Scene oldScene, UnityEngine.SceneManagement.Scene newScene) {
			AddSceneToRecent(oldScene);
			AddSceneToRecent(newScene);
		}
		*/

		[ConditionalMenuItem("Tools/Pantheon/Scene Browser" + ((EditorManager.UnityMajorVersion >= 2018) ? "\tShift+O" : ""), ToolsMenu.MenuPriority)]
		[ConditionalMenuItem("Window/General/Scene Browser" + ((EditorManager.UnityMajorVersion >= 2018) ? " #o" : ""), ToolsMenu.GeneralMenuPriority)]
		public static void ShowWindow() {
			var instance= VulcanWindowManager.OpenWindow<SceneBrowser>();
			instance.Focus();
		}

		protected override void InitializePanel() {
			hasPictures= true;
			hasFavorites= true;
			hasCategoriesMenu= false;
			hasCustomCategories= false;
			hideFileExtension= true;
			hasContextMenu= true;
			hasMultiSelection= false;

			defaultTitle= "Scene Browser";
			defaultIcon= EditorGUIUtility.IconContent("BuildSettings.Editor.Small").image;

			base.InitializePanel();
		}

		protected override void RefreshAssets() {
			RefreshAssets_static();
		}

		public static void RefreshAssets_static() {
			var assetPath= Application.dataPath;

			staticassets.allCategory.Clear();
			foreach(var filePath in Directory.GetFiles(assetPath, "*.unity", SearchOption.AllDirectories)) {
				var path= filePath;
				//path= path.Substring(0, filePath.Length - ".prefab".Length);
				path= path.Replace('\\','/');
				if(path.StartsWith(assetPath)) path= path.Substring(assetPath.Length + 1);
				path= "Assets/"+path;

				var guid= AssetDatabase.AssetPathToGUID(path);
				if(string.IsNullOrEmpty(guid)) continue;

				//var asset= AssetDatabase.LoadAssetAtPath(path, typeof(SceneAsset));
				//if(asset == null) continue;

				staticassets.allCategory.Add(guid);
			}
		}

		protected override void RefreshVisibleItems() {
			base.RefreshVisibleItems();

			if(showRecent) {
				visibleItems.RemoveAll((guid)=> {
					return !recentCategory.Contains(guid);
				});
			}
		}

		public override bool isObjectRelevant(UnityEngine.Object obj) {
			return obj is SceneAsset;
		}
		
 		protected override UnityEngine.Object GetRelevantAsset(UnityEngine.Object obj) {
			return isObjectRelevant(obj) ? obj : null;
		}

		protected override string GetGUID(UnityEngine.Object obj) {
			if(obj == null) return null;
			var path= AssetDatabase.GetAssetPath(obj);
			if(string.IsNullOrEmpty(path)) return null;
			return AssetDatabase.AssetPathToGUID(path);
		}

		//----------------------------------
		protected override void PopulateContextMenu(GenericMenu menu, List<string> guids) {
			var objects= new List<UnityEngine.Object>();
			foreach(var guid in guids) objects.Add(GetAsset(guid));

			bool singular= objects.Count == 1;

			if(singular) menu.AddItem(new GUIContent("Open"), false, ()=> { AssetDatabase.OpenAsset(objects[0]); });
			//menu.AddItem(new GUIContent("Spawn"), false, ()=> { foreach(var obj in objects) PrefabUtility.InstantiatePrefab(obj); });
			menu.AddItem(new GUIContent("Select asset" + (singular ? "" : "s")), false, ()=> { Selection.objects= objects.ToArray(); });

			base.PopulateContextMenu(menu, guids);
		}

		[SceneMenuItem]
		static IEnumerable<SceneMenuItem> PopulateSceneContextMenu(SceneMenuItem.Arguments arguments) {
			int counter= 0;
			foreach(var sceneGUID in recentScenes) {
				string path= AssetDatabase.GUIDToAssetPath(sceneGUID);
				string name= Path.GetFileNameWithoutExtension(path);
				var item= new SceneMenuItem("Recent Scenes/" + name);
				item.priority= 1000;
				item.subPriority= counter++;
				item.function= (arguments_) => {
					if(EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo()) {
						EditorSceneManager.OpenScene(path, OpenSceneMode.Single);
					}
				};
				yield return item;
			}
		}

		protected override void DrawToolbar(Rect rect) {
			base.DrawToolbar(rect);

			if(showRecent != DrawToolbarTab("Recent", showRecent)) {
				showRecent= !showRecent;
				RefreshContents();
			}
		}

		protected override void onDoubleClickedItem(string GUID) {
			var path= AssetDatabase.GUIDToAssetPath(GUID);
			if(string.IsNullOrEmpty(path)) {
				Debug.LogError("Could not find scene matching GUID '"+ GUID + "'");
				return;
			}

			EditorSceneManager.OpenScene(path);
		}
	}
}