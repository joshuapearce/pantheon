﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;

using UnityEngine;
using UnityEditor;
using UnityEngine.SceneManagement;

#pragma warning disable 0219

namespace Pantheon {
	[InitializeOnLoad]
	public class PrefabBrowser:
		VulcanAssetListWindow<UnityEngine.Object>
	{
		private static AssetCollection staticassets_;
		public static AssetCollection staticassets {
			get { return staticassets_ ?? (staticassets_= EditorManager.GetSettings<AssetCollection>("PrefabCollection.settings")); }
		}
		protected override AssetCollection assets { get { return staticassets; } }

		static PrefabBrowser() {
			EditorManager.RegisterModule("Prefab Browser");

			Manager.editorRunOnce += RefreshAssets;
			Manager.update += StaticUpdate;

			#if UNITY_2018_1_OR_NEWER
			EditorApplication.projectChanged += RefreshAssets;
			EditorApplication.hierarchyChanged += ()=> { dirtySpawnedList= true; };
			#else
			EditorApplication.projectWindowChanged += RefreshAssets;
			EditorApplication.hierarchyWindowChanged += ()=> { dirtySpawnedList= true; };
			#endif

			SceneManager.activeSceneChanged += (a, b)=> { dirtySpawnedList= true; };
		}

		static IEnumerable<PrefabBrowser> AllWindows {
			get {
				foreach(var v in VulcanWindowManager.AllWindows) {
					if(v is PrefabBrowser) yield return v as PrefabBrowser;
				}
			}
		}

		[ConditionalMenuItem("Tools/Pantheon/Prefab Browser" + ((EditorManager.UnityMajorVersion >= 2018) ? "\tShift+P" : ""), ToolsMenu.MenuPriority)]
		[ConditionalMenuItem("Window/General/Prefab Browser" + ((EditorManager.UnityMajorVersion >= 2018) ? " #p" : ""), ToolsMenu.GeneralMenuPriority)]
		public static void ShowWindow() {
			var instance= VulcanWindowManager.OpenWindow<PrefabBrowser>();
			instance.Focus();
		}

		static AssetCollection.Category spawnedCategory_;
		static AssetCollection.Category spawnedCategory {
			get {
				if(spawnedCategory_ == null) {
					staticassets.GetOrCreateCategory("All", true);
					spawnedCategory_= staticassets.GetOrCreateCategory("Spawned", true);
					spawnedCategory_.disableUserChanges= true;
					spawnedCategory_.codeGenerated= true;
				}
				return spawnedCategory_;
			}
		}

		static AssetCollection.Category recentCategory_;
		static AssetCollection.Category recentCategory {
			get {
				if(recentCategory_ == null) {
					recentCategory_= staticassets.GetOrCreateCategory("Recent");
					recentCategory_.special= true;
					recentCategory_.itemsAddedToStartOfList= true;
					recentCategory_.sortable= false;
					recentCategory_.disableUserChanges= true;
				}
				return recentCategory_;
			}
		}

		static HashSet<UnityEngine.Object> spawnedPrefabs= new HashSet<UnityEngine.Object>();

		static HashSet<string> allPrefabGUIDs;
		static double dirtySpawnedList_time= 0.0f;
		static bool dirtySpawnedList {
			get { return !double.IsNaN(dirtySpawnedList_time); }
			set { dirtySpawnedList_time= value ? EditorApplication.timeSinceStartup : double.NaN; }
		}

		protected override void InitializePanel() {
			hasPictures= true;
			hasFavorites= true;
			hasCategoriesMenu= true;
			hasCustomCategories= true;
			hideFileExtension= true;
			hasContextMenu= true;

			defaultTitle= "Prefab Browser";
			#if UNITY_2018_1_OR_NEWER
			defaultIcon= EditorGUIUtility.IconContent("d_Prefab Icon").image;
			#endif

			hasMultiFilters= true;
			hasAutoFilterRefresh= true;
			filterNames= new string[] {
				"Instantiated in scene",
				"Referenced in scene",
				"Always include nested prefabs"
			};

			base.InitializePanel();

			if(spawnedCategory == null) { } //initializer
			if(recentCategory == null) { }  //initializer
		}

		protected override void OnDestroy() {
			//if(instance == this) instance= null;

			base.OnDestroy();
		}

		static void StaticUpdate() {
			if(dirtySpawnedList && (EditorApplication.timeSinceStartup - dirtySpawnedList_time) > 0.5f) {
				List<PrefabBrowser> windows= new List<PrefabBrowser>();

				foreach(var window in AllWindows) {
					if(window.activeCategory == spawnedCategory) { 
						windows.Add(window);
					}
				}

				if(windows.Count > 0) {
					RelistSpawnedPrefabs();
					foreach(var window in windows) window.Refresh();
				}
			}
		}

		public static void AddRecent(UnityEngine.Object prefab) {
			if(!PrefabHelper.isPrefabAsset(prefab)) return;

			var guid= GetGUID_(prefab);

			recentCategory.Remove(guid);
			recentCategory.Add(guid);

			while(recentCategory.Count > 16) recentCategory.RemoveAt(16);
		}

		protected new static void RefreshAssets() {
			var assetPath= Application.dataPath;

			foreach(var filePath in Directory.GetFiles(assetPath, "*.prefab", SearchOption.AllDirectories)) {
				var path= filePath;
				path= path.Replace('\\','/');
				if(path.StartsWith(assetPath)) path= path.Substring(assetPath.Length + 1);
				path= "Assets/"+path;
				//var asset= AssetDatabase.LoadAssetAtPath(path, typeof(UnityEngine.Object));
				//if(asset == null) continue;

				var guid= AssetDatabase.AssetPathToGUID(path);
				if(string.IsNullOrEmpty(guid)) continue;
				staticassets.allCategory.Add(guid);

				if(EditorManager.debugPantheon) {
					//EditorManager.Log("Found prefab " + path);
				}
			}

			foreach(var window in AllWindows) {
				if(window is PrefabBrowser) {
					window.RefreshContents();
				}
			}
		}

		protected override void ManualRefresh() {
			RefreshAssets();
			base.ManualRefresh();
		}

		protected override void RefreshContents() {
			base.RefreshContents();

			if(filterEnabled) {
				var scene= SceneManager.GetActiveScene();
				bool includeInstances= GetFilterState(0);
				bool includeReferences= GetFilterState(1);
				bool includeNestedPrefabs= GetFilterState(2);

				var relevantPrefabs= new HashSet<UnityEngine.Object>();

				var tested= new HashSet<UnityEngine.Object>();

				ForeachObjectInScene((obj)=> {
					{
						var prefab= PrefabHelper.getPrefabAsset(obj);
						if(prefab != null) {
							if(includeInstances) {
								relevantPrefabs.Add(prefab);
							}

							//if(tested.ContainsKey(prefab)) return false;
							tested.Add(prefab);
						}
					}

					if(includeReferences) {
						var references= new HashSet<UnityEngine.Object>();
						PrefabHelper.GetObjectReferences(obj, references, tested, false);
						foreach(var i in references) {
							var prefab= PrefabHelper.getPrefabAsset(i);
							if(prefab != null) relevantPrefabs.Add(prefab);
						}
					}
					return true;
				});

				if(includeNestedPrefabs) {
					var stack= new Stack<UnityEngine.Object>();
					foreach(var i in relevantPrefabs) stack.Push(i);

					while(stack.Count > 0) {
						var prefab= stack.Pop();
						foreach(var child in GetAllChildren(prefab as GameObject)) {
							var nestedPrefab= PrefabHelper.getPrefabAsset(child);
							if((nestedPrefab != null) && !relevantPrefabs.Contains(nestedPrefab)) {
								relevantPrefabs.Add(nestedPrefab);
								stack.Push(nestedPrefab);
							}
						}
					}
				}

				//updating the visible list
				var relevantGUIDs= new HashSet<string>();
				foreach(var asset in relevantPrefabs) {
					relevantGUIDs.Add(GetGUID(asset));
				}

				visibleItems.RemoveAll((guid)=> {
					if(!TestGUID(guid)) return true;
					if(!relevantGUIDs.Contains(guid)) return true;
					return false;
				});

			}else{
				visibleItems.RemoveAll((guid)=> {
					return !TestGUID(guid);
				});
			}
		}

		static void RelistSpawnedPrefabs(bool force= false) {
			if(!force && !dirtySpawnedList) return;
			dirtySpawnedList= false;

			//Debug.Log("Relisting");
			//Doing the main work
			spawnedPrefabs.Clear();
			ForeachObjectInScene((obj)=> {
				if(PrefabHelper.IsAnyPrefabInstanceRoot(obj)) {
					spawnedPrefabs.Add(PrefabHelper.getPrefabAsset(obj));
					return false;
				}
				return true;
			});


			//Organization work
			spawnedCategory.Clear();
			foreach(var asset in spawnedPrefabs) {
				var guid= GetGUID_(asset);
				spawnedCategory.Add(guid);
			}
		}

		protected override void onClickedItem(string guid) {

		}

		protected override void onDoubleClickedItem(string guid) {
			AssetDatabase.OpenAsset(GetAsset(guid));
		}

		//----------------------------------
		protected new static string GetItemName(string guid, bool includeFull=false) {
			var path= AssetDatabase.GUIDToAssetPath(guid);
			if(string.IsNullOrEmpty(path)) return null;
			//if(string.IsNullOrEmpty(path)) throw new KeyNotFoundException("GUID not found: " + guid);

			string result;
			if(includeFull) {
				result= path.Substring("Assets/".Length);
				result= result.Substring(0, result.LastIndexOf('.'));
			}else{
				result= Path.GetFileNameWithoutExtension(path);
			}

			return result;
		}

		public override bool isObjectRelevant(UnityEngine.Object obj) {
			return PrefabHelper.isPrefabOrInstance(obj);
		}

		public static string GetGUID_(UnityEngine.Object asset) {
			if(asset == null) return null;

			var path= AssetDatabase.GetAssetPath(asset);
			if(string.IsNullOrEmpty(path) )return null;

			if(!path.EndsWith(".prefab")) return null;

			return AssetDatabase.AssetPathToGUID(path);
		}

 		protected override UnityEngine.Object GetRelevantAsset(UnityEngine.Object obj) {
			return PrefabHelper.getPrefabAsset(obj);
		}

		protected override string GetGUID(UnityEngine.Object obj) {
			return GetGUID_(obj);
		}

		//----------------------------------
		protected override void PopulateContextMenu(GenericMenu menu, List<string> guids) {
			if(guids.Count == 0) return; //shouldn't happen

			var objects= new List<UnityEngine.Object>();
			foreach(var guid in guids) objects.Add(GetAsset(guid));

			bool singular= objects.Count == 1;

#if UNITY_2018_1_OR_NEWER
			if(singular) menu.AddItem(new GUIContent("Open"), false, ()=> { AssetDatabase.OpenAsset(objects[0]); });
#endif

			menu.AddItem(new GUIContent("Spawn"), false, ()=> {
				foreach(var obj in objects) Spawn(obj);
			});

			menu.AddItem(new GUIContent("Select asset" + (singular ? "" : "s")), false, ()=> { Selection.objects= objects.ToArray(); });

			if(singular) {
				menu.AddItem(new GUIContent("Select file"), false, ()=> { EditorUtility.RevealInFinder(AssetDatabase.GUIDToAssetPath(guids[0])); });
			}

			menu.AddItem(new GUIContent("Select instances"), false, ()=> {
				var results= new List<GameObject>(10000);
				var assets= new Dictionary<UnityEngine.Object, int>();
				foreach(var obj in objects) assets.Add(obj, 0);

				ForeachObjectInScene((obj)=> {
					if(PrefabHelper.IsAnyPrefabInstanceRoot(obj)) {
						var asset= PrefabHelper.getPrefabAsset(obj);
						if((asset != null) && assets.ContainsKey(asset)) {
							results.Add(obj);
						}
						return false;
					}
					return true;
				});

				if(results.Count == 0) {
					EditorUtility.DisplayDialog("No instances found", "The selected prefab"+ ((assets.Count > 1) ? "s" :"") + " could not be found in the current scene.", "ok");
				}else{
					Selection.objects= results.ToArray();
				}
			});

			base.PopulateContextMenu(menu, guids);
		}

		static IEnumerable<SceneMenuItem> CreateSceneMenuItems_worker(string menuname, SceneMenuItem.Arguments arguments, IEnumerator<string> items, bool fullNames) {
			var assets= staticassets;
			int count= 0;

			menuname= menuname.TrimEnd('/');

			while(items.MoveNext()) {
				string guid= items.Current;
				var itemname= GetItemName(guid, fullNames);

				if(itemname != null) {
					var item= new SceneMenuItem();
					item.path= menuname + "/" + itemname;

					if(assets.isFavorite(guid)) {
						item.state= SceneMenuItem.ValidateResult.ValidAndMarked;
						//item.name= item.name.Insert(item.name.LastIndexOf('/')+1, "\u2606");
					}

					item.function= (arguments_) => {
						GameObject obj;
						if(SceneMenu.spawnAsChildren) obj= Spawn(guid); else obj= Spawn(guid, null);
						SceneMenu.OnObjectSpawnedFromMenu(obj, arguments_);
					};

					yield return item;
					++count;
				}
			}

			if(count == 0) {
				yield return new SceneMenuItem(menuname, SceneMenuItem.ValidateResult.Disabled);
			}
		}

		[SceneMenuItem]
		private static IEnumerable<SceneMenuItem> CreateSceneMenuItems(SceneMenuItem.Arguments arguments) {
			RelistSpawnedPrefabs();

			string pathprefix= "Spawn Prefab/";
			var assets= staticassets;
			var categories= assets.categories;

			foreach(var item in CreateSceneMenuItems_worker(pathprefix + "All", arguments, assets.GetCategory("All").GetEnumerator(), true)) {
				yield return item;
			}

			foreach(var item in CreateSceneMenuItems_worker(pathprefix + "Favorites", arguments, assets.favorites.GetEnumerator(), false)) {
				yield return item;
			}

			for(int i= 1; i<categories.Count; i++) {
				var category= categories[i];

				foreach(var item in CreateSceneMenuItems_worker(pathprefix + category.name, arguments, category.GetEnumerator(), false)) {
					yield return item;
				}
			}
		}

		//----------------------------------
		static GameObject Spawn(UnityEngine.Object asset) {
			Transform parent= null;
			if(Selection.activeGameObject != null) {
				parent= Selection.activeGameObject.transform;
			}
			return Spawn(asset, parent);
		}

		static GameObject Spawn(UnityEngine.Object asset, Transform parent) {
			AddRecent(asset);

			var names= new HashSet<string>();
			if(parent == null) {
				foreach(var child in UnityEngine.SceneManagement.SceneManager.GetActiveScene().GetRootGameObjects()) {
					names.Add(child.name);
				}
			}else{
				for(int i= 0; i<parent.childCount; i++) {
					names.Add(parent.GetChild(i).name);
				}
			}

			var obj= PrefabUtility.InstantiatePrefab(asset) as GameObject;
			Selection.activeObject= obj;
			EditorGUIUtility.PingObject(obj);
			if(parent != null) obj.transform.SetParent(parent);

			string basename= obj.name;
			
			int counter= 0;
			while(names.Contains(obj.name)) {
				++counter;
				obj.name= basename + " (" + counter + ")";
			}

			Undo.RegisterCreatedObjectUndo(obj, "Created object from prefab");
			return obj;
		}

		static GameObject Spawn(string guid) {
			return Spawn(PrefabHelper.getPrefabAsset(guid));
		}

		static GameObject Spawn(string guid, Transform parent) {
			return Spawn(PrefabHelper.getPrefabAsset(guid), parent);
		}

		private static void ForeachObjectInScene(Func<GameObject, bool> func) {
			Scene scene= SceneManager.GetActiveScene();

			foreach(var obj in scene.GetRootGameObjects()) {
				RecurseThroughGameObjectAndChildren(obj, func);
			}
		}

		private static void RecurseThroughGameObjectAndChildren(GameObject obj, Func<GameObject, bool> func) {
			if(func(obj)) {
				for(int i= 0; i<obj.transform.childCount; i++) {
					var child= obj.transform.GetChild(i).gameObject;
					if(child != null) RecurseThroughGameObjectAndChildren(child, func);
				}
			}
		}

		private static void ForEverySerializedField(UnityEngine.Object obj, Action<object> func, bool includeClassesAndStructs= false) {
			var sobj= new SerializedObject(obj);
			var iterator= sobj.GetIterator();

			while(iterator.Next(includeClassesAndStructs)) {
				func(iterator.objectReferenceValue);
			}
		}

		public static IEnumerable<GameObject> GetAllChildren(GameObject parent) {
			int i= 0;
			while(i < parent.transform.childCount) {
				var child= parent.transform.GetChild(i++).gameObject;
				yield return child;
				foreach(var subchild in GetAllChildren(child)) yield return subchild;
			}
		}
	}
}