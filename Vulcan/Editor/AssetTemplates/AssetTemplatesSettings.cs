using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

using UnityEditor;
using UnityEngine;

namespace Pantheon {
	[InitializeOnLoad]
	public class AssetTemplatesSettings:
		ModuleSettings,
		ISerializationCallbackReceiver
	{
		[Serializable]
		public struct TagInfo {
			public string Key;
			public string Value;
		}

		[SettingsField(
			"Custom tags: System",
			"These values will replace tagnames in script templates such as #NAME# or #FOO#\n"+
			"Format is \"tagname:tagvalue; tag2name: tag2value; etc\"\n"+
			"For more flexibility, use AssetParsingTagAttribute",
			SettingsFieldAttribute.Flags.SystemWide,
			100
		)]
		public string customTags_system {
			get { return EditorPrefs.GetString("Pantheon.customTags_system", ""); }
			set { EditorPrefs.SetString("Pantheon.customTags_system", value); }
		}

		[SettingsField(
			"Custom tags: Local",
			"These values will replace tagnames in script templates such as #NAME# or #FOO#\n"+
			"Format is \"tagname:tagvalue; tag2name: tag2value; etc\"\n"+
			"For more flexibility, use AssetParsingTagAttribute",
			SettingsFieldAttribute.Flags.Default,
			200
		)]
		public string customTags_project;

		//--------------------------------
		static AssetTemplatesSettings instance_;
		[SelfInitializing] public static AssetTemplatesSettings instance {
			get {
				if(instance_ == null) instance_= ModuleSettings.LoadOrCreate<AssetTemplatesSettings>("AssetTemplatesSettings.settings");
				return instance_;
			}
		}

		#if UNITY_2018_3_OR_NEWER
		[SettingsProvider]
    private static SettingsProvider InitializeSettingsProvider_static() {
			return instance.InitializeSettingsProvider();
		}
		#endif

		void LoadSystemTags() {

		}

		public void OnBeforeSerialize() {

		}

		public void OnAfterDeserialize() {
			//StringBuilder str= new StringBuilder();
			//foreach(var tag in customTags_system) {
			//	str.Append(tag.Key.Replace("\n","\\n")); str.Append('\n');
			//	str.Append(tag.Value.Replace("\n","\\n")); str.Append('\n');
			//}
			//EditorPrefs.SetString("Pantheon.customTags_system", str.ToString());

			//customTags_system= new List<TagInfo>();
			//var str= EditorPrefs.GetString("Pantheon.customTags_system", "").Split('\n');
			//for(int i= 0; i<str.Length; i += 2) {
			//	customTags_system.Add(new KeyValuePair<string, string>(str[i], str[i+1]));
			//}
		}

		//--------------------------------
		protected override void OnEnable() {
			base.OnEnable();
			instance_= this;
			showInConfigPanel= false;
			displayName= "Script Templates";

			helpFile= "ScriptTemplates.txt";
		}

		//--------------------------------
		public override String ConfigHeaderInfo() {
			return "More flexibility is available by using AssetParsingTagAttribute in C#";
		}

		public override void DrawContents(float contentWidth) {
			Debug.Log("A: " + helpFile);
			base.DrawContents(contentWidth);
		}
	}
}