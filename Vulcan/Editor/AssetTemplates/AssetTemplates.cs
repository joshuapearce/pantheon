﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;

using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Pantheon {
	[InitializeOnLoad]
	public class AssetTemplates {
		static AssetTemplatesSettings settings { get { return AssetTemplatesSettings.instance; } }

		static AssetTemplates() {
			EditorManager.RegisterModule("Asset Templates");
			MenuGenerator.insertCode.Add("AssetTemplates", GenerateMenuScriptCode);

			EditorManager.onEditorFocus += (state) => {
				if(state) {
					EditorManager.settings.testMenuScript= true;
				}
			};
		}

		static string creatingAsset; 

		static string default_cs_template {
			get {
				var field= typeof(MenuItems).GetField("default_cs_template", BindingFlags.NonPublic | BindingFlags.Static);
				if(field == null) return null;
				return field.GetValue(null) as string;
			}
		}

		static string path_LocalFolder { get { return Path.Combine(EditorManager.projectPath, "Assets/Settings/ScriptTemplates/"); } }
		[MenuItem ("Tools/Pantheon/Script Templates/Local assets", false, ToolsMenu.MenuPriority + 10)]
		public static void OpenLocalTemplatesFolder() {
			EditorManager.OpenFolder(path_LocalFolder, true);
		}

		static string path_SharedFolder { get { return Path.Combine(EditorManager.appDataPath, "ScriptTemplates/"); } }
		[MenuItem ("Tools/Pantheon/Script Templates/System wide templates", false, ToolsMenu.MenuPriority + 10)]
		public static void OpenSharedTemplatesFolder() {
			EditorManager.OpenFolder(path_SharedFolder, true);
		}

		[MenuItem ("Tools/Pantheon/Script Templates/Unity's templates", false, ToolsMenu.MenuPriority + 10)]
		public static void OpenUnityTemplatesFolder() {
			var path= Path.Combine(Path.GetDirectoryName(EditorApplication.applicationPath), "Data/Resources/ScriptTemplates");
			Debug.Log(path);
			EditorManager.OpenFolder(path, false);
		}

		static string path_IncludedFolder { get { return Path.Combine(EditorManager.projectPath, "Assets/Pantheon/ScriptTemplates/"); } }

		//[MenuItem ("Tools/Pantheon/Script Templates/Settings", false, ToolsMenu.MenuPriority + 30)]
		public static void OpenSettings() {
			settings.Display();
		}

		[MenuItem ("Tools/Pantheon/Script Templates/Show info", false, ToolsMenu.MenuPriority + 30)]
		public static void ShowInfo() {
			string str= "Script Templates Info\n";

			var entries= ParseEntries();
			if(entries.Count == 0) {
				str += "Detected no template files\n";
			}else {
				str += "Detected " + entries.Count + " template file" + ((entries.Count > 1) ? "s" : "") + ":\n";
				foreach(var entry in entries) {
					str += "\t" + Path.GetFileName(entry.fullPath) + "\t{ " + entry.menuPath + " }\n";
				}
			}
			str += "\n";
			str += "Template tags:\n";
			foreach(var tag in GetTags("Assets/TestDirectory/TestFilename.cs", true)) {
				str += "\t" + tag.Key + " = " + tag.Value + "\n";
			}
			Debug.LogWarning(str);
		}

		[MenuItem ("Tools/Pantheon/Script Templates/Help", false, ToolsMenu.MenuPriority + 30)]
		public static void ShowHelp() {
			settings.ShowHelp();
		}

		struct Entry {
			public string source;
			public string relativePath;
			public string relativeDir { get { var str= Path.GetDirectoryName(relativePath); if(str != "") str += "/"; return str; } }
			public string fullPath { get { return Path.Combine(source, relativePath); } }
			public string menuPath;
			public string menuName { get { return Path.GetFileName(menuPath); } }
			public string defaultName;
			public int priority;
		}

		static List<Entry> ParseEntries() {
			var list= new List<Entry>();

			var directories= new List<string>();
			//directories.Add("Assets/ScriptTemplates");
			directories.Add(path_IncludedFolder);
			directories.Add(path_LocalFolder);
			directories.Add(path_SharedFolder);
			//directories.Add(Path.Combine(EditorApplication.applicationContentsPath, "Pantheon/ScriptTemplates/"));

			directories.RemoveAll( (v)=> !Directory.Exists(v));

			if(directories.Count > 0) {
				foreach(var dir in directories) {
					foreach(var i in Directory.GetFiles(dir, "*.txt", SearchOption.AllDirectories)) {
						var fullPath= i.Replace('\\','/');
						if(fullPath.Contains("/~")) continue; //ignore this whole directory
						if(Path.GetFileName(fullPath)[0] == '~') continue; //ignore this file
						if(!Regex.IsMatch(fullPath, @".+\..+\.txt$")) continue; //This doesn't look like a template file

						string relativePath= Regex.Match(fullPath, "^.*\\/ScriptTemplates\\/(.*)$").Groups[1].Value;

						string fileName= Path.GetFileName(relativePath);
						string menuDir= (relativePath == fileName) ? "" : Path.GetDirectoryName(relativePath);
						string menuFileName;
						string defaultName= null;

						int priority= 84;

						fileName= Regex.Replace(fileName, @"\.txt$", "");
						//var extension= Path.GetExtension(fileName);
						fileName= Path.GetFileNameWithoutExtension(fileName);

						if(fileName.Contains("-")) {
							var parts= fileName.Split('-');

							string menu_hint;
							if(parts.Length >= 3) {
								int.TryParse(parts[0], out priority);
								defaultName= parts[2].Trim();
								menu_hint= parts[1].Trim();
							}else{
								defaultName= parts[1].Trim();
								menu_hint= parts[0].Trim();
							}

							menu_hint= menu_hint.Replace("__","/");
							menuFileName= Path.GetFileName(menu_hint);
							if(menuFileName != menu_hint) {
								menuDir= Path.Combine(menuDir, Path.GetDirectoryName(menu_hint));
							}
						}else{
							defaultName= Path.GetFileName(fileName).Replace(" ", "");
							menuFileName= ObjectNames.NicifyVariableName(fileName);
						}

						var entry= new Entry();

						entry.source= dir;
						entry.relativePath= relativePath;
						entry.menuPath= Path.Combine(menuDir, menuFileName).Replace('\\','/');
						entry.defaultName= defaultName;
						entry.priority= priority;

						list.Add(entry);
					}
				}
			}

			return list;
		}

		static string GenerateMenuScriptCode(bool silently) {
			var completed= new Dictionary<string, string>();
			string code= "";

			int i= 0;
			foreach(var entry in ParseEntries()) {
				++i;
				if(completed.ContainsKey(entry.menuPath)) {
					if(!silently) Debug.Log(entry.fullPath + " overridden by " + completed[entry.menuPath]);

				}else{
					if((entry.relativeDir == "") && (entry.menuName == "C# Script")) {
						code += "      const string default_cs_template= \"" + entry.fullPath + "\";\n\n";

					}else{
						code += "      [MenuItem(@\"Assets/Create/" + entry.menuPath + "\", false, " + entry.priority + ")]\n";
						code += "      private static void AssetMenuEntry_" + i + "() {\n";
						code += "        AssetTemplates.CreateNewAsset(@\"" + entry.fullPath + "\", \"" + entry.defaultName + "\");\n";
						code += "      }\n\n";
					}
					completed.Add(entry.menuPath, entry.fullPath);
				}

			}
			return code;
		}

		public static void CreateNewAsset(string resourcePath, string defaultName= null) {
			string path= null;

			if(Selection.activeObject != null) {
				var assetPath= AssetDatabase.GetAssetPath(Selection.activeObject);
				if(!string.IsNullOrEmpty(assetPath)) {
					if(Directory.Exists(assetPath)) path= assetPath + "/";
					else path= Path.GetDirectoryName(assetPath) + "/";
				}
			}
			 
			if(path == null) {
				var scene= SceneManager.GetActiveScene();
				if(scene.IsValid()) {
					path= Path.GetDirectoryName(scene.path) + "/";
				}
			}

			if(path == null) path= "Assets/";
			path= path.Replace('\\', '/');

			string templateName= Path.GetFileNameWithoutExtension(resourcePath);
			string fileName;
			if(defaultName != null) {
				fileName= defaultName;
			}else{
				fileName= Path.GetFileNameWithoutExtension(templateName);
				if(fileName.EndsWith(".txt")) {
					fileName= Path.GetFileNameWithoutExtension(fileName);
				}
			}

			if(fileName.EndsWith("." + Path.GetExtension(templateName))) {
				fileName= Path.GetFileNameWithoutExtension(fileName);
			}

			fileName= fileName.Replace(" ", "");

			string fileExtension= Path.GetExtension(templateName);
			string filePath;

			int counter= 0;
			while(true) {
				filePath= path;
				filePath += fileName;
				if(counter > 0) filePath += counter;
				filePath += fileExtension;

				if(!File.Exists(filePath)) break;
				++counter;
			}

			ProjectWindowUtil.StartNameEditingIfProjectWindowExists(
				0,
				ScriptableObject.CreateInstance<TemplateWorker>(),
				filePath,
				null,
				resourcePath
			);
			//Selection.activeObject= asset;
		}

		class TemplateWorker:
			UnityEditor.ProjectWindowCallback.EndNameEditAction
		{
			public override void Action(System.Int32 instanceId, System.String pathName, System.String resourceFile) {
				//Debug.Log("Action: " + pathName);
				//Debug.Log("resourceFile: " + resourceFile);

				creatingAsset= pathName;

				if(Path.GetExtension(resourceFile).ToLower() == ".txt") {
					string contents= File.ReadAllText(resourceFile);
					contents= ProcessFileText(pathName, contents);
					File.WriteAllText(pathName, contents);
					AssetDatabase.ImportAsset(pathName);
					EditorManager.Log("Created templated asset " + pathName, true);

				}else{
					File.Copy(resourceFile, pathName);
				}
			}

			//public override void Cancelled(System.Int32 instanceId, System.String pathName, System.String resourceFile) {
				//Debug.Log("Cancelled: " + pathName);
				//Debug.Log("resourceFile: " + resourceFile);
			//}

			//[AssetParsingTag("#TEST#")]
			//public static string TestTag(string path) {
			//	return "/*Ding*/";
			//}
		}

		class AssetMonitor:
			AssetPostprocessor
		{
			public override int GetPostprocessOrder() {
				return -1;
			}

			void OnPreprocessAsset() {
				if(!EditorManager.Initialized) return;

				string guid= AssetDatabase.AssetPathToGUID(assetPath);
				if(string.IsNullOrEmpty(guid) || !AssetTracker.Initialized) {
					//Too early, stop trying
					return;
				}
					
				if(AssetTracker.CheckGUID(guid)) {
					//We don't care, because it's not a new asset
					return; 
				}

				if(assetPath == creatingAsset) {
					//We don't care, because it's already created by this tool
					return;
				}

				if(Path.GetExtension(assetPath) == ".cs") {
					var default_cs_script= AssetTemplates.default_cs_template;
					if(default_cs_script == null) {
						//We don't care, because there's no override in place
						return;
					}

					//We can't hook into the default script creation or overwrite the menu entry
					//And changing the version in Unity/Data/Resources is unreliable/problematic from code
					//So instead we find that default version, and compare it to the new file.
					//If they match, we replace the contents of the new file with our prefered template
					//If the user has created a script in Assets/ScriptTemplates, it will conflict with this
					string defaultContents= File.ReadAllText(EditorApplication.applicationContentsPath + "/Resources/ScriptTemplates/81-C# Script-NewBehaviourScript.cs.txt");
					defaultContents= ProcessFileText(assetPath, defaultContents, false);

					if(defaultContents == File.ReadAllText(assetPath)) {
						string contents= File.ReadAllText(default_cs_script);
						contents= ProcessFileText(assetPath, contents);
						File.WriteAllText(assetPath, contents);
					}
				}
			}
		}

		static Dictionary<string, string> GetTags(string pathName, bool extended= true) {
			var tags= new Dictionary<string, string>();

			var scriptname= Path.GetFileNameWithoutExtension(pathName);

			tags.Add("#NAME#", scriptname); //Is this supposed to do something different than #SCRIPTNAME#?
			tags.Add("#SCRIPTNAME#", scriptname);
			tags.Add("#SCRIPTNAME_LOWER#", scriptname.ToLower());
			tags.Add("#NOTRIM#", "");

			if(extended) {
				tags.Add("#NAMESPACE#", "Namespace"); //In case nobody overrides it

				tags.Add("#SCRIPTNAME_UPPER#", scriptname.ToUpper());

				tags.Add("#DIRECTORY#", Path.GetDirectoryName(pathName) + "/");
				tags.Add("#FILENAME#", Path.GetFileName(pathName));
				tags.Add("#FULLPATH#", Path.GetFullPath(pathName));

				tags.Add("#PROJECTNAME#", PlayerSettings.productName);
				tags.Add("#PROJECTPATH#", EditorManager.projectPath);
				tags.Add("#COMPANYNAME#", PlayerSettings.companyName);
				//tags.Add("#TEMPLATENAME#", resourceFile);

				#if UNITY_2018_1_OR_NEWER
				tags.Add("#USERNAME#", UnityEditor.CloudProjectSettings.userName);
				tags.Add("#USERID#", UnityEditor.CloudProjectSettings.userId);
				#else
				tags.Add("#USERNAME#", "Unity2017");
				tags.Add("#USERID#", "Unity2017");
				#endif

				tags.Add("#VERSION#", PlayerSettings.bundleVersion);

				for(int i= 0; i<2; i ++) {
					DateTime now= (i == 0) ? DateTime.Now : DateTime.UtcNow;
					string postfix= (i == 0) ? "#" : "_UTC#";

					tags.Add("#DATE" + postfix, now.ToShortDateString());
					tags.Add("#TIME" + postfix, now.ToShortTimeString());
					tags.Add("#DATETIME" + postfix, now.ToString());

					tags.Add("#YEAR" + postfix, now.Year.ToString("D4"));
					tags.Add("#MONTH" + postfix, now.Month.ToString("D2"));
					tags.Add("#DAY" + postfix, now.Day.ToString("D2"));
				}

				foreach(var tagInfo in Manager.GetEveryMemberAttribute<AssetParsingTagAttribute>(BindingFlags.Static)) {
					var method= tagInfo.Item2 as MethodInfo;
					var tag= tagInfo.Item1;

					tags[tag.tagName]= tag.Invoke(method, pathName);
				}

				foreach(var str in (settings.customTags_system + ";" + settings.customTags_project).Split(';')) {
					if(string.IsNullOrEmpty(str) || (str.Trim() == "")) continue;
					int index= str.IndexOf(':');
					string tagname= str.Substring(0, index).Trim();
					if(!tagname.StartsWith("#")) tagname= "#" + tagname;
					if(!tagname.EndsWith("#")) tagname += "#";

					string value= str.Substring(index+1).Trim();
					if(!value.StartsWith("#")) value= "#" + value;
					if(!value.EndsWith("#")) value += "#";

					tags[tagname]= value;
				}
			}

			return tags;
		}

		private static string ProcessFileText(string pathName, string contents, bool extended= true) {
			var tags= GetTags(pathName, extended);

			foreach(var tag in tags) {
				//contents= contents.Replace(tag.Key, tag.Value);
				contents= Regex.Replace(contents, @"([^#])" + tag.Key, "$1"+tag.Value);
			}

			contents= Regex.Replace(contents, @"#(#[A-Za-z_]+#)", @"$1");

			return contents;
		}
	}
}