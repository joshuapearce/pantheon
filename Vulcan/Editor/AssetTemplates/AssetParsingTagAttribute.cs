﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;

namespace Pantheon {
	[System.AttributeUsage(AttributeTargets.Method)]
	public class AssetParsingTagAttribute:
		Attribute
	{
		public readonly string tagName;
		public AssetParsingTagAttribute(string tagName) {
			if(!tagName.StartsWith("#")) tagName= "#" + tagName;
			if(!tagName.EndsWith("#")) tagName += "#";

			this.tagName= tagName;
		}

		public string Invoke(MethodInfo method, string path) {
			var parameters= method.GetParameters();
			if(parameters.Length == 0) {
				return method.Invoke(null, null) as string;
			}else{
				return method.Invoke(null, new object[] { path }) as string;
			}
		}
	}
}