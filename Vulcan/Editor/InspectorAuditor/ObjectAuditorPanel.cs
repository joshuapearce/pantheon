using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Pantheon {
	[InitializeOnLoad]
	public class InspectorAuditor:
		VulcanWindow
	{
		[ConditionalMenuItem("Tools/Pantheon/Inspector Auditor", ToolsMenu.MenuPriority)]
		[ConditionalMenuItem("Window/General/Inspector Auditor", ToolsMenu.GeneralMenuPriority)]
		public static void Create() {
			var instance= VulcanWindow.GetWindow<InspectorAuditor>(tInspectorWindow);
			instance.Focus();
		}

		public static Type tInspectorWindow;

		static InspectorAuditor() {
			tInspectorWindow= typeof(Editor).Assembly.GetType("UnityEditor.InspectorWindow");
		}

		static Action<SerializedProperty, SerializedProperty> RenderSortingLayerFields;

		public EditorWindow inspectorWindow;

		HashSet<Component> minimizedComponents= new HashSet<Component>();

		public bool selectionLocked;
		public List<GameObject> selection= new List<GameObject>(); //only scene's gameobjects will end up here. If they are mixed, it will be empty
		enum ViewModeEnum {
			UnityInspectorReflection,
			SceneInfo,
			GameObjectInfo
		}

		ViewModeEnum viewMode;

		//--------------------------------
		protected override void InitializePanel() {
			defaultTitle= "Auditor";
			defaultIcon= EditorGUIUtility.IconContent("console.infoicon.sml").image;

			hasScrollingH= false;
			hasScrollingV= true;
			hideToolbar= true;

			//Works as of Unity 2019.3.5
			var tSortingLayerEditorUtility= typeof(Editor).Assembly.GetType("UnityEditor.SortingLayerEditorUtility");
			if(tSortingLayerEditorUtility == null) {
				Debug.LogWarning("Pantheon could not find UnityEditor.SortingLayerEditorUtility");
			}else{ 
				var m= tSortingLayerEditorUtility.GetMethod(
					"RenderSortingLayerFields",
					BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic,
					null,
					CallingConventions.Any,
					new Type[] { typeof(SerializedProperty),  typeof(SerializedProperty) },
					null
				);

				if(m == null) {
					Debug.LogWarning("Pantheon could not find UnityEditor.SortingLayerEditorUtility.RenderSortingLayerFields");
				}else{
					RenderSortingLayerFields= (sortingOrder, sortingLayer)=> {
						m.Invoke(null, new [] { sortingOrder, sortingLayer } );
					};
				}
			}

			RefreshSelection();
		}

		void OnFocus() {
			//inspectorWindow.Focus();
		}

		new void OnDestroy() {
			base.OnDestroy();
			//inspectorWindow.Close();
		}

		void OnSelectionChange() {
			RefreshSelection();
		}

		void RefreshSelection() {
			var activeSelection= Selection.gameObjects;
			var sceneObjects= Manager.AllSceneObjects;
			var sceneObjectsSet= new HashSet<GameObject>(sceneObjects); 

			selection.Clear();

			if(activeSelection.Length == 0) {
				viewMode= ViewModeEnum.SceneInfo;
			}else {
				viewMode= ViewModeEnum.GameObjectInfo;

				foreach(var obj in activeSelection) {
					if(!sceneObjectsSet.Contains(obj)) {
						viewMode= ViewModeEnum.UnityInspectorReflection;
						break;
					}
				}

				if(viewMode == ViewModeEnum.GameObjectInfo) {
					selection.AddRange(activeSelection);
				}
			}

			Refresh();
		}

		protected override void WindowUpdate() {
			refreshHiddenObjects= true;

			var position= this.position;
			position.yMin += 50;
			position.x -= 20;
			//inspectorWindow.position= position;
		}


		bool refreshHiddenObjects= true;
		List<GameObject> hiddenObjects_;
		List<GameObject> hiddenObjects {
			get {
				if(hiddenObjects_ == null) {
					hiddenObjects_= new List<GameObject>();
					refreshHiddenObjects= true;
				}

				if(refreshHiddenObjects) {
					refreshHiddenObjects= false;
					hiddenObjects_.Clear();

					foreach(var obj in Manager.AllSceneObjects) {
						if((obj.transform.hideFlags & HideFlags.HideInHierarchy) != 0) {
							hiddenObjects_.Add(obj as GameObject);
						}
					}
				}

				return hiddenObjects_;
			}
		}

		//--------------------------------
		protected override Rect DrawContents(Rect rect) {
			switch(viewMode) {
				case ViewModeEnum.GameObjectInfo: DrawObjectView(); break;
				case ViewModeEnum.SceneInfo: DrawSceneInfo(); break;
				case ViewModeEnum.UnityInspectorReflection: DrawStandardView(); break;
				default: throw new NotImplementedException();
			}

			var endRect= EditorGUILayout.GetControlRect(false, 0);
			return new Rect(rect.xMin, rect.yMin, rect.width, endRect.yMax);
		}

		//--------------------------------
		void DrawStandardView() {

		}

		//--------------------------------
		public bool expandComponentCounts;
		public bool expandHiddenObjects;
		struct Null { } //Used only to simplify code which lists types

		void DrawSceneInfo() {
			GUILayout.BeginVertical(EditorStyles.inspectorDefaultMargins);

			int sceneCount= SceneManager.sceneCount;
			GUILayout.Label("Loaded scene count: " + sceneCount, Styles.text);
			GUILayout.Label("Total GameObject count: " + Manager.AllSceneObjects.Count, Styles.text);

			if(hiddenObjects.Count == 0) {
				GUILayout.Label("No hidden objects in the active scene" + ((sceneCount > 1) ? "s" : ""), Styles.text);
			}
			GUILayout.EndVertical();

			if(hiddenObjects.Count > 0) {
				expandHiddenObjects= Styles.DrawFoldoutHeader(expandHiddenObjects, new GUIContent("Hidden objects: " + hiddenObjects.Count));

				if(expandHiddenObjects) {
					GUILayout.BeginVertical(EditorStyles.inspectorDefaultMargins);
					foreach(var obj in hiddenObjects) {
						if(GUILayout.Button(obj.FullName(), Styles.text)) {
							Selection.activeGameObject= obj;
						}
					}
					GUILayout.EndVertical();
				}
			}

			expandComponentCounts= Styles.DrawFoldoutHeader(expandComponentCounts, new GUIContent("Components summary"));
			if(expandComponentCounts) {
				var count= new Dictionary<Type, int>();
				foreach(var obj in Manager.AllSceneObjects) {
					foreach(var component in obj.GetComponents<Component>()) {
						Type t= typeof(Null);
						if(component != null) {
							t= component.GetType();
						}

						int tcount= 0;
						count.TryGetValue(t, out tcount); 
						count[t]= ++tcount;
					}
				}

				var list= new List<Type>(count.Keys);
				list.Sort((a, b)=> string.Compare(a.Name, b.Name, true));

				GUILayout.BeginVertical(EditorStyles.inspectorDefaultMargins);
				foreach(var t in list) {
					GUILayout.Label(count[t] + "x " + t.Name, Styles.text);
				}
				GUILayout.EndVertical();
			}
		}

		//--------------------------------
		void DrawObjectView() {
			var obj= selection[0];
			var components= obj.GetComponents(typeof(Component));

			GUILayout.BeginVertical(Styles.noPadding);
			{
				var serializedObject= new SerializedObject(obj);

				var title= EditorGUIUtility.ObjectContent(obj, typeof(GameObject));
				title.text += "\t(" + (components.Length - 1) + " component" +((components.Length > 2) ? "s" : "") + ")";
				GUILayout.Label(title, GUILayout.Height(26));
			}

			GUILayout.Space(20);
			for(int i= 0; i<components.Length; i++) {
				var c= components[i];
				if(c == null) {

				}else{
					var serializedObject= new SerializedObject(c);
					bool expanded= !minimizedComponents.Contains(c);

					var cTitle= new GUIContent(
						ObjectNames.NicifyVariableName(c.GetType().Name),
						EditorGUIUtility.ObjectContent(c, typeof(Component)).image
					);

					if(expanded != Styles.DrawFoldoutHeader(expanded, cTitle, (c.hideFlags & HideFlags.HideInInspector) == 0)) {
						expanded= !expanded;
						if(expanded) minimizedComponents.Remove(c);
						else minimizedComponents.Add(c);
					}

					if(expanded) {
						GUILayout.BeginVertical(EditorStyles.inspectorDefaultMargins);
						var hideFlags= serializedObject.FindProperty("m_ObjectHideFlags");
						hideFlags.intValue= (int)(HideFlags)EditorGUILayout.EnumFlagsField(new GUIContent("hideFlags"), (HideFlags)hideFlags.intValue);

						if(c is Renderer) {
							var r= c as Renderer;

							var m_SortingOrder = serializedObject.FindProperty("m_SortingOrder");
							var m_SortingLayerID = serializedObject.FindProperty("m_SortingLayerID");
							RenderSortingLayerFields(m_SortingOrder, m_SortingLayerID);
						}

						GUILayout.EndVertical();
					}
					serializedObject.ApplyModifiedProperties();
				}
			}

			Styles.DrawHorizontalLine();
			GUILayout.EndVertical();
		}
	}
}