using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.Text.RegularExpressions;
using UnityEditor;
using UnityEngine;

using ReorderableList = UnityEditorInternal.ReorderableList;

namespace Pantheon {
	[InitializeOnLoad]
	public class ScriptDefinesPanel:
		VulcanWindow
	{
		[ConditionalMenuItem("Tools/Pantheon/Define Script Symbols", ToolsMenu.MenuPriority)]
		[ConditionalMenuItem("Window/General/Define Script Symbols", ToolsMenu.GeneralMenuPriority)]
		public static void Create() {
			var instance= VulcanWindow.GetWindow<ScriptDefinesPanel>();
			instance.Focus();			
		}

		[Serializable]
		class Item {
			public Item(string str) {
				this.str= str;
			}

			public string str {
				get {	return (enabled ? "" : "__disabled_") + name; }
				set { 
					if(value.StartsWith("__disabled_")) {
						enabled= false;
						name= value.Substring("__disabled_".Length);
					}else{
						enabled= true;
						name= value;
					}
				}
			}

			public bool enabled;
			public string name;
		}

		public override bool isDynamic { get { return true; } }

		ReorderableList listWidget;

		BuildTargetGroup targetGroup;
		List<Item> values;
		bool dirtyValues= false;

		string[] availableGroups;

		static ScriptDefinesPanel() {
			//EditorManager.RegisterClass<ScriptDefinesPanel>();
		}

		//--------------------------------
		void LoadValues() {
			var availableGroups= new HashSet<string>();

			foreach(var e in Enum.GetValues(typeof(BuildTarget))) {
				var target= (BuildTarget)e;
				var group= BuildPipeline.GetBuildTargetGroup(target);

				Func<BuildTargetGroup, BuildTarget, bool> function;
				#if UNITY_2018_1_OR_NEWER
				function= BuildPipeline.IsBuildTargetSupported;
				#else
				//The function is the same in 2017, just not public
				var IsBuildTargetSupported= typeof(BuildPipeline).GetMethod("IsBuildTargetSupported", BindingFlags.Static | BindingFlags.NonPublic);
				function= (a, b) => { return (bool)IsBuildTargetSupported.Invoke(null, new object[] { a, b }); };
				#endif

				if(function(group, target)) {
					availableGroups.Add(group.ToString());
				}
			}
			var availableGroups_list= new List<string>(availableGroups);
			availableGroups_list.Sort();
			this.availableGroups= availableGroups_list.ToArray();

			//----
			values= new List<Item>();

			foreach(var str in PlayerSettings.GetScriptingDefineSymbolsForGroup(targetGroup).Split(';')) {
				if(string.IsNullOrEmpty(str)) continue;
				values.Add(new Item(str));
			}

			//----
			listWidget= new ReorderableList(values, typeof(Item));
			listWidget.onAddCallback += (list) => { values.Add(new Item("")); } ;
			listWidget.onRemoveCallback += (list) => { values.RemoveAt(list.index); } ;
			listWidget.headerHeight= 0;

			listWidget.drawElementCallback += (Rect rect, int index, bool active, bool focused) => {
				var item= values[index];
				rect.xMin += 2;
				rect.yMin += 2;
				rect.yMax -= 2;

				Rect toggleRect= new Rect(rect.xMin, rect.y, 15, rect.height);
				item.enabled= GUI.Toggle(toggleRect, item.enabled, "");

				rect.xMin= toggleRect.xMax + 4;
				if(!Regex.Match(item.name, "^[A-Za-z_][A-Za-z0-9_]+$").Success) {
					//Invalid name
					var warningRect= new Rect(rect.xMin - 4, rect.y, 20, rect.height);
					GUI.Label(
						warningRect,
						new GUIContent(
							EditorGUIUtility.IconContent("d_console.warnicon").image,
							"Valid characters for C# symbols are A-z, 0-9, and underscore. No spaces, and the first character must not be a number."
						),
						GUIStyle.none
					);
					rect.xMin= warningRect.xMax;
				}
				item.name= EditorGUI.TextField(rect, item.name);
			};

			dirtyValues= false;
		}

		void SaveValues() {
			string str= "";
			foreach(var value in values) {
				if(string.IsNullOrEmpty(value.name)) continue;

				str += value.str + "; ";
			}

			PlayerSettings.SetScriptingDefineSymbolsForGroup(targetGroup, str);
			dirtyValues= false;
		}

		void AskToSave() {
			if(!dirtyValues) return;

			if(EditorUtility.DisplayDialog("Unsaved script defines for " + targetGroup.ToString(), "Save values before continuing?", "Yes", "No")) {
				SaveValues();
			}
		}

		protected override void InitializePanel() {
			defaultTitle= "Script Symbols";
			defaultIcon= EditorGUIUtility.IconContent("cs Script Icon").image;
			hasScrollingH= false;
			hasScrollingV= true;

			targetGroup= BuildPipeline.GetBuildTargetGroup(EditorUserBuildSettings.activeBuildTarget);
			LoadValues();
		}

		protected override void WindowUpdate() {
			
		}

		protected override void OnDestroy() {
			AskToSave();
			base.OnDestroy();
		}

		//--------------------------------
		protected override void DrawToolbar(Rect rect) {
			EditorGUI.BeginDisabledGroup(!dirtyValues);
			if(GUILayout.Button("Apply", EditorStyles.toolbarButton)) {
				SaveValues();
			}
			EditorGUI.EndDisabledGroup();

			if(GUILayout.Button("Reset", EditorStyles.toolbarButton)) {
				LoadValues();
			}

			var targetGroup= this.targetGroup;
			int choice= 0;
			for(int i= 0; i < availableGroups.Length; i++) {
				if(targetGroup.ToString() == availableGroups[i]) {
					choice= i;
					break;
				}
			}
			choice= EditorGUILayout.Popup(choice, availableGroups);
			targetGroup= (BuildTargetGroup)Enum.Parse(typeof(BuildTargetGroup), availableGroups[choice]);
			if(targetGroup != this.targetGroup) {
				AskToSave();
				this.targetGroup= targetGroup;
				LoadValues();
			}

			base.DrawToolbar(rect);
		}

		protected override Rect DrawContents(Rect rect) {
			float contentHeight= listWidget.GetHeight();
			if(contentHeight > rect.height) rect.width -= 13;

			EditorGUI.BeginChangeCheck();

			listWidget.DoList(rect);
			GUILayout.Space(listWidget.GetHeight());

			if(GUI.changed) {
				dirtyValues= true;
			}
			EditorGUI.EndChangeCheck();
			return rect;
		}
	}
}