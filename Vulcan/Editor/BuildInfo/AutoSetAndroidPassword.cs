﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

using UnityEditor;
using UnityEditor.Build;
using UnityEngine;
using UnityEngine.SceneManagement;

#if UNITY_2018_1_OR_NEWER
using UnityEditor.Build.Reporting;
#endif

namespace Pantheon {
	[InitializeOnLoad]
	public class AutoSetAndroidPassword {
		static BuildInfoSettings settings { get { return BuildInfoSettings.instance; } }

		static string saved_password_keystore {
			get { return (settings.password_Android_keystore == null) ? "" : settings.password_Android_keystore; }
			set { settings.password_Android_keystore= (value == null) ? "" : value; }
		}

		static string active_password_keystore {
			get { return (PlayerSettings.keystorePass == null) ? "" : PlayerSettings.keystorePass; }
			set { PlayerSettings.keystorePass= value; }
		}

		static string saved_password_alias {
			get { return (settings.password_Android_alias == null) ? "" : settings.password_Android_alias; }
			set { settings.password_Android_alias= (value == null) ? "" : value; }
		}

		static string active_password_alias {
			get { return (PlayerSettings.keyaliasPass == null) ? "" : PlayerSettings.keyaliasPass; }
			set { PlayerSettings.keyaliasPass= value; }
		}

		static bool passwordsMatch {
			get {
				if(active_password_keystore != saved_password_keystore) return false;
				if(active_password_alias != saved_password_alias) return false;
				return true;
			}
		}

		[MenuItem("Tools/Pantheon/Advanced/Show android passwords", false, ToolsMenu.MenuPriority + 1000)]
		static void ShowPasswords() {
			string text= "Current Unity keystore password: " + active_password_keystore + "\n" +
				"Current Unity key alias password: " + active_password_alias + "\n" +
				"Cached keystore password: " + saved_password_keystore + "\n" +
				"Cached key alias password: " + saved_password_alias;

			if(EditorUtility.DisplayDialog(
				"Android build passwords",
				text,
				"Copy to clipboard",
				"Close"
			)) {
				EditorGUIUtility.systemCopyBuffer= text;
			}
		}

		static void OnBuild() {
			#if UNITY_ANDROID
			if(!settings.autoPassword_Android) {
				if((active_password_keystore != "") && !settings.autoPassword_PromptedUserOnce) {
					settings.autoPassword_PromptedUserOnce= true;

					if(EditorUtility.DisplayDialog(
						"Pantheon password manager",
						"Do you want Pantheon to start saving the password for your android builds?"+
						"\n\nAnytime you build with a new password, it will automatically replace the saved version. When Unity starts, your most recent password will be restored."+
						"\n\nThis dialog will not appear again, but you can change the option any time in the Pantheon setting panel."+
						"\n\nWarning: Passwords are serialized in plain text, in case you're security concious.",
						"Yes",
						"No"
					)) {
						EditorManager.Log("Pantheon will start saving the password for Android builds", true);
						settings.autoPassword_Android= true;
						SavePassword();
					}else{
						return;
					}
				}
			}
			#endif
		}

		static AutoSetAndroidPassword() {
			Manager.editorRunOnce += ()=> {
				if(EditorManager.isFirstEditorInstance) {
					FillPassword();
				}
			};

			Manager.editorUpdate += ()=> {
				SavePassword();
			};
		}

		#if UNITY_2018_1_OR_NEWER
		class MyCustomBuildProcessor:
			IProcessSceneWithReport
		{
      public int callbackOrder { get { return 0; } }

			public void OnProcessScene(Scene scene, BuildReport report) {
				OnBuild();
			}
		}
		#else
		class MyCustomBuildProcessor:
			IPreprocessBuild
		{
      public int callbackOrder { get { return 0; } }
			
			public void OnPreprocessBuild(BuildTarget target, string path) {
				OnBuild();
			}
		}
		#endif

		static void FillPassword() {
			if(!settings.autoPassword_Android) return;

			int state= 0;

			if((saved_password_keystore != "") && (active_password_keystore == "")) {
				active_password_keystore= saved_password_keystore;
				state |= 1;
			}

			if((saved_password_alias != "") && (active_password_alias == "")) {
				active_password_alias= saved_password_alias;
				state |= 2;
			}

			switch(state) {
				case 0: break;
				case 1:	EditorManager.Log("Pantheon automatically filled in the Android keystore password", true); break;
				case 2:	EditorManager.Log("Pantheon automatically filled in the Android key alias password", true); break;
				case 3:	EditorManager.Log("Pantheon automatically filled in the Android build passwords", true); break;
			}
		}

		static void SavePassword() {
			if(!settings.autoPassword_Android) {
				if((saved_password_keystore != "") || (saved_password_alias != "")) {
					saved_password_keystore= "";
					saved_password_alias= "";
					settings.SetDirty();
					EditorManager.Log("Pantheon has cleared the passwords it was saving for Android builds", true);
				}
			}else if(!passwordsMatch) {
				saved_password_keystore= active_password_keystore;
				saved_password_alias= active_password_alias;
				settings.SetDirty();
			}
		}
	}
}
