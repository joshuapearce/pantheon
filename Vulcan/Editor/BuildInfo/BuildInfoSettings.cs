﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

using UnityEditor;
using UnityEngine;

namespace Pantheon {
	[InitializeOnLoad]
	public class BuildInfoSettings:
		ModuleSettings
	{
		static BuildInfoSettings instance_;
		[SelfInitializing] public static BuildInfoSettings instance {
			get {
				if(instance_ == null) instance_= ModuleSettings.LoadOrCreate<BuildInfoSettings>("BuildInfo.settings");
				return instance_;
			}
		}

		#if UNITY_2018_3_OR_NEWER
		[SettingsProvider]
    private static SettingsProvider InitializeSettingsProvider_static() {
			return instance.InitializeSettingsProvider();
		}
		#endif


		new void OnEnable() {
			base.OnEnable();
			instance_= this;
			displayName= "Automatic Build Info";
			helpFile= "BuildInfo.txt";
		}

		[SettingsField("Autoincrement Patch Version", "This will turn X.Y.1 to X.Y.2 or X.Y.99 to X.Y.100, etc every time a build is made", SettingsFieldAttribute.Flags.Default, 100)]
		public bool autoincrementPatchVersion;

		[SettingsField("Autoincrement Android Build Number", "Add one to the build number every time an Android build is made", SettingsFieldAttribute.Flags.Default, 200)]
		public bool autoincrementAndroidBuildNumber;

		[SettingsField("Autoincrement iOS Build Number", "Add one to the build number every time an iOS build is made", SettingsFieldAttribute.Flags.Default, 300)]
		public bool autoincrementIOSBuildNumber;

		public override string ConfigHeaderInfo() {
			return "These options (and more) can be manually triggered using the Tools menu\n"+
			"\nCurrent version string: " + Application.version +
			"\nCurrent Android Bundle Number: " + PlayerSettings.Android.bundleVersionCode +
			"\nCurrent iOS Build Number: " + PlayerSettings.iOS.buildNumber
			;
		}

		[SettingsField(
			"Save Android Build Password",
			"Automatically fills in the android password for you when Unity starts\nWarning: This is saved in plain text",
			SettingsFieldAttribute.Flags.Default,
		320)]
		public bool autoPassword_Android;
		public bool autoPassword_PromptedUserOnce;

		[HideInInspector] public string password_Android_keystore;
		[HideInInspector] public string password_Android_alias;
	}
}