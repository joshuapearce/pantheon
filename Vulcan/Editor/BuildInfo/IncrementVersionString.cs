﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;

namespace Pantheon {
	[InitializeOnLoad]
	public class IncrementVersionString {
		static BuildInfoSettings settings { get { return BuildInfoSettings.instance; } }

		static IncrementVersionString() {
			EditorManager.RegisterModule("Increment VersionString");
		}

		[PostProcessBuildAttribute(1)]
		public static void OnPostprocessBuild(BuildTarget target, string pathToBuiltProject) {
			if(settings.autoincrementPatchVersion) {
				if(!Process(2, true)) {
					Debug.LogWarning("Could not automatically increment the version string.\nTry using the menu item [Tools/Version String/Increment Patch Version] for more details");
				}
			}
		}

		[MenuItem("Tools/Pantheon/Version String/Increment Patch Version", false, ToolsMenu.MenuPriority + 41)]
		public static void IncrementPatchVersion() {
			Process(2);
		}

		[MenuItem("Tools/Pantheon/Version String/Increment Minor Version", false, ToolsMenu.MenuPriority + 42)]
		public static void IncrementMinorVersion() {
			Process(1);
		}

		[MenuItem("Tools/Pantheon/Version String/Increment Major Version", false, ToolsMenu.MenuPriority + 43)]
		public static void IncrementMajorVersion() {
			Process(0);
		}

		static bool Process(int segment, bool silent= false) {
			var sections= new List<string>(Application.version.Split('.'));

			if(sections.Count > 3) {
				if(silent || !EditorUtility.DisplayDialog("Incrementing version number",
					"The string '" + Application.version + "' has more than three sections separated by a '.'\nOK to remove the extras?",
					"Yes",
					"Don't change anything"
				)) {
					return false;
				}

				sections.RemoveRange(3, sections.Count - 3);
			}

			while(sections.Count - 1 < segment) {
				sections.Add("0");
			}

			for(int i= segment; i < sections.Count; i++) {
				var str= sections[i].Trim('0');
				if(str == "") str= "0";
				int v;

				if(!int.TryParse(str, out v) || (v.ToString() != str)) {
					Debug.LogWarning("Segment #" + i + ": " + str);
					if(silent || !EditorUtility.DisplayDialog("Incrementing version number",
						"The substring '" + sections[i] + "' contains text or is not a valid number\nOK to clean this?",
						"Yes",
						"Don't change anything"
					)) {
						return false;
					}
				}
			
				if(i == segment) {
					sections[i]= (v+1).ToString();
				}else{
					sections[i]= "0";
				}
			}

			string output= "";
			for(int i= 0; i<3; i++) {
				if(i>0) output += '.';
				output += sections[i];
			}

			if(Regex.Matches(output, "[^0-9\\.]").Count > 0) {
				Debug.LogWarning("The current version string won't be valid on all platforms. Try to stick to patterns like 123 or #.# or #.#.#, and don't add any text");
			}


			Debug.Log("New version string: " + output + "\nDon't forget to increase the build numbers before publishing");
			PlayerSettings.bundleVersion= output;

			return true;
		}

	}
}