﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;

namespace  Pantheon {
	[InitializeOnLoad]
	public class AutoIncrementBuildNumber {
		static BuildInfoSettings settings { get { return BuildInfoSettings.instance; } }

		static AutoIncrementBuildNumber() {
			EditorManager.RegisterModule("AutoIncrement BuildNumber");
		}

		[PostProcessBuildAttribute(1)]
		public static void OnPostprocessBuild(BuildTarget target, string pathToBuiltProject) {
			switch(target) {
				case BuildTarget.Android:	if(settings.autoincrementAndroidBuildNumber) IncrementAndroid(); break;
				case BuildTarget.iOS: if(settings.autoincrementIOSBuildNumber) IncrementiOS(); break;
			}
		}

		[MenuItem("Tools/Pantheon/Build Number/Increment Current", true)]
		static bool IncrementCurrent_Validator() {
			#if UNITY_ANDROID
				return true;
			#elif UNITY_IOS
				return true;
			#else
				return false;
			#endif
		}

		[MenuItem("Tools/Pantheon/Build Number/Increment Current", false, ToolsMenu.MenuPriority + 130)]
		public static bool IncrementCurrent() {
			#if UNITY_ANDROID
				return IncrementAndroid();

			#elif UNITY_IOS
				return IncrementiOS();

			#else
				Debug.LogWarning("Current platform not supported for AutoIncrementBuildNumber");
				return false;
			#endif
		}

		[MenuItem("Tools/Pantheon/Build Number/Increment All", false, ToolsMenu.MenuPriority + 40)]
		public static void IncrementAll() {
			IncrementAndroid();
			IncrementiOS();
		}

		[MenuItem("Tools/Pantheon/Build Number/Increment Android", false, ToolsMenu.MenuPriority + 41)]
		public static bool IncrementAndroid() {
			PlayerSettings.Android.bundleVersionCode++;
			Debug.Log("New android bundle version: " + PlayerSettings.Android.bundleVersionCode);

			if(PlayerSettings.Android.bundleVersionCode >= 100000) {
				Debug.LogWarning("Android build numbers over 100,000 can cause issues");
			}

			return true;
		}
	
		[MenuItem("Tools/Pantheon/Build Number/Increment iOS", false, ToolsMenu.MenuPriority + 41)]
		public static bool IncrementiOS() {
			if(string.IsNullOrEmpty(PlayerSettings.iOS.buildNumber)) {
				PlayerSettings.iOS.buildNumber= "1";
			}else{
				var match= Regex.Match(PlayerSettings.iOS.buildNumber, "^(.*[^0-9]?)([0-9]+)$");

				if(match.Groups[2].Value == "") {
					Debug.LogWarning("iOS build number cannot be auto incremented because the current value is not an integer, and also does not end with an integer");
					return false;
				}

				PlayerSettings.iOS.buildNumber= match.Groups[1].Value + (int.Parse(match.Groups[2].Value) + 1);
			}

			Debug.Log("New iOS buildNumber: " + PlayerSettings.iOS.buildNumber);
			return true;
		}
	}
}