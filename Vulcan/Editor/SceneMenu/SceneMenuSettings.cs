using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

using UnityEditor;
using UnityEngine;

using ReorderableList= UnityEditorInternal.ReorderableList;


namespace Pantheon {
	[InitializeOnLoad]
	public class SceneMenuSettings:
		ModuleSettings
	{
		static SceneMenuSettings instance_;
		[SelfInitializing] public static SceneMenuSettings instance {
			get {
				if(instance_ == null) instance_= ModuleSettings.LoadOrCreate<SceneMenuSettings>("SceneMenuSettings.settings");
				return instance_;
			}
		}

		#if UNITY_2018_3_OR_NEWER
		[SettingsProvider]
    private static SettingsProvider InitializeSettingsProvider_static() {
			return instance.InitializeSettingsProvider();
		}
		#endif
		
		//--------------------------------
		[SettingsField("Opening Menu Requires Focus", "The menu will not open unless the scene view was already focused before clicking", SettingsFieldAttribute.Flags.SystemWide, null, 608)]
		public bool RequiresFocus {
			get { return EditorPrefs.GetBool("Pantheon.SceneContextMenu.RequiresFocus", true); }
			set { EditorPrefs.SetBool("Pantheon.SceneContextMenu.RequiresFocus", value); }
		}

		[SettingsField("Opening Menu Requires Ctrl Button", null, SettingsFieldAttribute.Flags.SystemWide, null, 609)]
		public bool RequiresCtrlButton { 
			get { return EditorPrefs.GetBool("Pantheon.SceneContextMenu.RequiresCtrlButton", false); }
			set { EditorPrefs.SetBool("Pantheon.SceneContextMenu.RequiresCtrlButton", value); }
		}

		[SettingsField("Opening Menu Requires Shift Button", null, SettingsFieldAttribute.Flags.SystemWide, null, 609)]
		public bool RequiresShiftButton { 
			get { return EditorPrefs.GetBool("Pantheon.SceneContextMenu.RequiresShiftButton", false); }
			set { EditorPrefs.SetBool("Pantheon.SceneContextMenu.RequiresShiftButton", value); }
		}

		[SettingsField("Opening Menu Requires Alt Button", null, SettingsFieldAttribute.Flags.SystemWide, null, 609)]
		public bool RequiresAltButton { 
			get { return EditorPrefs.GetBool("Pantheon.SceneContextMenu.RequiresAltButton", false); }
			set { EditorPrefs.SetBool("Pantheon.SceneContextMenu.RequiresAltButton", value); }
		}

		[SettingsField("Condense submenus (allow overlap)", "The context menu will take up less space, and require less mouse movement", SettingsFieldAttribute.Flags.SystemWide | SettingsFieldAttribute.Flags.Separator, null, 700)]
		public bool CondenseMenus {
			get { return EditorPrefs.GetBool("Pantheon.SceneContextMenu.CondenseSubmenus", false); }
			set { EditorPrefs.SetBool("Pantheon.SceneContextMenu.CondenseSubmenus", value); }
		}


		[SettingsField("Right click collapses menu", "Right clicking with the context menu open will close one level of it", SettingsFieldAttribute.Flags.SystemWide, null, 800)]
		public bool RightClickToCollapse {
			get { return EditorPrefs.GetBool("Pantheon.SceneContextMenu.RightClickToCollapse", true); }
			set { EditorPrefs.SetBool("Pantheon.SceneContextMenu.RightClickToCollapse", value); }
		}


		//--------------------------------
		new void OnEnable() {
			base.OnEnable();
			instance_= this;
			showInConfigPanel= true;
			displayName= "Scene Context Menu";
		}

		//--------------------------------
		public class MenuReferences {
			static List<Item> items_;
			public static List<Item> items {
				get {
					if(items_ == null) {
						items_= new List<Item>();
						LoadAliases();
					}
					return items_;
				}
			}

			static MenuReferences() {
				EditorManager.onEditorFocus += (state) => {
					if(state) LoadAliases();
					else SaveAliases();
				};
			}

			[Serializable]
			public class Item {
				public bool topOfList;
				public string systemAlias;
				public string path;

				public void ReadFromEditorPrefs(int index) {
					topOfList= EditorPrefs.GetBool("Pantheon.SceneContextMenu.Aliases."+index +".topOfList", false);
					systemAlias= EditorPrefs.GetString("Pantheon.SceneContextMenu.Aliases."+index +".systemAlias", "");
					path= EditorPrefs.GetString("Pantheon.SceneContextMenu.Aliases."+index +".path", "");
				}

				public void WriteToEditorPrefs(int index) {
					EditorPrefs.SetBool("Pantheon.SceneContextMenu.Aliases."+index +".topOfList", topOfList);
					EditorPrefs.SetString("Pantheon.SceneContextMenu.Aliases."+index +".systemAlias", systemAlias);
					EditorPrefs.SetString("Pantheon.SceneContextMenu.Aliases."+index +".path", path);
				}

				public static void RemoveEditorPrefs(int index) {
					EditorPrefs.DeleteKey("Pantheon.SceneContextMenu.Aliases."+index +".topOfList");
					EditorPrefs.DeleteKey("Pantheon.SceneContextMenu.Aliases."+index +".systemAlias");
					EditorPrefs.DeleteKey("Pantheon.SceneContextMenu.Aliases."+index +".path");
				}
			}

			static ReorderableList widget_;
			public static ReorderableList widget {
				get {
					if(widget_ == null) {
						widget_= new ReorderableList(instance.menuReferences, typeof(Item));
						widget_.drawHeaderCallback += DrawHeader;
						widget_.onAddCallback += AddItem;
						widget_.onRemoveCallback += RemoveItem;
						widget_.drawElementCallback += DrawElement;
						widget_.elementHeight= 42;
					}
					return widget_;
				}
			}

			private static void DrawHeader(Rect rect) {
				GUI.Label(rect, new GUIContent("Custom menu aliases", "These items will appear in the context menu (These settings are system wide)"));
			}

			private static void DrawElement(Rect rect, int index, bool active, bool focused) {
				var item= items[index];
				//rect= new Rect(0,0,rect.width,rect.height);
				EditorGUI.BeginChangeCheck();

				Rect line1= rect;
				line1.yMin += 2;
				line1.yMax -= rect.height / 2 + 2;
				Rect line2= rect;
				line2.yMin= line1.yMax + 4;
				line2.yMax -= 2;

				GUI.Label(line1, new GUIContent("System menu path", "Example: File/New Scene"));
				line1.xMin += 120;
				line1.xMax -= 50;
				items[index].systemAlias= EditorGUI.DelayedTextField(line1, item.systemAlias);
				line1.xMin= line1.xMax + 4;
				line1.xMax= rect.xMax;
				if(GUI.Button(line1, new GUIContent("Test", "Executes the alias you've entered"))) {
					EditorApplication.ExecuteMenuItem(item.systemAlias);
				}

				GUI.Label(line2, new GUIContent("Context menu path", "Leave blank to use the last part of the alias"));
				line2.xMin += 120;
				line2.xMax -= 50;
				items[index].path= EditorGUI.DelayedTextField(line2, item.path);

				line2.xMin= line2.xMax + 4;
				line2.xMax= rect.xMax;
				item.topOfList= GUI.Toggle(line2, item.topOfList, "Top");

				//GUILayout.EndArea();

				if(EditorGUI.EndChangeCheck()) {
          SaveAliases();
        }
			}

			private static void AddItem(ReorderableList list) {
				instance.menuReferences.Add(new MenuReferences.Item());
        SaveAliases();
			}
 
			private static void RemoveItem(ReorderableList list) {
				instance.menuReferences.RemoveAt(list.index);
				SaveAliases();
			}

			public static void SaveAliases() {
				for(int i= EditorPrefs.GetInt("Pantheon.SceneContextMenu.Aliases.Count", 0); i>=items.Count; i--) {
					Item.RemoveEditorPrefs(i);
				}

				EditorPrefs.SetInt("Pantheon.SceneContextMenu.Aliases.Count", items.Count);

				for(int i= 0; i<items.Count; i++) {
					items[i].WriteToEditorPrefs(i);
				}
			}

			public static void LoadAliases() {
				items.Clear();

				if(!EditorPrefs.HasKey("Pantheon.SceneContextMenu.Aliases.Countz")) {
					var item= new Item();
					item.systemAlias= "Tools/Pantheon/Scene Context Menu/Settings";
					item.path= "Pantheon Settings";
					items.Add(item);

					SaveAliases();

				}else{
					int count= EditorPrefs.GetInt("Pantheon.SceneContextMenu.Aliases.Count", 0);
					Debug.Log(count);

					for(int i= 0; i < items.Count; i++) {
						var item= new Item();
						item.ReadFromEditorPrefs(i);
						items.Add(item);
					}
				}
			}
		}
		public List<MenuReferences.Item> menuReferences { get { return MenuReferences.items; } }

		public override string ConfigHeaderInfo() {
			string str= "You can add custom options to the scene context menu in this panel, or by using the [SceneMenuItem] attribute in your code";

			if(!SceneMenu.enabled) str= "This module is disabled by your settings!\n\n" + str;

			return str;
		}

		public override void DrawContents(float contentWidth) {
			base.DrawContents(contentWidth);
			GUILayout.BeginVertical(defaultMargins);

			GUILayout.Space(10);
			MenuReferences.widget.DoLayoutList();

			GUILayout.EndVertical();
		}
	}
}