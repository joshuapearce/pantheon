﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

using UnityEditor;
using UnityEngine;

namespace Pantheon {
	public partial class SceneMenuItem {
		public enum ValidateResult {
			Void,
			Hidden,
			Disabled,
			Valid,
			ValidAndMarked
		}
	}

	[System.AttributeUsage(AttributeTargets.Method)]
	public class SceneMenuItemAttribute:
		Attribute
	{
		public string name;
		public bool isValidateFunction;
		public int priority;

		public SceneMenuItemAttribute() {

		}

		public SceneMenuItemAttribute(string name, bool isValidateFunction= false, int priority=0) {
			this.name= name;
			this.isValidateFunction= isValidateFunction;
			this.priority= priority;
		}

		public SceneMenuItem.ValidateResult Validate(MethodInfo method, SceneMenuItem.Arguments arguments) {
			if(!isValidateFunction) throw new InvalidOperationException();
			if(!method.IsStatic) throw new InvalidProgramException("SceneMenuItemAttribute validation functions must be static");

			if(method.ReturnType == typeof(SceneMenuItem.ValidateResult)) {
				return (SceneMenuItem.ValidateResult)method.Invoke(null, CreateArguments(method, arguments));

			}else if(method.ReturnType == typeof(bool)) {
				return (bool)method.Invoke(null, CreateArguments(method, arguments)) ? SceneMenuItem.ValidateResult.Valid : SceneMenuItem.ValidateResult.Disabled;

			}else{
				throw new InvalidProgramException("SceneMenuItemAttribute validation return type must be bool, or SceneMenuItem.ValidateResult");
			}
		}

		public bool isCreator(MethodInfo method) {
			if(isValidateFunction) return false;
			if(!method.IsStatic) return false;

			if(method.ReturnType == typeof(SceneMenuItem)) return true;
			if(method.ReturnType == typeof(IEnumerable<SceneMenuItem>)) return true;

			return false;
		}

		public IEnumerable<SceneMenuItem> Create(MethodInfo method, SceneMenuItem.Arguments arguments) {
			if(method.ReturnType == typeof(SceneMenuItem)) {
				var item= method.Invoke(null, CreateArguments(method, arguments)) as SceneMenuItem;
				if(item != null) {
					if(string.IsNullOrEmpty(item.path)) item.path= name;
					yield return item;
				}

			}else if(method.ReturnType == typeof(IEnumerable<SceneMenuItem>)) {
				foreach(var item in method.Invoke(null, CreateArguments(method, arguments)) as IEnumerable<SceneMenuItem>) {
					yield return item;
				}

			}else{
				throw new InvalidCastException("SceneMenuItem creator functions must return SceneMenuItem or IEnumerable<SceneMenuItem>");
			}
		}

		public SceneMenuItem Invoke(MethodInfo method, SceneMenuItem.Arguments arguments) {
			if(isValidateFunction) throw new InvalidOperationException();
			if(!method.IsStatic) throw new InvalidProgramException("SceneMenuItemAttribute functions must be static");

			return method.Invoke(null, CreateArguments(method, arguments)) as SceneMenuItem;
		}

		private static object[] CreateArguments(MethodInfo method, SceneMenuItem.Arguments arguments) {
			switch(method.GetParameters().Length) {
				case 0: return null;
				case 1: return new object[] { arguments };
				default: throw new InvalidCastException("SceneMenuItemAttribute functions only support 0 or 1 arguments");
			}
		}
	}
}