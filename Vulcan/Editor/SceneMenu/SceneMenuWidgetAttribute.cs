﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

using UnityEditor;
using UnityEngine;

#pragma warning disable 0414

namespace Pantheon {
	[AttributeUsage(AttributeTargets.Method, AllowMultiple= true)]
	public class SceneMenuWidgetAttribute:
		Attribute,
		SceneMenuWidgetInterface
	{
		static GUIStyle style= null;

		SceneMenuItem.Arguments arguments;

		string texturePath;
		Texture texture;
		string tooltip;
		Type componentType;
		bool closeMenuOnUse;

		public int rowID {get; set;}
		public SceneMenuItem.ValidateResult state { get; set; }

		GUIContent content;

		public MethodInfo method; //initialized by SceneMenu

		public SceneMenuWidgetAttribute(string textureAsset, Type componentType= null, bool closeMenuOnUse=true, int rowID= 0) {
			this.componentType= componentType;
			this.texturePath= textureAsset;
			this.closeMenuOnUse= closeMenuOnUse;
			this.rowID= rowID;

			if(!textureAsset.StartsWith("Assets/", true, System.Globalization.CultureInfo.CurrentCulture)) {
				Debug.LogError("Texture path '" + textureAsset + "' for SceneMenuWidgetAttribute must start with 'Assets/'");
			}
		}

		public SceneMenuWidgetAttribute(string textureAsset, string tooltip, Type componentType= null, bool closeMenuOnUse=true, int rowID= 0)
			:this(textureAsset, componentType, closeMenuOnUse, rowID)
		{
			this.tooltip= tooltip;
		}

		public void OnGUI(Rect rect, bool hasFocus) {

			Debug.Log(rect);
			GUILayout.BeginArea(rect);
			rect= new Rect(0,0,rect.width,rect.height);
			EditorGUI.BeginDisabledGroup(!hasFocus || (state == SceneMenuItem.ValidateResult.Disabled));

			if(GUI.Button(rect, new GUIContent(texture, tooltip), style)) {
				OnActivated();
				SceneMenu.Close();
			}

			EditorGUI.EndDisabledGroup();
			GUILayout.EndArea();
		}

		public void OnMenuOpened(SceneMenuItem.Arguments arguments) {
			this.arguments= arguments;
			if(texture == null) {
				texture= AssetDatabase.LoadAssetAtPath<Texture>(texturePath);
				if(texture == null) texture= AssetDatabase.LoadAssetAtPath<Texture>(texturePath + ".png");
				if(texture == null) texture= AssetDatabase.LoadAssetAtPath<Texture>(texturePath + ".jpg");
				if(texture == null) texture= EditorGUIUtility.IconContent("console.warnicon").image;
			}

			if(style == null) {
				style= new GUIStyle(GUIStyle.none);
				style.alignment= TextAnchor.MiddleCenter;
			}

			content= new GUIContent(texture);
			if(!string.IsNullOrEmpty(tooltip)) content.tooltip= tooltip;

			if(componentType != null) {
				if(arguments.selectedGameObjects.Count == 0) {
					state= SceneMenuItem.ValidateResult.Hidden;
					return;
				}

				foreach(var obj in arguments.selectedGameObjects) {
					if(obj.GetComponent(componentType) == null) {
						state= SceneMenuItem.ValidateResult.Disabled;
						return;
					}
				}
			}

			state= SceneMenuItem.ValidateResult.Valid;
		}

		public void OnMenuClosed() {
			content= null;
			texture= null;
		}

		public void OnActivated() {
			method.Invoke(null, new object[] { arguments });
		}
	}
}