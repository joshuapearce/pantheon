﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

using UnityEditor;
using UnityEngine;

namespace Pantheon {
	//Make sure to use SceneMenu.Register to activate an instance of this iterface, otherwise it will do nothing

	public interface SceneMenuWidgetInterface {
		//0 for default row, otherwise any positive or negative number.
		//Position in the row is assigned in order of registration
		int rowID { get; } 
		SceneMenuItem.ValidateResult state { get; } //SceneMenuItem.ValidateResult.Valid is normal

		void OnMenuOpened(SceneMenuItem.Arguments arguments); //can be ignored
		void OnMenuClosed(); //can be ignored

		void OnGUI(Rect rect, bool hasFocus);
	}
}