﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;

using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
//using UnityEngine.UIElements;

namespace Pantheon {
	[InitializeOnLoad]
	public class SceneMenu {
		public delegate IEnumerable<SceneMenuItem> CreateItemsFunction(SceneMenuItem.Arguments arguments);
		public delegate void ActivateItemFunction(SceneMenuItem.Arguments arguments);

		public static SceneMenuSettings settings { get { return SceneMenuSettings.instance; } }

		static bool enabled_;
		public static bool enabled {
			get { return enabled_; }
			private set {
				//Relies on EditorManager.RegisterModule for the state
				enabled_= value;
				UnityEditor.Menu.SetChecked("Tools/Pantheon/Scene Context Menu/Enable", value);
				UnityEditor.Menu.SetChecked("Tools/Pantheon/Scene Context Menu/Disable", !value);
			}
		}

		public static EditorManager.VoidFunction onNextLayout;

		public static void Close() {
			if(SceneMenuGUI.instance != null) {
				onNextLayout += CloseNow;
			}
		}

		private static void CloseNow() {
			foreach(var widget in widgets) widget.OnMenuClosed();
			SceneMenuGUI.instance= null;
		}

		public static List<SceneMenuWidgetInterface> widgets= new List<SceneMenuWidgetInterface>();

		public static void RegisterWidget(SceneMenuWidgetInterface widget) {
			widgets.Add(widget);
			widgets.OrderBy((v)=> v.rowID);
		}

		public static void UnregisterWidget(SceneMenuWidgetInterface widget) {
			widgets.Remove(widget);
		}

		[MenuItem("Tools/Pantheon/Scene Context Menu/Enable", false, ToolsMenu.MenuPriority + 8)]
		static void Menu_Enable() {
			EditorManager.SetModuleMode("Scene Context Menu", EditorManager.ModuleMode.Activated);
		}

		[MenuItem("Tools/Pantheon/Scene Context Menu/Disable", false, ToolsMenu.MenuPriority + 8)]
		static void Menu_Disable() {
			EditorManager.SetModuleMode("Scene Context Menu", EditorManager.ModuleMode.Disabled);
		}

		[MenuItem("Tools/Pantheon/Scene Context Menu/Settings", false, ToolsMenu.MenuPriority + 20)]
		static void OpenSettings() {
			SettingsWindow.Display(settings);
		}		

		static SceneMenu() {
			#if UNITY_2019_1_OR_NEWER
			SceneView.duringSceneGui += OnSceneGUI;
			#else
			SceneView.onSceneGUIDelegate += OnSceneGUI;
			#endif

			EditorManager.RegisterModule("Scene Context Menu", (state) => {
				enabled= state;
			});

			Manager.editorRunOnce += Initialize;
		}

		static void Initialize() {
			MainMenu_GameObject= GetMainMenuContents();
			MainMenu_GameObject.RemoveAll((str)=> !Regex.IsMatch(str, @"^GameObject/.*/.*$"));

			MainMenu_GameObject.Insert(0, "GameObject/Create Empty");
			//MainMenu_GameObject.Insert(1, "GameObject/Create Empty Child");
			MainMenu_GameObject.Insert(1, "GameObject/");
			MainMenu_GameObject.Add("GameObject/Camera");

			attributes_Validators= new Dictionary<string, Tuple<SceneMenuItemAttribute, MethodInfo>>();
			attributes_Creators= new List<Tuple<SceneMenuItemAttribute, MethodInfo>>();
			attributes_Callbacks= new Dictionary<string, Tuple<SceneMenuItemAttribute, MethodInfo>>();

			foreach(var attribinfo in Manager.GetEveryMemberAttribute<SceneMenuItemAttribute>(BindingFlags.Static)) {
				var name= attribinfo.Item1.name;
				var attrib= attribinfo.Item1;
				var method= attribinfo.Item2 as MethodInfo;

				if(attrib.isValidateFunction) attributes_Validators.Add(name, new Tuple<SceneMenuItemAttribute, MethodInfo>(attrib, method));
				else if(attrib.isCreator(method)) attributes_Creators.Add(new Tuple<SceneMenuItemAttribute, MethodInfo>(attrib, method));
				else attributes_Callbacks.Add(name, new Tuple<SceneMenuItemAttribute, MethodInfo>(attrib, method));
			}

			foreach(var info in Manager.GetEveryMemberAttribute<SceneMenuWidgetAttribute>(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static)) {
				var attrib= info.Item1;
				var member= info.Item2;

				attrib.method= (MethodInfo)member;
				RegisterWidget(attrib);
			}
		}


		static bool isDragging { get { return dragMask != 0; } }
		static int dragMask;
		static double clickTime= float.NaN;
		static bool requestMenu;

		public static SceneMenuItem.Arguments arguments;

		static List<string> MainMenu_GameObject;
		static Dictionary<string, Tuple<SceneMenuItemAttribute, MethodInfo>> attributes_Validators;
		static List<Tuple<SceneMenuItemAttribute, MethodInfo>> attributes_Creators;
		static Dictionary<string, Tuple<SceneMenuItemAttribute, MethodInfo>> attributes_Callbacks;

		public static bool spawnAsChildren {
			get { return EditorPrefs.GetBool("Pantheon.SceneContextMenu.spawnAsChildren", true); }
			private set { EditorPrefs.SetBool("Pantheon.SceneContextMenu.spawnAsChildren", value); }
		}

		static bool spawnAtOrigin {
			get { return EditorPrefs.GetBool("Pantheon.SceneContextMenu.spawnAtOrigin", false); }
			set { EditorPrefs.SetBool("Pantheon.SceneContextMenu.spawnAtOrigin", value); }
		}

		static bool spawnFacingCamera {
			get { return EditorPrefs.GetBool("Pantheon.SceneContextMenu.spawnFacingCamera", false); }
			set { EditorPrefs.SetBool("Pantheon.SceneContextMenu.spawnFacingCamera", value); }
		}

    static void OnSceneGUI(SceneView sceneView) {
			if(!enabled) return;

			var e= Event.current;

			bool Activate= false;
			bool wasDragging= isDragging;
			var prevDragMask= dragMask;

			switch(e.type) {
			case EventType.Repaint:
				if(!UnityEditorInternal.InternalEditorUtility.isApplicationActive) {
					CloseNow();

				}else if(EditorWindow.focusedWindow != sceneView) {
					CloseNow();

				}else	if(requestMenu) {
					requestMenu= false;
					foreach(var widget in widgets) {
						widget.OnMenuOpened(arguments);
					}
					SceneMenuGUI.Open(arguments);
					return;
				}
				break;

			case EventType.Layout:
				if(onNextLayout != null) {
					var actions= onNextLayout;
					onNextLayout= null;
					actions.Invoke();
				}
				break;
			}

			if((SceneMenuGUI.instance != null) && (SceneMenuGUI.instance.sceneView == sceneView)) {
				SceneMenuGUI.instance.OnGUI(e);

			}else{
				switch(e.type) {
				default:
					return;

				case EventType.MouseEnterWindow:
					dragMask= 0;
					break;

				case EventType.MouseLeaveWindow:
					dragMask= 0;
					break;

				case EventType.MouseDown:
					//Debug.Log("Down: " + e.button);
					dragMask |= (1 << e.button);
					break;

				case EventType.MouseDrag:
					//Debug.Log("Drag: " + e.button);
					dragMask |= (1 << e.button);
					break;

				case EventType.MouseUp:
					//Debug.Log("Up: " + e.button);
					dragMask &= ~(1 << e.button);
					if(e.button == 1) {
						Activate= true;
					}
					break;

				case EventType.ContextClick:
					//Debug.Log("Context");
					//Activate= true;
					//dragDistance= (drag_Start - e.mousePosition).magnitude;
					//isDragging= false;
					break;
				}

				dragMask &= 2; //workaround
				double dragDuration= EditorApplication.timeSinceStartup - clickTime;
				if(isDragging) {
					if(!wasDragging) {
						clickTime= EditorApplication.timeSinceStartup;
					}
				}else{
					clickTime= float.NaN;
				}

				if(SceneMenu.settings.RequiresFocus) {
					//Activate &= sceneFocusCount > 1;
				}

				if(SceneMenu.settings.RequiresCtrlButton) {
					Activate &= e.control;
				}

				if(SceneMenu.settings.RequiresShiftButton) {
					Activate &= e.shift;
				}

				if(SceneMenu.settings.RequiresAltButton) {
					Activate &= e.alt;
				}
			
				if(Activate) {
					if(!wasDragging || ((prevDragMask == 2) && (dragDuration < 0.2f))) {
						arguments= GenerateArguments();
						//SceneView.RepaintAll();
						//e.Use();
						 
						requestMenu= true;
					}
				}
			}
    }

		//----------------------------------
		static SceneMenuItem.Arguments GenerateArguments() {
			var v= new SceneMenuItem.Arguments();

			v.mousePosition= Event.current.mousePosition;
			v.mouseRay= HandleUtility.GUIPointToWorldRay(v.mousePosition);

			v.sceneView= SceneView.lastActiveSceneView;
			v.selectedGameObjects= new List<GameObject>(Selection.gameObjects);

			var obj= EditorHandles_UnityInternal.PickGameObject(v.mousePosition, false, false);
			v.objectIntersected= obj;

			if(obj != null) {
				var meshFilter= obj.GetComponent<MeshFilter>();
				if(meshFilter != null) {
					RaycastHit hit;
					EditorHandles_UnityInternal.IntersectRayMesh(v.mouseRay, meshFilter, out hit);
					v.sceneIntersection= hit.point;
				}else{
					float dist;
					if(new Plane(obj.transform.forward, obj.transform.position).Raycast(v.mouseRay, out dist)) {
						v.sceneIntersection= v.mouseRay.GetPoint(dist);
					}else{
						v.sceneIntersection= obj.transform.position;
					}
				}
			}else{

			}

			return v;
		}

    static void ShowClassicMenu() {
			if(true.Equals(true)) throw new NotImplementedException("This function has not been kept up to date, and is only here for archival purposes");

			if(!enabled) return;

			//Debug.Log((arguments.objectIntersected == null) ? "none" : arguments.objectIntersected.name);

			requestMenu= false;

			var menuItems= ConstructMenu(arguments);

			GenericMenu menu= new GenericMenu();

			foreach(var item in menuItems) {
				bool Checked= false;
				bool Disabled= false;

				if(item.isSubmenu) continue; //no need to add them directly

				switch(item.state) {
					case SceneMenuItem.ValidateResult.Hidden: continue;
					case SceneMenuItem.ValidateResult.Disabled: Disabled= true; break;
					case SceneMenuItem.ValidateResult.Valid: break;
					case SceneMenuItem.ValidateResult.ValidAndMarked: Checked= true; break;
					default: throw new NotImplementedException(item.state.ToString());
				}

				if(item.hasSeperator) menu.AddSeparator(item.path);

				var content= new GUIContent(item.path);

				if(Disabled) {
					#if UNITY_2018_2_OR_NEWER
					menu.AddDisabledItem(content, Checked);
					#else
					menu.AddDisabledItem(content);
					#endif
				}else{
					menu.AddItem(content, Checked, ()=> { item.function(arguments); });
				}
			}

			#if UNITY_2017_2_OR_NEWER
			//Trying to avoid potential bugs. Remove if it causes issues.
			EditorApplication.QueuePlayerLoopUpdate();
			#endif
			menu.ShowAsContext();
    }

		//----------------
		public static List<SceneMenuItem> ConstructMenu(SceneMenuItem.Arguments arguments) {
			if(MainMenu_GameObject == null) {
				//clicked too soon, probably while code was compiling
				return null;
			}

			var menuItems= new List<SceneMenuItem>();

			foreach(var item in attributes_Creators) {
				var attrib= item.Item1;
				var method= item.Item2;
				menuItems.AddRange(attrib.Create(method, arguments));
			}

			foreach(var item in attributes_Callbacks) {
				var name= item.Key;
				var attrib= item.Value.Item1;
				var method= item.Value.Item2;

				var menuitem= new SceneMenuItem(name);
				menuitem.priority= attrib.priority;
				menuitem.function= (arguments_)=> attrib.Invoke(method, arguments_);
			}

			int counter= 0;
			foreach(var itemname in MainMenu_GameObject) {
				SceneMenuItem item= new SceneMenuItem();
				if(itemname.EndsWith("/")) continue; //submenus are handled later

				item.path= "Spawn " + itemname;
				item.subPriority= counter++;
				item.function= (arguments_)=> {
					Selection.selectionChanged += ObjectSpawnWatcher;
					EditorApplication.ExecuteMenuItem(itemname);
				};
				menuItems.Add(item); 
			}

			foreach(var alias in settings.menuReferences) {
				SceneMenuItem item= new SceneMenuItem();
				string path= alias.path;
				if(string.IsNullOrEmpty(path)) path= alias.systemAlias;

				item.path= path;
				item.state= UnityEditor.Menu.GetChecked(alias.systemAlias) ? SceneMenuItem.ValidateResult.ValidAndMarked : SceneMenuItem.ValidateResult.Valid;
				item.priority= alias.topOfList ? 0 : 1000;

				item.function= (arguments_) => { EditorApplication.ExecuteMenuItem(alias.systemAlias); };
				menuItems.Add(item); 
			}

			//cleaning the list
			menuItems.RemoveAll((v)=> (v == null));

			//Processing item states (enabled, disabled, checked, whatever)
			foreach(var item in menuItems) {
				var state= item.state;

				if(attributes_Validators.ContainsKey(item.path)) {
					var validator= attributes_Validators[item.path];
					var state2= validator.Item1.Validate(validator.Item2, arguments);
					if(state2 != SceneMenuItem.ValidateResult.Void) state= state2;
				}
				if(state == SceneMenuItem.ValidateResult.Void) state= SceneMenuItem.ValidateResult.Valid;

				item.state= state;
			}

			//final clean/sort
			menuItems.RemoveAll((item)=> { return item.state == SceneMenuItem.ValidateResult.Hidden; });

			menuItems.Sort(
				(a, b) => {
					int c= a.priority.CompareTo(b.priority);
					if(c != 0) return c;
					c= a.subPriority.CompareTo(b.subPriority);
					if(c != 0) return c;
					c= string.Compare(a.path, b.path, true);
					return c;
				}
			);

			//inserting placeholders for subdirectories
			var dirs= new Dictionary<string,SceneMenuItem>();
			dirs.Add("", null);
			var menuItems_copy= new List<SceneMenuItem>(menuItems);

			for(int i= 0; i<menuItems_copy.Count; i++) {
				var item= menuItems_copy[i];
				int index= menuItems.IndexOf(item);

				var tempitem= item;
				while(!dirs.ContainsKey(tempitem.dir)) {
					var newitem= new SceneMenuItem(tempitem.dir);
					tempitem.parent= newitem;
					newitem.isSubmenu= true;
					newitem.priority= tempitem.priority;
					newitem.subPriority= tempitem.subPriority;
					newitem.state= SceneMenuItem.ValidateResult.Valid;

					menuItems.Insert(index, newitem);
					dirs.Add(newitem.path, newitem);
					tempitem= newitem;
				}
			}

			//finding what items should have separators, setting parents
			for(int i= 1; i<menuItems.Count; i++) {
				SceneMenuItem prevItem= menuItems[i-1];
				SceneMenuItem item= menuItems[i];

				item.parent= dirs[item.dir];
				if(item.parent != null) {
					item.parent.subItemCount++;
				}

				if(item.priority - prevItem.priority > 10) {
					item.hasSeperator= true;
				}
			}

			foreach(var item in menuItems) {
				if(item.isSubmenu && (item.subItemCount == 0)) {
					item.state= SceneMenuItem.ValidateResult.Disabled;
				}
			}

			return menuItems;
		}

		static void ObjectSpawnWatcher() {
			Selection.selectionChanged -= ObjectSpawnWatcher;
			if(Selection.objects.Length == 1) {
				GameObject obj= Selection.activeGameObject;
				OnObjectSpawnedFromMenu(obj, arguments);
			}
		}

		public static void OnObjectSpawnedFromMenu(GameObject obj, SceneMenuItem.Arguments arguments) {
			if(!enabled) return;

			if(obj != null) {
				if(spawnFacingCamera) {
					obj.transform.rotation= arguments.sceneView.camera.transform.rotation;
				}

				if((arguments.selectedGameObjects.Count == 1) && spawnAsChildren) {
					obj.transform.SetParent(arguments.selectedGameObjects[0].transform, true);
				}

				if(!spawnAtOrigin) {
					obj.transform.position= arguments.bestPosition;
				}else{
					obj.transform.localPosition= Vector3.zero;
				}
			}
		}

		//----------------------------------
		static List<string> GetMainMenuContents() {
			if(!enabled) return new List<string>();

			var lines= EditorGUIUtility.SerializeMainMenuToString().Split('\n');
			var output= new List<string>();

			List<string> treePath= new List<string>();
			treePath.Add(null);

			foreach(var line in lines) {
				if(string.IsNullOrEmpty(line) || (line.Trim() == "")) continue;

				int spaceCount= 0;
				while(line[spaceCount] == ' ') ++spaceCount;

				int indent= spaceCount / 4;

				if(indent >= treePath.Count) {
					output.RemoveAt(output.Count - 1);
				}

				while(indent < treePath.Count) {
					treePath.RemoveAt(treePath.Count - 1);
				}

				var name= line.TrimStart(' ');
				var match= Regex.Match(name, @"(.*) [%&]..?$");
				if(match.Success) {
					//removing hotkey info which can be visible in Unity 2017
					name= match.Groups[1].Value;
				}

				var path= "";
				foreach(var str in treePath) path += str + "/";
				treePath.Add(name);

				//path.Remove(0, 1); //removing leading '/';

				output.Add(path + name);
			}

			return output;
		}

		static class SceneMenuItems_GameObject {
			[SceneMenuItem("Spawn options/Spawn at cursor")]
			static SceneMenuItem ToggleSpawnAtCursor(SceneMenuItem.Arguments arguments) {
				var item= new SceneMenuItem();
				item.closesMenu= false;
				item.priority= 0;
				item.subPriority= 10;
				item.state= !spawnAtOrigin ? SceneMenuItem.ValidateResult.ValidAndMarked : SceneMenuItem.ValidateResult.Valid;
				item.function= (SceneMenuItem.Arguments arguments_) => {
					spawnAtOrigin= false;
				};
				
				return item;
			}

			[SceneMenuItem("Spawn options/Spawn at origin")]
			static SceneMenuItem ToggleSpawnAtOrigin(SceneMenuItem.Arguments arguments) {
				var item= new SceneMenuItem();
				item.closesMenu= false;
				item.priority= 0;
				item.subPriority= 10;
				item.state= spawnAtOrigin ? SceneMenuItem.ValidateResult.ValidAndMarked : SceneMenuItem.ValidateResult.Valid;
				item.function= (SceneMenuItem.Arguments arguments_) => {
					spawnAtOrigin= true;
				};
				
				return item;
			}

			[SceneMenuItem("Spawn options/Spawn as children")]
			static SceneMenuItem ToggleSpawnAsChildren(SceneMenuItem.Arguments arguments) {
				var item= new SceneMenuItem();
				item.closesMenu= false;
				item.priority= 0;
				item.subPriority= 10;
				item.state= spawnAsChildren ? SceneMenuItem.ValidateResult.ValidAndMarked : SceneMenuItem.ValidateResult.Valid;
				item.function= (SceneMenuItem.Arguments arguments_) => {
					spawnAsChildren= !spawnAsChildren;
				};
				
				return item;
			}

			[SceneMenuItem("Spawn options/Spawn facing camera")]
			static SceneMenuItem ToggleSpawnFacingCamera(SceneMenuItem.Arguments arguments) {
				var item= new SceneMenuItem();
				item.closesMenu= false;
				item.priority= 0;
				item.subPriority= 10;
				item.state= spawnFacingCamera ? SceneMenuItem.ValidateResult.ValidAndMarked : SceneMenuItem.ValidateResult.Valid;
				item.function= (SceneMenuItem.Arguments arguments_) => {
					spawnFacingCamera= !spawnFacingCamera;
				};
				
				return item;
			}


			[SceneMenuItem("Move here")]
			static SceneMenuItem MoveHere(SceneMenuItem.Arguments arguments) {
				if(arguments.selectedGameObjects.Count == 0) return null;

				var item= new SceneMenuItem();
				item.priority= 100;

				item.function= (arguments_) => {
					Vector3 centerOfGroup= Vector3.zero;
					foreach(var obj in arguments_.selectedGameObjects) centerOfGroup += obj.transform.position;
					centerOfGroup /= (float)arguments_.selectedGameObjects.Count;

					Vector3 diff= arguments.bestPosition - centerOfGroup;

					if(arguments.sceneView.in2DMode) {
						diff.z= 0;
					}

					var transform_list= new Transform[arguments_.selectedGameObjects.Count];
					for(int i= 0; i<transform_list.Length; i++) transform_list[i]= arguments_.selectedGameObjects[i].transform;
					Undo.RegisterCompleteObjectUndo(transform_list, "Moved object(s) to cursor");

					foreach(var obj in arguments_.selectedGameObjects) obj.transform.position += diff;
				};
				return item;
			}

			[SceneMenuItem("Move to/Global origin")]
			static SceneMenuItem MoveToGlobalOrigin(SceneMenuItem.Arguments arguments) {
				if(arguments.selectedGameObjects.Count == 0) return null;

				var item= new SceneMenuItem();
				item.priority= 100;

				item.function= (arguments_) => {
					var transform_list= new Transform[arguments_.selectedGameObjects.Count];
					for(int i= 0; i<transform_list.Length; i++) transform_list[i]= arguments_.selectedGameObjects[i].transform;
					Undo.RegisterCompleteObjectUndo(transform_list, "Moved object(s) to global origin");

					Vector3 centerOfGroup= Vector3.zero;
					foreach(var obj in arguments_.selectedGameObjects) centerOfGroup += obj.transform.position;
					centerOfGroup /= (float)arguments_.selectedGameObjects.Count;
					Vector3 diff= -centerOfGroup;

					foreach(var obj in arguments_.selectedGameObjects) obj.transform.position += diff;
				};
				return item;
			}

			[SceneMenuItem("Move to/Local origin")]
			static SceneMenuItem MoveToLocalOrigin(SceneMenuItem.Arguments arguments) {
				if(arguments.selectedGameObjects.Count == 0) return null;

				var item= new SceneMenuItem();
				item.priority= 100;

				item.function= (arguments_) => {
					var transform_list= new Transform[arguments_.selectedGameObjects.Count];
					for(int i= 0; i<transform_list.Length; i++) transform_list[i]= arguments_.selectedGameObjects[i].transform;
					Undo.RegisterCompleteObjectUndo(transform_list, "Moved object(s) to local origin");

					Vector3 pos= Vector3.zero;
					foreach(var obj in arguments_.selectedGameObjects) obj.transform.localPosition= Vector3.zero;
				};
				return item;
			}

			[SceneMenuItem("Rotate/Face Camera")]
			static SceneMenuItem FaceCamera(SceneMenuItem.Arguments arguments) {
				if(arguments.selectedGameObjects.Count == 0) return null;

				var item= new SceneMenuItem();
				item.priority= 100;

				item.function= (arguments_) => {
					var transform_list= new Transform[arguments_.selectedGameObjects.Count];
					for(int i= 0; i<transform_list.Length; i++) transform_list[i]= arguments_.selectedGameObjects[i].transform;
					Undo.RegisterCompleteObjectUndo(transform_list, "Rotated object(s) to face camera");

					Vector3 n= arguments_.sceneView.camera.transform.forward;
					foreach(var obj in arguments_.selectedGameObjects) obj.transform.forward= n;
				};

				return item;
			}

			[SceneMenuItem("Rotate/Face Away")]
			static SceneMenuItem FaceAway(SceneMenuItem.Arguments arguments) {
				if(arguments.selectedGameObjects.Count == 0) return null;

				var item= new SceneMenuItem();
				item.priority= 100;

				item.function= (arguments_) => {
					var transform_list= new Transform[arguments_.selectedGameObjects.Count];
					for(int i= 0; i<transform_list.Length; i++) transform_list[i]= arguments_.selectedGameObjects[i].transform;
					Undo.RegisterCompleteObjectUndo(transform_list, "Rotated object(s) to face away from camera");

					Vector3 n= arguments_.sceneView.camera.transform.forward;
					foreach(var obj in arguments_.selectedGameObjects) obj.transform.forward= -n;
				};

				return item;
			}
		}
	}
}