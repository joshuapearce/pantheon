﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

using UnityEditor;
using UnityEngine;

namespace Pantheon {
	public partial class SceneMenuItem {
		public class Arguments {
			public Ray mouseRay;
			public Vector2 mousePosition;

			public SceneView sceneView;
			public List<GameObject> selectedGameObjects;

			public Vector3 sceneIntersection;
			public GameObject objectIntersected;

			public Vector3 bestPosition {
				get {
					if(objectIntersected != null) {
						return sceneIntersection;
					}else{
						if(sceneView.in2DMode) {
							Vector3 pos= mouseRay.GetPoint(10);
							if(mouseRay.origin.z < 0.0f) pos.z= 0.0f;
							return pos;
						}else{
							float distance;
							if(!new Plane(new Vector3(0,1,0), Vector3.zero).Raycast(mouseRay, out distance)) distance= 10.0f;
							return mouseRay.GetPoint(distance);
						}
					}
				}
			}
		}

		string path_;
		public string path {
			get { return path_; }

			set {
				if(value == null) {
					path_= null;
					dir= null;
					name= null;
				}else{
					path_= value.Replace('\\','/').Trim('/');
					int index= path_.LastIndexOf('/');

					dir= (index < 0) ? "" : path.Substring(0, index);
					name= path_.Substring(index + 1);
				}
			}
		}

		public string dir { get; private set; }
		public string name { get; private set; }
		public Texture icon;

		public SceneMenuItem parent;
		public bool closesMenu= true;
		public bool hasSeperator;

		public bool isSubmenu;
		public int subItemCount; //internal use

		public int priority;
		public int subPriority;
		public ValidateResult state= ValidateResult.Void;
		public Action<Arguments> function;

		public GUIContent content { get { return new GUIContent(name); } }

		public SceneMenuItem(string path= null, ValidateResult state= ValidateResult.Void) {
			this.path= path;
			this.state= state;
		}

		public virtual void Execute(Arguments arguments) {
			if(function == null) {
				Debug.LogWarning("Null function provided to SceneMenuItem, name:'"+name+"'");
				return;
			}
			function(arguments);
		}
	}
}