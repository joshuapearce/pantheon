﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

using UnityEditor;
using UnityEngine;

namespace Pantheon {
	public class SceneMenuGUI {
		public static SceneMenuGUI instance;

		public SceneMenuItem.Arguments arguments;
		public List<SceneMenuItem> items;

		const float margins= 4;
		const float entryHeight= 16;
		const float entrySpacing= 4;
		const float separatorHeight= 10;

		const float widgetSize= 32;

		GUIStyle style;
		GUIStyle iconStyle;
		Texture checkmarkIcon;
		Texture submenuIcon;
		Texture submenuIcon_open;

		static bool buttonPressed;

		class SubmenuInfo {
			public SubmenuInfo(SceneMenuGUI owner, SceneMenuItem parent) {
				this.owner= owner;
				this.parent= parent;

				contentWidth= 0;
				contentHeight= 0;
				items= new List<SceneMenuItem>();

				foreach(var item in owner.items) {
					if(item.parent != parent) continue;

					items.Add(item);
					contentHeight += entryHeight;
					if(item.hasSeperator) contentHeight += separatorHeight;

					contentWidth= Mathf.Max(contentWidth, owner.style.CalcSize(item.content).x + (item.isSubmenu ? owner.submenuIcon.width : 0));
				}

				contentWidth += 16;
				contentWidth= Mathf.Max(contentWidth, 150);
				contentHeight += (items.Count - 1) * entrySpacing;
			}

			public void OnGUI() {
				EditorGUIUtility.AddCursorRect(rect, MouseCursor.Arrow);

				var GUIcolor_Prev= GUI.color;
				bool isCurrent= (this == owner.menus[owner.menus.Count - 1]);
				//isCurrent |= !SceneMenu.settings.CondenseMenus;

				float alpha= 1.0f;
				float depthShade= 1.0f;
				if(!isCurrent) {
					float index= (float)owner.menus.IndexOf(this);
					float count= (float)owner.menus.Count;

					depthShade= Mathf.Lerp(0.5f, 0.9f, (index + 1.0f) / count);
				}

				var BGcolor= new Color(0.85f,0.85f,0.85f);
				if(!isCurrent) BGcolor= new Color(0.75f,0.75f,0.75f) * depthShade;
				BGcolor.a= alpha;
				if(hasMouseFocus) BGcolor += new Color(0.05f, 0.10f, 0.15f);

				GUILayout.BeginArea(new Rect(position, new Vector2(width, height)));
				EditorGUI.DrawRect(new Rect(Vector2.zero, new Vector2(width, height)), BGcolor);
				if(GUI.Button(new Rect(Vector2.one, new Vector2(width-1, height-1)), "", GUIStyle.none)) {
					buttonPressed= true;
				}
				GUILayout.EndArea();

				GUILayout.BeginArea(new Rect(position + new Vector2(0, margins), new Vector2(width, contentHeight)));
				GUILayout.BeginVertical();
				float y= 0;
				foreach(var item in items) {
					if(item.hasSeperator) {
						EditorGUI.DrawRect(new Rect(1, y + separatorHeight / 2, width-2, 1) , new Color(0,0,0,0.25f));
						EditorGUI.DrawRect(new Rect(1, y + separatorHeight / 2 +1, width-2, 1) , new Color(0,0,0,0.15f));
						GUILayout.Space(separatorHeight);
						y += separatorHeight;
					}

					var rect= new Rect(1, y, width-2, entryHeight);
					bool isOpen= owner.openItems.Contains(item);

					GUI.color= new Color(depthShade, depthShade, depthShade, alpha);
					if(isOpen) GUI.color= new Color(1,1,1,alpha);
					
					//EditorGUI.BeginDisabledGroup((!isCurrent || (item.state == SceneMenuItem.ValidateResult.Disabled)) && !isOpen);
					EditorGUI.BeginDisabledGroup(item.state == SceneMenuItem.ValidateResult.Disabled);
					GUILayout.BeginHorizontal();

					Rect bgrect= new Rect(-margins + 1, y - 1, rect.width + margins * 2 - 2, rect.height);
					if(isActiveMenu && (owner.mouseFocusedItem == item)) {
						EditorGUI.DrawRect(bgrect, new Color32(32, 64, 128, 128));
					}else if(isOpen) {
						EditorGUI.DrawRect(bgrect, new Color32(32, 64, 128, 128));
					}

					GUILayout.Space(margins);

					{
						var icon= item.icon;
						if(item.state == SceneMenuItem.ValidateResult.ValidAndMarked) {
							icon= owner.checkmarkIcon;
						}

						if(icon != null) {
							GUILayout.Label(icon, owner.iconStyle, GUILayout.Width(entryHeight - 4), GUILayout.Height(entryHeight - 4), GUILayout.ExpandWidth(false), GUILayout.ExpandHeight(true));
							GUILayout.Space(4);
						}else{
							GUILayout.Space(entryHeight);
						}
					}

					GUILayout.Label(item.name, GUIStyle.none, GUILayout.Height(entryHeight));

					if(item.isSubmenu) {
						var icon= isOpen ? owner.submenuIcon_open : owner.submenuIcon;
						GUILayout.Label(icon, owner.iconStyle, GUILayout.Width(entryHeight), GUILayout.Height(owner.submenuIcon.height), GUILayout.ExpandWidth(false), GUILayout.ExpandHeight(false));
					}

					if(Event.current.type == EventType.Layout) {
						if(hasMouseFocus && rect.Contains(Event.current.mousePosition)) {
							owner.mouseFocusedItem= item;
						}
					}

					GUILayout.EndHorizontal();
					EditorGUI.EndDisabledGroup();
					GUILayout.Space(entrySpacing);
					y += entryHeight + entrySpacing;
				}

				GUILayout.EndVertical();
				GUILayout.EndArea();
				GUI.color= GUIcolor_Prev;
			}

			public float CalcYoffset(SceneMenuItem item) {
				float y= margins;

				foreach(var i in items) {
					if(i == item) break;

					y += entryHeight;
					if(i.hasSeperator) y += separatorHeight;
					y += entrySpacing;
				}

				return y;
			}

			public SceneMenuGUI owner;
			public SceneMenuItem parent;
			public SubmenuInfo parentMenu;
			public List<SceneMenuItem> items;

			public Vector2 position;
			public float contentWidth;
			public float contentHeight;

			public float width { get { return contentWidth + 2.0f * margins; } }
			public float height { get { return contentHeight + 2.0f * margins - 1.0f; } }
			public Rect rect { get { return new Rect(position.x, position.y, width, height); } }

			public bool isActiveMenu { get { return owner.activeSubmenu == this; } }
			public bool hasMouseFocus { get { return owner.mouseFocusedSubmenu == this; } }
		}


		public SceneView sceneView;
		List<SubmenuInfo> menus= new List<SubmenuInfo>();

		List<SceneMenuItem> openItems_;
		List<SceneMenuItem> openItems {
			get {
				if(openItems_ == null) {
					openItems_= new List<SceneMenuItem>();
					foreach(var menu in menus) if(menu.parent != null) openItems_.Add(menu.parent);
				}
				return openItems_;
			}
		}

		bool hasMouseFocus { get { return mouseFocusedSubmenu != null; } }
		SubmenuInfo mouseFocusedSubmenu;
		SceneMenuItem mouseFocusedItem;
		SubmenuInfo activeSubmenu { get { return menus[menus.Count - 1]; } }

		public static void Open(SceneMenuItem.Arguments arguments) {
			buttonPressed= false;

			var items= SceneMenu.ConstructMenu(arguments);
			instance= new SceneMenuGUI(arguments.sceneView, items, arguments.mousePosition);
			instance.arguments= arguments;
		}

		public static void RefreshMenu() {
			if(instance == null) return;

			var reopen= new List<string>();
			foreach(var item in instance.openItems) reopen.Add(item.name);

			Open(instance.arguments);

			foreach(var name in reopen) {
				if(!instance.ShowSubmenuByName(name)) break;
			}
		}

		private SceneMenuGUI(SceneView sceneView, List<SceneMenuItem> items, Vector2 position) {
			Styles.Initialize();

			this.sceneView= sceneView;

			style= new GUIStyle(Styles.text);
			style.wordWrap= false;
			style.alignment= TextAnchor.MiddleLeft;
			style.padding= new RectOffset(0,0,0,0);

			iconStyle= new GUIStyle();
			iconStyle.alignment= TextAnchor.MiddleCenter;

			checkmarkIcon= EditorGUIUtility.IconContent((EditorManager.UnityMajorVersion >= 2018) ? "FilterSelectedOnly" : "sv_icon_dot8_sml").image;
			submenuIcon= EditorGUIUtility.IconContent("Profiler.NextFrame").image;
			if(SceneMenu.settings.CondenseMenus) {
				submenuIcon_open= EditorGUIUtility.IconContent("icon dropdown").image;
			}else{
				submenuIcon_open= submenuIcon;
			}

			this.items= items;

			ShowSubmenu(null, position);
		}

		bool ShowSubmenuByName(string name) {
			var menu= menus[menus.Count - 1];
			foreach(var item in menu.items) {
				if(item.name == name) {
					ShowSubmenu(item);
					return true;
				}
			}
			return false;
		}

		void ShowSubmenu(SceneMenuItem parent) {
			ShowSubmenu(parent, Vector2.zero);
		}

		void ShowSubmenu(SceneMenuItem parent, Vector2 position) {
			if(parent != null) {
				var stack= new Stack<SubmenuInfo>();
				for(int i= menus.Count; --i>0;) {
					stack.Push(menus[i]);
				}

				ShowSubmenu_(null, Vector2.zero);
				foreach(var menu in stack) ShowSubmenu_(menu.parent, Vector2.zero);
			}
			ShowSubmenu_(parent, position);
		}

		void ShowSubmenu_(SceneMenuItem parent, Vector2 position) {
			openItems_= null;

			while(menus.Count > 0) {
				var menu = menus[menus.Count - 1];
				if(menu.parent == parent) return;
				if(menu.items.Contains(parent)) break;
				menus.RemoveAt(menus.Count - 1);
			}

			var newmenu = new SubmenuInfo(this, parent);

			if(menus.Count > 0) {
				var parentMenu = menus[menus.Count - 1];
				newmenu.parentMenu= parentMenu;

				newmenu.position= parentMenu.position;
				newmenu.position.x += parentMenu.width - margins;
				newmenu.position.y += parentMenu.CalcYoffset(parent);

				if(SceneMenu.settings.CondenseMenus) {
					float indent = 20;
					newmenu.position.x -= newmenu.width - indent;
					newmenu.position.x= Mathf.Max(newmenu.position.x, parentMenu.position.x + indent);

					newmenu.position.y += entryHeight;
				}
			}else{
				newmenu.position= position;
			}

			//keeping position on screen
			Rect bounds= new Rect(2, 1, sceneView.position.width - 6, sceneView.position.height - 23);

			if(newmenu.rect.xMax > bounds.width) {
				if(newmenu.parent == null) {
					newmenu.position.x= bounds.xMax - newmenu.width;
				}else{
					newmenu.position.x= newmenu.parentMenu.position.x - newmenu.width + margins;
				}
			}

			if(newmenu.rect.yMax > bounds.height) {
				newmenu.position.y= bounds.yMax - newmenu.height;
			}

			newmenu.position.x= Mathf.Max(bounds.xMin, newmenu.position.x);
			newmenu.position.y= Mathf.Max(bounds.yMin, newmenu.position.y);

			menus.Add(newmenu);
		}

		public void OnGUI(Event e) {
			var widgets= SceneMenu.widgets;
			if(widgets.Count > 0) {
				bool widgetsHaveFocus= menus.Count == 1;

				int zeroindex;
				for(zeroindex= 0; zeroindex<widgets.Count; zeroindex++) {
					var widget= widgets[zeroindex];
					if(widget.state == SceneMenuItem.ValidateResult.Hidden) continue;
					if(widgets[zeroindex].rowID >= 0) break;
				}

				Vector2 offset= new Vector2(0, 0);
				Vector2 dimensions= new Vector2(widgetSize, widgetSize);
				int row= -1;

				for(int i= zeroindex; i < widgets.Count; i++) {
					var widget= widgets[i];
					if(widget.state == SceneMenuItem.ValidateResult.Hidden) continue;

					if(row != widget.rowID) {
						offset.x= 0;
						offset.y -= widgetSize;
						row= widget.rowID;
					}else{
						offset.x += widgetSize;
					}

					widget.OnGUI(new Rect(menus[0].position + offset, dimensions), widgetsHaveFocus);
				}

				row= 1;
				offset= new Vector2(0, -widgetSize);
				for(int i= zeroindex - 1; i >= 0; i--) {
					var widget= widgets[i];
					if(widget.state == SceneMenuItem.ValidateResult.Hidden) continue;

					if(row != widget.rowID) {
						offset.x= -widgetSize;
						offset.y += widgetSize;
						row= widget.rowID;
					}else{
						offset.x -= widgetSize;
					}

					widget.OnGUI(new Rect(menus[0].position + offset, dimensions), widgetsHaveFocus);
				}
			}

			if(e.isMouse) {
				mouseFocusedSubmenu= null;
				foreach(var menu in menus) {
					if(menu.rect.Contains(e.mousePosition)) {
						mouseFocusedSubmenu= menu;
					}
				}
			}

			switch(e.type) {
			case EventType.Repaint:
				break;

			case EventType.Layout:
				mouseFocusedItem= null;
				break;


			case EventType.KeyDown:
				if(e.keyCode == KeyCode.Escape) {
					CloseOneMenu();
				}
				break;


			case EventType.MouseDown:
				if(!hasMouseFocus) SceneMenu.Close();
				break;

			case EventType.MouseUp:
				//if(hasMouseFocus) e.Use();
				break;

			default:
				e.Use();
				break;
			}

			foreach(var menu in menus) {
				menu.OnGUI();
			}

			if(buttonPressed) {
				buttonPressed= false;

				if(SceneMenu.settings.RightClickToCollapse && (e.button == 1)) {
					if(!CloseOneMenu()) {
						e.Use();
					}

				}else	if(SceneMenu.settings.CondenseMenus && (mouseFocusedSubmenu != activeSubmenu)) {
					ShowSubmenu(mouseFocusedSubmenu.parent);

				}else	if(mouseFocusedItem != null) {
					var activeMenu= menus[menus.Count - 1];
					bool isCurrentMenu= activeMenu.items.Contains(mouseFocusedItem);
					isCurrentMenu |= !SceneMenu.settings.CondenseMenus;

					if(!mouseFocusedItem.isSubmenu) {
						mouseFocusedItem.Execute(arguments);
						if(mouseFocusedItem.closesMenu) SceneMenu.Close();
						else RefreshMenu();
					}else if(openItems.Contains(mouseFocusedItem) || !isCurrentMenu) {
						ShowSubmenu(mouseFocusedItem.parent);

					}else{
						ShowSubmenu(mouseFocusedItem);
					}
				}
			}
		}

		public bool CloseOneMenu() {
			if(menus.Count == 1) {
				SceneMenu.Close();
				return false;
			}else{
				ShowSubmenu(menus[menus.Count - 1].parent.parent);
				return true;
			}
		}
	}
}