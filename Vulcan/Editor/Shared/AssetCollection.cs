﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

using UnityEditor;
using UnityEngine;

namespace Pantheon {
	[Serializable]
	public class AssetCollection:
		ScriptableObject,
		ISerializationCallbackReceiver
	{
		[NonSerialized] private AssetCollection.Category allCategory_;
		[SelfInitializing] public AssetCollection.Category allCategory {
			get {
				if(allCategory_ == null) {
					allCategory_= GetOrCreateCategory("All", true);
					allCategory_.special= true;
					allCategory_.codeGenerated= true;
					allCategory_.disableUserChanges= true;
				}
				return allCategory_;
			}
		}

		[Serializable]
		public class Category:
			IEnumerable<string>,
			ISerializationCallbackReceiver
		{
			public AssetCollection owner { get; set; }

			public string name= "<Unnamed>";
			public bool disableUserChanges;
			public bool sortable= true;
			public bool special;
			public bool invalid= false;
			public bool codeGenerated;
			public bool itemsAddedToStartOfList;

			[NonSerialized] private List<string> items= new List<string>();
			[NonSerialized] private HashSet<string> lookup= new HashSet<string>();
			[SerializeField] private List<string> items_serialized;

			public int Count { get { return items.Count; } }

			public void Clear() {
				items.Clear();
				lookup.Clear();
			}

			public bool Add(string guid) {
				if(string.IsNullOrEmpty(guid)) return false;

				if(Contains(guid)) {
					return false;
				}else{
					if(itemsAddedToStartOfList) {
						items.Insert(0, guid);
					}else{
						items.Add(guid);
					}
					lookup.Add(guid);
					if(!codeGenerated) owner.SetDirty();
					return true;
				}
			}

			public void Remove(string guid) {
				if(!Contains(guid)) return;

				items.Remove(guid);
				lookup.Remove(guid);
				if(!codeGenerated) owner.SetDirty();
			}

			public void RemoveAt(int index) {
				var guid= items[index];
				lookup.Remove(guid);
				items.RemoveAt(index);
				if(!codeGenerated) owner.SetDirty();
			}

			public bool Contains(string guid) {
				return lookup.Contains(guid);
			}

			public int IndexOf(string guid) {
				return items.IndexOf(guid);
			}

			//-----------------------------------
			public IEnumerator<string> GetEnumerator() {
				return items.GetEnumerator();
			}

			IEnumerator IEnumerable.GetEnumerator() {
				return this.GetEnumerator();
			}

			//-----------------------------------
			public void OnBeforeSerialize() {
				items_serialized= codeGenerated ? null : items;
			}

			public void OnAfterDeserialize() {
				items= codeGenerated ? new List<string>() : items_serialized;
				items_serialized= null;
				lookup= new HashSet<string>(items);
			}

			public void ClearOldReferences() {
				items.RemoveAll((guid)=> string.IsNullOrEmpty(AssetDatabase.GUIDToAssetPath(guid)));
				lookup= new HashSet<string>(items);
			}
		}

		public List<Category> categories= new List<Category>();
		[NonSerialized] public HashSet<string> favorites= new HashSet<string>();

		[SerializeField] private List<string> favorites_serialized= new List<string>();

		//----------------------------------
		public void OnBeforeSerialize() {
			if(favorites == null) favorites_serialized= null;
			else favorites_serialized= new List<string>(favorites);
		}

		public void OnAfterDeserialize() {
			if(favorites_serialized == null) {
				favorites= null;
			}else{
				favorites= new HashSet<string>(favorites_serialized);
				favorites_serialized= null;
			}

			foreach(var category in categories) {
				category.owner= this;
			}
		}

		new public void SetDirty() {
			EditorUtility.SetDirty(this);
		}

		public void ClearOldReferences() {
			foreach(var c in categories) {
				c.ClearOldReferences();
			}
			favorites.RemoveWhere((guid)=> string.IsNullOrEmpty(AssetDatabase.GUIDToAssetPath(guid)));
		}

		//----------------------------------
		public Category GetCategory(string name) {
			foreach(var c in categories) {
				if(string.Compare(c.name, name, true) == 0) return c;
			}
			return null;
		}

		public Category GetOrCreateCategory(string name, bool special= false) {
			var c= GetCategory(name);
			if(c == null) {
				c= new Category();
				c.name= name;
				c.special= special;
				c.owner= this;
				categories.Add(c);

				categories.Sort((a, b)=> {
					if(a.special || b.special) return b.special.CompareTo(a.special);
					return string.Compare(a.name, b.name, true);
				});

				var all= GetCategory("All");
				if((all != null) && (categories[0] != all)) {
					categories.Remove(all);
					categories.Insert(0, all);
				}

				SetDirty();
			}
			return c;
		}

		public void RemoveCategory(Category c) {
			if(c == null) return;
			if(c.special) return;
			c.invalid= true;
			categories.Remove(c);
			SetDirty();
		}

		//----------------------------------
		public bool isFavorite(string item) {
			return (item != null) && favorites.Contains(item);
		}

		public void setFavorite(string item, bool state= true) {
			if(item == null) return;
			if(state) favorites.Add(item);
			else favorites.Remove(item);
			SetDirty();
		}

	}
}