﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;

using UnityEditor;
using UnityEngine;

namespace Pantheon {
	[InitializeOnLoad]
	public class ThumbnailManager {
		public static Texture2D icon_defaultAsset;
		public static Texture2D icon_warning;
		public static Texture2D icon_error;

		static int debug_CacheUsed;
		static int debug_CacheAdded;

		class CacheEntry {
			public Texture2D texture;
			public bool isIcon;
			public double time_created;
			public double time_accessed;
		}
		static Dictionary<string, CacheEntry> cache= new Dictionary<string, CacheEntry>();

		static ThumbnailManager() {
			Manager.editorUpdate += StaticUpdate;
			Manager.editorRunOnce += Initialize;
		}

		static void Initialize() {
			AssetPreview.SetPreviewTextureCacheSize(1000);
			icon_defaultAsset= EditorGUIUtility.IconContent("DefaultAsset Icon").image as Texture2D;
			icon_warning= EditorGUIUtility.IconContent("console.warnicon").image as Texture2D;
			icon_error= EditorGUIUtility.IconContent("console.erroricon").image as Texture2D;
		}

		static void StaticUpdate() {

		}

		//--------------------------------
		public static Texture2D Get(string guid, UnityEngine.Object asset= null, Type type= null) {
			bool icon;
			return Get(guid, asset, type, out icon);
		}

		public static Texture2D Get(string guid, UnityEngine.Object asset, Type type, out bool isIcon) {
			isIcon= false;

			//optimization: The lookup can be very expensive, so we avoid it if the texture won't be used
			if(Event.current.type != EventType.Repaint) {
				return null;
			}

			CacheEntry cacheEntry= null;
			if(cache.TryGetValue(guid, out cacheEntry) && (cacheEntry != null) && (cacheEntry.texture != null)) {
				++debug_CacheUsed;
				isIcon= cacheEntry.isIcon;
				cacheEntry.time_accessed= EditorApplication.timeSinceStartup;
				return cacheEntry.texture;
			}

			Texture2D texture= null;

			if(guid == null) {
				if(asset != null) guid= AssetDatabase.AssetPathToGUID(AssetDatabase.GetAssetPath(asset));
			}

			if(guid != null) texture= GetFromGUID(guid);
			
			if(texture == null) {
				if(asset == null) texture= GetFromAsset(AssetDatabase.GUIDToAssetPath(guid), out isIcon);
				else texture= GetFromAsset(asset, out isIcon);
			}

			if(texture == null) {
				isIcon= true;
				if((type == null) && (asset != null)) type= asset.GetType();
				texture= GetFromType(type);
			}

			cacheEntry= new CacheEntry();
			cacheEntry.texture= texture;
			cacheEntry.isIcon= isIcon;
			cacheEntry.time_created= EditorApplication.timeSinceStartup;
			cacheEntry.time_accessed= EditorApplication.timeSinceStartup;
			++debug_CacheAdded;

			cache[guid]= cacheEntry;

			return texture;
		}

		protected static Texture2D GetFromGUID(string guid) {
			Texture2D texture= null;

			for(int i= 0; i<2; i++) {
				string extension= (i == 0) ? ".jpg" : ".png";
				var filePath= EditorManager.projectPath + "icons/" + guid + extension;

				try {
					if(!File.Exists(filePath)) continue;
					var fileData= File.ReadAllBytes(filePath);

					if(fileData != null) {
						texture= new Texture2D(2,2);
						if(!texture.LoadImage(fileData)) {
							Debug.LogError("Error trying to parse " + filePath);
							texture= null;
							continue;
						}else{
							break;
						}
					}

				}catch(Exception e) {
					Debug.LogError("Error trying to read " + filePath);
					Debug.LogException(e);
				}

			}

			return texture;
		}

		protected static Texture2D GetFromAsset(string assetPath, out bool isIcon) {
			Texture2D texture= null;

			//if(texture == null) texture= AssetDatabase.GetCachedIcon(assetPath) as Texture2D;

			{
				UnityEngine.Object asset= null;

				if(!string.IsNullOrEmpty(assetPath)) {
					asset= AssetDatabase.LoadAssetAtPath<UnityEngine.Object>(assetPath);
				}

				texture= GetFromAsset(asset, out isIcon);
			}

			return texture;
		}

		protected static Texture2D GetFromAsset(UnityEngine.Object asset, out bool isIcon) {
			Texture2D texture= null;

			if(asset == null) {
				isIcon= true;
				texture= icon_error;
			}else{
				texture= AssetPreview.GetAssetPreview(asset);
				isIcon= (texture == null);

				if(texture == null) texture= EditorGUIUtility.ObjectContent(asset, asset.GetType()).image as Texture2D;
				if(texture == null) texture= AssetPreview.GetMiniThumbnail(asset);
			}

			return texture;
		}

		public static Texture2D GetFromType(Type type) {
			Texture2D texture= null;
			if(texture == null) texture= AssetPreview.GetMiniTypeThumbnail(type);
			if(texture == null) texture= icon_defaultAsset;
			return texture;
		}

		public static Texture2D GenerateThumbnail(string inputPath) {
			inputPath= Path.GetFullPath(inputPath);
			var dir= EditorManager.tempDataPath_absolute + "Thumbnails/";

			if(!System.IO.File.Exists(inputPath)) {
				return null;
			}

			//We hash part of the file path to avoid filesystem overflows
			//Should be plenty good enough, and also stable
			string hashcode= Path.GetDirectoryName(inputPath).GetHashCode().ToString("X") + "-" + Path.GetFileName(inputPath);

			var outputPath= dir + hashcode + ".png";
			var tex= new Texture2D(64, 64);

			if(System.IO.File.Exists(outputPath)) {
				if(ImageConversion.LoadImage(tex, System.IO.File.ReadAllBytes(outputPath))) {
					return tex;
				}else{
					return null;
				}
			}else{
				if(ImageConversion.LoadImage(tex, System.IO.File.ReadAllBytes(inputPath))) {
					int w= tex.width;
					int h= tex.height;
					if(w > h) {
						h /= w / 64;
						w= 64;
					}else{
						w /= h / 64;
						h= 64;
					}

					TextureScale.Point(tex, w, h);
					Directory.CreateDirectory(Path.GetDirectoryName(outputPath));
					//if(System.IO.File.Exists(outputPath)) {
						//somehow we tried creating the same file twice?
					//}else{
						File.WriteAllBytes(outputPath, tex.EncodeToPNG());
					//}

					return tex;
				}else{
					return null;
				}
			}
		}

		//--------------------------------
		public static Rect GUI_DrawThumbnail(Texture texture, float width, float height, int borderSize= 2) {
			return GUI_DrawThumbnail(texture, null, width, height, borderSize);
		}

		public static Rect GUI_DrawThumbnail(Texture texture, string tooltip, float width, float height, int borderSize= 2) {
			return GUILayout_DrawThumbnail(texture, tooltip, width, height, borderSize, Color.white);
		}

		public static Rect GUI_DrawThumbnail(Rect rect, Texture texture, string tooltip, int borderSize, Color color) {
			var height= rect.height;
			var width= rect.width;

			//float pictureHeight= height * Mathf.Min(1.0f, (float)texture.height / (float)texture.width);
			//float pictureWidth= height * Mathf.Min(1.0f, (float)texture.width / (float)texture.height);

			var picRect= new Rect(rect.center - new Vector2(width,height) / 2.0f, new Vector2(width, height));

			//EditorGUI.DrawPreviewTexture(picRect, texture);
			//EditorGUI.DrawTextureTransparent(picRect, texture);
			GUI.DrawTexture(picRect, texture, ScaleMode.ScaleToFit);

			if(Event.current.type == EventType.Repaint) {
				if(borderSize > 0) {
					float t= borderSize;
					EditorGUI.DrawRect(new Rect(picRect.xMin, picRect.yMin, t, picRect.height), color); //left
					EditorGUI.DrawRect(new Rect(picRect.xMax - t, picRect.yMin, t, picRect.height), color); //right
					EditorGUI.DrawRect(new Rect(picRect.xMin + t, picRect.yMin, picRect.width - t * 2, t), color); //top
					EditorGUI.DrawRect(new Rect(picRect.xMin + t, picRect.yMax - t, picRect.width - t * 2, t), color); //bottom
				}
			}

			if(!string.IsNullOrEmpty(tooltip)) {
				GUI.Label(picRect, new GUIContent("", tooltip));
			}

			return picRect;
		}

		public static Rect GUILayout_DrawThumbnail(Texture texture, string tooltip, float width, float height, int borderSize, Color color) {
			//float pictureHeight= height * Mathf.Min(1.0f, (float)texture.height / (float)texture.width);
			float pictureWidth= height * Mathf.Min(1.0f, (float)texture.width / (float)texture.height);

			var style= new GUIStyle(GUIStyle.none);
			style.alignment= TextAnchor.MiddleCenter;

			//GUILayout.Space((height - pictureWidth) * 0.5f);
			GUILayout.Label(
				new GUIContent(texture, tooltip),
				style,
				GUILayout.Width(width),
				GUILayout.Height(height)
			);

			var picRect= GUILayoutUtility.GetLastRect();
			GUILayout.Space((height - pictureWidth) * 0.5f);

			if(Event.current.type == EventType.Repaint) {
				if(borderSize > 0) {
					float t= borderSize;
					EditorGUI.DrawRect(new Rect(picRect.xMin, picRect.yMin, t, picRect.height), color); //left
					EditorGUI.DrawRect(new Rect(picRect.xMax - t, picRect.yMin, t, picRect.height), color); //right
					EditorGUI.DrawRect(new Rect(picRect.xMin + t, picRect.yMin, picRect.width - t * 2, t), color); //top
					EditorGUI.DrawRect(new Rect(picRect.xMin + t, picRect.yMax - t, picRect.width - t * 2, t), color); //bottom
				}
			}

			return picRect;
		}
	}
}
