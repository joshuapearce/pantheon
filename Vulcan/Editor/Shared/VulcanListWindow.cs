﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using UnityEditor;
using UnityEngine;

using ItemType= System.String;

namespace Pantheon {
	public abstract class VulcanListWindow:
		VulcanWindow
	{
		public bool hasPictures;
		public bool hasFavorites;
		public bool hasDirectories= true;
		public bool allowGridView= true;
		public bool hasCategoriesMenu;
		public bool hasCustomCategories;
		public bool hasMultiSelection;

		public bool hasFilters { get { return (filterNames != null) && (filterNames.Length > 0); } }
		public bool hasMultiFilters;
		public bool hasAutoFilterRefresh;
		protected string[] filterNames;

		protected bool isDragFocus;

		protected bool favoritesVisible;

		private ItemType overAsset_;
		protected ItemType overAsset {
			get { return overAsset_; }
			set {
				if(overAsset_ == value) return;
				overAsset_= value;
				//if(EditorManager.debugPantheon) Debug.Log("overAsset= " + overAsset_);
			}
		}
		protected ItemType activeAsset;
		protected ItemType recentlyClickedItem;

		protected abstract AssetCollection assets { get; }

		double timeOfRefreshContents= float.NegativeInfinity;
		double timeSinceRefreshContents {
			get { return EditorApplication.timeSinceStartup - timeOfRefreshContents; }
			set { timeOfRefreshContents= EditorApplication.timeSinceStartup - value; }
		}

		//------------------------------
		[NonSerialized] protected GUIStyle listStyle;
		[NonSerialized] protected GUIStyle listStyle_selected;
		[NonSerialized] protected GUIStyle favStyle;
		[NonSerialized] protected GUIStyle gearStyle;

		protected float rowContentHeightMinimum= 20;
		protected float rowContentHeight { get { return Mathf.Max(rowContentHeightMinimum, showPictures ? pictureSize : 0); } }
		protected float rowHeight { get { return rowContentHeight + rowPadding; } }
		protected float rowPadding { get { return (pictureSize > 0) ? 8 : 0; } }
		protected float pictureSize { get { return (!showPictures && !showGridView) ? 0 : pictureSize_forced; } }
		protected float pictureSize_forced { get { return largePictures ? 64 : 32; } }
		const float marginSize= 8;

		//------------------------------
		protected HashSet<string> selectedItems= new HashSet<ItemType>();
		protected List<string> visibleItems= new List<ItemType>();

		//------------------------------
		[VulcanWindowSetting] public int activeCategory_index;
		public AssetCollection.Category activeCategory {
			get { return activeCategory_index < 0 ? null : assets.categories[activeCategory_index]; }
			protected set { activeCategory_index= (value == null) ? -1 : assets.categories.IndexOf(value); }
		}

		//--------------------------------
		protected class WindowItem {
			public WindowItem(VulcanListWindow window) {
				this.window= window;
			}

			readonly VulcanListWindow window;

			public ItemType key;
			public string path;
			public string entryText;
			public string section;

			//public UnityEngine.Object asset;
			//public int instanceID;

			public bool thumbnailIsIcon;
			public Texture2D thumbnail {
				get {
					if(thumbnail_ == null) {
						if(EditorApplication.timeSinceStartup - time_IconRefreshed > 1.0f) {
							thumbnail_= window.GetItemPicture(key, out thumbnailIsIcon) as Texture2D;
						}
					}
					if(thumbnail_ != null) return thumbnail_;
					return ThumbnailManager.icon_defaultAsset;
				}

				set {
					thumbnail_= value;
					time_IconRefreshed= EditorApplication.timeSinceStartup;
				}
			}
			Texture2D thumbnail_;
			double time_IconRefreshed= 0.0f;
		}
		List<WindowItem> windowItems= new List<WindowItem>();

		//--------------------------------
		protected override void InitializePanel() {
			hasScrollingH= false;
			hasScrollingV= true;
			hasMultiSelection= true;

			listStyle= new GUIStyle(Styles.text);
			listStyle.alignment= TextAnchor.MiddleLeft;
			listStyle.margin= new RectOffset(5,5,0,0);

			listStyle_selected= new GUIStyle(listStyle);
			listStyle_selected.normal.textColor= Color.white;

			//var BG= new Texture2D(1,1);
			//BG.SetPixel(0,0, new Color32(62,125,231,255));
			//BG.Apply();
			//listStyle_selected.normal.background= BG;

			favStyle= new GUIStyle();
			favStyle.alignment= TextAnchor.MiddleCenter;
			favStyle.margin= new RectOffset(5,5,0,0);

			gearStyle= new GUIStyle();
			gearStyle.alignment= TextAnchor.UpperRight;
		}

		protected override void WindowUpdate() {
			if(!hasPictures) showPictures= false;
			if(!hasFavorites) showOnlyFavorites= false;

			if(
				filterEnabled &&
				autoFilterRefresh &&
				(timeSinceRefreshContents >= 1.0f) &&
				UnityEditorInternal.InternalEditorUtility.isApplicationActive)
			{
				Refresh();
			}
		}

		//------------------------------
		protected override void ProcessEvents(Event e) {
			base.ProcessEvents(e);

			hasContentsAttention &= isDragFocus || hasAttention;

			switch(e.type) {
			case EventType.Repaint:
				overAsset= null; //let the GUI code reselect it
				break;

			case EventType.KeyUp:
				switch(e.keyCode) {
				case KeyCode.A:
					if(Event.current.command || Event.current.control) {
						selectedItems.Clear();
						foreach(var obj in visibleItems) {
							doSelectItem(obj);
						}
					}
					break;

				case KeyCode.Escape:
					selectedItems.Clear();
					Repaint();
					break;
				}
				break;

			case EventType.MouseDown:
				isDragFocus= contentRect.Contains(e.mousePosition);
				break;

			case EventType.MouseUp:
				isDragFocus= false;

				if(!hasMultiSelection) {
					//nothing important to do
					selectedItems.Clear();

				}else if(e.shift) {
					int a= visibleItems.IndexOf(activeAsset);
					int b= visibleItems.IndexOf(overAsset);

					for(int i= Math.Min(a, b); i <= Math.Max(a, b); i++) {
						doSelectItem(visibleItems[i]);
					}

				}else if(e.control) {
					if(selectedItems.Count == 0) {
						doSelectItem(activeAsset);
					}
					doSelectItem(overAsset, !selectedItems.Contains(overAsset));

				}else if(!selectedItems.Contains(overAsset)) {
					selectedItems.Clear();
					Repaint();
				}

				activeAsset= null;
				break;

			case EventType.MouseLeaveWindow:
				overAsset= null;
				break;
			}

		}
		
		//--------------------------------
		protected override void RefreshContents() {
			timeSinceRefreshContents= 0;

			favoritesVisible= false;

			if((activeCategory != null) && !assets.categories.Contains(activeCategory)) activeCategory= null;
			if(activeCategory == null) activeCategory= assets.allCategory;

			RefreshVisibleItems();

			//--------------------------------
			var dict= new Dictionary<ItemType, WindowItem>();
			foreach(var item in windowItems) dict.Add(item.key, item);
			windowItems.Clear();
			foreach(var key in visibleItems) {
				if(key == null) continue;

				WindowItem item;
				dict.TryGetValue(key, out item);
				if(item == null) {
					item= new WindowItem(this);
					item.key= key;
				}

				GetItemInfo(key, item);

				//item.instanceID= 0;
				//item.asset= null;
				//item.icon= null;

				windowItems.Add(item);
			}
		}

		protected virtual void RefreshVisibleItems() {
			visibleItems.Clear();
			foreach(var obj in activeCategory) {
				visibleItems.Add(obj);
				favoritesVisible |= assets.isFavorite(obj);
			}

			if(showOnlyFavorites && favoritesVisible) {
				visibleItems.RemoveAll((obj)=> {
					return !assets.isFavorite(obj);
				});
			}

			if(activeCategory.sortable) SortList(visibleItems);
		}

		protected override void DrawToolbar(Rect rect) {
			if(hasDirectories) {
				if(GUILayout.Toggle(showDirectories, "Directories", EditorStyles.toolbarButton) != showDirectories) {
					showDirectories= !showDirectories;
					RefreshContents();
				}
			}

			if(allowGridView) {
				if(GUILayout.Toggle(showGridView, "Grid", EditorStyles.toolbarButton) != showGridView) {
					showGridView= !showGridView;
					RefreshContents();
				}
			}

			if(hasFavorites) {
				EditorGUI.BeginDisabledGroup(!favoritesVisible);
				if(GUILayout.Toggle(showOnlyFavorites, "Favorites", EditorStyles.toolbarButton) != showOnlyFavorites) {
					showOnlyFavorites= !showOnlyFavorites;
					RefreshContents();
				}
				EditorGUI.EndDisabledGroup();
			}

			/*
			if(hasPictures) {
				if(GUILayout.Toggle(settings.showPictures, "Previews", EditorStyles.toolbarButton) != settings.showPictures) {
					settings.showPictures= !settings.showPictures;
					Refresh();
				}
			}
			*/

			if(hasFilters && (filterMask != 0)) {
				if(GUILayout.Toggle(filterEnabled, "Filtered", EditorStyles.toolbarButton) != filterEnabled) {
					filterEnabled= !filterEnabled;

					RefreshContents();
					//Refresh();
				}
			}

			if(hasCategoriesMenu) {
				GUILayout.FlexibleSpace();

				var names= new List<string>();
				bool separator_needed= false;
				bool separator_added= false;

				int choiceIndex= -1;
				foreach(var c in assets.categories) {
					if(c.special) {
						separator_needed= true;
					}else if(separator_needed && !separator_added) {
						names.Add("");
						separator_added= true;
					}

					if(c == activeCategory) choiceIndex= names.Count;

					names.Add(c.name);
				}

				if(hasCustomCategories) {
					if(names.Count > 0) names.Add("");
					names.Add("+New");
				}

				choiceIndex= EditorGUILayout.Popup(choiceIndex, names.ToArray());
				if(choiceIndex >= 0) {
					if(choiceIndex == names.Count - 1) {
						CreateUserCategory();
					}else{
						var choiceName= names[choiceIndex];
						if(!string.IsNullOrEmpty(choiceName)) {
							SetCategory(choiceName);
						}
					}
				}
			}
		}

		//--------------------------------
		protected override void AddItemsToMenu_(GenericMenu menu) {
			if(hasPictures) {
				menu.AddItem(new GUIContent("Thumbnails/Off"), !showPictures, ()=> {
					showPictures= false;
					Refresh();
				});

				menu.AddItem(new GUIContent("Thumbnails/Normal"), showPictures && !largePictures, ()=> {
					showPictures= true;
					largePictures= false;
					RefreshContents();
				});

				menu.AddItem(new GUIContent("Thumbnails/Large"), showPictures && largePictures, ()=> {
					showPictures= true;
					largePictures= true;
					Refresh();
				});

				menu.AddItem(new GUIContent("Thumbnails/Show Outlines"), showPictureOutlines, ()=> {
					showPictureOutlines= !showPictureOutlines;
					Refresh();
				});
			}

			if(hasFilters) {
				menu.AddItem(new GUIContent("Filters/None"), filterMask == 0, ()=> {
					filterEnabled= false;
					filterMask= 0;
				});

				if(hasMultiFilters) {
					int mask_AllFilters= (1 << (filterNames.Length)) - 1;
					menu.AddItem(new GUIContent("Filters/All"), filterMask == mask_AllFilters, ()=> {
						filterEnabled= true;
						filterMask= mask_AllFilters;
					});
				}

				if(hasAutoFilterRefresh) {
					menu.AddItem(new GUIContent("Filters/Auto refresh"), autoFilterRefresh, ()=> {
						autoFilterRefresh= !autoFilterRefresh;
					});			
				}

				menu.AddSeparator("Filters/");

				for(int i= 0; i<filterNames.Length; i++) {
					var name= filterNames[i];
					if(string.IsNullOrEmpty(name)) continue;

					int index= i; //avoids wrong value being used in worker  function
					menu.AddItem(new GUIContent("Filters/" + name), GetFilterState(i), ()=> {
						if(hasMultiFilters) {
							ToggleFilterState(index);
						}else{
							filterMask= 0;
							SetFilterState(index, true);
						}
						filterEnabled= (filterMask != 0);
						Refresh();
					});				
				}
			}

			if(!activeCategory.disableUserChanges && !activeCategory.special) {
				menu.AddSeparator("");
				menu.AddItem(new GUIContent("Rename category"), false, ()=> {
					TextInputDialog.Create("Renaming category", (value)=> {
						if(!string.IsNullOrEmpty(value)) activeCategory.name= value;
					},
					activeCategory.name);
				});

				menu.AddItem(new GUIContent("Delete category"), false, ()=> {
					if(EditorUtility.DisplayDialog("Confirm deletion", "Delete the category '" +  activeCategory.name + "'?", "Confirm")) {
						assets.RemoveCategory(activeCategory);
						activeCategory_index= 0;
					}
				});
			}

			menu.AddItem(new GUIContent("Advanced/Refresh panel"), false, ()=> {
				ManualRefresh();
			});
		}
		
		protected override void onClicked() {
			activeAsset= overAsset;
			if(overAsset != null) onClickedItem(overAsset);
		}

		protected override void onDoubleClicked() {
			if(overAsset != null) onDoubleClickedItem(overAsset);
		}

		protected virtual void onClickedItem(ItemType obj) {
			var recentlyClickedItem= this.recentlyClickedItem;
			this.recentlyClickedItem= obj;

			//if(obj == null) return;

			if(Event.current.shift) {
				if((recentlyClickedItem != null) && (recentlyClickedItem != obj)) {
					var visibleItems= this.visibleItems;
					int a= visibleItems.IndexOf(obj);
					int b= visibleItems.IndexOf(recentlyClickedItem);

					for(int i= Math.Min(a,b); i<=Math.Max(a,b); i++) {
						doSelectItem(visibleItems[i]);
					}
				}else{
					doSelectItem(recentlyClickedItem);
				}

			}else if(Event.current.command || Event.current.control) {
				doSelectItem(obj, !selectedItems.Contains(obj));
				recentlyClickedItem=  obj;

			//}else if((obj == null) || !selectedItems.ContainsKey(obj)){
			}else{
				selectedItems.Clear();

				//We don't officially "select" individual items, unless directed to by the user through ctrl/command/shift
				//doSelectItem(obj);
			}
		}

		protected virtual void onDoubleClickedItem(ItemType obj) {

		}

		protected void doSelectItem(ItemType obj, bool state= true) {
			if(obj != null) {
				if(state) selectedItems.Add(obj);
				else selectedItems.Remove(obj);
			}
			Repaint();
		}

		protected override Rect DrawContents(Rect rect) {
			bool showGridView= this.showGridView && hasPictures;
			bool showPictures= this.showPictures || showGridView;
			var pictureSize= showPictures ? pictureSize_forced : 0;

			Rect result= rect;

			int drawCount= 0;
			int drawHeaderCount= 0;

			if(visibleItems.Count == 0) {
				EditorGUILayout.HelpBox("No items visible", MessageType.Info);

			}else{
				Rect scrollRect= new Rect(0, scrollPosition_safe.y, rect.width, rect.height);
				float itemwidth;
				float itemheight;
				int columnCount;

				if(showGridView) {
					itemwidth= pictureSize;
					itemheight= pictureSize;

					var w= rect.width - (GUI.skin.verticalScrollbar.fixedWidth + marginSize * 2) + rowPadding;
					columnCount= Mathf.Max(1, Mathf.FloorToInt(w / (itemwidth + rowPadding)));
				}else{
					itemwidth= rect.width - GUI.skin.verticalScrollbar.fixedWidth - marginSize * 2;
					itemheight= rowContentHeight;
					columnCount= 1;
				}

				{
					float yPos= 0;
					float rowPos= yPos;
					float columnIndex= 0;

					Rect headerRect= new Rect();
					Rect firstSectionItemRect= new Rect();
					string sectionTitle= "";

					for(int itemIndex= 0; itemIndex<windowItems.Count; itemIndex++) {
						WindowItem item= windowItems[itemIndex];
						WindowItem nextItem= (itemIndex <  windowItems.Count - 1) ? windowItems[itemIndex + 1] : null;
						bool isFirstItemInSection= false;
						bool isLastItemInSection= (nextItem == null) || (nextItem.section != item.section);

						if(showDirectories && (item.section != sectionTitle)) {
							isFirstItemInSection= true;
							sectionTitle= item.section;

							if(itemIndex > 0) yPos += 8;
							columnIndex= 0;

							headerRect= new Rect(0, yPos, rect.width, 16);
							//draw header was here, moved elsewhere

							yPos += headerRect.height;
							rowPos= yPos;
						}

						if(columnIndex >= columnCount) {
							columnIndex= 0;
							rowPos= yPos;
						}

						Rect paddedRect;
						Rect itemRect;

						if(showGridView) {
							paddedRect= new Rect(
								marginSize + columnIndex * (itemwidth + rowPadding),
								rowPos,
								itemwidth + rowPadding,
								itemheight + rowPadding
							);

							itemRect= new Rect(paddedRect.xMin + rowPadding / 2.0f, paddedRect.yMin + rowPadding / 2.0f, itemwidth, itemheight);

						}else{
							paddedRect= new Rect(0, rowPos, scrollRect.width, itemheight + rowPadding);
							itemRect= new Rect(paddedRect.xMin + marginSize, paddedRect.yMin + rowPadding / 2, paddedRect.width - marginSize * 2, itemheight);
						}

						if(paddedRect.Overlaps(scrollRect)) {
							if(Event.current.type == EventType.Repaint) {
								if(hasMouseCursorInContent && paddedRect.Contains(Event.current.mousePosition)) {
									overAsset= item.key;
								}
							}
							++drawCount;
							DrawItem(item, itemRect, paddedRect, !showGridView, showPictures);
						}
						yPos= Mathf.Max(yPos, paddedRect.yMax);
						++columnIndex;

						if(isFirstItemInSection) {
							firstSectionItemRect= itemRect;
						}

						if(isLastItemInSection) {
							var fillerRect= paddedRect;
							for(float fillerIndex= 0; fillerIndex < (columnCount - columnIndex); fillerIndex++) {
								fillerRect.x += fillerRect.width;

								var cellRect= itemRect;
								cellRect.x += fillerRect.x - paddedRect.x;
								cellRect.y += fillerRect.y - paddedRect.y;

								EditorGUI.DrawRect(cellRect, new Color(0.5f, 0.5f, 0.5f, 0.1f));
							}

							bool drawHeader= true;

							if(paddedRect.yMin <= scrollRect.yMax) drawHeader= true;
							if(paddedRect.yMax <= scrollRect.yMin) drawHeader= false;
							if(headerRect.yMin > scrollRect.yMax) drawHeader= false;

							if(drawHeader) {
								++drawHeaderCount;

								if(headerRect.yMin < scrollRect.yMin) {
									var h= headerRect.height;

									headerRect.yMin= scrollRect.yMin;
									headerRect.yMax= headerRect.yMin + h;

									headerRect.yMax= Mathf.Min(headerRect.yMax, paddedRect.center.y);
									headerRect.yMin= headerRect.yMax - h;

									if(headerRect.yMax >= firstSectionItemRect.yMax) {
										sectionTitle= "..." + sectionTitle;
									}
								}

								DrawHeader(headerRect, sectionTitle);
							}
						}
					}

					GUILayout.Space(yPos + marginSize);
					result.yMax= yPos + marginSize;
				}

			}

			return result;
		}

		protected void MarkFavorite(ItemType item, bool state) {
			//We don't want the list to abruptly become a single item long, but we also want to turn this setting off as rarely as possible
			showOnlyFavorites &= favoritesVisible;

			assets.setFavorite(item, state);
			Refresh();
		}

		void DrawHeader(Rect rect, string title) {
			var color= backgroundColor;
			color *= 0.9f;
			color.a= 1.0f;
			EditorGUI.DrawRect(rect, color);
			rect.xMin += marginSize;
			GUI.Label(rect, title);
		}

		void DrawItem(WindowItem windowItem, Rect contentRect, Rect paddedRect, bool wideView, bool showPicture) {
			var item= windowItem.key;
			bool isMouseOver= (overAsset == item);
			bool isSelected;
			if(!hasMultiSelection) isSelected= false;
			else if(selectedItems.Count > 0) isSelected= selectedItems.Contains(item);
			else if(item == activeAsset) isSelected= hasAttention && (Event.current.control || Event.current.shift);
			else isSelected= false;

			bool isFavorite= hasFavorites && assets.isFavorite(item);

			var style= isSelected ? listStyle_selected : listStyle;
			if(isSelected) EditorGUI.DrawRect(paddedRect, new Color32(62,125,231,255));
			else if(isMouseOver) EditorGUI.DrawRect(paddedRect, new Color32(62,125,231,64));
			//else EditorGUI.DrawRect(contentRect, new Color32(0,0,128,64));

			if(hasFavorites && wideView) {
				GUIContent favcontent;
				if(isFavorite) favcontent= Styles.favorite;
				else if(isMouseOver) favcontent= Styles.nonFavorite_over;
				else favcontent= Styles.nonFavorite;

				if(GUI.Button(
					new Rect(contentRect.xMin - 4, contentRect.yMin, 20, rowContentHeight),
					favcontent,
					favStyle
				)) {
					MarkFavorite(item, !isFavorite);
				}
				contentRect.xMin += 16;
			}

			GUIContent content= new GUIContent();
			content.text= windowItem.entryText;
			content.tooltip= windowItem.path;

			if(showPicture) {
				
				if(showDirectories) {
					var dirname= Path.GetDirectoryName(content.text);
					if(!string.IsNullOrEmpty(dirname)) {
						dirname += Path.DirectorySeparatorChar;
						var filename= Path.GetFileName(content.text);
						content.text= dirname + "\n  " + filename;
					}
				}
				
				Texture2D texture= windowItem.thumbnail;
				bool isIcon= windowItem.thumbnailIsIcon;
				if(texture == null) {
					texture= ThumbnailManager.icon_warning;
					isIcon= true;
				}

				var picRect= new Rect(contentRect.xMin, contentRect.yMin, pictureSize, pictureSize);

				if(isIcon) {
					EditorGUI.DrawRect(picRect, new Color32(0x52,0x52,0x52,32));
				}

				picRect= ThumbnailManager.GUI_DrawThumbnail(
					picRect,
					texture,
					content.tooltip,
					showPictureOutlines ? 2 : 0,
					isFavorite ? new Color(0.8f, 0.55f, 0, 1.0f) : new Color(0.5f, 0.5f, 0.5f, 1.0f)
				);

				if(!wideView && isFavorite) {
					var favrect= new Rect(picRect.xMin - 8, picRect.yMin - 8, 20, 20);
					GUI.DrawTexture(favrect, Styles.favorite.image);
				}

				contentRect.x += pictureSize + 8;
			}

			if(wideView) {
				GUI.Label(contentRect, content, style);
			}

			if(hasContextMenu && isMouseOver && wideView) {
				float buttonSize= 20;
				if(GUI.Button(
					new Rect(contentRect.xMax - buttonSize - 5, contentRect.yMin + 4, buttonSize, buttonSize),
					EditorGUIUtility.IconContent("_Popup"),
					gearStyle
				)) {
					requestContextMenu= true;
				}
			}
		}

		//--------------------------------
		protected virtual string GetItemName(string itemname, bool includeFull= false) {
			throw new NotImplementedException();
		}

		protected virtual void GetItemInfo(ItemType key, WindowItem item) {
			item.path= GetItemName(key, true);
			item.section= Path.GetDirectoryName(item.path);
			item.entryText= Path.GetFileName(item.path);
		}

		protected virtual Texture GetItemPicture(ItemType item, out bool isIcon) {
			throw new NotImplementedException();
		}

		public abstract bool isObjectRelevant(UnityEngine.Object obj);

		protected void CreateUserCategory(Action<AssetCollection.Category> onSuccess= null) {
			TextInputDialog.Create("New category name?", (value)=> {
				if(!string.IsNullOrEmpty(value)) {
					//Avoids duplicates
					var c= assets.GetOrCreateCategory(value);
					SetCategory(c);
					Refresh();

					if(onSuccess != null) onSuccess(c);
				}
			});
		}

		public void SetCategory(string name) {
			SetCategory(assets.GetCategory(name));
		}

		private void SetCategory(int index) {
			SetCategory(assets.categories[index]);
		}

		private void SetCategory(AssetCollection.Category c) {
			if(c == null) return;
			if(c == activeCategory) return;

			activeCategory= c;
			scrollPosition= Vector2.zero;
			selectedItems.Clear();
			recentlyClickedItem= null;
			Refresh();
		}

		//--------------------------------
		protected virtual void SortList(List<ItemType> objects) {
			var list= new List<Tuple<string, ItemType>>(objects.Count);
			foreach(var obj in objects) {
				list.Add(new Tuple<ItemType, ItemType>(
					GetItemName(obj, showDirectories),
					obj
				));
			}

			list.Sort((itema, itemb)=> {
				var obja= itema.Item2;
				var objb= itemb.Item2;

				if(obja == null) {
					return (objb == null) ? 0 : 1;
				}else if(objb == null) {
					return -1;
				}

				string a= itema.Item1;
				string b= itemb.Item1;

				if(naturalSorting) {
					return EditorUtility.NaturalCompare(a, b);
				}else{
					return string.Compare(a, b, true);
				}
			});

			objects.Clear();
			foreach(var item in list) objects.Add(item.Item2);
		}

		//--------------------------------
		protected override GenericMenu CreateContextMenu() {
			var objects= new List<ItemType>(selectedItems);
			//foreach(var i in selectedItems_prev) objects.Add(i);
			if((overAsset != null) && !objects.Contains(overAsset)) objects.Add(overAsset);

			SortList(objects);
			objects.RemoveAll((a)=> { return a == null; });
			return CreateContextMenu(objects);
		}

		protected GenericMenu CreateContextMenu(List<ItemType> objects) {
			if(objects.Count == 0) return null;

			var menu= new GenericMenu();
			PopulateContextMenu(menu, objects);

			return menu;
		}

		protected virtual void PopulateContextMenu(GenericMenu menu, List<ItemType> objects) {
			bool singular= objects.Count == 1;

			if(hasFavorites) {
				menu.AddSeparator("");

				if(singular) {
					menu.AddItem(new GUIContent("Favorite"), assets.isFavorite(objects[0]), ()=> {
						MarkFavorite(objects[0], !assets.isFavorite(objects[0]));
					});

				}else{
					menu.AddItem(new GUIContent("Mark all favorite"), false, ()=> {
						foreach(var obj in objects) {
							MarkFavorite(obj, true);
						}
					});

					menu.AddItem(new GUIContent("Unfavorite all"), false, ()=> {
						foreach(var obj in objects) {
							MarkFavorite(obj, false);
						}
					});
				}
			}

			if(hasCategoriesMenu) {
				menu.AddSeparator("");

				if(!activeCategory.disableUserChanges) {
					menu.AddItem(new GUIContent("Remove from this category"), false, ()=> {
						foreach(var asset in objects) activeCategory.Remove(asset);
						selectedItems.Clear();
					});
				}

				bool didListCategory= false;
				foreach(var c in assets.categories) {
					if(c.disableUserChanges) continue;

					didListCategory= true;
					bool b= false;
					foreach(var obj in objects) b |= c.Contains(obj);

					menu.AddItem(new GUIContent("Categories/"+c.name), b, ()=> {
						foreach(var obj in objects) {
							if(!b) c.Add(obj);
							else c.Remove(obj);
						}
					});
				}

				if(hasCustomCategories) {
					if(didListCategory) menu.AddSeparator("Categories/");
					menu.AddItem(new GUIContent("Categories/+New"), false, ()=> {
						CreateUserCategory((c)=> {
							foreach(var obj in objects) {
								c.Add(obj);
							}
						});
					});
				}
			}
		}

	}
}