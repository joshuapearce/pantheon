﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;

using UnityEditor;
using UnityEngine;

namespace Pantheon {
	[InitializeOnLoad]
	public abstract class VulcanAssetListWindow<ItemType>:
		VulcanListWindow
		where ItemType: UnityEngine.Object
	{
		protected bool hideFileExtension;

		protected override void ProcessEvents(Event e) {
			base.ProcessEvents(e);

			switch(e.type) {
			case EventType.DragUpdated:
 				if(!activeCategory.disableUserChanges) {
					foreach(var obj in DragAndDrop.objectReferences) {
						if(isObjectRelevant(obj)) {
							Event.current.Use();
							DragAndDrop.visualMode= DragAndDropVisualMode.Link;
							break;
						}
					}
				}
				break;

			case EventType.DragPerform:
 				if(!activeCategory.disableUserChanges) {
					DragAndDrop.AcceptDrag();
					var newItems= new HashSet<string>();

					foreach(var obj_ in DragAndDrop.objectReferences) {
						var obj= obj_ as ItemType;
						if(obj == null) continue;

						var guid= GetGUID(GetRelevantAsset(obj));
						if(!activeCategory.Contains(guid)) {
							activeCategory.Add(guid);
							newItems.Add(guid);
						}
					}

					if(newItems.Count > 0) {
						//Debug.Log("Added " + newItems.Count + " assets to category '" + activeCategory.name + "'");
						selectedItems.Clear();
						foreach(var obj in newItems) {
							doSelectItem(obj);
							if(showOnlyFavorites) MarkFavorite(obj, true);
						}

						Refresh();
					}
				}
				break;

			case EventType.MouseDrag:
				if(isDragFocus && contentRect.Contains(e.mousePosition)) {
					isDragFocus= false;
					DragAndDrop.PrepareStartDrag();

					var list= new List<UnityEngine.Object>();
					foreach(var guid in selectedItems) list.Add(GetAsset(guid));

					var overAsset = GetAsset(this.overAsset);
					if(!list.Contains(overAsset)) list.Add(overAsset);
					DragAndDrop.objectReferences= list.ToArray();
					DragAndDrop.StartDrag("VulcanListWindow.DragAsset");
				}
				break;
			}
		}

		protected override void AddItemsToMenu_(GenericMenu menu) {
			base.AddItemsToMenu_(menu);

			menu.AddItem(new GUIContent("Advanced/Clear old references"), false, assets.ClearOldReferences);
		}

		protected abstract UnityEngine.Object GetRelevantAsset(UnityEngine.Object obj);
		protected abstract string GetGUID(UnityEngine.Object obj);

		protected override string GetItemName(string guid, bool includeFull=false) {
			var path= AssetDatabase.GUIDToAssetPath(guid);
			if(string.IsNullOrEmpty(path)) return guid;

			string result;
			if(includeFull) {
				result= path.Substring("Assets/".Length);
			}else{
				result= Path.GetFileName(path);
			}

			if(hideFileExtension && Path.HasExtension(result)) result= result.Substring(0, result.LastIndexOf('.'));
			return result;
		}

		protected bool TestGUID(string guid) {
			if(string.IsNullOrEmpty(guid)) return false;

			var path= AssetDatabase.GUIDToAssetPath(guid);
			if(string.IsNullOrEmpty(path)) return false;

			return true;
		}

		protected UnityEngine.Object GetAsset(string guid) {
			if(string.IsNullOrEmpty(guid)) return null;

			var path= AssetDatabase.GUIDToAssetPath(guid);
			if(string.IsNullOrEmpty(path)) return null;

			var asset= AssetDatabase.LoadAssetAtPath<UnityEngine.Object>(path);
			return asset;
		}


		protected override Texture GetItemPicture(string guid, out bool isIcon) {
			return ThumbnailManager.Get(guid, null, null, out isIcon);
		}
	}
}