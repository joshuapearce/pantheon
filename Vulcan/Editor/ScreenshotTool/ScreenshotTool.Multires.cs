﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;

using UnityEditor;
using UnityEngine;

using GameView = Pantheon.UnityReflection.GameView;

namespace Pantheon {
	public static partial class ScreenshotTool {
		public static class Multires {
			public static bool working { get; private set; }

			public static void Activate() {
				if(busy) return;
				if(!Application.isPlaying) return;
				busy= true;

				//foreach(var m in GetRelevantModes()) {
					//Debug.Log(m.groupType + ": " + m.fullname);
				//}

				if(GetRelevantModes().Count == 0) {
					EditorUtility.DisplayDialog("Pantheon Multires configuration problem", "There are no available screen modes with the current platform and/or settings, Multires has nothing to do", "Open settings");
					settings.Display();
				}

				Manager.StartCoroutine(Worker_coroutine());
			}

			static IEnumerator Worker_coroutine() {
				var originalGroup= GameView.s_GameViewSizeGroupType;
				var originalIndex= GameView.selectedSizeIndex;
				var originalViewScale= GameView.m_Scale; //fix for an intermittent bug

				var modes= GetRelevantModes();
				var now= DateTime.Now;
				var startTime= EditorApplication.timeSinceStartup;

				string filepath= GetScreenshotFilepath(null, now, false);
				if(settings.multires_useSubdirectories && (modes.Count > 1)) {
					filepath= filepath.TrimEnd(new char[] { Path.DirectorySeparatorChar }); //safety
					filepath += Path.DirectorySeparatorChar;
					Directory.CreateDirectory(filepath); //should be redundant, doesn't hurt
					filepath= GetScreenshotFilepath(filepath, now, false);
				}

				float originalTimeScale= Time.timeScale;
				if(settings.multires_FreezeTime) Time.timeScale= 0.0000001f;

				foreach(var mode in modes) {
					var currentfilepath= filepath + " [" + mode.safename + "]";

					GameView.CurrentMode= mode;
					yield return null; //for reinitialization of view settings
					yield return TakeSceneScreenshot_Coroutine(currentfilepath);

				}

				//done, restore the screen settings
				GameView.RestoreMode(originalGroup, originalIndex);
				GameView.m_Scale= originalViewScale;
				yield return null;

				if(settings.multires_FreezeTime) Time.timeScale= originalTimeScale;
				if(EditorManager.debugPantheon) {
					Debug.Log("Created " + modes.Count + " screenshots: " + (EditorApplication.timeSinceStartup - startTime).ToString("F1") + " seconds");
				}
				busy= false;
			}

			static bool Landscape {	get { return settings.multires_Landscape; } }
			static bool Portrait {	get { return settings.multires_Portrait; } }
			static bool CurrentViewSize { get { return settings.multires_CurrentViewSize; } }

			public static List<GameView.ModeInfo> GetRelevantModes(bool ignoreFilters= false) {
				#if UNITY_ANDROID
				var mode= GameViewSizeGroupType.Android;
				#elif UNITY_IOS
				var mode= GameViewSizeGroupType.iOS;
				#else
				var mode= GameViewSizeGroupType.Standalone;
				#endif

				var list= GameView.GetModes(mode);
				if(!ignoreFilters) {
					FilterList(list, settings.getFilteredBlacklist(mode));
				}

				if(!Landscape) {
					list.RemoveAll((v)=> v.w > v.h);
				}

				if(!Portrait) {
					list.RemoveAll((v)=> v.h > v.w);
				}

				//if(CurrentViewSize && !list.Contains(GameView.CurrentMode)) {
				//	list.Insert(0, GameView.CurrentMode);
				//}

				return list;
			}

			static void FilterList(List<GameView.ModeInfo> items, List<string> blacklist) {
				blacklist= new List<string>(blacklist);
				for(int i= 0; i<blacklist.Count; i++) blacklist[i]= blacklist[i].Trim().ToLower();
				blacklist.RemoveAll((str) => string.IsNullOrEmpty(str));

				var output= new List<GameView.ModeInfo>();
				GameView.ModeInfo largest_w= items[0];
				GameView.ModeInfo largest_h= items[0];

				items.RemoveAll((v)=> {
					var name= v.name.ToLower().Trim();
					return blacklist.Contains(name);
				});
			}
		}
	}
}