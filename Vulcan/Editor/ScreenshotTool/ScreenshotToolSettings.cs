﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

using UnityEditor;
using UnityEngine;
using GameView= Pantheon.UnityReflection.GameView;

#pragma warning disable 0429

namespace Pantheon {
	[InitializeOnLoad]
	public class ScreenshotToolSettings:
		ModuleSettings
	{
		static ScreenshotToolSettings instance_;
		[SelfInitializing] public static ScreenshotToolSettings instance {
			get {
				if(instance_ == null) instance_= ModuleSettings.LoadOrCreate<ScreenshotToolSettings>("ScreenshotTool.settings");
				return instance_;
			}
		}

		#if UNITY_2018_3_OR_NEWER
		[SettingsProvider]
    private static SettingsProvider InitializeSettingsProvider_static() {
			return instance.InitializeSettingsProvider();
		}
		#endif

		[Serializable] public struct FilterEntry {
			public UnityEditor.GameViewSizeGroupType platform;
			public string name;
		}

		public List<FilterEntry> filteredBlacklist;
		public List<string> getFilteredBlacklist(UnityEditor.GameViewSizeGroupType platform) {
			var list= new List<string>();
			foreach(var e in filteredBlacklist) {
				if(e.platform == platform) list.Add(e.name);
			}

			return list;
		}

		bool getFilterState(GameView.ModeInfo mode) {
			var entry= new FilterEntry { platform= mode.groupType, name= mode.name };
			return filteredBlacklist.Contains(entry);
		}

		void setFilterState(GameView.ModeInfo mode, bool state) {
			var entry= new FilterEntry { platform= mode.groupType, name= mode.name };

			if(state) {
				if(!filteredBlacklist.Contains(entry)) filteredBlacklist.Add(entry);
			}else{
				filteredBlacklist.Remove(entry);
			}
		}

		protected override void OnEnable() {
			base.OnEnable();
			instance_= this;
			showInConfigPanel= true;
			displayName= "Screenshots";
			helpFile= "Screenshots.txt";
		}

		[SettingsField("Use PrintScreen button", 100)]
		public bool key_Printscreen= true;

		[SettingsField("Use F12 button", 200)]
		public bool key_F12;

		[SettingsField("Use Mouse click", "Every time the user clicks while playing, a screenshot will be taken", 300)]
		public bool key_Click;

		[SettingsField(
			"Enable user hooks",
			"When enabled, each screenshot will have multiple versions depending on [ScreenshotVersionHookAttribute] functions defined by you",
			SettingsFieldAttribute.Flags.Default,
			250
		)]
		[SelfInitializing] public bool EnableHooks {
			get { return EditorPrefs.GetBool("Pantheon.Screenshots.EnableHooks", true); }
			set {
				EditorPrefs.SetBool("Pantheon.Screenshots.EnableHooks", value);
				UnityEditor.Menu.SetChecked(ScreenshotTool.MenuName + "Enable User Version Hooks", value);
			}
		}
		[MenuItem(ScreenshotTool.MenuName + "Enable User Version Hooks", false, ScreenshotTool.MenuPriority + 22)]
		static void Toggle_EnableHooks() {
			instance.EnableHooks= !instance.EnableHooks;
		}

		[SettingsField("Always focus on scene in playmode", "If this option is disabled, editor windows which have focus will be the target of screenshots", SettingsFieldAttribute.Flags.SystemWide, 400)]
		public bool doForceGameFocusInPlay {
			get { return EditorPrefs.GetBool("Pantheon.Screenshots.doForceGameFocusInPlay", true); }
			set { EditorPrefs.SetBool("Pantheon.Screenshots.doForceGameFocusInPlay", value); }
		}

		[SettingsField("Log each screenshot path to console", null, SettingsFieldAttribute.Flags.SystemWide, 500)]
		public bool doLogScreenshotPaths {
			get { return EditorPrefs.GetBool("Pantheon.Screenshots.doLogScreenshotPaths", true); }
			set { EditorPrefs.SetBool("Pantheon.Screenshots.doLogScreenshotPaths", value); }
		}

		[SettingsField("Show a notification for screenshots", null, SettingsFieldAttribute.Flags.SystemWide, 510)]
		public bool doNotify {
			get { return EditorPrefs.GetBool("Pantheon.Screenshots.doNotify", true); }
			set { EditorPrefs.SetBool("Pantheon.Screenshots.doNotify", value); }
		}

		[SettingsField("Fastest Implementation", "Disable this if there are problems", SettingsFieldAttribute.Flags.Advanced | SettingsFieldAttribute.Flags.SystemWide, 600)]
		public bool FastestImplementation {
			get { return EditorPrefs.GetBool("Pantheon.Screenshots.FastestImplementation", true); }
			set { EditorPrefs.SetBool("Pantheon.Screenshots.FastestImplementation", value); }
		}

		[SettingsField("Fill Alpha Channel", "This will set the alpha to 1.0 on all pixels.\nDefault state is on, turn off only if you have a specific reason for things to be weird", SettingsFieldAttribute.Flags.SystemWide, 700)]
		public bool FillAlphaChannel {
			get { return EditorPrefs.GetBool("Pantheon.Screenshots.FillAlphaChannel", true); }
			set { EditorPrefs.SetBool("Pantheon.Screenshots.FillAlphaChannel", value); }
		}		

		[SettingsField(
			"Png Compression: High",
			"Default is on. Image quality should not be affected, and file size is dramatically reduced",
			SettingsFieldAttribute.Flags.SystemWide,
			800
		)]
		public bool Png_HighCompression {
			get { return EditorPrefs.GetBool("Pantheon.Screenshots.Png_HighCompression", true); }
			set { EditorPrefs.SetBool("Pantheon.Screenshots.Png_HighCompression", value); }
		}

		//--------------------------------
		public string Multires_Standalone_filters;
		public string Multires_iOS_filters;
		public string Multires_Android_filters;

		[SettingsField(
			"Multires: Use as default",
			"This setting will cause Multires to be the default mode for screenshots during playmode",
			SettingsFieldAttribute.Flags.Separator,
			900
		)]
		[SelfInitializing] public bool multires_useAsDefault {
			get {
				return EditorPrefs.GetBool("Pantheon/Screenshot/Multires/useAsDefault", true);
			}
			set {
				EditorPrefs.SetBool("Pantheon/Screenshot/Multires/useAsDefault", value);
				UnityEditor.Menu.SetChecked(ScreenshotTool.MenuName + "Multires settings/Use as default", value);
			}
		}		
		[MenuItem(ScreenshotTool.MenuName + "Multires settings/Use as default", false, ScreenshotTool.MenuPriority + 21)]
		static void Toggle_multires_useAsDefault() {
			instance.multires_useAsDefault= !instance.multires_useAsDefault;
		}

		[SettingsField(
			"Multires: Use subdirectories",
			"This setting will cause multires screenshots to be organized into directories. Default value is on",
			SettingsFieldAttribute.Flags.Default,
			1000
		)]
		[SelfInitializing] public bool multires_useSubdirectories {
			get {
				return EditorPrefs.GetBool("Pantheon/Screenshot/Multires/useSubdirectories", true);
			}
			set {
				EditorPrefs.SetBool("Pantheon/Screenshot/Multires/useSubdirectories", value);
				UnityEditor.Menu.SetChecked(ScreenshotTool.MenuName + "Multires settings/Use subdirectories", value);
			}
		}		
		[MenuItem(ScreenshotTool.MenuName + "Multires settings/Use subdirectories", false, ScreenshotTool.MenuPriority + 21)]
		static void Toggle_multires_useSubdirectories() {
			instance.multires_useSubdirectories= !instance.multires_useSubdirectories;
		}

		[SettingsField(
			"Multires: Use multithreading",
			"This setting will cause multires screenshots to be organized into directories. Default value is off for maximum compatibility",
			SettingsFieldAttribute.Flags.Default,
			1050
		)]
		[SelfInitializing] public bool multires_useMultithreading {
			get {
				return EditorPrefs.GetBool("Pantheon/Screenshot/Multires/useMultithreading", false);
			}
			set {
				EditorPrefs.SetBool("Pantheon/Screenshot/Multires/useMultithreading", value);
				UnityEditor.Menu.SetChecked(ScreenshotTool.MenuName + "Multires settings/Use multithreading", value);
			}
		}		
		[MenuItem(ScreenshotTool.MenuName + "Multires settings/Use multithreading", false, ScreenshotTool.MenuPriority + 21)]
		static void Toggle_multires_useMultithreading() {
			instance.multires_useMultithreading= !instance.multires_useMultithreading;
		}

		[SettingsField("Multires: Freeze time", "This setting will cause the game speed to be set to zero while multiple screenshots are being generated, which can take several frames", 1100)]
		[SelfInitializing] public bool multires_FreezeTime {
			get {
				return EditorPrefs.GetBool("Pantheon/Screenshot/Multires/freezeTime", false);
			}
			set {
				EditorPrefs.SetBool("Pantheon/Screenshot/Multires/freezeTime", value);
				UnityEditor.Menu.SetChecked(ScreenshotTool.MenuName + "Multires settings/Freeze time", value);
			}
		}
		[MenuItem(ScreenshotTool.MenuName + "Multires settings/Freeze time", false, ScreenshotTool.MenuPriority + 21)]
		static void Toggle_multires_FreezeTime() {
			instance.multires_FreezeTime= !instance.multires_FreezeTime;
		}

		//--------------------------------
		[SettingsField("Multires: Landscape", null, SettingsFieldAttribute.Flags.Separator, 1200)]
		[SelfInitializing] public bool multires_Landscape {
			get {
				return EditorPrefs.GetBool("Pantheon/Screenshot/Multires/Landscape", true);
			}
			set {
				EditorPrefs.SetBool("Pantheon/Screenshot/Multires/Landscape", value);
				UnityEditor.Menu.SetChecked(ScreenshotTool.MenuName + "Multires settings/Landscape", value);
			}
		}		
		[MenuItem(ScreenshotTool.MenuName + "Multires settings/Landscape", false, ScreenshotTool.MenuPriority + 35)]
		static void Toggle_multires_Landscape() {
			instance.multires_Landscape= !instance.multires_Landscape;
		}

		[SettingsField("Multires: Portrait", 1300)]
		[SelfInitializing] public bool multires_Portrait {
			get {
				return EditorPrefs.GetBool("Pantheon/Screenshot/Multires/Portrait", true);
			}
			set {
				EditorPrefs.SetBool("Pantheon/Screenshot/Multires/Portrait", value);
				UnityEditor.Menu.SetChecked(ScreenshotTool.MenuName + "Multires settings/Portrait", value);
			}
		}		
		[MenuItem(ScreenshotTool.MenuName + "Multires settings/Portrait", false, ScreenshotTool.MenuPriority + 35)]
		static void Toggle_multires_Portrait() {
			instance.multires_Portrait= !instance.multires_Portrait;
		}

		[SettingsField("Multires: Current view size", 1400)]
		[SelfInitializing] public bool multires_CurrentViewSize {
			get {
				return EditorPrefs.GetBool("Pantheon/Screenshot/Multires/CurrentViewSize", true);
			}
			set {
				EditorPrefs.SetBool("Pantheon/Screenshot/Multires/CurrentViewSize", value);
				UnityEditor.Menu.SetChecked(ScreenshotTool.MenuName + "Multires settings/Current view size", value);
			}
		}		
		[MenuItem(ScreenshotTool.MenuName + "Multires settings/Current view size", false, ScreenshotTool.MenuPriority + 35)]
		static void Toggle_multires_FreeAspect() {
			instance.multires_CurrentViewSize= !instance.multires_CurrentViewSize;
		}


		//--------------------------------
		public override string ConfigHeaderInfo() {
			return "More features are available in the " + ScreenshotTool.MenuName.Trim('/') + " menu";
		}

		//--------------------------------
		public List<GameViewSizeGroupType> ExpandFilterList= new List<GameViewSizeGroupType>();

		[ScreenshotVersionHook] static string TestHook() {
			return "test";
		}

		public override void DrawContents(float contentWidth) {
			GUILayout.BeginVertical(defaultMargins);
			var hooks= ScreenshotTool.ScreenshotVersionAttributeList;
			if(hooks.Count > 0) {
				string text= "Detected " + hooks.Count + " user hook" + ((hooks.Count > 1) ? "s":"") + " for screenshots";
				EditorGUILayout.TextArea(text, GUI.skin.GetStyle("HelpBox"));
			}

			Styles.DrawLink("Open screenshots directory", ScreenshotTool.rootPath);
			Styles.DrawLink("Open screenshots viewer", (EditorManager.UnityMajorVersion >= 2018) ? EditorGUIUtility.IconContent("PreTextureArrayFirstSlice").image : null, ScreenshotPanel.Create);

			GUILayout.EndVertical();

			base.DrawContents(contentWidth);

			GUILayout.BeginVertical(defaultMargins);
			{
				var groups= new[] {
					GameViewSizeGroupType.Standalone,
					GameViewSizeGroupType.Android,
					GameViewSizeGroupType.iOS
				};

				var styleActive= new GUIStyle(Styles.text);
				var styleInactive= new GUIStyle(styleActive);

				{
					var color= styleInactive.normal.textColor;
					color.a= 0.5f;
					styleInactive.normal.textColor= color;
				}

				foreach(var group in groups) {
					bool expanded= ExpandFilterList.Contains(group);
					var sectionTitle= group + " Multires display size filters";
					#if UNITY_ANDROID
					bool groupActive= (group == GameViewSizeGroupType.Android);
					#elif UNITY_IOS
					bool groupActive= (group == GameViewSizeGroupType.iOS);
					#else
					bool groupActive= (group == GameViewSizeGroupType.Standalone);
					#endif
					if(groupActive) sectionTitle += "\t[Active]";

					if(expanded != EditorGUILayout.Foldout(expanded, sectionTitle, true)) {
						expanded= !expanded;
						if(expanded) ExpandFilterList.Add(group); else ExpandFilterList.Remove(group);
					}

					if(expanded) {
						GUILayout.BeginVertical(defaultMargins);

						var allModes= GameView.GetModes(group);
						var enabledModes= new HashSet<string>();
						foreach(var mode in ScreenshotTool.Multires.GetRelevantModes(true)) {
							enabledModes.Add(mode.name);
						}

						GUILayout.BeginHorizontal();
						bool? setState= null;
						if(GUILayout.Button("All/Default")) {
							setState= false;
						}
						if(GUILayout.Button("Clear")) {
							setState= true;
						}
						GUILayout.FlexibleSpace();
						GUILayout.EndHorizontal();

						
						foreach(var mode in allModes) {
							if(setState != null) {
								setFilterState(mode, setState.Value);
							}

							bool state= getFilterState(mode);
							bool modeActive= groupActive && enabledModes.Contains(mode.name);

							if(EditorGUILayout.ToggleLeft(mode.name, !state, (modeActive ? styleActive : styleInactive)) == state) {
								setFilterState(mode, !state);
							}
						}
						
						GUILayout.Space(10);
						GUILayout.EndVertical();
					}
				}
			}
			GUILayout.EndVertical();
		}
	}
}