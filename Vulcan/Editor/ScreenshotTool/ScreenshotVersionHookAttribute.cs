using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using Pantheon.UnityReflection;
using UnityEngine;

namespace Pantheon {
	[System.AttributeUsage(AttributeTargets.Method)]
	public class ScreenshotVersionHookAttribute:
		Attribute
	{
		public bool Validate(MemberInfo member, bool quietly= true) {
			var fullname= member.DeclaringType.FullName + "." + member.Name;

			var method= member as MethodInfo;
			if((method == null) || !method.IsStatic) {
				if(!quietly) Debug.LogError(fullname + " must be a static method");
				return false;
			}

			var parameters= method.GetParameters();
			if(method.ReturnType != typeof(string)) {
				if(!quietly) Debug.LogError(fullname + " must return a string; (This will be appended to filenames, or return null if the hook doesn't want to run)");
				return false;
			}

			if(parameters.Length > 0) {
				if((parameters.Length > 1) || (parameters[0].ParameterType != typeof(int))) {
					if(!quietly) Debug.LogError(fullname + " must have zero or one arguments, and the argument must be an int for the index");
					return false;
				}
			}

			return true;
		}

		public IEnumerable<string> Activate(MethodInfo method) {
			if(!Validate(method)) yield break;

			var parameters= method.GetParameters();

			if(parameters.Length == 0) {
				var result= method.Invoke(null, null) as string;
				if(result == null) {
					yield break;
				}else{
					if(result == "") result= method.Name;
					yield return result;
				}

			}else if(parameters.Length == 1) {
				int index= 0;
				while(true) {
					var result= method.Invoke(null, new object[] { index++ }) as string;
					if(result == null) {
						yield break;
					}else{
						if(result == "") result= method.Name + "#" + index;

						yield return result;
					}

					if(index >= 32) {
						//It's safe to disable this check, it's just for sanity.
						//If you want to have more than 32 versions, you can. It's just weird.
						Debug.LogError("A ScreenshotVersionAttribute iterated too many times, and was canceled");
						yield break;
					}
				}

			}else{
				throw new ArgumentOutOfRangeException();
			}
		}
	}
}