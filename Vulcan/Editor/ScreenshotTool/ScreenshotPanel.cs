using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using UnityEditor;
using UnityEngine;

namespace Pantheon {
	[InitializeOnLoad]
	public class ScreenshotPanel:
		VulcanListWindow
	{
		static ScreenshotPanel instance;
		static AssetCollection staticassets;
		protected override AssetCollection assets { get { return staticassets; } }

		protected override ModuleSettings settingsModule { get { return ScreenshotToolSettings.instance; } }

		class ItemInfo {
			public string name;
			public string systempath;
			public string previewSourcePath;
			public bool isDir;
			public int count;

			public DateTime dateTime;
			public Texture preview;
			public bool previewError;

			public bool updated;
		}
		static Dictionary<string, ItemInfo> items;

		[MenuItem("Tools/Screenshot/Screenshot Viewer", false, ScreenshotTool.MenuPriority)]
		[ConditionalMenuItem("Tools/Pantheon/Screenshot Viewer", ToolsMenu.MenuPriority)]
		[ConditionalMenuItem("Window/General/Screenshot Viewer", ToolsMenu.GeneralMenuPriority)]
		public static void Create() {
			instance= VulcanWindowManager.OpenWindow<ScreenshotPanel>();
			instance.Focus();
		}

		static ScreenshotPanel() {
			EditorManager.RegisterModule("Screenshot Viewer");
		}

		//--------------------------------
		protected override void InitializePanel() {
			base.InitializePanel();

			if(staticassets == null) {
				staticassets= ScriptableObject.CreateInstance<AssetCollection>();
			}

			instance= this;
			defaultTitle= "Screenshots";
			defaultIcon= EditorGUIUtility.IconContent("Texture Icon").image;

			hasFavorites= false;
			hasCategoriesMenu= false;
			hasDirectories= false;
			hasScrollingH= false;
			hasScrollingV= true;
			hasPictures= true;
			hideToolbar= false;
			hasContextMenu= true;
			hasMultiSelection= true;

			rowContentHeightMinimum= 40;

			showDirectories= false;

			activeCategory= assets.GetOrCreateCategory("All", true);
			activeCategory.codeGenerated= true;
			activeCategory.disableUserChanges= true;
			activeCategory.sortable= false;
			activeCategory.itemsAddedToStartOfList= true;

			EditorManager.onEditorFocus += onEditorFocus;
			ScreenshotTool.onScreenshotTaken += onScreenshotTaken;
		}

		protected override void OnDestroy() {
			EditorManager.onEditorFocus -= onEditorFocus;
			ScreenshotTool.onScreenshotTaken -= onScreenshotTaken;
		}

		void onEditorFocus(bool state) {
			if(state) RefreshAssets();
		}

		protected override void ProcessEvents(Event e) {
			switch(e.type) {
			case EventType.MouseDrag:
				if(contentRect.Contains(e.mousePosition)) {
					isDragFocus= false;

					var set= new HashSet<string>(selectedItems);
					//if(activeAsset != null) set.Add(activeAsset);
					if(overAsset != null) set.Add(overAsset);

					var list= new List<string>();
					foreach(var name in set) {
						list.Add(ScreenshotTool.rootPath + name);
					}

					DragAndDrop.PrepareStartDrag();
					DragAndDrop.paths= list.ToArray();
					DragAndDrop.StartDrag("VulcanListWindow.DragScreenshots");
				}
				break;
			}

			base.ProcessEvents(e);
		}

		protected override void WindowUpdate() {
			
		}

		public override bool isObjectRelevant(UnityEngine.Object obj) {
			return false;
		}

		protected override void GetItemInfo(string itemname, WindowItem info) {
			var item= items[itemname];
			string str= "";

			if(item.dateTime == DateTime.MinValue) {
				str += "No date/time info";
			}else{
				str += item.dateTime.ToString();
			}

			info.section= "";
			info.path= str;

			if(item.isDir) {
				str += "\n" + item.count + " versions";
			}else{
				var match= Regex.Match(item.systempath, "\\[([^\\]]*)\\]");
				if(match.Success) str += "\n" + match.Groups[1].Value;
				else str += "\n" + itemname;
			}

			info.entryText= str;
		}

		protected override Texture GetItemPicture(string itemname, out bool isIcon) {
			var item= items[itemname];

			if(!item.previewError && (item.preview == null)) {
				item.preview= ThumbnailManager.GenerateThumbnail(item.previewSourcePath);
				if(item.preview == null) item.previewError= true;
			}

			if(item.previewError) {
				isIcon= true;
				return EditorGUIUtility.IconContent("console.warnicon").image;
			}else	if(item.preview == null) {
				isIcon= true;
				return EditorGUIUtility.IconContent("Texture2D Icon").image;
			}else{
				isIcon= false;
				return item.preview;
			}
		}

		protected override void RefreshAssets() {
			if(items == null) items= new Dictionary<string, ItemInfo>();
			items.Clear();

			foreach(var item in items) {
				item.Value.updated= false;
			}

			var rootPath= ScreenshotTool.rootPath;
			if(Directory.Exists(rootPath)) {
				foreach(var filepath in Directory.GetFiles(rootPath, "*.png")) {
					onScreenshotTaken(filepath);
				}
				foreach(var dirpath_ in Directory.GetDirectories(rootPath)) {
					var dirpath= dirpath_ + Path.DirectorySeparatorChar;
					foreach(var filepath in Directory.GetFiles(dirpath, "*.png")) {
						onScreenshotTaken(filepath);
					}
				}
			}

			//Clearing invalid items (maybe deleted externally)
			foreach(var item in new List<KeyValuePair<string, ItemInfo>>(items)) {
				if(!item.Value.updated) items.Remove(item.Key);
			}

			activeCategory.Clear();
			var itemsSorted=  new List<ItemInfo>(items.Values);
			itemsSorted.Sort((a,b) => a.dateTime.CompareTo(b.dateTime));
			foreach(var item in itemsSorted) {
				activeCategory.Add(item.name);
			}
		}

		protected void onScreenshotTaken(string fullpath) {
			string path= ScreenshotTool.GetScreenShotFolderPath(fullpath);
			if(path == null) return;

			var dir= Path.GetDirectoryName(path);
			ItemInfo item;
			bool processItem= false;

			if(string.IsNullOrEmpty(dir)) {
				if(!items.TryGetValue(path, out item)) {
					item= new ItemInfo();
					item.name= path;
					item.systempath= fullpath;
					item.previewSourcePath= fullpath;
					item.isDir= false;
					item.count= 1;
					processItem= true;
				}

			}else{
				//is an image in a subdirectory (should be a multi-res screenshot)
				if(items.TryGetValue(dir, out item)) {
					++item.count;
				}else{
					item= new ItemInfo();
					item.name= dir;
					item.systempath= Path.GetDirectoryName(fullpath);
					item.previewSourcePath= fullpath;
					item.isDir= true;
					item.count= 1;
					processItem= true;
				}
			}

			if(processItem) {
				var dateTime= ScreenshotTool.GetDateTimeFromPath(path);
				if(dateTime != DateTime.MinValue) {
					item.dateTime= dateTime;
					items.Add(item.name, item);
					activeCategory.Add(item.name);
				}
			}

			item.updated= true;

			Refresh();
		}

		//--------------------------------
		protected override GenericMenu CreateContextMenu() {
			var menu= new GenericMenu();

			bool canOpen= false;
			bool canShow= false;

			var list= new List<string>(selectedItems);
			if((overAsset != null) && !list.Contains(overAsset)) list.Add(overAsset);

			if(list.Count == 0) return null;

			if(list.Count <= 1) {
				if(overAsset == null) return null;
				canShow= true;
				var item= items[overAsset];
				if(!item.isDir) canOpen= true;
			}

			if(!canOpen) {
				menu.AddDisabledItem(new GUIContent("Open"));

			}else{
				menu.AddItem(new GUIContent("Open"), false, OpenInSystem, list[0]);
			}

			if(!canShow) {
				menu.AddDisabledItem(new GUIContent("Show in system"));

			}else{
				menu.AddItem(new GUIContent("Show in system"), false, ShowInSystem, list[0]);
			}

			menu.AddItem(new GUIContent("Delete"), false, ()=> {
				DeleteFiles(list);
			});

			return menu;
		}

		protected override void DrawToolbar(Rect rect) {
			if(Application.isPlaying) {
				if(GUILayout.Button("Take screenshot", EditorStyles.toolbarButton)) {
					ScreenshotTool.TakeSingleScreenshot();
				}

				if(GUILayout.Button("Take multishot", EditorStyles.toolbarButton)) {
					ScreenshotTool.TakeMultiScreenshot();
				}
			}

			if(GUILayout.Button("Open Folder", EditorStyles.toolbarButton)) {
				ScreenshotTool.OpenScreenshotFolder();
			}

			base.DrawToolbar(rect);
			GUILayout.FlexibleSpace();
		}

		protected override Rect DrawContents(Rect rect) {
			return base.DrawContents(rect);
		}

		protected override void onDoubleClickedItem(string name) {
			OpenInSystem(name);
		}

		static void OpenInSystem(object name_) {
			string name= name_ as string;
			var item= items[name];

			EditorUtility.OpenWithDefaultApp(item.systempath);
		}

		static void ShowInSystem(object name_) {
			string name= name_ as string;
			var item= items[name];

			if(item.isDir) {
				System.Diagnostics.Process.Start(item.systempath);
			}else{
				//System.Diagnostics.Process.Start("explorer.exe", "-p /select," + item.systempath);
				EditorUtility.RevealInFinder(item.systempath);
			}

		}

		static void DeleteFiles(IEnumerable<string> names) {
			foreach(var name in names) {
				if(string.IsNullOrEmpty(name)) continue;
				var item= items[name];
				if(item.isDir) {
					Directory.Delete(item.systempath, true);
				}else {
					File.Delete(item.systempath);
				}
			}
			if(instance != null) {
				instance.Refresh();
				instance.RefreshAssets();
			}
		}
	}
}