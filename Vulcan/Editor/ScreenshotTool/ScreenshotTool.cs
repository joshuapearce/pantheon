﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using UnityEditor;
using UnityEngine;

using GameView= Pantheon.UnityReflection.GameView;

namespace Pantheon {
	[InitializeOnLoad]
	public static partial class ScreenshotTool {
		static ScreenshotToolSettings settings { get { return ScreenshotToolSettings.instance; } }
		public const int MenuPriority= ToolsMenu.MenuPriority + 1;

		public static EditorManager.StringFunction onScreenshotTaken;
		private static Queue<string> onScreenshotTaken_queue= new Queue<string>();

		static List<Tuple<ScreenshotVersionHookAttribute, MemberInfo>> ScreenshotVersionAttributeList_;
		public static List<Tuple<ScreenshotVersionHookAttribute, MemberInfo>> ScreenshotVersionAttributeList {
			get {
				if(ScreenshotVersionAttributeList_ == null) {
					ScreenshotVersionAttributeList_= new List<Tuple<ScreenshotVersionHookAttribute, MemberInfo>>(Manager.GetEveryMemberAttribute<ScreenshotVersionHookAttribute>());
					ScreenshotVersionAttributeList_.RemoveAll((item)=> !item.Item1.Validate(item.Item2, false));

				}
				return ScreenshotVersionAttributeList_;
			}
		}

		public static Manager.CallbackFunction callback_OnScreenshotStarted;
		public static Manager.CallbackFunction callback_OnScreenshotFinished;

		static ScreenshotTool() {
			#if NET_4_6
			WriteTextureAsync= _WriteTextureAsync;
			useMultithreading= ()=> { return settings.multires_useMultithreading; };
			#endif

			EditorManager.RegisterModule("Screenshot Tool");
			Manager.editorUpdate += StaticUpdate;
			EditorManager.globalKeyEventListener += GlobalEvent;

			EditorApplication.playModeStateChanged += (mode)=>{
				Slideshow.interval= 0;
			};

			onScreenshotTaken_queue= new Queue<string>();
		}

		//--------------------------------
		public const string MenuName= "Tools/Screenshot/";

		static bool busy= false;

		//--------------------------------
		[MenuItem(MenuName + "Save screenshot", false, ScreenshotTool.MenuPriority + 0)]
		static void Menu_TakeScreenshotMenu() {
			TakeSingleScreenshot();
		}

		[MenuItem(MenuName + "Open Folder", false, ScreenshotTool.MenuPriority + 101)]
		public static void OpenScreenshotFolder() {
			Directory.CreateDirectory(rootPath); 
			System.Diagnostics.Process.Start(rootPath);
		}

		[MenuItem("Tools/Screenshot/Help", false, ToolsMenu.MenuPriority + 102)]
		public static void ShowHelp() {
			settings.ShowHelp();
		}

		[MenuItem(MenuName + "Settings", false, ScreenshotTool.MenuPriority + 103)]
		public static void OpenSettings() {
			settings.Display();
		}

		//--------------------------------
		//Returns the relative path if it's inside the screenshots folder, otherwise null
		public static string GetScreenShotFolderPath(string path) {
			path= Path.GetFullPath(path);
			if(!path.StartsWith(rootPath)) return null;

			path= path.Remove(0, rootPath.Length);
			path= path.Replace(Path.DirectorySeparatorChar, '/');
			return path;
		}

		public static string GetScreenshotFilepath(string path= null) {
			return GetScreenshotFilepath(path, DateTime.Now);
		}

		public static string GetScreenshotFilepath(string path, DateTime now, bool addExtension= true) {
			if(path == null) {
				path= rootPath;
			}else{
				path= path.Replace(Path.AltDirectorySeparatorChar, Path.DirectorySeparatorChar);
			}

			if(path.EndsWith(Path.DirectorySeparatorChar.ToString())) {
				path += Application.productName + " - " + now.ToString("yyyy-MM-dd - HH,mm,ss");
				Directory.CreateDirectory(Path.GetDirectoryName(path));
			}

			if(addExtension) {
				if(!path.EndsWith(".png")) path += ".png";
			}else{
				if(path.EndsWith(".png")) path= path.Remove(path.Length - 4);
			}

			return path;
		}

		//--------------------------------
		public static DateTime GetDateTimeFromPath(string path) {
			var match= Regex.Match(path, "[^\\d](\\d\\d\\d\\d)-(\\d\\d)-(\\d\\d) - (\\d\\d),(\\d\\d),(\\d\\d)[^\\d]");

			if(match.Success) {
				return new DateTime(
					int.Parse(match.Groups[1].Value),
					int.Parse(match.Groups[2].Value),
					int.Parse(match.Groups[3].Value),
					int.Parse(match.Groups[4].Value),
					int.Parse(match.Groups[5].Value),
					int.Parse(match.Groups[6].Value)
				);
			}else{
				//Debug.LogError("Could not parse filename for screenshot info: " + path);
				//return DateTime.MinValue;
				return File.GetCreationTime(rootPath + path);
			}
		}

		//--------------------------------
		public static void TakeScreenshot() {
			//GameView_Reflection.SetMode(GameViewSizeGroupType.Standalone, GameView_Reflection.selectedSizeIndex + 1);
			if(busy) return;

			if(Application.isPlaying && settings.multires_useAsDefault) {
				Multires.Activate();
			}else{
				TakeSingleScreenshot();
			}
		}

		public static void TakeSingleScreenshot() {
			TakeSingleScreenshot(null);
		}

		public static void TakeSingleScreenshot(string path) {
			if(busy) return;

			if(Application.isPlaying) {
				if(path == null) {
					path= GetScreenshotFilepath(path, DateTime.Now, false);
					path += "[" + GameView.CurrentMode.safename + "].png";
				}
				Manager.StartCoroutine(TakeSceneScreenshot_Coroutine(path));
			}else{
				TakeEditorScreenshot(null);
			}
		}

		[MenuItem(MenuName + "Save multires screenshot", true)]
		static bool TakeMultiScreenshot_validate() {
			return Application.isPlaying;
		}

		[MenuItem(MenuName + "Save multires screenshot", false, ScreenshotTool.MenuPriority + 1)]
		public static void TakeMultiScreenshot() {
			TakeMultiScreenshot(null);
		}

		public static void TakeMultiScreenshot(string path) {
			if(busy) return;
			if(!Application.isPlaying) return;

			Multires.Activate();
		}

		static IEnumerator TakeSceneScreenshot_Coroutine(string path= null) {
			yield return TakeIndividualSceneScreenshot_Coroutine(path);

			if(settings.EnableHooks) {
				if(path.EndsWith(".png")) path= path.Remove(path.Length - 4);
				if(settings.multires_useSubdirectories) {
					var dirpath= path += " - options";
					Directory.CreateDirectory(dirpath);
					path= dirpath + Path.DirectorySeparatorChar + Path.GetFileName(path);
				}

				foreach(var e in ScreenshotVersionAttributeList) {
					if(e.Item1 != null) {
						foreach(var result in e.Item1.Activate(e.Item2 as MethodInfo)) {
							var name= Regex.Replace(result, "[?*\\/|:<>]","-");
							yield return TakeIndividualSceneScreenshot_Coroutine(path + " - " + name + ".png");
						}
					}
				}
			}
		}

		static IEnumerator TakeIndividualSceneScreenshot_Coroutine(string path= null) {
			if(!Application.isPlaying) {
				throw new InvalidOperationException();
			}

			if(path == null) {
				path= GetScreenshotFilepath(path, DateTime.Now, false);
				path += "[" + GameView.CurrentMode.safename + "].png";
			}

			if(!path.EndsWith(".png")) path += ".png";

			if(callback_OnScreenshotStarted != null) callback_OnScreenshotStarted();

			yield return new WaitForEndOfFrame();
			var texture= ScreenCapture.CaptureScreenshotAsTexture();
			if(settings.FillAlphaChannel) {
				if((texture.mipmapCount == 1) && (texture.format == TextureFormat.RGBA32)) {
					var bytes= texture.GetRawTextureData();
					for(int i= 3; i<bytes.Length; i += 4) {
						bytes[i]= 255;
					}

				}else{
					var pixels= texture.GetPixels32();
					for(int i= 0; i<pixels.Length; i++) {
						pixels[i].a= 255;
					}
					texture.SetPixels32(pixels);
				}
			}

			WriteTexture(texture, path, settings.multires_useMultithreading, ReportScreenshot);
			//ReportScreenshot(texture, path);
			if(callback_OnScreenshotFinished != null) callback_OnScreenshotFinished();

			//Application.CaptureScreenshot(path);
			//WriteTexture(CaptureSceneNow(), path, true, ReportScreenshot);
			yield return null;
		}

		//------------------------------
		public static bool TakeEditorScreenshot(string path= null) {
			return TakeEditorScreenshot(SceneView.lastActiveSceneView, path); 
		}

		public static bool TakeEditorScreenshot(EditorWindow panel, string path= null) {
			if(panel == null) panel= EditorWindow.focusedWindow;
			if(panel == null) panel= EditorWindow.mouseOverWindow;
			if(panel == null) panel= SceneView.lastActiveSceneView;

			if(path == null) {
				path= GetScreenshotFilepath(path, DateTime.Now, false);
				string name= null;
				if(panel.titleContent != null) name= panel.titleContent.text;
				if(string.IsNullOrEmpty(name)) name= panel.GetType().Name;

				path += " [Editor - " + name + "].png";
			}

			var texture= CaptureEditorNow(panel);
			if(texture == null) {
				return false;
			}else{
				WriteTexture(texture, path, true, ReportScreenshot);
				return true;
			}
		}

		//--------------------------------
		static void ReportScreenshot(Texture texture, string path) {
			if(settings.doLogScreenshotPaths) Debug.Log("Screenshot taken: " + path);
			if(settings.doNotify) EditorManager.Notify("Screenshot taken");

			if(onScreenshotTaken_queue != null) {
				lock(onScreenshotTaken_queue) {
					onScreenshotTaken_queue.Enqueue(path);
				}
			}
		}

		//------------------------------
		static void CaptureScene(string path, bool async= true) {
			CaptureSceneTexture((texture)=> {
				WriteTexture(texture, path, async);
			});
		}

		static void CaptureSceneTexture(Action<Texture2D> callback) {
			Manager.StartCoroutine(CaptureSceneNow(callback));
		}

		static IEnumerator CaptureSceneNow(Action<Texture2D> callback) {
			yield return new WaitForEndOfFrame();
			Texture2D tex= new Texture2D(Screen.width, Screen.height, TextureFormat.RGBA32, false);
			tex.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0, false);
			callback(tex);
		}

		#if NET_4_6
		struct AsyncWorkerInfo {
			public byte[] data;
			public int width;
			public int height;
			public string tempPath;
			public string filePath;
			public bool highCompression;
			public bool fillAlphaChannel;
			public ReportCallback onComplete;
		}

		public static async void _WriteTextureAsync(Texture2D tex, string filepath, ReportCallback onComplete= null) {
			if(tex == null) return;

			if(settings.FastestImplementation && (tex.format == TextureFormat.RGBA32) && FreeImage.isAvailable) {
				try {
					byte[] data= tex.GetRawTextureData();

					ThreadPool.QueueUserWorkItem(
						WriteTextureAsync_ThreadWorker,
						new AsyncWorkerInfo {
							data= data,
							width= tex.width,
							height= tex.height,
							filePath= filepath,
							tempPath= Path.GetFullPath(FileUtil.GetUniqueTempPathInProject() + ".png"), //avoiding race conditions while the file is being written
							highCompression= settings.Png_HighCompression,
							fillAlphaChannel= settings.FillAlphaChannel,
							onComplete= onComplete
						}
					);

				}catch(Exception e) {
					Debug.LogError("ScreenshotTool.WriteTextureAsync threw an exception");
					Debug.LogException(e);
				}

			}else{
				if(settings.FillAlphaChannel) {
					var data= tex.GetRawTextureData();
					for(int i= 3; i<data.Length; i += 4) {
						data[i]= 255;
					}
				}

				var pngData= tex.EncodeToPNG();

				string tempPath= FileUtil.GetUniqueTempPathInProject() + ".png"; //avoiding race conditions while the file is being written
				using(FileStream sourceStream = new FileStream(tempPath, FileMode.Create, FileAccess.Write, FileShare.None, 32768, true)) {
					await sourceStream.WriteAsync(pngData, 0, pngData.Length);
					File.Move(tempPath, filepath);
				}

				if(onComplete != null) onComplete(tex, Path.GetFullPath(filepath));
			}
		}

		static void WriteTextureAsync_ThreadWorker(object stateObj) {
			var args= (AsyncWorkerInfo)stateObj;

			//swapping red and blue channels, because FreeImage is a bit wrongheaded about endianness
			//This also makes the original texture garbage, but we don't care
			//We do two versions, for efficiency

			var data= args.data;

			if(args.fillAlphaChannel) {
				for(int i= 0; i<data.Length; i += 4) {
					byte temp= data[i];
					data[i]= data[i+2];
					data[i+2]= temp;
					data[i+3]= 255;
				}
			}else{
				for(int i= 0; i<data.Length; i += 4) {
					byte temp= data[i];
					data[i]= data[i+2];
					data[i+2]= temp;
				}
			}


			var handle= FreeImage.FreeImage_ConvertFromRawBits(
				data,
				args.width,
				args.height, 
				args.width * 4,
				32,
				0, 0, 0, //the masks are only used for 16bit images
				false
			);
			
			try {
				FreeImage.FreeImage_Save(FreeImage.FREE_IMAGE_FORMAT.FIF_PNG, handle, args.tempPath, args.highCompression ? 9 : 1);
				File.Move(args.tempPath, args.filePath);
			}catch(Exception e) {
				Debug.LogException(e);
			}
			FreeImage.FreeImage_Unload(handle);

			if(args.onComplete != null) {
				Manager.runOnce += ()=> args.onComplete(null, Path.GetFullPath(args.filePath));
			}
		}
		#endif

		//--------------------------------
		static class Slideshow {
			static int interval_= 0;

			[SelfInitializing] public static int interval {
				get {
					return interval_;
				}

				set {
					interval_= Mathf.Max(0, value);

					UnityEditor.Menu.SetChecked(MenuName, interval > 0);
					UnityEditor.Menu.SetChecked(MenuName + "Slideshow mode/Off", interval <= 0);
					UnityEditor.Menu.SetChecked(MenuName + "Slideshow mode/5s", interval == 5);
					UnityEditor.Menu.SetChecked(MenuName + "Slideshow mode/10s", interval == 10);
					UnityEditor.Menu.SetChecked(MenuName + "Slideshow mode/30s", interval == 30);

					if(interval <= 0) {
						timer= float.NaN;
						currentDirPath= null;
					}else{
						timer= 1.0f;
						currentDirPath= GetScreenshotFilepath(null, DateTime.Now, false) + "[Slideshow]/";
					}
				}
			}

			public static float timer;
			public static string currentDirPath;

			[MenuItem(MenuName + "Slideshow mode/5s", true)]
			[MenuItem(MenuName + "Slideshow mode/10s", true)]
			[MenuItem(MenuName + "Slideshow mode/30s", true)]
			static bool menu_SlideshowMode() {
				return Application.isPlaying;
			}

			[MenuItem(MenuName + "Slideshow mode/Off", false, ScreenshotTool.MenuPriority + 20)]
			static void menu_SlideshowMode_Off() {
				interval= 0;
			}

			[MenuItem(MenuName + "Slideshow mode/5s", false, ScreenshotTool.MenuPriority + 20)]
			static void menu_SlideshowMode_5s() {
				interval= 5;
			}

			[MenuItem(MenuName + "Slideshow mode/10s", false, ScreenshotTool.MenuPriority + 20)]
			static void menu_SlideshowMode_10s() {
				interval= 10;
			}

			[MenuItem(MenuName + "Slideshow mode/30s", false, ScreenshotTool.MenuPriority + 20)]
			static void menu_SlideshowMode_30s() {
				interval= 30;
			}
		}

		//--------------------------------
		static void StaticUpdate() {
			if(Slideshow.interval > 0) {
			  Slideshow.timer -= Time.deltaTime;
				if(!busy && (Slideshow.timer <= 0) && Application.isFocused) {
					var path= GetScreenshotFilepath(Slideshow.currentDirPath, DateTime.Now);
					//TakeSingleScreenshot(path);
					CaptureScene(path, true);
					Slideshow.timer= (float)Slideshow.interval - Mathf.Abs(Slideshow.timer);
				}
			}

			if(Application.isPlaying) {
				if(settings.key_Click && (Input.GetMouseButtonDown(0) || Input.GetMouseButtonDown(1))) {
					TakeSingleScreenshot();
				}
			}

			lock(onScreenshotTaken_queue) {
				while(onScreenshotTaken_queue.Count > 0) {
					string path= onScreenshotTaken_queue.Dequeue();
					if(onScreenshotTaken != null) onScreenshotTaken(path);
				}
			}
		}

		static double GlobalEvent_printscreen_timer= -1.0f;
		static void GlobalEvent(Event e) {
			bool takeScreenshot= false;

			if((e.type == EventType.KeyUp) && (e.keyCode == KeyCode.F12) && settings.key_F12) {
				takeScreenshot= true;
			}

			//The keypress for printscreen can be unreliable (at best), so we spam-read multiple possible events and use a timer to throttle it
			
			if(
				((e.type == EventType.KeyUp) || (e.type == EventType.KeyDown)) &&
				((e.keyCode == KeyCode.SysReq) || (e.keyCode == KeyCode.Print)) &&
				settings.key_Printscreen
			){
				var now= EditorApplication.timeSinceStartup;
				if(now - GlobalEvent_printscreen_timer > 1.0f) {
					GlobalEvent_printscreen_timer= now;
					takeScreenshot= true;
				}
			}

			if(takeScreenshot) {
				var window= EditorWindow.focusedWindow;
				if(Application.isPlaying) {
					if(settings.doForceGameFocusInPlay || (window == null) || GameView.t_GameView.IsAssignableFrom(window.GetType())) {
						TakeScreenshot();
						return;
					}
				}
				TakeEditorScreenshot(EditorWindow.focusedWindow);
			}
		}


	}
}