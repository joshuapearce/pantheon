﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

using UnityEditor;
using UnityEngine;

using ReorderableList = UnityEditorInternal.ReorderableList;

namespace Pantheon {
	public class PlayerPrefsPanel:
		VulcanWindow
	{
		public static void ShowWindow() {
			VulcanWindowManager.OpenWindow<PlayerPrefsPanel>().Focus();
		}

		[NonSerialized] public bool needsSorting= false;
		[NonSerialized] public bool hasInvalidString= false;

		enum ValueType {
			Integer,
			Float,
			String
		};

		class Item {
			public bool isValid { get { return !string.IsNullOrEmpty(name); } }

			public string name;
			public object value= null;
			public double timeOfChange= -1000;

			ValueType valueType_;
			public ValueType valueType {
				get { return valueType_; }
				set {
					var newType= value;
					if(newType == valueType_) return;

					string value_str= (this.value == null) ? "" : this.value.ToString();
					double value_f= 0; double.TryParse(value_str, out value_f);

					switch(newType) {
					case ValueType.Integer:	this.value= (int)value_f; break;
					case ValueType.Float:	this.value= (float)value_f; break;
					case ValueType.String: this.value= value_str; break;
					}

					valueType_= newType;
					timeOfChange= EditorApplication.timeSinceStartup;
				}
			}

			public Item() {
				name= null;
				valueType= ValueType.String;
				value= "";
			}

			public void SaveValue() {
				if(!isValid) return;

				PlayerPrefs.DeleteKey(name); //safety in case the type changed

				switch(valueType) {
					case ValueType.Integer: PlayerPrefs.SetInt(name, (int)value); break;
					case ValueType.Float: PlayerPrefs.SetFloat(name, (float)value); break;
					case ValueType.String: PlayerPrefs.SetString(name, (string)value); break;
				}
				PlayerPrefs.Save();
			}

			public void LoadValue() {
				var newType= PlayerPrefsReader.GetValueType(name);
				if(newType == typeof(int)) valueType= ValueType.Integer;
				else if(newType == typeof(float)) valueType= ValueType.Float;
				else if(newType == typeof(string)) valueType= ValueType.String;

				switch(valueType) {
					case ValueType.Integer: {
						int i= PlayerPrefs.GetInt(name);
						if(i != (int)value) {
							value= i;
							timeOfChange= EditorApplication.timeSinceStartup;
						}
						break;
					}

					case ValueType.Float: {
						float f= PlayerPrefs.GetFloat(name);
						if(f != (float)value) {
							value= f;
							timeOfChange= EditorApplication.timeSinceStartup;
						}
						break;
					}

					case ValueType.String: {
						string str= PlayerPrefs.GetString(name);
						if(str != (string)value) {
							value= str;
							timeOfChange= EditorApplication.timeSinceStartup;
						}
						break;
					}
				}
			}
		}
		List<Item> items= new List<Item>();

		ReorderableList listWidget;

		protected override void InitializePanel() {
			hasScrollingH= false;
			hasScrollingV= true;
			autoRepaintOnSceneChange= true;
			repaintOccasionallyInEdit= true;
			repaintConstantlyInPlay= true;
			defaultTitle= "Player Prefs";
			defaultIcon= EditorGUIUtility.IconContent("UnityEditor.HierarchyWindow").image;

			RefreshContents();

			listWidget= new ReorderableList(items, typeof(Item));
			listWidget.draggable= false;

			listWidget.onRemoveCallback += (list) => {
				PlayerPrefs.DeleteKey(items[list.index].name);
				items.RemoveAt(list.index);
			};

			listWidget.onAddCallback += (list) => {
				var item= new Item();
				item.timeOfChange= EditorApplication.timeSinceStartup;
				items.Add(item);
			};

			listWidget.drawHeaderCallback += (Rect rect) => {
				string str;
				if(items.Count == 0) str= "No player prefs set";
				else if(items.Count == 1) str= "1 value";
				else str= items.Count + " values";

				GUI.Label(rect, str);
			};

			listWidget.drawElementCallback += (Rect rect, int index, bool active, bool focused) => {
				var item= items[index];
				bool changed= false;

				rect.yMin += 2;
				rect.yMax -= 2;

				Rect typeRect= new Rect(rect.xMax - 60, rect.y, 60, rect.height);
				rect.xMax= typeRect.xMin - 4;

				if(string.IsNullOrEmpty(item.name)) {
					GUI.Label(rect, "Enter value name: ");
					rect.xMin += 120;
					string newname= EditorGUI.DelayedTextField(rect, "");
					if(!string.IsNullOrEmpty(newname)) {
						item.name= newname;
						changed= true;
						needsSorting= true;
					}

				}else{
					float labelWidth= EditorGUIUtility.labelWidth;
					GUI.Label(rect, new GUIContent(item.name, item.name));
					rect.xMin += labelWidth + 4;

					switch(item.valueType) {
						case ValueType.Integer: {
							var v= EditorGUI.DelayedIntField(rect, (int)item.value);
							if(v != (int)item.value) {
								item.value= v;
								changed= true;
							}
							break;
						}

						case ValueType.Float: {
							var v= EditorGUI.DelayedFloatField(rect, (float)item.value);
							if(v != (float)item.value) {
								item.value= v;
								changed= true;
							}
							break;
						}

						case ValueType.String: {
							var v_= (string)item.value;
							if(!isStringPrintable(v_)) {
								if(Event.current.type == EventType.Repaint) {
									hasInvalidString= true;
								}
								v_= getStringPrintable(v_);

								var warningLabel= new GUIContent("", "This value contains unprintable characters");
								warningLabel.image= EditorGUIUtility.IconContent("console.warnicon").image;
								if(GUI.Button(new Rect(rect.x - 16, rect.y + 1, 16, 16), warningLabel, GUIStyle.none)) {
									Debug.Log("Base 64 encoding for value of '" + item.name + "'\n" + Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes((string)item.value)));
								}
							}
							var v= EditorGUI.DelayedTextField(rect, v_);
							if(v != v_) {
								item.value= v;
								changed= true;
							}
							break;
						}
					}
				}

				var newType= (ValueType)EditorGUI.EnumPopup(typeRect, item.valueType);
				if(newType != item.valueType) {
					changed= true;
					item.valueType= newType;
				}

				if(changed) {
					item.SaveValue();
					item.timeOfChange= EditorApplication.timeSinceStartup;
				}
			};
		}

		protected override void AddItemsToMenu_(GenericMenu menu) {
			base.AddItemsToMenu_(menu);

			menu.AddItem(new GUIContent("Reset after play mode exits"), EditorManager.settings.restorePlayerPrefsAfterPlaying, ()=> {
				EditorManager.settings.restorePlayerPrefsAfterPlaying= !EditorManager.settings.restorePlayerPrefsAfterPlaying;
			});
		}

		protected override void DrawToolbar(Rect rect) {
			EditorGUI.BeginDisabledGroup(!needsSorting);
			if(GUILayout.Button("Sort", EditorStyles.toolbarButton)) {
				items.Sort((a, b)=> string.Compare(a.name, b.name, true));
			}
			EditorGUI.EndDisabledGroup();

			if(GUILayout.Button("Import", EditorStyles.toolbarButton)) {
				PlayerPrefsManager.LoadFromFile();
			}

			if(GUILayout.Button("Export", EditorStyles.toolbarButton)) {
				PlayerPrefsManager.SaveToFile();
			}
			
			if(GUILayout.Button("Clear all", EditorStyles.toolbarButton)) {
				if(EditorUtility.DisplayDialog("Confirmation dialog", "Really delete all player prefs?", "Yes", "No!")) {
					PlayerPrefsReader.FromString("", true);
					//PlayerPrefs.DeleteAll();
				}
			}


			base.DrawToolbar(rect);
		}

		static bool isCharPrintable(char c) {
			if(c <= 8) return false;
			if(c <= 10) return true;
			if(c <= 31) return false;
			if(c <= 127) return true;
			if(c <= 159) return false;
			return true;
		}

		static bool isStringPrintable(string str) {
			if(str == null) return false;

			foreach(var c in str) {
				if(!isCharPrintable(c)) return false;
			}

			return true;
		}

		static string getStringPrintable(string str) {
			StringBuilder output= new StringBuilder();

			foreach(var c in str) {
				if(isCharPrintable(c)) output.Append(c);
				else output.Append("?");
			}

			return output.ToString();
		}

		protected override Rect DrawContents(Rect rect) {
			if(hasInvalidString) {
				EditorGUILayout.HelpBox("One or more of the string values has unprintable characters\nEditing them here will not preserve their values", MessageType.Warning);
				rect.y += GUILayoutUtility.GetLastRect().height;

				if(Event.current.type == EventType.Repaint) {
					hasInvalidString= false;
				}
			}

			float contentHeight= listWidget.GetHeight();
			if(contentHeight > rect.height) rect.width -= GUI.skin.verticalScrollbar.fixedWidth;

			if(EditorManager.settings.restorePlayerPrefsAfterPlaying) {
				if(!GUILayout.Toggle(true, new GUIContent("Reset PlayerPrefs after play mode ends", "If activated, this will cause the PlayerPrefs to be restored to the state they had before play mode started, once play mode ends"))) {
					EditorManager.settings.restorePlayerPrefsAfterPlaying= false;
				}

				GUILayout.Space(10);
			}


			listWidget.DoList(rect);
			GUILayout.Space(listWidget.GetHeight());

			//if(count == 0) {
			//	GUILayout.Label("No preferences set");
			//}

			return rect;
		}

		protected override void WindowUpdate() {
			RefreshContents();
		}


		protected override void RefreshContents() {
			var existingitems= new Dictionary<string, Item>();
			foreach(var item in items) {
				if(!item.isValid) continue;
				existingitems.Add(item.name, item);
			}

			var newnames_sorted= PlayerPrefsReader.allNames_sorted;
			var newnames_lookup= new HashSet<string>(PlayerPrefsReader.allNames_sorted);

			items.RemoveAll((item)=> {
				if(!item.isValid) return false;
				if(newnames_lookup.Contains(item.name)) return false;
				return true;
			});

			foreach(var name in newnames_sorted) {
				Item item;
				if(!existingitems.TryGetValue(name, out item)) {
					item= new Item();
					items.Add(item);

					item.name= name;
					needsSorting= true;

				}
				item.LoadValue();
			}

			if(existingitems.Count == 0) needsSorting= false;
		}
	}
}