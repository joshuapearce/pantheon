﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;

using UnityEditor;
using UnityEngine;

namespace Pantheon {
	[InitializeOnLoad]
	public static class PlayerPrefsManager {
		static bool enableRestoreCheck { get { return false; } }

		static PlayerPrefsManager() {
			EditorApplication.playModeStateChanged += playModeStateChanged;
			Manager.editorRunOnce += Initialize;
		}

		[ConditionalMenuItem("Tools/Pantheon/Player Prefs/Viewer", ToolsMenu.MenuPriority)]
		[ConditionalMenuItem("Window/General/Player Prefs", ToolsMenu.GeneralMenuPriority)]
		static void ShowWindow() {
			PlayerPrefsPanel.ShowWindow();
		}

		//[MenuItem("Tools/Pantheon/Player Prefs/Quicksave", false, ToolsMenu.MenuPriority + 0)]
		static void QuickSave() {
			PlayerPrefsReader.ToFile(quickSavePath);
		}

		[MenuItem("Tools/Pantheon/Player Prefs/Quickload", true)]
		static bool QuickLoad_Validator() {
			return File.Exists(quickSavePath);
		}

		[MenuItem("Tools/Pantheon/Player Prefs/Quickload", false, ToolsMenu.MenuPriority + 20)]
		static void QuickLoad() {
			if(File.Exists(quickSavePath)) {
				PlayerPrefsReader.FromFile(quickSavePath, true);
			}
		}

		[MenuItem("Tools/Pantheon/Player Prefs/Restore previous", true)]
		static bool RestorePrevious_Validator() {
			return File.Exists(previousStatePath) || File.Exists(playStartedPath) || File.Exists(editRestorePath);
		}

		[MenuItem("Tools/Pantheon/Player Prefs/Restore previous", false, ToolsMenu.MenuPriority + 20)]
		static void RestorePrevious() {
			if(File.Exists(previousStatePath)) {
				PlayerPrefsReader.FromFile(editRestorePath, true);
				EditorUtility.DisplayDialog("Previous player prefs restored", "Players prefs were loaded from the previous state before the most recent load operation. Restoring again would return them to what they were before this operation.", "OK");
			}else if(File.Exists(editRestorePath)) {
				PlayerPrefsReader.FromFile(editRestorePath, true);
				EditorUtility.DisplayDialog("Previous player prefs restored", "Players prefs were loaded from the state they had before playing the most recent time.", "OK");
			}else{
				PlayerPrefsReader.FromFile(playStartedPath, true);
				EditorUtility.DisplayDialog("Previous player prefs restored", "Players prefs were loaded from the state they had at the start of playing the most recent time.", "OK");
			}
		}

		[MenuItem("Tools/Pantheon/Player Prefs/Save", false, ToolsMenu.MenuPriority + 40)]
		public static void SaveToFile() {
			string path= EditorUtility.SaveFilePanel("Save player prefs to text file", "", "PlayerPrefs.txt", "txt");
			if(!string.IsNullOrEmpty(path)) {
				PlayerPrefsReader.ToFile(path);
			}
		}

		[MenuItem("Tools/Pantheon/Player Prefs/Load", false, ToolsMenu.MenuPriority + 40)]
		public static void LoadFromFile() {
			string path= EditorUtility.OpenFilePanel("Load player prefs from text file", "", "txt");

			if(!string.IsNullOrEmpty(path)) {
				bool result= PlayerPrefsReader.FromFile(path, true);
				if(result) EditorUtility.DisplayDialog("Success", "New player prefs loaded", "ok");
				else EditorUtility.DisplayDialog("Error", "There was a problem loading the player prefs", "ok");
			}
		}

		public static string previousStatePath { get { return Path.GetFullPath(EditorManager.projectPath + "Pantheon/PlayerPrefs - previous state.txt"); } }
		static string editRestorePath { get { return Path.GetFullPath(EditorManager.projectPath + "Pantheon/PlayerPrefs - temporary - values before playing.txt"); } }
		static string playStartedPath { get { return Path.GetFullPath(EditorManager.projectPath + "Pantheon/PlayerPrefs - started play mode.txt"); } }
		static string playFinishedPath { get { return Path.GetFullPath(EditorManager.projectPath + "Pantheon/PlayerPrefs - finished play mode.txt"); } }
		static string quickSavePath { get { return Path.GetFullPath(EditorManager.projectPath + "Pantheon/PlayerPrefs - quicksave.txt"); } }

		static void Initialize() {
			if(enableRestoreCheck && !Application.isPlaying && File.Exists(editRestorePath)) {
				if(EditorUtility.DisplayDialog(
					"Pantheon: Orphaned PlayerPrefs state detected",
					"A temporary PlayerPrefs file was found, which means Unity may have crashed while a game was playing.\n"+
					"Do you want to restore the PlayerPrefs state which the editor had before play mode was started?",
					"Yes", "No, leave PlayerPrefs as they are"
				)) {
					PlayerPrefsReader.FromFile(editRestorePath, true);
				}
				File.Delete(editRestorePath);

				//This doesn't fire when play mode is active as of Unity 2019.2
				//Disabled entirely to prevent unexpected "features"
				//EditorApplication.quitting += ()=> {
				//	if(Application.isPlaying) {
				//		OnPlayStopped();
				//	}
				//};
			}
		}

		static void playModeStateChanged(PlayModeStateChange state) {
			switch(state) {
			case PlayModeStateChange.EnteredPlayMode:
				PlayerPrefsReader.ToFile(playStartedPath);
				break;

			case PlayModeStateChange.ExitingPlayMode:
				OnPlayStopped();
				break;

			case PlayModeStateChange.ExitingEditMode:
				PlayerPrefsReader.ToFile(editRestorePath);
				break;
			}
		}

		static void OnPlayStopped() {
			PlayerPrefsReader.ToFile(playFinishedPath);

			if(EditorManager.settings.restorePlayerPrefsAfterPlaying) {
				if(File.Exists(editRestorePath)) {
					PlayerPrefsReader.FromFile(editRestorePath, true);
				}
			}
			File.Delete(editRestorePath);
		}
	}
}