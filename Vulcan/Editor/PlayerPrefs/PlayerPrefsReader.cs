﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;

using System.Runtime.InteropServices;

using UnityEngine;
using System.Text;
using System.IO;

#pragma warning disable 0162

/*
  Note: While this script can work outside an editor folder, this is currently
	completely unsupported, incomplete, and should be treated as completely untested.
	In the future this may be officially supported, either completely, or just
	for debugging.

	2019-08-30
*/

namespace Pantheon {
	public static class PlayerPrefsReader {
		static bool gaveWarning;

		static PlayerPrefsReader() {
			ignoredNames= new HashSet<string>();
			ignoredNames.Add("UnityGraphicsQuality");
		}

		public static HashSet<string> ignoredNames;

		public static IEnumerable<string> allNames {
			get {
				if(Manager.RunningOnWindows()) {
					string keyname= "Software\\";
					#if UNITY_EDITOR
						keyname += "Unity\\UnityEditor\\";
					#endif
					keyname += Application.companyName + "\\" + Application.productName;
					var key= Microsoft.Win32.Registry.CurrentUser.OpenSubKey(keyname);

					if(key != null) {
						foreach(var str in key.GetValueNames()) {
							var name= ProcessValueName(str);
							if(name != null) yield return name;
						}
					}else{
						throw new InvalidOperationException("There was an error finding the registry keys for player prefs");
					}
					yield break;

				}else if(Manager.RunningOnMacOS()) {
					string filepath= Path.Combine(
						Environment.GetFolderPath(Environment.SpecialFolder.Personal),
						"Library/Preferences/unity." + Application.companyName + "." + Application.productName + ".plist"
					);
					filepath= Path.GetFullPath(filepath);

					var dict= (Dictionary<string, object>)ThirdParty.PlistCS.Plist.readPlist(filepath);

					foreach(var value in dict) {
						var name= ProcessValueName(value.Key);
						if(name != null) yield return name;
					}
					yield break;

				}else if(Manager.RunningOnLinux()) {
					throw new NotImplementedException();

					//This line is untested
					string filepath= Path.GetFullPath(Path.Combine(
						Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
						"unity3d/" + Application.companyName + "/" + Application.productName + ".xml")
					);

					//this line is definitely wrong, but all the Unity documents say for linux is "can be found in", not even the file type, so I couldn't write it yet
					var dict= (Dictionary<string, object>)ThirdParty.PlistCS.Plist.readPlist(filepath);

					foreach(var value in dict) {
						var name= ProcessValueName(value.Key);
						if(name != null) yield return name;
					}
					yield break;

				}else{
					throw new NotImplementedException();

					#if UNITY_ANDROID
					{
						//Not tested enough
						if(!gaveWarning) {
							Debug.LogWarning("Reading player prefs directly is only for debug purposes. The device must be rooted, and external file access must be enabled for the app.");
							gaveWarning= true;
						}

						string filepath= "file:///data/data/" + Application.identifier + "/shared_prefs/" + Application.identifier + ".xml";
						if(!File.Exists(filepath)) {
							Debug.LogError("Unable to read player prefs directly");
						}else{
							Debug.Log(File.ReadAllText(filepath));
						}
						yield break;
					}
					#elif UNITY_IOS
					{
						//Not tested
						if(!gaveWarning) {
							Debug.LogWarning("Reading player prefs directly is only for debug purposes");
							gaveWarning= true;
						}
					}
					#else
					{
						throw new NotImplementedException();
					}
					#endif
				}
			}
		}

		public static List<string> allNames_sorted {
			get {
				var list= new List<string>();
				foreach(var name in allNames) list.Add(name);
				list.Sort();
				return list;
			}
		}

		public static Type GetValueType(string name) {
			if(!PlayerPrefs.HasKey(name)) return null;

			//To feigure out if the value is a specific type, we pass two different default values.
			//If the result is equal *both* times, we know it's not really that type at all

			if((PlayerPrefs.GetFloat(name, 1.0f) != 1.0f) || (PlayerPrefs.GetFloat(name, 2.0f) != 2.0f)) return typeof(float);
			if((PlayerPrefs.GetInt(name, 1) != 1) || (PlayerPrefs.GetInt(name, 2) != 2)) return typeof(int);
			if((PlayerPrefs.GetString(name, "1") != "1") || (PlayerPrefs.GetString(name, "2") != "2")) return typeof(string);

			throw new NotImplementedException("Unknown player pref type for '" + name + "'");
		}

		static string ProcessValueName(string name) {
			if(name.StartsWith("unity.")) return null;

			var match= Regex.Match(name, "^(.*)_h\\d+$");
			string result;
			if(match.Success) result= match.Groups[1].Value;
			else result= name;

			if(ignoredNames.Contains(result)) return null;
			return result;
		}

		public new static string ToString() {
			var str= new StringBuilder();
			bool first= true;
			foreach(var name in allNames) {
				if(first) first= false;
				else str.Append("\n");
				str.Append(name);
				str.Append("= ");
				str.Append(PlayerPrefs.GetString(name));
			}

			return str.ToString();
		}

		public static bool ToFile(string path) {
			var str= ToString();
			if(str == null) return false;
			File.WriteAllText(path, str);
			return true;
		}

		public static bool FromString(string str, bool clear= false) {
			if(str == null) {
				Debug.LogError("str argument was null");
				return false;
			}

			File.WriteAllText(PlayerPrefsManager.previousStatePath, ToString());
			Debug.Log("Existing PlayerPrefs state was written to " + PlayerPrefsManager.previousStatePath);

			if(clear) PlayerPrefs.DeleteAll();
			if(str != "") foreach(var line in str.Split('\n')) {
				var match= Regex.Match(line, "^([^=]+)=(.*)$");
				if(!match.Success) {
					Debug.LogError("There was an error parsing '" + line + "'; Player prefs may now be invalid.");
					return false;
				}
				var name= match.Groups[1].Value.Trim();
				var value= match.Groups[2].Value.Trim();
				PlayerPrefs.SetString(name, value);
			}

			return true;
		}

		public static bool FromFile(string path, bool clear) {
			if(!File.Exists(path)) {
				Debug.LogError("File '" + path + "' does not exist");
				return false;
			}

			string str= File.ReadAllText(path);
			return FromString(str, clear);
		}
	}
}