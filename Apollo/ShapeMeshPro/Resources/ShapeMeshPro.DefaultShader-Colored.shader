﻿Shader "ShapeMeshPro/DefaultShader-Colored"
{
	Properties
	{
			_Color("Tint", Color) = (1,1,1,1)

			[MaterialToggle] PixelSnap("Pixel snap", Float) = 0
			[HideInInspector] _RendererColor("RendererColor", Color) = (1,1,1,1)
			[HideInInspector] _Flip("Flip", Vector) = (1,1,1,1)

      // required for UI.Mask
      [HideInInspector] _StencilComp ("Stencil Comparison", Float) = 8
      [HideInInspector] _Stencil ("Stencil ID", Float) = 0
      [HideInInspector] _StencilOp ("Stencil Operation", Float) = 0
      [HideInInspector] _StencilWriteMask ("Stencil Write Mask", Float) = 255
      [HideInInspector] _StencilReadMask ("Stencil Read Mask", Float) = 255
      [HideInInspector] _ColorMask ("Color Mask", Float) = 15
	}

	SubShader
		{
				Tags
				{
						"Queue" = "Transparent"
						"IgnoreProjector" = "True"
						"RenderType" = "Transparent"
						"PreviewType" = "Plane"
						"CanUseSpriteAtlas" = "True"
				}

				Cull Off
				Lighting Off
				ZWrite Off
				Blend One OneMinusSrcAlpha

         // required for UI.Mask
         Stencil
         {
             Ref [_Stencil]
             Comp [_StencilComp]
             Pass [_StencilOp] 
             ReadMask [_StencilReadMask]
             WriteMask [_StencilWriteMask]
         }
         ColorMask [_ColorMask]

				Pass
				{
				CGPROGRAM
					#pragma vertex SpriteVert
					#pragma fragment frag
					#pragma target 2.0
					#pragma multi_compile_instancing
					#pragma multi_compile_local _ PIXELSNAP_ON
					#pragma multi_compile _ ETC1_EXTERNAL_ALPHA
					#include "UnitySprites.cginc"

					fixed4 frag(v2f v): SV_Target	{
						fixed4 c= v.color;
						c.rgb *= c.a;
						return c;
					}

				ENDCG
				}
		}
}
