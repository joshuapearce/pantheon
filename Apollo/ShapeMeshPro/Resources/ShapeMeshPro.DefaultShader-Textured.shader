﻿Shader "ShapeMeshPro/DefaultShader-Textured"
{
	Properties
	{
			[PerRendererData] _MainTex("Sprite Texture", 2D) = "white" {}
			_Color("Tint", Color) = (1,1,1,1)

			[MaterialToggle] PixelSnap("Pixel snap", Float) = 0
			[HideInInspector] _RendererColor("RendererColor", Color) = (1,1,1,1)
			[HideInInspector] _Flip("Flip", Vector) = (1,1,1,1)

      // required for UI.Mask
      [HideInInspector] _StencilComp ("Stencil Comparison", Float) = 8
      [HideInInspector] _Stencil ("Stencil ID", Float) = 0
      [HideInInspector] _StencilOp ("Stencil Operation", Float) = 0
      [HideInInspector] _StencilWriteMask ("Stencil Write Mask", Float) = 255
      [HideInInspector] _StencilReadMask ("Stencil Read Mask", Float) = 255
      [HideInInspector] _ColorMask ("Color Mask", Float) = 15
	}

	SubShader
		{
				Tags
				{
						"Queue" = "Transparent"
						"IgnoreProjector" = "True"
						"RenderType" = "Transparent"
						"PreviewType" = "Plane"
						"CanUseSpriteAtlas" = "True"
				}

				Cull Off
				Lighting Off
				ZWrite Off
				Blend One OneMinusSrcAlpha

         // required for UI.Mask
         Stencil
         {
             Ref [_Stencil]
             Comp [_StencilComp]
             Pass [_StencilOp] 
             ReadMask [_StencilReadMask]
             WriteMask [_StencilWriteMask]
         }
         ColorMask [_ColorMask]

				Pass
				{
				CGPROGRAM
					#pragma vertex vert
					#pragma fragment frag
					#pragma target 2.0
					#pragma multi_compile_instancing
					#pragma multi_compile_local _ PIXELSNAP_ON
					#pragma multi_compile _ ETC1_EXTERNAL_ALPHA
					#include "UnitySprites.cginc"

					//intrinsic isnan is unreliable!
					float isnan(float f) {
						return (f < 0.0 || f > 0.0 || f == 0.0) ? 0 : 1;
					}

					v2f vert(
						appdata_t IN,
						in float2 uv1: TEXCOORD1,
						in float2 uv2: TEXCOORD2,
						out float4 spriteCoords: TEXCOORD1, 
						out float2 flags: TEXCOORD2
					) {
						v2f v= SpriteVert(IN);

						if((uv2.x == 0) && (uv2.y == 0)) uv2.xy= float2(1,1); //in case the Unity context isn't passing the extra channels, we set some useful defaults

						spriteCoords.xy= uv1.xy;
						spriteCoords.zw= uv2.xy;
						
						flags[0]= !isnan(IN.texcoord.x); //flag for ignoring any texture passed, for batching efficiency
						flags[1]= 0; //unused

						return v;
					}

					fixed4 frag(
						v2f v,
						in float4 spriteCoords: TEXCOORD1, 
						in float2 flags: TEXCOORD2 
					): SV_Target
					{
						float textureWeight= flags[0];

						float2 uv= frac(v.texcoord.xy);
						uv= lerp(spriteCoords.xy, spriteCoords.zw, uv);

						fixed4 c= textureWeight ? SampleSpriteTexture(uv) : fixed4(1,1,1,1);
						c *= v.color;
						c.rgb *= c.a;
						return c;
					}

				ENDCG
				}
		}
}
