﻿using UnityEditor;
using UnityEngine;

namespace Pantheon {
	public static class ShapeMeshClipboardUtility {
		public static void CopyStylesToClipboard (ShapeMeshPro.Data data) {
			var json = JsonUtility.ToJson (data);
			EditorGUIUtility.systemCopyBuffer = "[SMP.StyleData]" + json;
		}
		public static bool ClipboardContainsStyleData() => !string.IsNullOrEmpty(EditorGUIUtility.systemCopyBuffer) && EditorGUIUtility.systemCopyBuffer.IndexOf("[SMP.StyleData]") == 0;

		public static ShapeMeshPro.Data GetClipboardAsStyleData () {
			var json = EditorGUIUtility.systemCopyBuffer.Substring (15);
			try {
				return JsonUtility.FromJson<ShapeMeshPro.Data> (json);
			} catch {
				Debug.LogWarning ("Error pasting style data");
			}
			return null;
		}
	}

}