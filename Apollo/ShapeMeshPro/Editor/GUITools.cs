﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Pantheon {
	public static class GUITools {

		public static GUIStyle foldoutHeader;
		public static GUIStyle header;
		public static GUIStyle dropbox;
		public static GUIStyle dropboxText;
		public static GUIStyle dropboxTextOver;
		public static GUIStyle iconButtonStyle;
		public static GUIStyle titleStyle;
		public static GUIStyle container;
		public static GUIStyle thumbnailButton;
		public static GUIStyle label;
		public static GUIStyle centerLabel;

		public static Texture2D optionsButton;
		public static Texture2D addButton;

		public static Color buttonColor;
		public static Color selectedButtonColor;

		static bool initialized;

		private static Texture2D staticRectTexture;
		private static GUIStyle staticRectStyle;

		public static void Init () {
			if (initialized) return;
			initialized = true;

			foldoutHeader = new GUIStyle ("ShurikenModuleTitle") {
				font = new GUIStyle ("Label").font,
					border = new RectOffset (15, 7, 4, 4),
					fixedHeight = 22,
					contentOffset = new Vector2 (22f, -2f),
			};

			header = new GUIStyle ("ShurikenModuleTitle") {
				font = new GUIStyle ("Label").font,
					border = new RectOffset (4, 4, 4, 4),
					fixedHeight = 22,
					contentOffset = new Vector2 (22f, -2f),
			};

			iconButtonStyle = new GUIStyle ("Button") {
				padding = new RectOffset (24, 6, 2, 3),
					alignment = TextAnchor.MiddleLeft
			};

			if (EditorGUIUtility.isProSkin) {
				buttonColor = new Color (0.25f, 0.25f, 0.25f);
				selectedButtonColor = new Color (0.4f, 0.4f, 0.4f);
			} else {
				buttonColor = new Color (0.75f, 0.75f, 0.75f);
				selectedButtonColor = new Color (0.4f, 0.4f, 0.4f);
			}

			label = new GUIStyle ("Label") {
				alignment = TextAnchor.MiddleLeft
			};

			centerLabel = new GUIStyle ("Label") {
				alignment = TextAnchor.MiddleCenter
			};

			thumbnailButton = new GUIStyle {
				padding = new RectOffset (0, 0, 0, 0),
					margin = new RectOffset (2, 2, 2, 2)
			};

			titleStyle = new GUIStyle ("BoldLabel") {
				fontSize = 12
			};

			container = new GUIStyle ("ShurikenEffectBg") {
				padding = new RectOffset (2, 2, 2, 2)
			};

			optionsButton = LoadEditorAsset<Texture2D> ("Builtin Skins/DarkSkin/Images/pane options.png", "Builtin Skins/LightSkin/Images/pane options.png");
			addButton = LoadEditorAsset<Texture2D> ("Builtin Skins/DarkSkin/Images/shurikenplus.png", "Builtin Skins/LightSkin/Images/shurikenplus.png");

		}

		private static Texture2D ColorTexture (Color color) {
			var ret = new Texture2D (1, 1, TextureFormat.RGBA32, false);
			ret.SetPixel (0, 0, color);
			ret.Apply ();
			return ret;
		}

		public static void GUIDrawRect (Rect position, Color color) {
			if (staticRectTexture == null) {
				staticRectTexture = new Texture2D (1, 1);
			}

			if (staticRectStyle == null) {
				staticRectStyle = new GUIStyle ();
			}

			staticRectTexture.SetPixel (0, 0, color);
			staticRectTexture.Apply ();

			staticRectStyle.normal.background = staticRectTexture;

			GUI.Box (position, GUIContent.none, staticRectStyle);

		}

		public static T LoadAsset<T> (string proPath, string normalPath = null) where T : Object {
			if (normalPath == null) normalPath = proPath;
			if (EditorGUIUtility.isProSkin) {
				return AssetDatabase.LoadAssetAtPath<T> (proPath);
			} else {
				return AssetDatabase.LoadAssetAtPath<T> (normalPath);
			}
		}

		public static T LoadEditorAsset<T> (string proPath, string normalPath = null) where T : Object {
			if (normalPath == null) normalPath = proPath;
			if (EditorGUIUtility.isProSkin) {
				return (T) EditorGUIUtility.LoadRequired (proPath);
			} else {
				return (T) EditorGUIUtility.LoadRequired (normalPath);
			}
		}

		public static bool FoldoutHeader (string title, bool unfolded, GUIStyle style, System.Action contextMenu, Color? iconColor) {
			var e = Event.current;
			if (style == null) style = foldoutHeader;

			var rect = GUILayoutUtility.GetRect (16f, 20f, style);
			GUI.Box (rect, title, style);

			var foldoutRect = new Rect (rect.x + 4f, rect.y + 2f, 13f, 13f);

			if (iconColor != null) {
				GUITools.GUIDrawRect (foldoutRect, unfolded ? Color.white : buttonColor);
				var innerRect = new Rect (foldoutRect.x + 1, foldoutRect.y + 1, foldoutRect.width - 2, foldoutRect.height - 2);
				GUITools.GUIDrawRect (innerRect, Color.Lerp ((Color) iconColor, buttonColor, unfolded ? 0 : 0.65f));
			} else {
				if (e.type == EventType.Repaint)
					new GUIStyle ("Foldout").Draw (foldoutRect, false, false, unfolded, false);
			}

			var popupRect = new Rect (rect.x + rect.width - optionsButton.width - 5f, rect.y + 6, optionsButton.width, optionsButton.height);
			popupRect = new Rect (rect.x + rect.width - optionsButton.width, rect.y + 1.5f, optionsButton.width, optionsButton.height);
			if (contextMenu != null) {
				GUI.DrawTexture (popupRect, optionsButton);
			}
			if (e.type == EventType.MouseDown) {
				if (contextMenu != null && (popupRect.Contains (e.mousePosition) || (e.button == 1 && rect.Contains (e.mousePosition)))) {
					contextMenu ();
					e.Use ();
				} else if (rect.Contains (e.mousePosition) && e.button == 0) {
					unfolded = !unfolded;
					GUI.FocusControl (null);
					e.Use ();
				}
			}

			return unfolded;
		}

		public static void Header (string title, System.Action contextMenu = null) {
			var e = Event.current;

			var rect = GUILayoutUtility.GetRect (16f, 22f, titleStyle);
			GUI.Box (rect, title, titleStyle);
		}

		public static bool ToggleButton (string text, bool selected) {
			return GUILayout.Toggle (selected, text, "Button");;
		}

		public static bool CheckButton (string text, bool selected, Color iconColor) {

			var e = Event.current;

			var rect = GUILayoutUtility.GetRect (16f, 22f, "Button");
			if (e.type == EventType.Repaint) {
				iconButtonStyle.Draw (rect, new GUIContent (text), false, false, selected, false);
			}

			var iconRect = new Rect (rect.x + 5f, rect.y + 5f, 13f, 13f);

			GUITools.GUIDrawRect (iconRect, selected ? Color.white : buttonColor);
			var innerRect = new Rect (iconRect.x + 1, iconRect.y + 1, iconRect.width - 2, iconRect.height - 2);
			GUITools.GUIDrawRect (innerRect, Color.Lerp ((Color) iconColor, buttonColor, selected ? 0 : 0.65f));

			if (e.type == EventType.MouseDown) {
				Debug.Log (rect + " " + e.mousePosition);
				if (rect.Contains (e.mousePosition) && e.button == 0) {
					selected = !selected;
					e.Use ();
				}
			}
			return selected;
		}

		public static bool FoldOut (string key, string title, System.Action show, System.Action contextMenu = null) {
			return FoldOut (key, title, 0, false, null, null, show, contextMenu);
		}
		public static bool FoldOut (string key, string title, float indent, System.Action show, System.Action contextMenu = null) {
			return FoldOut (key, title, indent, false, null, null, show, contextMenu);
		}
		public static bool FoldOut (string key, string title, bool defaultFoldout, System.Action show, System.Action contextMenu = null) {
			return FoldOut (key, title, 0, defaultFoldout, null, null, show, contextMenu);
		}
		public static bool FoldOut (string key, string title, GUIStyle headerStyle, GUIStyle boxStyle, System.Action show, System.Action contextMenu = null) {
			return FoldOut (key, title, 0, false, headerStyle, boxStyle, show, contextMenu);
		}
		public static bool FoldOut (string key, string title, float indent, GUIStyle headerStyle, GUIStyle boxStyle, System.Action show, System.Action contextMenu = null) {
			return FoldOut (key, title, indent, false, headerStyle, boxStyle, show, contextMenu);
		}
		public static bool FoldOut (string key, string title, bool defaultFoldout, GUIStyle headerStyle, GUIStyle boxStyle, System.Action show, System.Action contextMenu = null) {
			return FoldOut (key, title, 0, defaultFoldout, headerStyle, boxStyle, show, contextMenu);
		}
		public static bool FoldOut (string key, string title, float indent, bool defaultFoldout, GUIStyle headerStyle, GUIStyle boxStyle, System.Action show, System.Action contextMenu = null) {
			if (headerStyle == null) headerStyle = foldoutHeader;
			if (boxStyle == null) boxStyle = "ShurikenModuleBg";

			var unfolded = EditorPrefs.GetBool (key, defaultFoldout);

			EditorGUILayout.BeginHorizontal ();
			GUILayout.Space (indent);
			EditorGUILayout.BeginVertical (boxStyle);
			var unfoldedNew = GUITools.FoldoutHeader (title, unfolded, headerStyle, contextMenu, null);

			if (unfolded) show ();

			if (unfolded != unfoldedNew) {
				EditorPrefs.SetBool (key, unfoldedNew);
				unfolded = unfoldedNew;
			}

			EditorGUILayout.EndVertical ();
			EditorGUILayout.EndHorizontal ();

			return unfolded;
		}

		public static Color TimeColor (Color color1, Color color2) {
			return Color.Lerp (color1, color2, Mathf.Sin (Time.realtimeSinceStartup * 5f));
		}

		public static void Seperator () {
			EditorGUILayout.LabelField (GUIContent.none, GUI.skin.horizontalSlider);
		}

	}
}