﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using Object = UnityEngine.Object;

namespace Pantheon {
	[CustomEditor (typeof (ShapeMeshPro))]
	[CanEditMultipleObjects]
	public class ShapeMeshProEditor : Editor {
		public List<ShapeMeshPro> shapes;
		public List<ShapeMeshPro> templateChildren;

		public ShapeMeshProStyleEditor styleEditor;
		private SerializedProperty templateFlags;
		private SerializedProperty graphicColor;
		private SerializedProperty raycastTarget;

		private SerializedProperty shapeType;
		private SerializedProperty shapeFittingMode;
		private SerializedProperty templateGeometry;
		private SerializedProperty templateStyle;

		private bool showDefaultInspector;
		private bool editingShape;

		private List<Object> templateLookup;
		private List<string> templateStrings;

		public static List<T> FindAssetsByType<T> () where T : UnityEngine.Object {
			List<T> assets = new List<T> ();
			string[] guids = AssetDatabase.FindAssets (string.Format ("t:{0}", typeof (T)));
			for (int i = 0; i < guids.Length; i++) {
				string assetPath = AssetDatabase.GUIDToAssetPath (guids[i]);
				T asset = AssetDatabase.LoadAssetAtPath<T> (assetPath);
				if (asset != null) {
					assets.Add (asset);
				}
			}
			return assets;
		}
		public static string ConvertSlashToUnicodeSlash (string text_) {
			return text_.Replace ('/', '\u2215');
		}

		void OnEnable () {
			shapes = new List<ShapeMeshPro> ();
			foreach (var obj in targets) {
				if (obj is ShapeMeshPro) shapes.Add (obj as ShapeMeshPro);
			}

			if (targets.Length > 1) {
				templateChildren = null;
				//throw new NotImplementedException();

			} else {
				templateChildren = new List<ShapeMeshPro> ();

				for (int sceneIndex = 0; sceneIndex < SceneManager.sceneCount; sceneIndex++) {
					var scene = SceneManager.GetSceneAt (sceneIndex);

					foreach (var rootObj in scene.GetRootGameObjects ()) {
						foreach (var shape in rootObj.GetComponentsInChildren<ShapeMeshPro> ()) {
							if (shape.templateStyle == shapes[0]) templateChildren.Add (shape);
						}
					}
				}
			}

			graphicColor = serializedObject.FindProperty ("m_Color");
			raycastTarget = serializedObject.FindProperty ("m_RaycastTarget");

			var data = serializedObject.FindProperty ("_data");

			styleEditor = new ShapeMeshProStyleEditor (data, new List<Object> (targets));

			templateFlags = serializedObject.FindProperty ("_templateFlags");
			templateGeometry = serializedObject.FindProperty ("_templateGeometry");
			templateStyle = serializedObject.FindProperty ("_templateStyle");

			shapeType = serializedObject.FindProperty ("_shapeType");
			shapeFittingMode = data.FindPropertyRelative ("shapeFittingMode");

			RefreshTemplateLists ();
		}

		public override void OnInspectorGUI () {
			GUITools.Init ();

			ShapeMeshPro.ShapeEnum? shapeType = shapes[0].shapeType;
			ShapeMeshPro.TemplateFlags flags = 0;
			ShapeMeshPro.TemplateFlags flags_all = (ShapeMeshPro.TemplateFlags) ~0;

			bool changed = false;

			//--------------------------------
			//Figuring out UI states

			foreach (var shape in shapes) {
				var value = shape.templateFlags;
				flags |= value;
				flags_all &= value;

				if (shape.shapeType != shapeType) shapeType = null;
			}
			var flags_mixed = ~flags_all & flags;

			Func<ShapeMeshPro.TemplateFlags, bool?> CalcFlagState = (value) => {
				if ((flags_mixed & value) != 0) return null;
				if ((flags_all & value) != 0) return true;
				if ((flags & value) == 0) return false;
				throw new InvalidProgramException (); //This means the flag summing logic screwed up, or 0 was passed
			};

			bool showCornersUI = false;
			bool showCurvesUI = false;
			bool showMeshUI = false;
			bool showMaterials = false;
			bool showTemplateChildren = (templateChildren != null) && (templateChildren.Count > 0);
			int forceVertexCount = -1;

			switch (shapeType) {
				case null:
					forceVertexCount = 0;
					showCornersUI = true;
					showMaterials = true;
					showCurvesUI = true;
					break;

				case ShapeMeshPro.ShapeEnum.Round:
					forceVertexCount = 0;
					break;

				case ShapeMeshPro.ShapeEnum.Rectangle:
					showCornersUI = true;
					showMaterials = true;
					showCurvesUI = true;
					forceVertexCount = 4;
					break;

				case ShapeMeshPro.ShapeEnum.Polygon:
					showCornersUI = true;
					showMaterials = true;
					showCurvesUI = true;
					break;

				case ShapeMeshPro.ShapeEnum.FreeForm:
					showCornersUI = true;
					showMaterials = true;
					showCurvesUI = true;
					showMeshUI = (shapes.Count == 1);
					break;

				default:
					throw new NotImplementedException ();
			}

			//--------------------------------
			serializedObject.Update ();

			//--------------------------------
			//Open UI
			EditorGUILayout.BeginHorizontal ();
			EditorGUILayout.Space (-18, false);
			EditorGUILayout.BeginVertical ();
			//--------------------------------
			//Actual UI

			DrawGraphicEditor ();
			EditorGUILayout.BeginVertical (GUITools.container, GUILayout.ExpandHeight (false));
			GUITools.FoldOut ("Pantheon.SMP.Geometry", "Geometry", true, () => DrawGeometryEditor (shapeType));
			GUITools.FoldOut ("Pantheon.SMP.Style", "Style", true, () => DrawStyleEditor (CalcFlagState, showCornersUI, showCurvesUI, showMaterials), ShowStyleContextMenu);
			GUITools.FoldOut ("Pantheon.SMP.Advanced", "Advanced", false, DrawAdvancedEditor);

			//--------------------------------
			//Setting object values

			if (GUI.changed || changed) {
				serializedObject.ApplyModifiedProperties ();
				foreach (var shape in shapes) {
					shape.Refresh (true);
					EditorUtility.SetDirty (shape);
					var parts = shape.GetComponentsInChildren<UnityEngine.UI.Graphic> ();
					foreach (var part in parts) {
#warning hack : disable and re-enable to force a refresh
						if (part.gameObject.activeInHierarchy) {
							part.gameObject.SetActive (false);
							part.gameObject.SetActive (true);
						}
						EditorUtility.SetDirty (part);
					}
				}
			}

			//--------------------------------
			//Draw default inspector
			//Note: has to happen after ApplyModifiedProperties
			GUITools.FoldOut ("Pantheon.SMP.DefaultInspector", "Default Inspector", false, DrawDefaultEditor);

			//--------------------------------
			//Close UI
			EditorGUILayout.EndVertical ();
			EditorGUILayout.EndVertical ();
			EditorGUILayout.EndHorizontal ();
		}

		private void DrawGraphicEditor () {
			EditorGUILayout.PropertyField (graphicColor);
			if(raycastTarget != null) EditorGUILayout.PropertyField (raycastTarget);
		}

		private void DrawGeometryEditor (ShapeMeshPro.ShapeEnum? shapeType) {
			EditorGUILayout.PropertyField (this.shapeType);
			if (shapeType == ShapeMeshPro.ShapeEnum.FreeForm) {
				if (shapes.Count > 1) EditorGUI.BeginDisabledGroup (true);
				EditorGUILayout.BeginHorizontal ();
				EditorGUILayout.PrefixLabel ("Edit Shape");
				editingShape = GUITools.ToggleButton ("Edit Shape", editingShape);
				EditorGUILayout.EndHorizontal ();
				if (shapes.Count > 1) {
					EditorGUI.EndDisabledGroup ();
					EditorGUILayout.HelpBox ("Freeform shape editing is disabled when multiple objects are selected.", MessageType.Info);
				}
			} else if (shapeType == ShapeMeshPro.ShapeEnum.Polygon) {
				foreach (var shape in shapes) {
					if (shape.shapeVertexCount != shapes[0].shapeVertexCount) EditorGUI.showMixedValue = true;
				}
				EditorGUI.BeginChangeCheck ();
				var points = EditorGUILayout.IntField ("Points", shapes[0].shapeVertexCount);
				points = Mathf.Clamp (points, 3, 256);
				if (EditorGUI.EndChangeCheck ()) {
					foreach (var shape in shapes) {
						Undo.RegisterCompleteObjectUndo (shape, "Change Point Count");
						shape.shapeVertexCount = points;
					}
				}
				EditorGUI.showMixedValue = false;
			}
			EditorGUILayout.PropertyField (this.shapeFittingMode);
		}

		private void DrawStyleEditor (Func<ShapeMeshPro.TemplateFlags, bool?> CalcFlagState, bool showCornersUI, bool showCurvesUI, bool showMaterials) {
			DrawStyleTemplateSelector ();
			var style = new GUIStyle ("ShurikenModuleBg") { padding = new RectOffset (4, 4, 4, 4) };
			EditorGUILayout.BeginVertical (style);
			styleEditor.Draw (showCornersUI, showCurvesUI, showMaterials, CalcFlagState);
			EditorGUILayout.EndVertical ();
		}

		private void DrawAdvancedEditor () {
			EditorGUILayout.PropertyField (this.templateFlags);
		}

		private void DrawStyleTemplateSelector () {
			var selected = 0;

			for (var i = 0; i < templateLookup.Count; i++) {
				if (templateLookup[i] == shapes[0].templateStyle) {
					selected = i;
					break;
				}
			}

			foreach (var shape in shapes) {
				if (shape.templateStyle != shapes[0].templateStyle) {
					EditorGUI.showMixedValue = true;
					break;
				}
			}

			EditorGUILayout.BeginHorizontal ();
			EditorGUI.BeginChangeCheck ();
			selected = EditorGUILayout.Popup ("Style Template", selected, templateStrings.ToArray ());
			EditorGUI.showMixedValue = false;

			if (EditorGUI.EndChangeCheck ()) {
				foreach (var shape in shapes) {
					Undo.RegisterCompleteObjectUndo (shape, "Change Style Template");
					shape.templateStyle = templateLookup[selected];
				}
			}
			if (selected > 0) {
				if (GUILayout.Button ("Select", GUILayout.Width (50))) {
					Selection.activeObject = shapes[0].templateStyle;
				}
			}
			EditorGUILayout.EndHorizontal ();
		}

		private void RefreshTemplateLists () {
			templateStrings = new List<string> () { "None" };
			templateLookup = new List<Object> () { null };
			var templates = FindAssetsByType<ShapeMeshProTemplate> ();
			foreach (var template in templates) {
				templateStrings.Add ("Template Assets/" + template.name);
				templateLookup.Add (template);
			}

			var allShapes = Resources.FindObjectsOfTypeAll<ShapeMeshPro> ();
			foreach (var shape in allShapes) {
				if (shapes.Contains (shape)) continue;
				var circular = false;
				var i = shape;
				while (i != null) {
					if (shapes.Contains (i)) {
						circular = true;
						break;
					}
					i = i.templateStyle as ShapeMeshPro;
				}
				if (circular) {
					continue;
				}
				templateStrings.Add ("ShapeMeshPro Instances/" + shape.name);
				templateLookup.Add (shape);

			}
		}

		private void ShowStyleContextMenu () {
			var menu = new GenericMenu ();
			if (targets.Length == 1) {
				menu.AddItem (new GUIContent ("Copy style settings"), false, () => {
					ShapeMeshClipboardUtility.CopyStylesToClipboard (shapes[0].data);
				});
			} else {
				menu.AddDisabledItem (new GUIContent ("Copy style settings"), false);
			}
			if (ShapeMeshClipboardUtility.ClipboardContainsStyleData ()) {
				menu.AddItem (new GUIContent ("Paste style settings"), false, () => {
					var styleData = ShapeMeshClipboardUtility.GetClipboardAsStyleData ();
					if (styleData != null) {
						foreach (var shape in shapes) {
							Undo.RegisterCompleteObjectUndo (shape, "Paste style settings");
							shape.data = styleData.Clone ();
						}
					}
				});
			} else {
				menu.AddDisabledItem (new GUIContent ("Paste style settings"), false);
			}
			menu.AddSeparator ("");
			menu.AddItem (new GUIContent ("Create new template from style settings"), false, () => {
				var name = "StyleTemplate[" + target.name + "].asset";
				var path = EditorUtility.SaveFilePanelInProject ("Create Style Template", name, "asset", "Please enter a filename");
				if (!string.IsNullOrEmpty (path)) {
					var template = ScriptableObject.CreateInstance<ShapeMeshProTemplate> ();
					template.data = shapes[0].data;
					AssetDatabase.CreateAsset (template, path);
				}
			});
			menu.ShowAsContext ();
		}

		private void DrawDefaultEditor () {
			EditorGUILayout.BeginHorizontal ();
			EditorGUILayout.Space (0); {
				EditorGUILayout.BeginVertical ();
				DrawDefaultInspector ();
				EditorGUILayout.EndVertical ();
			}
			EditorGUILayout.EndHorizontal ();
			EditorGUILayout.Space (5);
		}
	}
}