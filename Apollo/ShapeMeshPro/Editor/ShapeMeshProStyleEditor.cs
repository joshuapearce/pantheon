﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Pantheon {
	public class ShapeMeshProStyleEditor {

		class ColorInfoProperties {
			public SerializedProperty color;
			public SerializedProperty gradientColor1;
			public SerializedProperty gradientColor2;
			public SerializedProperty curve;
			public SerializedProperty useGradient;

			public ColorInfoProperties (SerializedProperty colorInfo) {
				color = colorInfo.FindPropertyRelative ("color");
				var gradient = colorInfo.FindPropertyRelative ("gradient");
				gradientColor1 = gradient.FindPropertyRelative ("color1");
				gradientColor2 = gradient.FindPropertyRelative ("color2");
				curve = gradient.FindPropertyRelative ("curve");
				useGradient = colorInfo.FindPropertyRelative ("useGradient");
			}
		}

		private List<Object> objects;
		private List<IStyleDataProvider> dataProviders;

		private SerializedProperty shapeFittingMode;
		private SerializedProperty cornerType;
		private SerializedProperty cornerSegmentCount;
		private SerializedProperty cornerRadius;
		private SerializedProperty cornerRandomizationMin;
		private SerializedProperty cornerRandomizationMax;

		private SerializedProperty shapeCurveStrength;
		private ColorInfoProperties shapeColor;
		private SerializedProperty shapeMaterial;
		private SerializedProperty shapeTexture;

		private SerializedProperty outlineWidth;
		private SerializedProperty outlineOffset;
		private ColorInfoProperties outlineColor;
		private SerializedProperty outlineMaterial;
		private SerializedProperty outlineTexture;

		private SerializedProperty shadowOffset;
		private ColorInfoProperties shadowColor;
		private SerializedProperty shadowFuzziness;
		private SerializedProperty shadowMaterial;

		public ShapeMeshProStyleEditor (SerializedProperty data, List<Object> objects) {
			this.objects = objects;
			dataProviders = new List<IStyleDataProvider> ();
			foreach (var o in objects) dataProviders.Add (o as IStyleDataProvider);
			cornerType = data.FindPropertyRelative ("cornerType");
			cornerSegmentCount = data.FindPropertyRelative ("cornerSegmentCount");
			cornerRadius = data.FindPropertyRelative ("cornerSizeValue");
			cornerRandomizationMin = data.FindPropertyRelative ("cornerRandomizationMin");
			cornerRandomizationMax = data.FindPropertyRelative ("cornerRandomizationMax");

			shapeCurveStrength = data.FindPropertyRelative ("shapeCurveStrength");
			shapeColor = new ColorInfoProperties (data.FindPropertyRelative ("fillColor"));
			shapeMaterial = data.FindPropertyRelative ("fillMaterial");
			shapeTexture = data.FindPropertyRelative ("fillTexture");

			outlineWidth = data.FindPropertyRelative ("outlineWidth");
			outlineOffset = data.FindPropertyRelative ("outlineOffset");
			outlineColor = new ColorInfoProperties (data.FindPropertyRelative ("outlineColor"));
			outlineMaterial = data.FindPropertyRelative ("outlineMaterial");
			outlineTexture = data.FindPropertyRelative ("outlineTexture");

			shadowOffset = data.FindPropertyRelative ("shadowOffset");
			shadowColor = new ColorInfoProperties (data.FindPropertyRelative ("shadowColor"));
			shadowFuzziness = data.FindPropertyRelative ("shadowFuzziness");
			shadowMaterial = data.FindPropertyRelative ("shadowMaterial");

		}

		public void Draw (bool showCornersUI, bool showCurvesUI, bool showMaterials, Func<ShapeMeshPro.TemplateFlags, bool?> calcFlagState) {
			GUIStyle headerStyle = new GUIStyle ("OL Title");
			headerStyle = new GUIStyle ("Label");
			headerStyle.padding = new RectOffset (20, 0, 2, 4);

			if (showCornersUI) GUITools.FoldOut ("Pantheon.SMP.Style.Corners", "Corners", false, headerStyle, "Helpbox", () => DrawCornerUI (calcFlagState));
			if (showCurvesUI) GUITools.FoldOut ("Pantheon.SMP.Style.Curves", "Curving", false, headerStyle, "Helpbox", () => DrawCurveUI (calcFlagState));
			GUITools.FoldOut ("Pantheon.SMP.Style.Fill", "Fill", false, headerStyle, "Helpbox", () => DrawFillUI (showMaterials, calcFlagState));
			GUITools.FoldOut ("Pantheon.SMP.Style.Outline", "Outline", false, headerStyle, "Helpbox", () => DrawOutlineUI (showMaterials, calcFlagState));
			GUITools.FoldOut ("Pantheon.SMP.Style.Shadow", "Drop Shadow", false, headerStyle, "Helpbox", () => DrawShadowUI (calcFlagState));
		}

		private void DrawCornerUI (Func<ShapeMeshPro.TemplateFlags, bool?> calcFlagState) {
			DrawCornerWidget (ShapeMeshPro.TemplateFlags.CornerType, calcFlagState);
			if (dataProviders[0].data.cornerType == ShapeMeshPro.CornerType.Rounded) {
				DrawOverridableProperty (this.cornerRadius, ShapeMeshPro.TemplateFlags.CornerRadius, calcFlagState, "Radius");
			}
			DrawDualOverridableProperty (this.cornerRandomizationMin, this.cornerRandomizationMax, ShapeMeshPro.TemplateFlags.CornerRandomization, calcFlagState, "Drift Range");
		}

		private void DrawCurveUI (Func<ShapeMeshPro.TemplateFlags, bool?> calcFlagState) {
			DrawOverridableProperty (this.shapeCurveStrength, ShapeMeshPro.TemplateFlags.ShapeCurveStrength, calcFlagState, "Strength");
		}

		private void DrawFillUI (bool showMaterials, Func<ShapeMeshPro.TemplateFlags, bool?> calcFlagState) {
			DrawColorInfoWidget (this.shapeColor, ShapeMeshPro.TemplateFlags.FillColor, calcFlagState);
			if (showMaterials) DrawOverridableProperty (this.shapeMaterial, ShapeMeshPro.TemplateFlags.FillMaterial, calcFlagState, "Material");
			DrawOverridableProperty (this.shapeTexture, ShapeMeshPro.TemplateFlags.FillTexture, calcFlagState, "Texture");
		}

		private void DrawOutlineUI (bool showMaterials, Func<ShapeMeshPro.TemplateFlags, bool?> calcFlagState) {
			DrawOverridableProperty (this.outlineWidth, ShapeMeshPro.TemplateFlags.OutlineWidth, calcFlagState, "Width");
			DrawOverridableProperty (this.outlineOffset, ShapeMeshPro.TemplateFlags.OutlineOffset, calcFlagState, "Offset");
			DrawColorInfoWidget (this.outlineColor, ShapeMeshPro.TemplateFlags.OutlineColor, calcFlagState);
			if (showMaterials) DrawOverridableProperty (this.outlineMaterial, ShapeMeshPro.TemplateFlags.OutlineMaterial, calcFlagState, "Material");
			DrawOverridableProperty (this.outlineTexture, ShapeMeshPro.TemplateFlags.OutlineTexture, calcFlagState, "Texture");
		}

		private void DrawShadowUI (Func<ShapeMeshPro.TemplateFlags, bool?> calcFlagState) {
			DrawOverridableProperty (this.shadowOffset, ShapeMeshPro.TemplateFlags.ShadowTransform, calcFlagState, "Offset");
			DrawOverridableProperty (this.shadowFuzziness, ShapeMeshPro.TemplateFlags.ShadowFuzziness, calcFlagState, "Fuzziness");
			DrawColorInfoWidget (this.shadowColor, ShapeMeshPro.TemplateFlags.ShadowColor, calcFlagState);
			DrawOverridableProperty (this.shadowMaterial, ShapeMeshPro.TemplateFlags.ShadowMaterial, calcFlagState, "Material");
		}

		private void DrawOverridableProperty (SerializedProperty property, ShapeMeshPro.TemplateFlags flag, Func<ShapeMeshPro.TemplateFlags, bool?> calcFlagState, string label = null) {
			if (label == null) label = ObjectNames.NicifyVariableName (property.name);
			EditorGUILayout.BeginHorizontal ();

			bool showToggle, toggleChecked;
			DrawOverridableCheckbox (flag, calcFlagState, out showToggle, out toggleChecked);

			EditorGUILayout.PrefixLabel (label);
			EditorGUI.BeginDisabledGroup (showToggle && !toggleChecked);
			EditorGUILayout.PropertyField (property, GUIContent.none);
			EditorGUI.EndDisabledGroup ();
			EditorGUILayout.EndHorizontal ();
		}

		private void DrawDualOverridableProperty (SerializedProperty property1, SerializedProperty property2, ShapeMeshPro.TemplateFlags flag, Func<ShapeMeshPro.TemplateFlags, bool?> calcFlagState, string label) {
			EditorGUILayout.BeginHorizontal ();
			bool showToggle, toggleChecked;
			DrawOverridableCheckbox (flag, calcFlagState, out showToggle, out toggleChecked);

			EditorGUILayout.PrefixLabel (label);
			EditorGUI.BeginDisabledGroup (showToggle && !toggleChecked);
			EditorGUILayout.PropertyField (property1, GUIContent.none);
			EditorGUILayout.PropertyField (property2, GUIContent.none);
			EditorGUI.EndDisabledGroup ();
			EditorGUILayout.EndHorizontal ();
		}

		private void DrawColorInfoWidget (ColorInfoProperties colorInfo, ShapeMeshPro.TemplateFlags flag, Func<ShapeMeshPro.TemplateFlags, bool?> calcFlagState) {
			EditorGUILayout.BeginHorizontal ();
			bool showToggle, toggleChecked;
			DrawOverridableCheckbox (flag, calcFlagState, out showToggle, out toggleChecked);

			EditorGUILayout.PrefixLabel ("Color");
			EditorGUI.BeginDisabledGroup (showToggle && !toggleChecked);
			EditorGUILayout.BeginVertical ();

			EditorGUILayout.PropertyField (colorInfo.color, GUIContent.none);

			EditorGUILayout.BeginHorizontal ();
			EditorGUI.showMixedValue = colorInfo.useGradient.hasMultipleDifferentValues;
			EditorGUI.BeginChangeCheck ();
			var val = EditorGUILayout.Toggle (colorInfo.useGradient.boolValue, GUILayout.ExpandWidth (false), GUILayout.Width (15));
			EditorGUI.showMixedValue = false;
			if (EditorGUI.EndChangeCheck ()) {
				foreach (var o in objects) Undo.RegisterCompleteObjectUndo (o, "Change Use Gradient");
				colorInfo.useGradient.boolValue = val;
			}
			EditorGUILayout.LabelField ("Use gradient");
			EditorGUILayout.EndHorizontal ();

			if (colorInfo.useGradient.boolValue) {
				EditorGUILayout.BeginHorizontal ();
				EditorGUILayout.PropertyField (colorInfo.gradientColor1, GUIContent.none);
				EditorGUILayout.PropertyField (colorInfo.gradientColor2, GUIContent.none);

				EditorGUI.BeginChangeCheck ();
				EditorGUI.showMixedValue = colorInfo.curve.hasMultipleDifferentValues;
				var curve = EditorGUILayout.CurveField (colorInfo.curve.animationCurveValue, Color.green, new Rect (0, 0, 1, 1));
				EditorGUI.showMixedValue = false;
				if (EditorGUI.EndChangeCheck ()) {
					foreach (var o in objects) Undo.RegisterCompleteObjectUndo (o, "Change Gradient Curve");
					colorInfo.curve.animationCurveValue = curve;
				}
				EditorGUILayout.EndHorizontal ();
			}

			EditorGUILayout.EndVertical ();
			EditorGUI.EndDisabledGroup ();
			EditorGUILayout.EndHorizontal ();
		}

		private void DrawCornerWidget (ShapeMeshPro.TemplateFlags flag, Func<ShapeMeshPro.TemplateFlags, bool?> calcFlagState) {
			EditorGUILayout.BeginHorizontal ();
			bool showToggle, toggleChecked;
			DrawOverridableCheckbox (flag, calcFlagState, out showToggle, out toggleChecked);

			EditorGUI.BeginDisabledGroup (showToggle && !toggleChecked);
			var showSegmentCount = false;
			foreach (var dp in dataProviders) {
				if((dp.data.cornerType & ShapeMeshPro.CornerType.Segmented) != 0) {
					showSegmentCount = true;
					break;
				}
			}

			EditorGUILayout.PrefixLabel ("Corner Type");
			EditorGUILayout.PropertyField (cornerType, GUIContent.none);
			if (showSegmentCount) {
				EditorGUILayout.PropertyField (cornerSegmentCount, GUIContent.none, GUILayout.Width(40));
			}
			EditorGUI.EndDisabledGroup ();
			EditorGUILayout.EndHorizontal ();
		}

		private void DrawOverridableCheckbox (ShapeMeshPro.TemplateFlags flag, Func<ShapeMeshPro.TemplateFlags, bool?> calcFlagState, out bool showToggle, out bool toggleChecked) {
			showToggle = shouldShowToggle;
			toggleChecked = false;
			bool toggleMixed = false;
			var flagState = calcFlagState != null ? calcFlagState (flag) : null;
			if (flagState == null) toggleMixed = true;
			else toggleChecked = !(bool) flagState;

			if (showToggle) {
				if (toggleMixed) EditorGUI.showMixedValue = true;
				EditorGUI.BeginChangeCheck ();
				toggleChecked = EditorGUILayout.Toggle (toggleChecked, GUILayout.Width (15));
				if (EditorGUI.EndChangeCheck ()) {
					for (var i = 0; i < objects.Count; i++) {
						Undo.RegisterCompleteObjectUndo (objects[i], "Change Template Override Flags");
						var shape = objects[i] as ShapeMeshPro;
						if (toggleChecked)
							shape.templateFlags &= ~flag;
						else
							shape.templateFlags |= flag;
					}
				}
				if (toggleMixed) EditorGUI.showMixedValue = false;
			}
		}

		private bool shouldShowToggle {
			get {
				foreach (var o in objects) {
					var shape = o as ShapeMeshPro;
					if (shape == null) return false;
					if (shape.templateStyle != null) {
						return true;
					}
				}
				return false;
			}
		}
	}
}