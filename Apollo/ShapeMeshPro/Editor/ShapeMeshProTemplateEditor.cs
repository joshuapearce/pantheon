﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Pantheon {
	[CustomEditor (typeof (ShapeMeshProTemplate))]
	[CanEditMultipleObjects]
	public class ShapeMeshProTemplateEditor : Editor {
		private ShapeMeshProStyleEditor styleEditor;

		void OnEnable () {
			var data = serializedObject.FindProperty ("_data");
			styleEditor = new ShapeMeshProStyleEditor(data, new List<Object>(targets));
		}

		public override void OnInspectorGUI () {
			GUITools.Init ();

			var template = this.target as ShapeMeshProTemplate;

			bool changed = false;

			serializedObject.Update ();

			EditorGUILayout.BeginHorizontal ();
			EditorGUILayout.Space (-15, false);
			EditorGUILayout.BeginVertical (GUITools.container);

			styleEditor.Draw (true, true, true, null);
			EditorGUILayout.Space (2, false);
			if (targets.Length > 1) EditorGUI.BeginDisabledGroup (false);
			if (GUILayout.Button ("Copy Style")) {
				ShapeMeshClipboardUtility.CopyStylesToClipboard (template.data);
			}
			if (targets.Length > 1) EditorGUI.EndDisabledGroup ();

			if (ShapeMeshClipboardUtility.ClipboardContainsStyleData ()) {
				if (GUILayout.Button ("Paste Style")) {
					var styleData = ShapeMeshClipboardUtility.GetClipboardAsStyleData ();
					if (styleData != null) {
						foreach (var target in targets) {
							Undo.RegisterCompleteObjectUndo (target, "Paste style settings");
							(target as ShapeMeshProTemplate).data = styleData.Clone ();
						}
					}
				}
			}
			EditorGUILayout.Space (6, false);
			EditorGUILayout.EndVertical ();
			EditorGUILayout.EndHorizontal ();

			if (GUI.changed || changed) {
				serializedObject.ApplyModifiedProperties ();
			}

		}
	}
}