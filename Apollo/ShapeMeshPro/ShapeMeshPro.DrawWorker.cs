﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Pantheon {
	public partial class ShapeMeshPro {
		class DrawWorker:
			MaskableGraphic
		{
			public static DrawWorker CreatePart(ShapeMeshPro owner) {
				DrawWorker v= new GameObject("ShapeMeshPro.DrawWorker").AddComponent<DrawWorker>();
				v.transform.SetParent(owner.transform);
				v.transform.localPosition= Vector3.zero;
				v.owner= owner;
				v.gameObject.hideFlags= HideFlags.DontSaveInBuild | HideFlags.DontSaveInEditor;
				v.transform.hideFlags= HideFlags.DontSaveInBuild | HideFlags.DontSaveInEditor;

				var rect= v.transform as RectTransform;
				rect.transform.localScale= Vector3.one;
				rect.pivot= (owner.transform as RectTransform).pivot;
				rect.offsetMin= Vector2.zero;
				rect.offsetMax= Vector2.zero;
				rect.anchorMin= Vector2.zero;
				rect.anchorMax= Vector2.one;

				return v;
			}

			//--------------------------------
			[Serializable]
			public struct Data {
				static Texture defaultTexture { get { return null; } } //Replace it with Texture2D.whiteTexture if there are issues in the future

				static Material defaultMaterial_textured_;
				public static Material defaultMaterial_textured {
					get {
						if(defaultMaterial_textured_ == null) {
							defaultMaterial_textured_= Resources.Load<Material>("ShapeMeshPro.DefaultMaterial-Textured");
						}
						return defaultMaterial_textured_;
					}
				}

				static Material defaultMaterial_colored_;
				public static Material defaultMaterial_colored {
					get {
						if(defaultMaterial_colored_ == null) {
							defaultMaterial_colored_= Resources.Load<Material>("ShapeMeshPro.DefaultMaterial-Colored");
						}
						return defaultMaterial_colored_;
					}
				}

				public MeshData mesh;
				public Material material;
				public Texture texture;
				public Sprite sprite;

				public UIVertex basicVertex;

				public Data(MeshData mesh, Material material, Texture texture, Sprite sprite) {
					this.mesh= mesh;
					this.material= material;

					if(texture == null) {
						if(sprite != null) texture= sprite.texture;
						else texture= defaultTexture;
					}
					this.texture= texture;
					this.sprite= sprite;

					basicVertex= new UIVertex();

					Refresh();
				}

				public bool BatchesWith(Data b) {
					if(material != b.material) return false;
					if((texture != null) && (b.texture != null)) { //null texture is a special case where it can match anything
						if(texture != b.texture) return false;
					}
					return true;
				}

				private void Refresh() {
					if(sprite != null) {
						//Passing sprite coords to our shader, so it can tile even if the sprite is in an atlas
						var texWH= new Vector2(sprite.texture.width, sprite.texture.height);
						basicVertex.uv1= sprite.textureRect.min / texWH;
						basicVertex.uv2= sprite.textureRect.max / texWH;

					}else if(texture != null) {
						basicVertex.uv1= Vector2.zero;
						basicVertex.uv2= Vector2.one;

					}else{
						//Signaling our custom shader to ignore the texture
						//This is better for batching
						
						//relying on nanTextureUVs
					}
				}
			}

			[NonSerialized] public ShapeMeshPro owner;
			public List<Data> contents= new List<Data>();

			public Texture texture;
			public override Texture mainTexture { get { return texture; } }

			public Material defaultMaterial_textured { get { return Data.defaultMaterial_textured; } }
			public Material defaultMaterial_colored { get { return Data.defaultMaterial_colored; } }


			protected override void Start() {
				base.Start();
				CheckValidity();
			}

			protected void Update() {
				if(!CheckValidity()) { 
					GameObject.DestroyImmediate(gameObject);
					return;
				}
			}

			public bool CheckValidity() {
				if((MonoBehaviour)this == null) return false;
				if(gameObject == null) return false;

				if(owner == null) {
					gameObject.name= "Destroyed";
					return false;
				}

				return true;
			}

			public void Refresh() {
				texture= null;
				foreach(var data in contents) {
					if(data.texture != null) {
						texture= data.texture;
						break;
					}
				}

				var material= contents[0].material;
				if(material == null) {
					material= defaultMaterial_colored;
					if((material == null) || (texture != null) || owner.hasTexture) material= defaultMaterial_textured;
				}
				this.material= material;

				SetAllDirty();
			}

			protected override void OnPopulateMesh(VertexHelper vh) {
				if(!CheckValidity()) return;
				owner.Refresh(false, false);
				if(!CheckValidity()) return;

				vh.Clear();
				int vOffset= 0;
				foreach(var data in contents) {
					var mesh= data.mesh;

					bool hasVertexColor= mesh.hasVertexColor;

					Color c= Color.white;
					if(!hasVertexColor) c= mesh.color.GetColor(0.0f);

					for(int i= 0; i<mesh.vcount; i++) {
						if(hasVertexColor) c= mesh.colors[i];
						if(mesh.color.isDynamic) c *= owner.color;

						var v= mesh.vertices[i];
						Vector2 uv= (mesh.hasUVs) ? mesh.uvs[i] : new Vector2(float.NaN, -999.0f);
						var vert= data.basicVertex;
						vert.position= v;
						vert.color= c;
						vert.uv0= uv;

						vh.AddVert(vert);
					}

					for(int i= 0; i <mesh.triangles.Count; i += 3) {
						vh.AddTriangle(
							mesh.triangles[i] + vOffset,
							mesh.triangles[i + 1] + vOffset,
							mesh.triangles[i + 2] + vOffset
						);
					}

					vOffset += mesh.vertices.Count;
				}
			}
		}
	}
}
