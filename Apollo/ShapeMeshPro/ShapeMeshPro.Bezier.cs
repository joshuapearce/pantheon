﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Pantheon {
	using static Mathf;

	public partial class ShapeMeshPro {
		public static partial class Geometry {
			public static class Bezier {

				public static void Curve(List<Vector2> source, List<Vector2> output, int segments, float strength, bool exactSegments) {
					if(segments < 2) throw new InvalidOperationException("Too few vertices for a curve");

					for(int i = 0; i < source.Count; i++) {
						Vector2 thisPoint = source[i];
						Vector2 prevPoint = source[i == 0 ? source.Count - 1 : i - 1];
						Vector2 nextPoint = source[i == source.Count - 1 ? 0 : i + 1];
						Vector2 nextNextPoint = source[(i + 2) % source.Count];

						Vector2 controlLeft = thisPoint + GetNormal(source, i) * Vector2.Distance(thisPoint, nextPoint) * strength / 2f;
						Vector2 controlRight = nextPoint - GetNormal(source, i + 1) * Vector2.Distance(nextPoint, nextNextPoint) * strength / 2f;

						int seg;
						if(!exactSegments) {
							float dist = Vector2.Distance(thisPoint, nextPoint);
							seg = Mathf.RoundToInt(segments * dist);
						} else {
							seg = segments;
						}

						float stepLength = 1f / seg;


						for(int j = 0; j < seg; j++) {
							output.Add(GetPoint(thisPoint, controlLeft, controlRight, nextPoint, j * stepLength));
						}

					}
				}

				public static void Curve_Symmetrical(List<Vector2> source, List<Vector2> output, int segments, float strength, bool exactSegments) {
					if(segments < 2) throw new InvalidOperationException("Too few vertices for a curve");

					for(int i = 0; i < source.Count; i++) {
						Vector2 thisPoint = source[i];
						Vector2 prevPoint = source[i == 0 ? source.Count - 1 : i - 1];
						Vector2 nextPoint = source[i == source.Count - 1 ? 0 : i + 1];
						Vector2 nextNextPoint = source[(i + 2) % source.Count];

						Vector2 controlLeft = thisPoint + GetNormal(source, i) * Vector2.Distance(thisPoint, nextPoint) * 0.5f;
						Vector2 controlRight = nextPoint - GetNormal(source, i + 1) * Vector2.Distance(nextPoint, nextNextPoint) * 0.5f;

						int seg;
						if(!exactSegments) {
							float dist = Vector2.Distance(thisPoint, nextPoint);
							seg = Mathf.RoundToInt(segments * dist);
						} else {
							seg = segments;
						}

						float stepLength = 1f / seg;


						int startIndex= output.Count;
						for(int j = 0; j < seg; j++) {
							output.Add(GetPoint(thisPoint, controlLeft, controlRight, nextPoint, j * stepLength));
						}

						if(strength != 2.0f) {
							var va= thisPoint;
							var segmentNormal= (nextPoint - thisPoint).normalized;
							var surfaceNormal= new Vector2(-segmentNormal.y, segmentNormal.x);
							float yMagnitude= 0.0f;

							for(int z= startIndex; z<output.Count; z++) {
								var v= output[z];
								var b= (v - va);
								var n= b.normalized;
								var h= b.magnitude;
								var xh= RadiansNormalized(segmentNormal, n);

								float x= Mathf.Cos(xh) * h;
								float y= Mathf.Sin(xh) * h;

								yMagnitude= Mathf.Max(yMagnitude, Mathf.Abs(y));
								output[z]= new Vector2(x, y);
							}

							for(int z= startIndex; z<output.Count; z++) {
								float x= output[z].x;
								float y= output[z].y;

								y *= strength;

								output[z]= va + (segmentNormal * x) + (surfaceNormal * y);
							}
						}
					}
				}

				public static Vector2 GetPoint(Vector2 p0, Vector2 p1, Vector2 p2, Vector2 p3, float t) {
					t = Mathf.Clamp01(t);
					float oneMinusT = 1f - t;
					return
							oneMinusT * oneMinusT * oneMinusT * p0 +
							3f * oneMinusT * oneMinusT * t * p1 +
							3f * oneMinusT * t * t * p2 +
							t * t * t * p3;
				}

				public static Vector2 GetNormal(List<Vector2> source, int id) {
					id = id % source.Count;
					Vector2 thisPoint = source[id % source.Count];
					Vector2 nextPoint = source[(id + 1) % source.Count];
					Vector2 prevPoint = source[(id - 1 + source.Count) % source.Count];
					return ((nextPoint - thisPoint) - (prevPoint - thisPoint)).normalized;
				}

			}
		}
	}
}