﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Pantheon {
	[ExecuteInEditMode]
	[RequireComponent(typeof(RectTransform))]
	public sealed partial class ShapeMeshPro:
		MonoBehaviour, IStyleDataProvider
	{
		public enum ShapeEnum {
			Rectangle,
			Polygon,
			Round,
			FreeForm
		}

		public enum FittingMode {
			CompletelyInside,
			DecorationsOutside,
			OutlinesOutside
		}

		public enum CornerType {
			Normal= 0x0,
			Segmented= 0x1,
			Rounded= 0x2,
			Bezier= 0x4, //overrides all other flags
			ConstantArcLength= 0x10,
			RelativeSize= 0x20,

			Rounded_ConstantArcLength= Rounded | ConstantArcLength,
			Segments_ArcLength= Segmented | ConstantArcLength,
			RoundedSegments= Rounded | Segmented,
			RoundedSegments_ArcLength= Rounded | Segmented | ConstantArcLength,

			RadiusFlags= ConstantArcLength | RelativeSize
		}

		public enum GradientScaling {
			Vertex,
			Circle,
			Container,
			Screen
		}

		[Serializable]
		public struct GradientInfo {
			public static GradientInfo CreateDefault() {
				return new GradientInfo {
					color1= Color.white, color2= Color.white,
					curve= AnimationCurve.Linear(0, 0, 1, 1)
				};
			}

			public GradientScaling scaling;
			public Color color1;
			public Color color2;

			public float radians;
			public float degrees {
				get { return Mathf.Rad2Deg * radians; }
				set { radians= Mathf.Deg2Rad * value; }
			}

			public AnimationCurve curve;
		}

		[Serializable]
		public struct ColorInfo {
			public ColorInfo(Color c) {
				color= c;
				gradient= GradientInfo.CreateDefault();
				isDynamic= true;
				useGradient= false;
			}

			public void CopySettings(ColorInfo b) {
				isDynamic= b.isDynamic;
				useGradient= b.useGradient;
			}

			public Color color;
			public float a {
				get { return color.a; }
				set { color.a= value; }
			}

			public Color GetColor(float delta) {
				if(useGradient) {
					return color * Color.Lerp(gradient.color1, gradient.color2, gradient.curve.Evaluate(delta));
				} else {
					return color;
				}
			}

			public GradientInfo gradient;
			public bool isDynamic;
			public bool useGradient;
		}

		[Serializable]
		public struct DecorationInfo {
			public int SegmentIndex;
			public float height;
			public float angle_left;
			public float angle_right;
		}

		[Serializable]
		public class Data {
			public FittingMode shapeFittingMode;
			public float shapeCurveStrength;
			public ColorInfo fillColor= new ColorInfo(Color.white);
			public Material fillMaterial;
			public Texture fillTexture;
			public Sprite fillSprite;

			public ColorInfo outlineColor= new ColorInfo(Color.grey);
			public float outlineWidth;
			public float outlineOffset;
			public Material outlineMaterial;
			public Texture outlineTexture;
			public Sprite outlineSprite;

			public CornerType cornerType= CornerType.Normal;
			public int cornerSegmentCount= 0;
			public float cornerSizeValue= 1.0f;
			public float cornerLerping= 1.0f;
			public float cornerRandomizationMin;
			public float cornerRandomizationMax;

			public Vector2 shadowOffset;
			public ColorInfo shadowColor= new ColorInfo(Color.grey * 0.5f);
			public float shadowScale= 1.0f;
			public float shadowFuzziness;
			public Material shadowMaterial;

			public Data Clone() {
				return JsonUtility.FromJson<Data> (JsonUtility.ToJson (this));
			}

			public void CopyFrom(Data b) {
				CopyFrom(b, (TemplateFlags)~0);
			}

			public void CopyFrom(Data b, TemplateFlags flags) {
				if((flags & TemplateFlags.FillColor) != 0) fillColor.color= b.fillColor.color;
				if((flags & TemplateFlags.FillGradient) != 0) fillColor.gradient= b.fillColor.gradient;
				if((flags & TemplateFlags.FillColorSettings) != 0) fillColor.CopySettings(b.fillColor);
				if((flags & TemplateFlags.ShapeFittingMode) != 0) shapeFittingMode= b.shapeFittingMode;
				if((flags & TemplateFlags.ShapeCurveStrength) != 0) shapeCurveStrength= b.shapeCurveStrength;
				if((flags & TemplateFlags.FillMaterial) != 0) fillMaterial= b.fillMaterial;
				if((flags & TemplateFlags.FillTexture) != 0) fillTexture= b.fillTexture;

				if((flags & TemplateFlags.OutlineColor) != 0) outlineColor.color= b.outlineColor.color;
				if((flags & TemplateFlags.OutlineGradient) != 0) outlineColor.gradient= b.outlineColor.gradient;
				if((flags & TemplateFlags.OutlineColorSettings) != 0) outlineColor.CopySettings(b.outlineColor);
				if((flags & TemplateFlags.OutlineWidth) != 0) outlineWidth= b.outlineWidth;
				if((flags & TemplateFlags.OutlineOffset) != 0) outlineOffset= b.outlineOffset;
				if((flags & TemplateFlags.OutlineMaterial) != 0) outlineMaterial= b.outlineMaterial;
				if((flags & TemplateFlags.OutlineTexture) != 0) outlineTexture= b.outlineTexture;

				if((flags & TemplateFlags.CornerType) != 0) cornerSegmentCount= b.cornerSegmentCount;
				if((flags & TemplateFlags.CornerRadius) != 0) cornerSizeValue= b.cornerSizeValue;
				if((flags & TemplateFlags.CornerRandomization) != 0) {
					cornerRandomizationMin= b.cornerRandomizationMin;
					cornerRandomizationMax= b.cornerRandomizationMax;
				}

				if((flags & TemplateFlags.ShadowColor) != 0) shadowColor.color= b.shadowColor.color;
				if((flags & TemplateFlags.ShadowGradient) != 0) shadowColor.gradient= b.shadowColor.gradient;
				if((flags & TemplateFlags.ShadowColorSettings) != 0) shadowColor.CopySettings(b.shadowColor);
				if((flags & TemplateFlags.ShadowFuzziness) != 0) shadowFuzziness= b.shadowFuzziness;
				if((flags & TemplateFlags.ShadowTransform) != 0) {
					shadowScale= b.shadowScale;
					shadowOffset= b.shadowOffset;
				}
				if((flags & TemplateFlags.ShadowMaterial) != 0) shadowMaterial= b.shadowMaterial;
			}
		}

		[Serializable]
		class MeshData {
			public string name;
			public List<Vector2> vertices;
			public List<Vector2> uvs;
			public List<Color> colors;
			public List<int> triangles;

			public bool hasUVs {
				get { return (uvs != null) && (uvs.Count > 0); }
				set { 
					if(!value) uvs= null;
					else if(uvs == null) uvs= new List<Vector2>(vertices.Count);
				}
			}

			public ColorInfo color;
			public bool hasVertexColor { get { return (colors != null) && (colors.Count > 0); } }

			public int vcount { get { return (vertices == null) ? 0 : vertices.Count; } }
			public int tcount { get { return (triangles == null) ? 0 : triangles.Count; } }

			public MeshData(string name) {
				this.name= name;
			}

			public void Clear() {
				if(colors == null) colors= new List<Color>();
				else colors.Clear();

				if(vertices == null) vertices= new List<Vector2>();
				else vertices.Clear();

				if(hasUVs) uvs.Clear();

				if(triangles == null) triangles= new List<int>();
				else triangles.Clear();
			}

			public int AddTriangleSlice(Vector3 v) {
				int i= vertices.Count;
				vertices.Add(v);

				if(i > 1) {
					triangles.Add(0);
					triangles.Add(i - 1);
					triangles.Add(i);
				}

				return i;
			}

			public int AddJoinedTriangle(Vector3 v) {
				int i= vertices.Count;
				vertices.Add(v);

				if(i > 1) {
					triangles.Add(i - 2);
					triangles.Add(i - 1);
					triangles.Add(i);
				}

				return i;
			}

			public void AddTriangle(Vector3 a, Vector3 b, Vector3 c) {
				triangles.Add(vertices.Count); vertices.Add(a);
				triangles.Add(vertices.Count); vertices.Add(b);
				triangles.Add(vertices.Count); vertices.Add(c);
			}

			public void AddTriangle(int a, int b, int c) {
				triangles.Add(a);
				triangles.Add(b);
				triangles.Add(c);
			}

			public void UpdateColors(ColorInfo c) {
				color= c;
				if(!color.useGradient) {
					colors= null;
				}else{
					if(colors == null) colors= new List<Color>();
					else colors.Clear();

					var yValues= BufferPool.Create<List<float>>();
					float yMin= 0;
					float yMax= 0;

					foreach(var v in vertices) {
						var y= Geometry.Rotate(v, color.gradient.radians).y;
						yValues.Add(y);
						yMin= Mathf.Min(yMin, y);
						yMax= Mathf.Max(yMax, y);
					}

					foreach(var y in yValues) {
						float t= Mathf.InverseLerp(yMin, yMax, y);
						colors.Add(color.GetColor(t));
					}

					BufferPool.Return(yValues);
				}
			}

			public void GenerateUVs(Rect rect, Sprite sprite= null) {
				hasUVs= true;
				uvs.Clear();

				bool hasSprite= (sprite != null);
				bool strictBounds= false;

				Vector2 xyMin;
				Vector2 xyMax;

				if(strictBounds) {
					xyMin= rect.min;
					xyMax= rect.max;

				}else{
					xyMin= vertices[0];
					xyMax= vertices[0];

					foreach(var v in vertices) {
						xyMin.x= Mathf.Min(xyMin.x, v.x);
						xyMin.y= Mathf.Min(xyMin.y, v.y);
						xyMax.x= Mathf.Max(xyMax.x, v.x);
						xyMax.y= Mathf.Max(xyMax.y, v.y);
					}
				}

				Vector2 offset= Vector2.zero;
				Vector2 scale= Vector2.one;
				var w= xyMax.x - xyMin.x;
				var h= xyMax.y - xyMin.y;
				Vector2 pivot= new Vector2(0.5f, 0.5f);

				if(hasSprite) {
					scale.x= w / (sprite.textureRect.width / sprite.pixelsPerUnit);
					scale.y= h / (sprite.textureRect.height / sprite.pixelsPerUnit);
					pivot= sprite.pivot;

				}else{
					scale.x= Mathf.Min(1.0f, w / h);
					scale.y= Mathf.Min(1.0f, h / w);
				}

				offset= scale * -0.5f + pivot;

				foreach(var v in vertices) {
					Vector2 uv;
					uv.x= Mathf.InverseLerp(xyMin.x, xyMax.x, v.x);
					uv.y= Mathf.InverseLerp(xyMin.y, xyMax.y, v.y);
					uv *= scale;
					uv += offset;

					uvs.Add(uv);
				}
			}
		}

		[Flags]
		public enum TemplateFlags {
			AllShapeFlags=         0xF,
			AllFillFlags=        0xFF0,
			AllOutlineFlags=   0xFF000,
			AllCornerFlags=   0xF00000,
			AllShadowFlags= 0x7F000000,

			ShapeGeometry=             0x1,
			ShapeFittingMode=          0x2,
			ShapeCurveStrength=        0x4,

			FillColor=                0x10,
			FillGradient=             0x20,
			FillColorSettings=        0x40,
			FillMaterial=             0x80,
			FillTexture=             0x100,

			OutlineWidth=          0x01000,
			OutlineOffset=         0x02000,
			OutlineColor=          0x04000,
			OutlineGradient=       0x08000,
			OutlineColorSettings=  0x10000,
			OutlineMaterial=       0x20000,
			OutlineTexture=        0x40000,

			CornerType=           0x100000,
			CornerRadius=         0x200000,
			CornerRandomization=  0x400000,

			ShadowTransform=     0x1000000,
			ShadowColor=         0x2000000,
			ShadowGradient=      0x4000000,
			ShadowColorSettings= 0x8000000,
			ShadowFuzziness=    0x10000000,
			ShadowScale=        0x20000000,
			ShadowMaterial=     0x40000000,
		}


		static System.Random randomSeeds= new System.Random();

		[SerializeField] private Color m_Color= Color.white;
		public Color color {
			get { return m_Color; }
			set { flagsDirty |= TemplateFlags.FillColor; m_Color= value; }
		}

		[SerializeField] public Data _data= new Data();
		public Data data {
			get { return _data; }
			set { _data = value; }
		}
		public int seed= -1;
		public float vertexTrimming= 0.0f; //1.0 == about 15 degrees either direction

		public bool isDirty {
			get { return flagsDirty != 0; }
			set { flagsDirty= value ? (TemplateFlags)~0 : 0; }
		}
		[NonSerialized] public TemplateFlags flagsDirty= (TemplateFlags)~0;
		[NonSerialized] public int tickRefreshed= -1;
		public Canvas canvas { get { return GetComponentInParent<Canvas>(); } }
		public bool isCanvas { get { return canvas != null; } }

		[SerializeField] private UnityEngine.Object _templateStyle;
		public UnityEngine.Object templateStyle {
			get {
				return _templateStyle;
			}

			set {
				if(_templateStyle == value) return;
				if(!(value is IStyleDataProvider) && value != null) throw new InvalidCastException("Invalid template");

				if(value is ShapeMeshPro) {
					var i= value as ShapeMeshPro;
					while(i != null) {
						if(i == this) throw new OverflowException("Circular reference for ShapeMeshPro template detected");
						i= i.templateStyle as ShapeMeshPro;
					}
				}

				isDirty= true;

				if(value != null) {
					_templateStyle= value;
					Refresh();

				}else{
					CopyTemplateValues(false);
					_templateStyle= null;
				}
			}
		}
		
		public IStyleDataProvider templateStyleProvider {
			get {
				return _templateStyle as IStyleDataProvider;
			}
		}

		public TemplateFlags _templateFlags= (TemplateFlags)~0;
		public TemplateFlags templateFlags {
			get { return _templateFlags; }
			set {
				CopyTemplateValues(false);
				isDirty= true;
				_templateFlags= value;
			}
		}

		//--------------------------------
		public ShapeEnum _shapeType= ShapeEnum.Rectangle;
		public ShapeEnum shapeType {
			get { return _shapeType; }
			set {
				isDirty= true;
				_shapeType= value;
			}
		}

		public int shapeVertexCount {
			get {
				switch(shapeType) {
					case ShapeEnum.Round: return 0;
					case ShapeEnum.Rectangle: return 4;
					case ShapeEnum.Polygon: return vertices.Count;
					case ShapeEnum.FreeForm: return vertices.Count;
					default: throw new NotImplementedException();
				}
			}

			set {
				flagsDirty |= TemplateFlags.ShapeGeometry;

				switch(shapeType) {
					case ShapeEnum.Polygon: break;
					case ShapeEnum.FreeForm: break;

					default: throw new InvalidOperationException();
				}

				if(value < 3) throw new IndexOutOfRangeException();

				if(vertices == null) vertices= new List<Vector2>();
				if(value < vertices.Count) vertices.RemoveRange(value, vertices.Count - value);
				while(vertices.Count < value) vertices.Add(Vector2.zero);
			}
		}

		MeshData fillMesh;
		MeshData outlineMesh;
		MeshData shadowMesh;

		//--------------------------------
		public FittingMode shapeFittingMode {
			get { return data.shapeFittingMode; }
			set {	data.shapeFittingMode= value; flagsDirty |= TemplateFlags.ShapeFittingMode; templateFlags &= ~TemplateFlags.ShapeFittingMode; }
		}

		public float shapeLineCurving {
			get { return data.shapeCurveStrength; }
			set {	data.shapeCurveStrength= value; flagsDirty |= TemplateFlags.ShapeCurveStrength; templateFlags &= ~TemplateFlags.ShapeCurveStrength; }
		}

		//--------------------------------
		public bool hasFill {
			get { return fillColor.a > 0.0f; }
		}

		public ColorInfo fillColor {
			get { return data.fillColor; }
			set {	data.fillColor= value; flagsDirty |= TemplateFlags.FillColor; templateFlags &= ~TemplateFlags.FillColor; }
		}

		public Material fillMaterial {
			get { return data.fillMaterial; }
			set { data.fillMaterial= value; flagsDirty |= TemplateFlags.FillMaterial; templateFlags &= ~TemplateFlags.FillMaterial; }
		}

		public Texture fillTexture {
			get {
				if(data.fillTexture != null) return data.fillTexture;
				if(data.fillSprite != null) return data.fillSprite.texture;
				return null;
			}

			set {
				fillSprite= null;
				if(value != fillTexture) {
					data.fillTexture= value;
					flagsDirty |= TemplateFlags.FillTexture; templateFlags &= ~TemplateFlags.FillTexture;
				}
			}
		}

		public Sprite fillSprite {
			get { return data.fillSprite; }
			set {
				if(value == fillSprite) return;
				data.fillSprite= value;
				flagsDirty |= TemplateFlags.FillTexture; templateFlags &= ~TemplateFlags.FillTexture;
			}
		}

		//--------------------------------
		public CornerType cornerType {
			get { return data.cornerType; }
			set { data.cornerType= value; flagsDirty |= TemplateFlags.CornerType; templateFlags &= ~TemplateFlags.CornerType; }
		}

		public int cornerSegmentCount {
			get { return data.cornerSegmentCount; }
			set {	data.cornerSegmentCount= value; flagsDirty |= TemplateFlags.CornerType; templateFlags &= ~TemplateFlags.CornerType;	}
		}

		public float cornerRadius {
			get { return data.cornerSizeValue; }
			set { data.cornerSizeValue= value; flagsDirty |= TemplateFlags.CornerRadius; templateFlags &= ~TemplateFlags.CornerRadius; }
		}

		public float cornerRandomizationMin {
			get { return data.cornerRandomizationMin; }
			set { data.cornerRandomizationMin= value; flagsDirty |= TemplateFlags.CornerRandomization; templateFlags &= ~TemplateFlags.CornerRandomization; }
		}

		public float cornerRandomizationMax {
			get { return data.cornerRandomizationMax; }
			set { data.cornerRandomizationMax= value; flagsDirty |= TemplateFlags.CornerRandomization; templateFlags &= ~TemplateFlags.CornerRandomization; }
		}

		//--------------------------------
		public bool hasOutline { get { return outlineWidth > 0; } }

		public float outlineWidth {
			get { return data.outlineWidth; }
			set { data.outlineWidth= value; flagsDirty |= TemplateFlags.OutlineWidth; templateFlags &= ~TemplateFlags.OutlineWidth; }
		}

		public float outlineOffset {
			get { return data.outlineOffset; }
			set { data.outlineOffset= value; flagsDirty |= TemplateFlags.OutlineOffset; templateFlags &= ~TemplateFlags.OutlineOffset; }
		}

		public ColorInfo outlineColor {
			get { return data.outlineColor; }
			set {	data.outlineColor= value; flagsDirty |= TemplateFlags.OutlineColor; templateFlags &= ~TemplateFlags.OutlineColor; }
		}

		public Material outlineMaterial {
			get { return data.outlineMaterial; }
			set { data.outlineMaterial= value; flagsDirty |= TemplateFlags.OutlineMaterial; templateFlags &= ~TemplateFlags.OutlineMaterial; }
		}

		public Texture outlineTexture {
			get {
				if(data.outlineTexture != null) return data.outlineTexture;
				if(data.outlineSprite != null) return data.outlineSprite.texture;
				return null;
			}

			set {
				outlineSprite= null;
				if(value != outlineTexture) {
					data.outlineTexture= value;
					flagsDirty |= TemplateFlags.OutlineTexture; templateFlags &= ~TemplateFlags.OutlineTexture;
				}
			}
		}

		public Sprite outlineSprite {
			get { return data.outlineSprite; }
			set {
				if(value == outlineSprite) return;
				data.outlineSprite= value;
				flagsDirty |= TemplateFlags.OutlineTexture; templateFlags &= ~TemplateFlags.OutlineTexture;
			}
		}

		//--------------------------------
		public bool hasShadow {
			get { return (shadowOffset != Vector2.zero) && (shadowColor.color.a > 0); }
		}

		public float shadowScale {
			get { return data.shadowScale; }
			set { data.shadowScale= value; flagsDirty |= TemplateFlags.ShadowTransform; templateFlags &= ~TemplateFlags.ShadowTransform; }
		}

		public Vector2 shadowOffset {
			get { return data.shadowOffset; }
			set { data.shadowOffset= value; flagsDirty |= TemplateFlags.ShadowTransform; templateFlags &= ~TemplateFlags.ShadowTransform; }
		}

		public ColorInfo shadowColor {
			get { return data.shadowColor; }
			set { data.shadowColor= value; flagsDirty |= TemplateFlags.ShadowColor; templateFlags &= ~TemplateFlags.ShadowColor; }
		}

		public float shadowFuzziness {
			get { return data.shadowFuzziness; }
			set { data.shadowFuzziness= value; flagsDirty |= TemplateFlags.ShadowFuzziness; templateFlags &= ~TemplateFlags.ShadowFuzziness; }
		}

		public Material shadowMaterial {
			get { return data.shadowMaterial; }
			set { data.shadowMaterial= value; flagsDirty |= TemplateFlags.ShadowMaterial; templateFlags &= ~TemplateFlags.ShadowMaterial; }
		}

		//--------------------------------
		public List<Vector2> vertices;

		public bool hasTexture {
			get {
				if(fillTexture != null) return true;
				if(outlineTexture != null) return true;
				return false;
			}
		}

		public bool hasMaterial {
			get {
				if(fillMaterial != null) return true;
				if(outlineMaterial != null) return true;
				if(shadowMaterial != null) return true;
				return false;
			}
		}

		[SerializeField] private List<DrawWorker> canvasParts;

		static Vector3[] localCorners_= new Vector3[4];
		Vector3[] localCorners { get { (transform as RectTransform).GetLocalCorners(localCorners_); return localCorners_; } }

		//--------------------------------
		private void OnEnable() {
			Initialize();
		}

		private void Start() {
			//Note: Start may not be called "reliably" in a canvas context
			Initialize();
		}

		void Initialize() {
			Refresh(true);
		}

		void Update() {
			Refresh();
		}

		void CopyTemplateValues(bool all=false) {
			if(templateStyle == null) return;

			if(all) data.CopyFrom(templateStyleProvider.data);
			else data.CopyFrom(templateStyleProvider.data, templateFlags);
		}

		public void Refresh(bool force= false, bool rebuild= true) {
			if(templateStyle is ShapeMeshPro) {
				var template = templateStyle as ShapeMeshPro;
				template.Refresh();
				isDirty |= (template.tickRefreshed > tickRefreshed);
			}

			if(!force && !isDirty) return;
			tickRefreshed= Time.frameCount;
			isDirty= false;

			var rect= transform as RectTransform;
			var localCorners= this.localCorners;

			Vector2 center= (localCorners[2] + localCorners[0]) * 0.5f;
			float w= localCorners[2].x - localCorners[0].x;
			float h= localCorners[2].y - localCorners[0].y;

			var BufferPoolGroupID= BufferPool.GetGroupID();
			var vertices= BufferPool.Create<List<Vector2>>();
			bool simpleTriangles= true;

			//initializing
			var randomState_prev= UnityEngine.Random.state; //being polite to outside code
			if(seed < 0) seed= randomSeeds.Next();
			UnityEngine.Random.InitState(seed);

			CopyTemplateValues(false);

			//generating raw shape
			switch(shapeType) {
				case ShapeEnum.Round: {
					Geometry.GeneratePolygon(vertices, 32, center, w * 0.5f, h * 0.5f);
					break;
				}
				case ShapeEnum.Rectangle: {
					foreach(var v in localCorners) vertices.Add(v);
					break;
				}

				case ShapeEnum.Polygon: {
					Geometry.GeneratePolygon(vertices, shapeVertexCount, center, w * 0.5f, h * 0.5f);
					break;
				}

				case ShapeEnum.FreeForm: {
					//throw new NotImplementedException();
					break;
				}
			}
			//basic shape is now created

			//generating the detailed shape
			if(fillMesh == null) fillMesh= new MeshData("Fill");
			fillMesh.Clear();
			List<Vector2> vn= new List<Vector2>();

			//"wobbling" the corners
			if((shapeType != ShapeEnum.FreeForm) && ((cornerRandomizationMax != 0) || (cornerRandomizationMin != 0))) {
				Geometry.GenerateVertexNormals(vertices, vn);

				for(int i= 0; i<vertices.Count; i++) {
					float r= UnityEngine.Random.value;
					var v= vertices[i];
					var n= vn[i];

					float d= Mathf.Lerp(cornerRandomizationMin, cornerRandomizationMax, r);
					v += n * d;

					vertices[i]= v;
				}
			}

			if(cornerType == CornerType.Bezier) {
				var originalVertices= vertices;
				vertices= BufferPool.Create<List<Vector2>>(BufferPoolGroupID);
				Geometry.Bezier.Curve_Symmetrical(originalVertices, vertices, 10, data.cornerSizeValue, true);
				simpleTriangles= false;

			}else if(cornerType != CornerType.Normal) {
				bool rounded= (cornerType & CornerType.Rounded) != 0;
				bool segmented= (cornerType & CornerType.Segmented) != 0;
				int segmentCount= segmented ? cornerSegmentCount : 0;

				var originalVertices= vertices;
				vertices= BufferPool.Create<List<Vector2>>(BufferPoolGroupID);

				Geometry.GenerateCornerSegments(originalVertices, vertices, cornerType & CornerType.RadiusFlags, segmentCount, cornerRadius, data.cornerLerping);
				if(segmented && rounded) {
					//add corners to the corners
					var temp= originalVertices;
					originalVertices= vertices;
					vertices= temp;
					vertices.Clear();

					Geometry.GenerateCornerSegments(originalVertices, vertices, 0, 3, cornerRadius * 0.4f, 1.0f);
				}

				simpleTriangles &= (data.cornerLerping >= 0.0f);
			}

			if(vertexTrimming > 0.0f) {
				Geometry.RemoveWastedVertices(fillMesh.vertices, vertexTrimming * 0.26f);
			}

			//The main shape is now completed
			fillMesh.vertices.Clear();
			fillMesh.vertices.AddRange(vertices); //could be more efficient, but then it would be less bug-proof
			fillMesh.UpdateColors(fillColor);

			//generating outline
			if(hasOutline) {
				GenerateOutline(vertices);
				outlineMesh.UpdateColors(outlineColor);
			}

			//generating triangles for main shape
			if(simpleTriangles) {
				int vCount= fillMesh.vertices.Count;
				int centerIndex= vCount;
				fillMesh.vertices.Add(center);
				for(int i= 0; i<vCount; i++) {
					fillMesh.AddTriangle(centerIndex, i, (i + 1) % vCount);
				}
			}else{
				Geometry.GenerateTriangles(fillMesh.vertices, fillMesh.triangles);
			}

			if((fillMaterial != null) || (fillTexture != null)) {
				fillMesh.GenerateUVs(rect.rect, fillSprite);
			}else{
				fillMesh.hasUVs= false;
				//The draw worker should pass NaN as the texture coords, it's an important flag for the material we use
			}

			if(hasShadow) {
				if(shadowMesh == null) shadowMesh= new MeshData("Shadow");
				shadowMesh.Clear();
				shadowMesh.vertices.AddRange(fillMesh.vertices);
				shadowMesh.triangles.AddRange(fillMesh.triangles);

				for(int i= 0; i<shadowMesh.vertices.Count; i++) {
					Vector2 v= shadowMesh.vertices[i];
					v += (v - center) * (shadowScale - 1.0f);
					v += shadowOffset;
					shadowMesh.vertices[i]= v;
				}

				shadowMesh.UpdateColors(shadowColor);
			}

			//generating colors
			if(fillMesh != null) fillMesh.UpdateColors(fillColor);
			if(outlineMesh != null) outlineMesh.UpdateColors(outlineColor);
			if(shadowMesh != null) shadowMesh.UpdateColors(shadowColor);

			//Creating worker parts, however many are needed
			ConfigureCanvasParts();
			foreach(var v in canvasParts) {
				if(rebuild) v.SetAllDirty();
			}


			UnityEngine.Random.state= randomState_prev;
			BufferPool.ReturnEntireGroup(BufferPoolGroupID);
		}

		void GenerateOutline(List<Vector2> vertices) {
			if(!hasOutline) throw new InvalidOperationException();

			if(outlineMesh == null) outlineMesh= new MeshData("Outline");
			outlineMesh.Clear();

			bool hasUVs= (outlineMaterial != null) || (outlineTexture != null);
			float offset= outlineOffset;
			float width= outlineWidth;

			if(width < 0) {
				offset -= width;
				width= -width;
			}

			var en= BufferPool.Create<List<Vector2>>();

			var count= vertices.Count;
			for(int i= 0; i < count; i++) {
				int i1= (i + 1) % count;

				var v= vertices[i];
				var v2= vertices[i1];
				var n= (v2 - v).normalized;
				
				en.Add(new Vector2(-n.y, n.x));
			}

			for(int i= 0; i < count; i++) {
				int i1= (i + 1) % count;
				int i2= (i + 2) % count;

				var v= vertices[i];
				var v1= vertices[i1];
				var v2= vertices[i2];

				var n= en[i];
				var n2= en[i1];

				for(int z= 0; z<2; z++) {
					var m= outlineOffset;
					if(z == 0) m -= outlineWidth;

					var a1= v + n * m;
					var a2= v1 + n * m;
					var b1= v1 + n2 * m;
					var b2= v2 + n2 * m;

					Vector2 intersection;
					if(!Geometry.LineIntersection(a1, a2, b1, b2, out intersection)) {
						intersection= a2;
					}

					outlineMesh.vertices.Add(intersection);
				}

				int t0= i * 2;
				int t1= i * 2 + 1;
				int t2= ((i + 1) % count) * 2;
				int t3= t2 + 1;

				outlineMesh.AddTriangle(t0, t1, t2);
				outlineMesh.AddTriangle(t1, t2, t3);
			}

			float mSum= 0.0f;
			var mValues= BufferPool.Create<List<float>>();
			for(int i= 0; i<count; i++) {
				var v= outlineMesh.vertices[i * 2];
				var v1= outlineMesh.vertices[((i + 1) % count) * 2];

				var m= (v1 - v).magnitude;
				mValues.Add(mSum);
				mSum += m;
			}

			outlineMesh.hasUVs= hasUVs;
			if(hasUVs) {
				outlineMesh.uvs.Clear();
				for(int i= 0; i<count; i++) {
					var x= mValues[i] / mSum;
					outlineMesh.uvs.Add(new Vector2(x, 0.0f));
					outlineMesh.uvs.Add(new Vector2(x, 1.0f));
				}
			}

			BufferPool.Return(en);
			BufferPool.Return(mValues);
		}

		//------------------------------
		private void OnValidate() {
			isDirty= true;
		}

		private void OnRectTransformDimensionsChange() {
			isDirty= true;
		}

		static Stack<DrawWorker.Data> buffer_ConfigureCanvasParts= new Stack<DrawWorker.Data>();

		void ConfigureCanvasParts() {
			if(canvasParts == null) canvasParts= new List<DrawWorker>();
			canvasParts.RemoveAll((v)=> (v == null));
			int canvasPartIndex= -1;

			var partData= buffer_ConfigureCanvasParts;
			partData.Clear();

			if(hasOutline) partData.Push(new DrawWorker.Data(outlineMesh, outlineMaterial, outlineTexture, outlineSprite));
			if(hasFill) partData.Push(new DrawWorker.Data(fillMesh, fillMaterial,	fillTexture, fillSprite));
			if(hasShadow) partData.Push(new DrawWorker.Data(shadowMesh,	shadowMaterial,	null,	null));

			bool hasSpriteAtlas= false;

			while(partData.Count > 0) {
				DrawWorker part= null;
				++canvasPartIndex;
				if(canvasPartIndex < canvasParts.Count) {
					part= canvasParts[canvasPartIndex];
				}else{
					part= DrawWorker.CreatePart(this);
					canvasParts.Add(part);
					part.transform.SetSiblingIndex(canvasPartIndex);
				}
				part.owner= this;
				part.contents.Clear();

				while(true) {
					var data= partData.Pop();
					part.contents.Add(data);

					hasSpriteAtlas |= (data.sprite != null) && data.sprite.packed;

					if(partData.Count == 0) break;
					if(!partData.Peek().BatchesWith(data)) break;
				}

				part.Refresh();
			}

			while(canvasParts.Count > canvasPartIndex + 1) {
				var v= canvasParts[canvasParts.Count - 1];
				canvasParts.RemoveAt(canvasParts.Count - 1);
				v.owner= null;
				v.CheckValidity();
			}

			if(hasSpriteAtlas) {
				var canvas= this.canvas;
				if(canvas != null) {
					canvas.additionalShaderChannels |= AdditionalCanvasShaderChannels.TexCoord1;
					canvas.additionalShaderChannels |= AdditionalCanvasShaderChannels.TexCoord2;
				}
			}
		}
	}

	public interface IStyleDataProvider {
		ShapeMeshPro.Data data { get; }
	}
}