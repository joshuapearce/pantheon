﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Pantheon {
	public partial class ShapeMeshPro {
		public static partial class Geometry {

			//---------------------------------
			public static void GeneratePolygon(List<Vector2> vertices, int count, Vector3 center, float x_scale, float y_scale) {
				vertices.Clear();

				float radians= Mathf.PI * 0.5f;
				float delta= -(Mathf.PI * 2.0f) / (float)count;

				for(float i= 0; i<count; i++) {
					Vector3 v;
					v.x= Mathf.Cos(radians);
					v.y= Mathf.Sin(radians);
					v.z= 0.0f;

					v.x *= x_scale;
					v.y *= y_scale;

					vertices.Add(center + v);

					radians += delta;
				}
			}

			//---------------------------------
			public static float Radians(Vector2 n) {
				return Mathf.Atan2(-n.y,-n.x) + Mathf.PI;
			}

			public static float Radians(Vector2 a, Vector2 b) {
				return RadiansNormalized(a.normalized, b.normalized);
			}

			public static float RadiansNormalized(Vector2 na, Vector2 nb) {
				return Mathf.Acos(Mathf.Clamp(Vector2.Dot(na, nb), -1f, 1f));
			}

			public static Vector2 CalcNormal(float radians) {
				return new Vector2(Mathf.Cos(radians), Mathf.Sin(radians));
			}

			public static Vector2 Rotate(Vector2 v, float radians) {
				float sin_r= Mathf.Sin(radians);
				float cos_r= Mathf.Cos(radians);
				return new Vector2(
					v.x * cos_r - v.y * sin_r,
					v.x * sin_r + v.y * cos_r
				);
			}

			public static float AngleDiffCW(float radians_L, float radians_R) {
				var v= radians_R - radians_L;
				if(v > 0) v -= Mathf.PI * 2.0f;

				return v;
			}

			public static float LerpUnclamped(float a, float b, float t) {
				return a + (b - a) * t;
			}

			public static Vector2 LerpUnclamped(Vector2 a, Vector2 b, float t) {
				return a + (b - a) * t;
			}

			//---------------------------------
			public static bool SegmentIntersection(Vector2 a1, Vector2 a2, Vector2 b1, Vector2 b2, out Vector2 result) {
				float s1_x = a2.x - a1.x;
				float s1_y = a2.y - a1.y;
				float s2_x = b2.x - b1.x;
				float s2_y = b2.y - b1.y;

				float s= (-s1_y * (a1.x - b1.x) + s1_x * (a1.y - b1.y)) / (-s2_x * s1_y + s1_x * s2_y);
				float t= ( s2_x * (a1.y - b1.y) - s2_y * (a1.x - b1.x)) / (-s2_x * s1_y + s1_x * s2_y);

				if(float.IsInfinity(s) || float.IsInfinity(t)) {
					result= new Vector2(float.NaN, float.NaN);
					return false;
				}

				//Intersection possible
				result.x= a1.x + (t * s1_x);
				result.y= a1.y + (t * s1_y);

				bool segmentCollision= (s >= 0 && s <= 1 && t >= 0 && t <= 1);
				return segmentCollision;
			}

			//Same as segment intersection, without range checking
			public static bool LineIntersection(Vector2 a1, Vector2 a2, Vector2 b1, Vector2 b2, out Vector2 result) {
				float s1_x = a2.x - a1.x;
				float s1_y = a2.y - a1.y;
				float s2_x = b2.x - b1.x;
				float s2_y = b2.y - b1.y;

				float s= (-s1_y * (a1.x - b1.x) + s1_x * (a1.y - b1.y)) / (-s2_x * s1_y + s1_x * s2_y);
				float t= ( s2_x * (a1.y - b1.y) - s2_y * (a1.x - b1.x)) / (-s2_x * s1_y + s1_x * s2_y);

				if(float.IsInfinity(s) || float.IsInfinity(t)) {
					result= new Vector2(float.NaN, float.NaN);
					return false;
				}

				//Intersection possible
				result.x= a1.x + (t * s1_x);
				result.y= a1.y + (t * s1_y);
				return true;
			}

			//---------------------------------
			public static void RemoveWastedVertices(List<Vector2> vertices, float range= 0.01f) {
				var n_prev= (vertices[0] - vertices[vertices.Count - 1]);

				int i= 1;
				while(i < vertices.Count) {
					var n= (vertices[i] - vertices[i-1]);

					//bool match= (Vector2.Dot(n, n_prev) > range);
					bool match= Mathf.Abs(Geometry.AngleDiffCW(Geometry.Radians(n_prev), Geometry.Radians(n))) < range;

					if(match) {
						vertices.RemoveAt(i-1);
						n_prev= (vertices[i-1] - vertices[(i == 1) ? (vertices.Count - 1) : (i - 2)]);
					}else{
						++i;
						n_prev= n;
					}
				}
			}

			public static void GenerateVertexNormals(List<Vector2> vertices, List<Vector2> output) {
				var en= new List<Vector2>(vertices.Count);
				for(int i= 0; i<vertices.Count; i++) {
					var a= vertices[i];
					var b= vertices[(i + 1) % vertices.Count];
					var n= (b - a).normalized;

					en.Add(new Vector2(-n.y, n.x));
				}

				output.Clear();
				for(int i= 0; i<vertices.Count; i++) {
					var a= en[i];
					var b= en[(i - 1 + vertices.Count) % vertices.Count];

					output.Add((a + b).normalized);
				}
			}

			public static void GenerateCornerSegments(List<Vector2> vertices, List<Vector2> output, CornerType cornerType, int segmentCount, float cornerRadius, float cornerLerping= 1.0f) {
				var count= vertices.Count;

				for(int i= 0; i < count; i++) {
					Vector2 A= vertices[(i + count - 1) % count];
					Vector2 B= vertices[i];
					Vector2 C= vertices[(i + 1) % count];

					var cornerInfo= CalcCornerArc(A, B, C, cornerType, cornerRadius);
					var a= cornerInfo.start;
					var c= cornerInfo.end;

					output.Add(a);
					for(int s= 1; s < segmentCount; s++) {
						float frac= (float)s / (float)segmentCount;
						Vector2 N= Geometry.CalcNormal(cornerInfo.angle1 + (cornerInfo.angle_delta * frac));
						var v= cornerInfo.axis + N * cornerInfo.radius;

						if(cornerLerping != 1.0f) {
							v= Geometry.LerpUnclamped(Vector2.Lerp(a, c, frac), v, cornerLerping); 
						}

						output.Add(v);
					}
					output.Add(c);
				}
			}

			public struct CornerRoundingValues {
				public Vector2 axis;
				public float radius;
				public float angle1;
				public float angle2 { get { return angle1 + angle_delta; } }
				public float angle_delta;

				public Vector2 start { get { return axis + Geometry.CalcNormal(angle1) * radius; } }
				public Vector2 end { get { return axis + Geometry.CalcNormal(angle2) * radius; } }
			}

			public static CornerRoundingValues CalcCornerArc(Vector2 A, Vector2 B, Vector2 C, CornerType cornerType, float cornerRadius) {
				Vector2 BA= A - B;
				Vector2 BC= C - B;
				Vector2 nBA= BA.normalized;
				Vector2 nBC= BC.normalized;

				Vector2 nAxis= (nBA + nBC).normalized;

				float angle1= Geometry.Radians(new Vector2(nBA.y, -nBA.x));
				float angle2= Geometry.Radians(new Vector2(-nBC.y, nBC.x));
				float angle_delta= -Geometry.AngleDiffCW(angle1, angle2);

				//Debug.Log(angle1 * Mathf.Rad2Deg + "; " + angle2 * Mathf.Rad2Deg + "; " + angle_delta * Mathf.Rad2Deg + "; ");

				float halfarc= angle_delta * 0.5f;

				float radius;

				if((cornerType & CornerType.ConstantArcLength) != 0) {
					float arc_width= angle_delta;
					radius= cornerRadius / arc_width;

				}else{
					radius= cornerRadius;
				}

				float mAxis= radius / Mathf.Cos(halfarc); //adjusting so that the arc meets the line segments
				Vector2 axis= B + mAxis * nAxis;

				//output
				CornerRoundingValues v;
				v.axis= axis;
				v.angle1= angle1;
				v.angle_delta= -angle_delta;
				v.radius= radius;

				return v;
			}

			//---------------------------------
			public static void GenerateTriangles(List<Vector2> outline, List<int> triangles) {
				triangles.Clear();
				Triangulator.Triangulate(outline, triangles);
			}

			//---------------------------------
			private class Triangulator {
				//Taken from MogulTech.UniWorld.Utilities

				public static void Triangulate(List<Vector2> points, List<int> indices) {
					int n = points.Count;
					if(n < 3) throw new InvalidOperationException("Too few vertices for triangulation");

					int[] V = new int[n];
					if(Area(points) > 0) {
						for(int v = 0; v < n; v++)
							V[v] = v;
					} else {
						for(int v = 0; v < n; v++)
							V[v] = (n - 1) - v;
					}

					int nv = n;
					int count = 2 * nv;
					for(int m = 0, v = nv - 1; nv > 2;) {
						if((count--) <= 0) return;

						int u = v;
						if(nv <= u)
							u = 0;
						v = u + 1;
						if(nv <= v)
							v = 0;
						int w = v + 1;
						if(nv <= w)
							w = 0;

						if(Snip(points, u, v, w, nv, V)) {
							int a, b, c, s, t;
							a = V[u];
							b = V[v];
							c = V[w];
							indices.Add(a);
							indices.Add(b);
							indices.Add(c);
							m++;
							for(s = v, t = v + 1; t < nv; s++, t++)
								V[s] = V[t];
							nv--;
							count = 2 * nv;
						}
					}

					indices.Reverse();
				}

				private static float Area(List<Vector2> points) {
					int n = points.Count;
					float A = 0.0f;
					for(int p = n - 1, q = 0; q < n; p = q++) {
						Vector2 pval = points[p];
						Vector2 qval = points[q];
						A += pval.x * qval.y - qval.x * pval.y;
					}
					return (A * 0.5f);
				}

				private static bool Snip(List<Vector2> points, int u, int v, int w, int n, int[] V) {
					int p;
					Vector2 A = points[V[u]];
					Vector2 B = points[V[v]];
					Vector2 C = points[V[w]];
					if(Mathf.Epsilon > (((B.x - A.x) * (C.y - A.y)) - ((B.y - A.y) * (C.x - A.x))))
						return false;
					for(p = 0; p < n; p++) {
						if((p == u) || (p == v) || (p == w))
							continue;
						Vector2 P = points[V[p]];
						if(InsideTriangle(A, B, C, P))
							return false;
					}
					return true;
				}

				private static bool InsideTriangle(Vector2 A, Vector2 B, Vector2 C, Vector2 P) {
					float ax, ay, bx, by, cx, cy, apx, apy, bpx, bpy, cpx, cpy;
					float cCROSSap, bCROSScp, aCROSSbp;

					ax = C.x - B.x;
					ay = C.y - B.y;
					bx = A.x - C.x;
					by = A.y - C.y;
					cx = B.x - A.x;
					cy = B.y - A.y;
					apx = P.x - A.x;
					apy = P.y - A.y;
					bpx = P.x - B.x;
					bpy = P.y - B.y;
					cpx = P.x - C.x;
					cpy = P.y - C.y;

					aCROSSbp = ax * bpy - ay * bpx;
					cCROSSap = cx * apy - cy * apx;
					bCROSScp = bx * cpy - by * cpx;

					return ((aCROSSbp >= 0.0f) && (bCROSScp >= 0.0f) && (cCROSSap >= 0.0f));
				}
			}
		}
	}
}
