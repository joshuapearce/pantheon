﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Pantheon {
	[CreateAssetMenu(fileName = "Style Template", menuName = "ShapeMeshPro/StyleTemplate", order = 1)]
	public class ShapeMeshProTemplate :
		ScriptableObject, IStyleDataProvider
	{
		[SerializeField] ShapeMeshPro.Data _data;
		public ShapeMeshPro.Data data {
			get { return _data; }
			set { _data = value; }
		}
	}
}