﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;

using UnityEngine;
using UnityEngine.Profiling;
using UnityEngine.SceneManagement;

namespace Pantheon {
	#if UNITY_EDITOR
	[UnityEditor.InitializeOnLoad]
	#endif
	public static class Manager {
		public const int PantheonBuildID= 20200318;
		public const string PantheonVersionString= "1.0.0";
		public const string PantheonGUID= "d152d0ebad3244b298f77327b665b019"; //No meaning to this, it's just unique
		public const VersionSourceEnum PantheonVersionStream= VersionSourceEnum.Release;

		public enum VersionSourceEnum {
			Undefined,
			Release,
			Beta,
			Alpha,
			Internal
		}

		public static Thread UnityThread= Thread.CurrentThread;
		public static HashSet<Assembly> assemblies= new HashSet<Assembly>();

		public delegate void CallbackFunction();
		#if UNITY_EDITOR
		[ComVisibleAttribute(false)] public static CallbackFunction editorUpdate; //treat as private
		[ComVisibleAttribute(false)] public static CallbackFunction editorRunOnce; //treat as private
		#endif
		public static CallbackFunction update;
		public static CallbackFunction updateWithGame;

		static CallbackFunction _runOnce;
		static object _runOnce_lock= new object();
		public static CallbackFunction runOnce {
			get { return _runOnce; }
			set { lock(_runOnce_lock) { _runOnce= value; } }
		}


		public static void RegisterAssembly(Assembly v) {
			assemblies.Add(v);
		}

		static Manager() {
			RegisterAssembly(typeof(Manager).Assembly);

			#if UNITY_EDITOR
			UnityEditor.EditorApplication.update += Initialize;
			#endif
		}

		public static void StaticInitialize() {
			//let the static constructor do the work
		}

		public static bool isUnityThread() {
			return UnityThread == Thread.CurrentThread;
		}

		public static bool RunningOnWindows() {
			if(Application.platform == RuntimePlatform.WindowsEditor) return true;
			if(Application.platform == RuntimePlatform.WindowsPlayer) return true;
			return false;
		}

		public static bool RunningOnMacOS() {
			if(Application.platform == RuntimePlatform.OSXEditor) return true;
			if(Application.platform == RuntimePlatform.OSXPlayer) return true;
			return false;
		}

		public static bool RunningOnLinux() {
			if(Application.platform == RuntimePlatform.LinuxEditor) return true;
			if(Application.platform == RuntimePlatform.LinuxPlayer) return true;
			return false;
		}

		[RuntimeInitializeOnLoadMethod]
		static void Initialize() {
			#if UNITY_EDITOR
			UnityEditor.EditorApplication.update -= Initialize;
			#endif

			//Debug.Log("Pantheon.Manager Initialized");

			if(Application.isPlaying) ManagerRuntime.Instantiate("Instantiated while playing");

			#if UNITY_EDITOR
			UnityEditor.EditorApplication.update += StaticUpdate;

			if(Application.isPlaying) {
				ManagerRuntime.Instantiate();
			}
			#endif
		}

		//--------------------------------
		public static IEnumerable<Type> GetTypes(bool recursive= true) {
			foreach(var assembly in assemblies) {
				Type[] types= null;

				try {
					types= assembly.GetTypes();
				}catch(Exception e) {
					Debug.LogError("Exception for assembly " + assembly.FullName);
					Debug.LogException(e);

					if(e is System.Reflection.ReflectionTypeLoadException) {
						var e_= e as System.Reflection.ReflectionTypeLoadException;
						foreach(var v in e_.LoaderExceptions) {
							Debug.LogException(v);
							var v_= v as System.TypeLoadException;
							Debug.LogError("Problem type: " + v_.TypeName);
						}
					}
					continue;
				}

				foreach(var t in types) {
					if(t == null) continue;

					yield return t;

					if(recursive) {
						foreach(var t_ in GetSubtypes(t, true)) yield return t_;
					}
				}


			}
		}

		static IEnumerable<Type> GetSubtypes(Type root, bool recursive= true) {
			foreach(var t in root.GetNestedTypes()) {
				if(t == null) continue;

				yield return t;

				if(recursive) {
					foreach(var t_ in GetSubtypes(t, true)) yield return t_;
				}
			}
		}

		//--------------------------------
		public static IEnumerable<Tuple<T,MemberInfo>> GetEveryMemberAttribute<T>(BindingFlags flags= 0) where T:Attribute {
			if((flags & (BindingFlags.Static | BindingFlags.Instance)) == 0) {
				flags |= BindingFlags.Static | BindingFlags.Instance;
			}

			if((flags & (BindingFlags.Public | BindingFlags.NonPublic)) == 0) {
				flags |= BindingFlags.Public | BindingFlags.NonPublic;
			}

			foreach(var type in GetTypes()) {
				foreach(var member in type.GetMembers(flags)) {
					foreach(var attribute in member.GetCustomAttributes(typeof(T), false)) {
						yield return new Tuple<T, MemberInfo>(attribute as T, member);
					}
				}
			}
		}

		//--------------------------------
		public static IEnumerable<Tuple<T,MemberInfo>> GetMemberAttributesOf<T>(Type ObjType) where T:Attribute {
			BindingFlags flags= BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.DeclaredOnly;

			while(ObjType != null) {
				foreach(var member in ObjType.GetMembers(flags)) {
					foreach(var attribute in member.GetCustomAttributes(typeof(T), false)) {
						yield return new Tuple<T, MemberInfo>(attribute as T, member);
					}
				}
				ObjType= ObjType.BaseType;
			}
		}

		//--------------------------------
		public static void StaticUpdate() {
			AllSceneObjects_dirty= true;

			#if UNITY_EDITOR
			if(editorRunOnce != null) {
				var f= editorRunOnce;
				editorRunOnce= null;
				f.Invoke();
			}
			if(editorUpdate != null) editorUpdate.Invoke();
			#endif

			lock(_runOnce_lock) {
				if(runOnce != null) {
					var temp= runOnce;
					runOnce= null;
					temp.Invoke();
				}
			}

			if(update != null) update.Invoke();
		}

		public static Coroutine StartCoroutine(IEnumerator routine) {
			if(!Application.isPlaying) {
				throw new InvalidProgramException("Use EditorManager.StartCoroutine when not playing (or reconsider your algorithm)");
			}else{
				return ManagerRuntime.Instance.StartCoroutine(routine);
			}
		}

		//--------------------------------
		static bool AllSceneObjects_dirty= true;
		static List<GameObject> AllSceneObjects_;

		public static List<GameObject> AllSceneObjects {
			get {
				if(AllSceneObjects_ == null) {
					AllSceneObjects_= new List<GameObject>();
					AllSceneObjects_dirty= true;
				}

				if(AllSceneObjects_dirty) {
					AllSceneObjects_dirty= false;
					AllSceneObjects_.Clear();

					for(int sceneIndex= 0; sceneIndex<SceneManager.sceneCount; sceneIndex++) {
						var scene= SceneManager.GetSceneAt(sceneIndex);

						foreach(var rootObj in scene.GetRootGameObjects()) {
							AllSceneObjects_.Add(rootObj);
							AllChildObjects(rootObj, AllSceneObjects_);
						}
					}
				}

				return AllSceneObjects_;
			}
		}

		static void AllChildObjects(GameObject obj, List<GameObject> output) {
			var t= obj.transform;
			for(int i= 0; i<t.childCount; i++) {
				var child= t.GetChild(i).gameObject;
				output.Add(child);
				AllChildObjects(child, output);
			}
		}
	}

}