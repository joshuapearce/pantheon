﻿#if !CSHARP_7_3_OR_NEWER

public class Tuple<T1>  
{ 
    public Tuple(T1 Item1) 
    { 
        this.Item1 = Item1; 
    }   

    public T1 Item1 { get; set; }  
} 

public class Tuple<T1, T2> : Tuple<T1>  
{ 
    public Tuple(T1 Item1, T2 Item2) : base(Item1) 
    { 
        this.Item2 = Item2; 
    } 

    public T2 Item2 { get; set; }  
} 

public class Tuple<T1, T2, T3> : Tuple<T1, T2>  
{ 
    public Tuple(T1 Item1, T2 Item2, T3 Item3) : base(Item1, Item2) 
    { 
        this.Item3 = Item3; 
    } 

    public T3 Item3 { get; set; }  
}

public class Tuple<T1, T2, T3, T4> : Tuple<T1, T2, T3>  
{ 
    public Tuple(T1 Item1, T2 Item2, T3 Item3, T4 Item4) : base(Item1, Item2, Item3) 
    { 
        this.Item4 = Item4; 
    } 

    public T4 Item4 { get; set; }  
}

#endif