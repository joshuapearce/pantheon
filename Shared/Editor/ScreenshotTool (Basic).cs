﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;

using UnityEditor;
using UnityEngine;

namespace Pantheon {
	public static partial class ScreenshotTool {
		public delegate void ReportCallback(Texture2D texture, string path); //texture may be null

		public static string rootPath { get { return Path.GetFullPath(Path.Combine(EditorManager.projectPath, "Screenshots/")); } }

		private static Func<bool> useMultithreading= ()=> { return false; };
		private static Action<Texture2D, string, ReportCallback> WriteTextureAsync; //An alias for WriteTextureAsync, for compatibility only this part of this partial class is present

		public static void WriteTexture(Texture2D tex, string filepath, bool AllowAsync= true, ReportCallback onComplete= null) {
			if(tex == null) {
				Debug.LogWarning("Failed to save '" + filepath + "'; Reason: Null texture"); 
				return;
			}

			#if NET_4_6
			if(AllowAsync && useMultithreading() && (WriteTextureAsync != null)) {
				WriteTextureAsync(tex, filepath, onComplete);
			}else
			#endif
			{
				File.WriteAllBytes(filepath, tex.EncodeToPNG());
				if(onComplete != null) onComplete(tex, Path.GetFullPath(filepath));
			}
		}

		public static Texture2D CaptureSystemNow(Rect rect) {
			int w= (int)rect.width;
			int h= (int)rect.height;
			var texture= new Texture2D(w, h, TextureFormat.RGBA32, false);
			var colors= UnityEditorInternal.InternalEditorUtility.ReadScreenPixel(new Vector2(rect.x, rect.y), w, h);
			texture.SetPixels(colors);

			return texture;
		}

		public static Texture2D CaptureEditorNow(EditorWindow panel) {
			//We don't want to risk capturing possibly private non Unity screen shots, in case some other window is drawing over the editor
			//It may happen anyways, but this reduces that chance a lot
			if(!UnityEditorInternal.InternalEditorUtility.isApplicationActive) return null;

			return CaptureSystemNow(panel.position);
		}
	}
}