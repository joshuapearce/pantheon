﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using UnityEditor;
using UnityEngine;

#pragma warning disable 0414

namespace Pantheon {
	namespace UnityReflection {
		public static class GameView {
			public struct ModeInfo {
				public string name;
				public string fullname;
				public int w;
				public int h;
				public GameViewSizeGroupType groupType;
				public int index;

				public string safename {
					get { return Regex.Replace(name, "[/\\:.?*|<>\"]", "_").Trim(); }
				}

				public ModeInfo(GameViewSizeGroupType groupType, int index, string str) {
					var match= Regex.Match(str, "(.+)\\((\\d+)x(\\d+)\\)$");
					fullname= str;
					this.groupType= groupType;
					this.index= index;

					if(match.Success) {
						name= match.Groups[1].Value;
						w= int.Parse(match.Groups[2].Value);
						h= int.Parse(match.Groups[3].Value);
					}else{
						name= fullname;
						w= h= -1;
					}
				}
			}

			//--------------------------------
			public static Type t_GameView;
			static Type t_GameViewSizes;

			static FieldInfo m_defaultScale;
			static FieldInfo f_GameViewSizeGroupType;
			static FieldInfo m_ZoomArea;
			static PropertyInfo selectedSizeIndexProp;
			static MethodInfo getGroup;

			static EditorWindow gvWnd { get { return EditorWindow.GetWindow(t_GameView); } }
			static object gameViewSizesInstance;

			//--------------------------------
			public static GameViewSizeGroupType s_GameViewSizeGroupType {
				get { return (GameViewSizeGroupType)f_GameViewSizeGroupType.GetValue(null); }
				private set {	f_GameViewSizeGroupType.SetValue(null, value); }
			}

			public static int selectedSizeIndex {
				get { return (int)selectedSizeIndexProp.GetValue(gvWnd, null); }
				private set { selectedSizeIndexProp.SetValue(gvWnd, value, null); }
			}

			public static ModeInfo CurrentMode {
				get { return GetMode(); }
				set {	SetMode(value);	}
			}

			//--------------------------------
			public static Vector2 m_Scale {
				get {
					var areaField = t_GameView.GetField("m_ZoomArea", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic);
					var areaObj= areaField.GetValue(gvWnd);
					var field= areaObj.GetType().GetField("m_Scale", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic);
					return (Vector2)field.GetValue(areaObj);
				}
				set {
					var areaField = t_GameView.GetField("m_ZoomArea", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic);
					var areaObj= areaField.GetValue(gvWnd);
					var field= areaObj.GetType().GetField("m_Scale", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic);
					field.SetValue(areaObj, value);
				}
			}

			//--------------------------------
			static GameView() {
				try {
					t_GameView= typeof(Editor).Assembly.GetType("UnityEditor.GameView");
					t_GameViewSizes= typeof(Editor).Assembly.GetType("UnityEditor.GameViewSizes");

					getGroup= t_GameViewSizes.GetMethod("GetGroup");
					f_GameViewSizeGroupType= t_GameViewSizes.GetField("s_GameViewSizeGroupType", BindingFlags.NonPublic | BindingFlags.Static);
					selectedSizeIndexProp= t_GameView.GetProperty("selectedSizeIndex", BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);

					m_ZoomArea= t_GameView.GetField("m_ZoomArea", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic);
					m_defaultScale= t_GameView.GetField("m_defaultScale", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic);

					var singleType= typeof(ScriptableSingleton<>).MakeGenericType(t_GameViewSizes);
					var instanceProp= singleType.GetProperty("instance");
					gameViewSizesInstance= instanceProp.GetValue(null, null);

				}catch(Exception e) {
					Debug.LogError("Pantheon.GameViewhandler had an error finding internal Unity functions for changing the game view resolution");
					Debug.LogException(e);
				}

				//EditorApplication.playModeStateChanged += (mode)=> { RefreshModes(); };
				//Manager.AddInitializer(RefreshModes);
			}

			//--------------------------------
			public static List<ModeInfo> GetModes() {
				return GetModes(s_GameViewSizeGroupType);
			}

			public static List<ModeInfo> GetModes(GameViewSizeGroupType groupType) {
				var group= getGroup.Invoke(gameViewSizesInstance, new object[] { (int)groupType });
				var getDisplayTexts= group.GetType().GetMethod("GetDisplayTexts");
				var displayTexts= getDisplayTexts.Invoke(group, null) as string[];

				var list= new List<ModeInfo>();
				for(int i= 0; i<displayTexts.Length; i++) {
					var str= displayTexts[i];

					if(str == "Free Aspect") continue;
					if(str.StartsWith("Remote")) continue;
					if(str.StartsWith("~")) continue;

					var info= new ModeInfo(groupType, i, str);
					if((info.w > 0) && (info.h > 0)) {
						list.Add(info);
					}
				}

				return list;
			}

			//Warning! This function can force Unity or the game view panel to take focus for magical reasons
			public static ModeInfo GetMode() {
				var gvWndType= typeof(Editor).Assembly.GetType("UnityEditor.GameView");
				var gvWnd= EditorWindow.GetWindow(gvWndType);

				var group= getGroup.Invoke(gameViewSizesInstance, new object[] { (int)s_GameViewSizeGroupType });
				var getDisplayTexts= group.GetType().GetMethod("GetDisplayTexts");
				var displayTexts= getDisplayTexts.Invoke(group, null) as string[];

				return new ModeInfo(s_GameViewSizeGroupType, selectedSizeIndex, displayTexts[(int)selectedSizeIndexProp.GetValue(gvWnd, null)]);
			}

			public static void RestoreMode(GameViewSizeGroupType group, int index) {
				SetMode(group, index);
			}

			public static bool SetMode(ModeInfo mode) {
				if(mode.index < 0) {
					return false;
				}

				SetMode(mode.groupType, mode.index);
				return true;
			}

			public static void SetMode(GameViewSizeGroupType groupType, int index) {
				s_GameViewSizeGroupType= groupType;
				selectedSizeIndex= index;

				var SizeSelectionCallback= t_GameView.GetMethod("SizeSelectionCallback", BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
				SizeSelectionCallback.Invoke(gvWnd, new object[] { index, null });
				Canvas.ForceUpdateCanvases();
			}

		}
	}
}