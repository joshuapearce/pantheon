﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

using UnityEngine;
using UnityEngine.Profiling;

namespace Pantheon {
	[AttributeUsage(AttributeTargets.Method, AllowMultiple = true)]
	public sealed class ConditionalMenuItem:
		Attribute
	{
		public enum ValidateResult {
			Hidden,
			Disabled,
			Enabled
		}

		public string menuItem;
		public int? priority;
		public bool validate;

		public ConditionalMenuItem(string itemName):
			this(itemName, false)	
		{

		}

		public ConditionalMenuItem(string itemName, bool isValidateFunction) :
			this(itemName, isValidateFunction, 0)	
		{
			priority= null;
		}

		public ConditionalMenuItem(string itemName, int priority) {
			menuItem= itemName;
			this.priority= priority;
			validate= false;
		}

		public ConditionalMenuItem(string itemName, bool isValidateFunction, int priority) {
			menuItem= itemName;
			this.priority= priority;
			validate= isValidateFunction;
		}
	}
}