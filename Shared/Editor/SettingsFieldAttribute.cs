﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

using UnityEditor;
using UnityEngine;

namespace Pantheon {
	[System.AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
	public class SettingsFieldAttribute:
		Attribute	
	{
		public enum Flags {
			Default= 0,
			Hidden= 1,
			Advanced= 2,
			SystemWide= 4,
			Separator= 8,
			RegeneratesDynamicScript= 16
		}

		public bool hidden;
		public bool advanced;
		public bool systemWide;
		public bool seperated;
		public bool triggersScriptGeneration;

		public string category;
		public string keyName;
		public string displayName;
		public string tooltip;

		public string requiredModule; //If set, this module must be activated for the setting to be visible

		public int ordering;

		public SettingsFieldAttribute(string displayName,
			#if NET_4_6
			[CallerLineNumber]
			#endif
			int lineNumber=0
		)
			:this(null, displayName, null, Flags.Default, null, lineNumber)
		{

		}

		public SettingsFieldAttribute(string displayName, string tooltip,
			#if NET_4_6
			[CallerLineNumber]
			#endif
			int lineNumber=0
		) 
			:this(null, displayName, tooltip, Flags.Default, null, lineNumber)
		{

		}

		public SettingsFieldAttribute(string displayName, string tooltip, Flags flags,
			#if NET_4_6
			[CallerLineNumber]
			#endif
			int lineNumber=0
		)
			:this(displayName, tooltip, flags, null, lineNumber)
		{

		}

		public SettingsFieldAttribute(string displayName, string tooltip, Flags flags, string keyName,
			#if NET_4_6
			[CallerLineNumber]
			#endif
			int lineNumber=0
		) 
			:this(null, displayName, tooltip, flags, keyName, lineNumber)
		{

		}

		public SettingsFieldAttribute(string requiredModule, string displayName, string tooltip, Flags flags= Flags.Default,
			#if NET_4_6
			[CallerLineNumber]
			#endif
			int lineNumber=0
		)
			:this(requiredModule, displayName, tooltip, flags, null, lineNumber)
		{

		}

		public SettingsFieldAttribute(string requiredModule, string displayName, string tooltip, Flags flags= Flags.Default, string keyName= null,
			#if NET_4_6
			[CallerLineNumber]
			#endif
			int lineNumber=0
		)	{
			this.requiredModule= requiredModule;
			this.displayName= displayName;
			this.keyName= (keyName ?? displayName).Replace(':','_').Replace(' ','_');
			this.tooltip= tooltip;

			hidden= (flags & Flags.Hidden) != 0;
			advanced= (flags & Flags.Advanced) != 0;
			systemWide= (flags & Flags.SystemWide) != 0;
			seperated= (flags & Flags.Separator) != 0;
			triggersScriptGeneration= (flags & Flags.RegeneratesDynamicScript) != 0;

			ordering= lineNumber;
		}
	}
}