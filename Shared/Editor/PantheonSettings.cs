﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;

using UnityEditor;
using UnityEngine;

#if !UNITY_2018_1_0_OR_NEWER
#pragma warning disable 0429 //There's some bug in unity 2017 which insists very simple code is "unreachable"
#endif

namespace Pantheon {
	public class PantheonSettings:
		ModuleSettings,
		ISerializationCallbackReceiver
	{
		static PantheonSettings instance_;
		public static PantheonSettings instance {
			get { 
				if(instance_ == null) instance_= ModuleSettings.LoadOrCreate<PantheonSettings>("Pantheon.settings");
				return instance_;
			}
		}

		static UpdateManagerInterface UpdateManager { get { return EditorManager.GetInterface<UpdateManagerInterface>(); } }

		#if UNITY_2018_3_OR_NEWER
		[SettingsProvider]
    private static SettingsProvider InitializeSettingsProvider_static() {
			var temp= instance.displayName;
			instance.displayName= null;
			var v= instance.InitializeSettingsProvider(); 
			instance.displayName= temp;

			v.guiHandler= (searchContext) => {
				SettingsWindow.showAdvanced= GUILayout.Toggle(SettingsWindow.showAdvanced, "Advanced settings");
				Styles.DrawHorizontalLine(1, 4);
				GUILayout.Space(8);

				GUILayout.BeginVertical();
				instance.DrawHeader();
				GUILayout.EndVertical();
				instance.DrawContents(450);

				GUILayout.Space(50);
				PantheonSettings.DrawCreditsBox();
			};

			return v;
		}
		#endif

		public PantheonSettings() {
			configPriority= 1000;
			displayName= "Main Settings";
		}

		void ISerializationCallbackReceiver.OnBeforeSerialize() {
			__moduleNames= new List<string>(moduleModes.Keys);
			__moduleNames.Sort();

			__moduleModes= new List<EditorManager.ModuleMode>();
			foreach(var name in __moduleNames) {
				__moduleModes.Add(moduleModes[name]);
			}
		}

		void ISerializationCallbackReceiver.OnAfterDeserialize() {
			moduleModes= new Dictionary<string, EditorManager.ModuleMode>();
			for(int i= 0; i<__moduleNames.Count; i++) {
				moduleModes.Add(__moduleNames[i], __moduleModes[i]);
			}

			__moduleModes.Clear();
			__moduleNames.Clear();
		}

		[SerializeField] List<string> __moduleNames= new List<string>();
		[SerializeField] List<EditorManager.ModuleMode> __moduleModes= new List<EditorManager.ModuleMode>();
		[NonSerialized] public Dictionary<string, EditorManager.ModuleMode> moduleModes= new Dictionary<string, EditorManager.ModuleMode>();
		public static Dictionary<string, EditorManager.BoolFunction> ModuleActivationCallbacks= new Dictionary<string, EditorManager.BoolFunction>();

		[SettingsField("Update Manager", "Update check interval (days)", "0 to disable", SettingsFieldAttribute.Flags.SystemWide, null, 100)]
		public int UpdateFrequency {
			get { return EditorPrefs.GetInt("Pantheon.UpdateFrequency", 1); }
			set { EditorPrefs.SetInt("Pantheon.UpdateFrequency", value); }
		}

		public int totalUpdateQueries;

		[SettingsField("Update Manager", "Update check stream", null, SettingsFieldAttribute.Flags.Default, 200)]
		public Manager.VersionSourceEnum VersionSource= Manager.VersionSourceEnum.Release;

		#if UNITY_EDITOR_WIN
		[SettingsField("Use roaming appdata folder", "Changing this setting won't move or copy any files. It should take effect immediately, but restarting Unity is a good idea.", SettingsFieldAttribute.Flags.SystemWide, 300)]
		#endif
		public bool useRoamingAppdata {
			get { return EditorPrefs.GetBool("Pantheon.useRoamingAppdata", false); }
			set { EditorPrefs.SetBool("Pantheon.useRoamingAppdata", value); }
		}
		public bool restorePlayerPrefsAfterPlaying;

		public override string ConfigHeaderInfo() {
			string str= "";
			str += "Contact: pantheon@GamesFromSpace.com\n";
			str += "Current Pantheon version: " + Manager.PantheonVersionString + "\n";
			str += "Current Pantheon BuildID: " + Manager.PantheonBuildID  + " (" + Manager.PantheonVersionStream + ")\n";

			if(EditorManager.GetModuleMode("Update Manager") && (UpdateManager != null)) {
				str += "\n";
				str += UpdateManager.GetUpdateDisplayString();
			}else{
				str += "\nPantheon updates are disabled or unavailable";
			}

			return str.Trim();
		}

		//----------------------------------
		const bool allowTransparentMenus= false;
		[SettingsField(
			allowTransparentMenus ? "Menu Generator" : "<disabled>",
			"Enable transparent menus",
			"Some Pantheon custom menus implement a transparency effect",
			SettingsFieldAttribute.Flags.SystemWide | SettingsFieldAttribute.Flags.Separator,
			null,
			400
		)]
		public bool enableTransparentMenus {
			get { return allowTransparentMenus && EditorPrefs.GetBool("Pantheon.enableTransparentMenus", true); }
			set { EditorPrefs.SetBool("Pantheon.enableTransparentMenus", value); }
		}

		[SettingsField(
			"Menu Generator",
			"Enable automatic menuscript regeneration",
			"Some values (Generally things changing default Unity menus) can't be updated without recompiling the code",
			SettingsFieldAttribute.Flags.SystemWide | SettingsFieldAttribute.Flags.RegeneratesDynamicScript,
			null,
			400
		)]
		public bool enableAutoScriptGeneration {
			get { return EditorPrefs.GetBool("Pantheon.enableAutoScriptGeneration", true); }
			set { EditorPrefs.SetBool("Pantheon.enableAutoScriptGeneration", value); }
		}

		[SerializeField] private bool autoscriptIsDirty_;
		public bool testMenuScript {
			get {	return autoscriptIsDirty_; }
			set {
				if(autoscriptIsDirty_ != value) {
					autoscriptIsDirty_= value;
					SetDirty();
				}
			}
		}

		//[SettingsField("Menu Generator", "Enable Menu/File/Player Settings", null, SettingsFieldAttribute.Flags.SystemWide | SettingsFieldAttribute.Flags.RegeneratesDynamicScript, null, 490)]
		public bool EnableMenuPlayerSettings {
			get { return EditorPrefs.GetBool("Pantheon.Menu.PlayerSettings", true); }
			set { EditorPrefs.SetBool("Pantheon.Menu.PlayerSettings", value); }
		}

		[ConditionalMenuItem("File/Player Settings", true)] 
		static ConditionalMenuItem.ValidateResult OpenPlayerSettings_Validator() {
			return ConditionalMenuItem.ValidateResult.Hidden;
			//if(!PantheonSettings.instance.EnableMenuPlayerSettings) return ConditionalMenuItem.ValidateResult.Hidden;
			//return ConditionalMenuItem.ValidateResult.Enabled;
		}

		[ConditionalMenuItem("File/Player Settings", false, 151)] 
		public static void OpenPlayerSettings() {
			Selection.activeObject= Unsupported.GetSerializedAssetInterfaceSingleton("PlayerSettings");
		}

		[SettingsField(
			"Scene Browser",
			"Enable Menu/File/Open Recent Scenes",
			(EditorManager.UnityMajorVersion == 2017) ? "Warning: In Unity 2017 these options will appear at the end of the file menu" : null,
			SettingsFieldAttribute.Flags.SystemWide | SettingsFieldAttribute.Flags.RegeneratesDynamicScript,
			null,
			500
		)]
		public bool EnableMenuOpenRecentScenes {
			get { return EditorPrefs.GetBool("Pantheon.Menu.OpenRecentScenes", true); }
			set { EditorPrefs.SetBool("Pantheon.Menu.OpenRecentScenes", value); }
		}

		[SettingsField("Scene Browser", "Enable Menu/File/Open Local Scenes", null, SettingsFieldAttribute.Flags.SystemWide | SettingsFieldAttribute.Flags.RegeneratesDynamicScript, null, 600)]
		public bool EnableMenuOpenLocalScenes {
			get { return EditorPrefs.GetBool("Pantheon.Menu.OpenLocalScenes", true); }
			set { EditorPrefs.SetBool("Pantheon.Menu.OpenLocalScenes", value); }
		}

		//----------------------------------
		public override void DrawHeader() {
			GUILayout.BeginVertical(defaultMargins);
			Styles.DrawLink("Pantheon.GamesFromSpace.com", "http://Pantheon.GamesFromSpace.com");
			GUILayout.EndVertical();

			base.DrawHeader();

			//Debug.Log(MenuGenerator.scriptNeedsRebuilding);
			if(!enableAutoScriptGeneration) {
				var MenuGenerator= EditorManager.GetInterface<MenuGeneratorInterface>();
				if((MenuGenerator != null) && MenuGenerator.scriptNeedsRebuilding) {
					GUILayout.BeginVertical(defaultMargins);

					EditorGUILayout.HelpBox("Click here to regenerate menu scripts", MessageType.Warning, true);
					if(GUI.Button(GUILayoutUtility.GetLastRect(), "", GUIStyle.none)) {
						MenuGenerator.Regenerate(false);
					}
					GUILayout.EndVertical();
				}

			}
		}

		public override void DrawContents(float contentWidth) {
			if(SettingsWindow.showAdvanced) {
				GUILayout.BeginVertical(defaultMargins);
				Styles.DrawLink("Open appsdata directory", EditorManager.appDataPath);
				Styles.DrawLink("Open project settings directory", EditorManager.projectPath + "Pantheon/");

				if(EditorManager.debugPantheon) {
					GUILayout.Space(10);
					foreach(var module in ModuleSettings.All) {
						EditorGUILayout.LabelField(module.displayName + ": " + module.name);
					}
				}

				GUILayout.EndVertical();
			}

			base.DrawContents(contentWidth);

			if(ModuleActivationCallbacks.Count > 0) {
				GUILayout.Label("Module states", EditorStyles.centeredGreyMiniLabel);

				GUILayout.BeginVertical(defaultMargins);
				GUILayout.BeginHorizontal();
				EditorGUILayout.PrefixLabel(".");
				GUILayout.Label("System", GUILayout.Width(80));
				GUILayout.Label("Project", GUILayout.Width(80));
				GUILayout.EndHorizontal();


				foreach(var item in ModuleActivationCallbacks) {
					var ID= item.Key;
					var function= item.Value;

					GUILayout.BeginHorizontal();
					bool state= EditorManager.GetModuleMode(ID);

					EditorGUILayout.PrefixLabel(ID + (state ? " (Active)" : " (Disabled)"));


					EditorManager.SetModuleModeGlobal(
						ID,
						(EditorManager.ModuleMode)EditorGUILayout.EnumPopup(
							EditorManager.GetModuleModeGlobal(ID),
							GUILayout.Width(80)
						)
					);


					EditorManager.SetModuleMode(
						ID,
						(EditorManager.ModuleMode)EditorGUILayout.EnumPopup(
							moduleModes[ID],
							GUILayout.Width(80)
						)
					);

					GUILayout.EndHorizontal();

					if(state != EditorManager.GetModuleMode(ID)) {
						function(!state);
					}
				}
				GUILayout.EndVertical();
			}
		}

		public static void DrawCreditsBox() {
			EditorGUILayout.TextArea(
				"Credits\n\n"+
				"Created and owned by Joshua Pearce\n"+
				"Special thanks to Sven Magnus and Tobias Padilla for their extensive testing and bug finding\n"+
				"Extra thanks to nerual for additional resources",
				GUI.skin.GetStyle("HelpBox")
			);

			GUILayout.Space(10);
		}
	}
}