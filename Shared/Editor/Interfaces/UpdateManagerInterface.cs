﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

using UnityEditor;
using UnityEngine;

namespace Pantheon {
	public interface UpdateManagerInterface {
		string updateInfoWaiting { get; }
		int minutesSinceUpdateCheck { get; }
		int minutesSinceUpdateCheckAttempt { get; }

		void CheckForUpdate(bool silently= false); 

		string GetUpdateDisplayString();
	}
}