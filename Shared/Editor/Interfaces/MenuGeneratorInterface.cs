﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

using UnityEditor;
using UnityEngine;

namespace Pantheon {
	public interface MenuGeneratorInterface {
		void Regenerate(bool silently=false);
		void ForceRegenerate(bool silently=false);
		bool scriptNeedsRebuilding { get; }
	}
}