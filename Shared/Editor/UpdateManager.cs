﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

using UnityEditor;
using UnityEngine;
using UnityEngine.Networking;

namespace Pantheon {
	[InitializeOnLoad]
	public static class UpdateManager {
		//const string updateQueryURL= "https://drive.google.com/uc?export=download&id=12qMUk8Ybp7kC0JWeGqf3LOrlUBbaqrH-";
		const string updateQueryURL= "https://dl.dropboxusercontent.com/s/odghztkn417i8vg/Pantheon.updateinfo.json?dl=0";

		public static bool enabled { get; private set; }

		//--------------------------------
		class Interface: UpdateManagerInterface {
			public string updateInfoWaiting { get { return UpdateManager.updateInfoWaiting; } }
			public int minutesSinceUpdateCheck { get { return UpdateManager.minutesSinceUpdateCheck; } }
			public int minutesSinceUpdateCheckAttempt { get { return UpdateManager.minutesSinceUpdateCheckAttempt; } }

			public void CheckForUpdate(bool silently= false) {
				UpdateManager.CheckForUpdate(silently);
			}

			public string GetUpdateDisplayString() {
				return UpdateManager.GetUpdateDisplayString();
			}
		}

		//--------------------------------
		public static long timeofLastUpdateCheck {
			get { return long.Parse(EditorPrefs.GetString("Pantheon.timeofLastUpdateCheck", "0")); }
			set { EditorPrefs.SetString("Pantheon.timeofLastUpdateCheck", value.ToString()); }
		}

		public static long timeofLastUpdateCheckAttempt {
			get { return long.Parse(EditorPrefs.GetString("Pantheon.timeofLastUpdateCheckAttempt", "0")); }
			set { EditorPrefs.SetString("Pantheon.timeofLastUpdateCheckAttempt", value.ToString()); }
		}

		public static string updateInfoWaiting {
			get { return EditorPrefs.GetString("Pantheon.updateInfoWaiting"); }
			set { EditorPrefs.SetString("Pantheon.updateInfoWaiting", value); }
		}
		public static int minutesSinceUpdateCheck {
			get {	return (int)(DateTime.UtcNow - DateTime.FromFileTimeUtc(timeofLastUpdateCheck)).TotalMinutes; }
		}
		public static int minutesSinceUpdateCheckAttempt {
			get {	return (int)(DateTime.UtcNow - DateTime.FromFileTimeUtc(timeofLastUpdateCheck)).TotalMinutes; }
		}

		//--------------------------------
		static UpdateManager() {
			EditorManager.RegisterInterface<UpdateManagerInterface>(new Interface());

			EditorManager.RegisterModule("Update Manager", (state) => {
				enabled= state;
			});

			Manager.editorUpdate += StaticUpdate;
		}

		static void StaticUpdate() {
			if(enabled && (EditorManager.settings.UpdateFrequency > 0) && string.IsNullOrEmpty(updateInfoWaiting)) {
				if(
					(timeofLastUpdateCheck == 0) ||
					((int)Math.Floor((float)(DateTime.UtcNow - DateTime.FromFileTimeUtc(timeofLastUpdateCheck)).TotalDays) >= EditorManager.settings.UpdateFrequency)
				){

					long then= timeofLastUpdateCheckAttempt;
					bool doCheck= true;
					if(then > 0) {
						var span= DateTime.UtcNow - DateTime.FromFileTimeUtc(then);
						if(span.TotalSeconds < 6 * 3600) doCheck= false;
					}

					if(doCheck) {
						EditorManager.Log("Automatically checking for an update", false);
						CheckForUpdate(true);
					}
				}

			}
		}

		//--------------------------------
		public static string GetUpdateDisplayString() {
			if(!enabled) return "The update module is disabled";

			var diff= DateTime.UtcNow - DateTime.FromFileTimeUtc(UpdateManager.timeofLastUpdateCheck);
			var str= "Last checked for update: ";

			int days= (int)Math.Floor(diff.TotalDays);
			int hours= diff.Hours;
			int minutes= diff.Minutes;

			if((days == 0) && (hours == 0)) {
				str += minutes + " minute";
				if(minutes != 1) str += "s";
			}else{
				if(days >= 1) str += days + " days, ";
				str += hours + " hour";
				if(hours != 1) str += "s";
			}
			str += " ago\n";

			if(!string.IsNullOrEmpty(UpdateManager.updateInfoWaiting)) {
				var updateData= ParseUpdateInfo(UpdateManager.updateInfoWaiting);
				var selected= SelectBestUpdate(updateData, EditorManager.settings.VersionSource);
				if(selected != Manager.VersionSourceEnum.Undefined) {
					var update= updateData[selected];
					str += "Update available: v" + update["Version"] + " (" + selected +")\n";
					str += "\n";
				}
			}

			return str;
		}

		//--------------------------------
#if UNITY_2018_1_OR_NEWER
		class LazyCertificateHandler: CertificateHandler {
			public static LazyCertificateHandler instance= new LazyCertificateHandler();
			protected override bool ValidateCertificate(Byte[] certificateData) {
				return true;
			}
		}
#endif

		[MenuItem("Tools/Pantheon/Check for update", false, ToolsMenu.MenuPriority + 1004)]
		public static void CheckForUpdate() {
			CheckForUpdate(false);
		}

		public static void CheckForUpdate(bool silently) {
			EditorManager.StartCoroutine(CheckForUpdate_coroutine(silently));
		}

		static IEnumerator CheckForUpdate_coroutine(bool silently= false) {
			string title= "Pantheon Update Check";
			++PantheonSettings.instance.totalUpdateQueries;
			timeofLastUpdateCheckAttempt= DateTime.UtcNow.ToFileTimeUtc();

			var webRequest= UnityWebRequest.Get(updateQueryURL);
			//webRequest.certificateHandler= LazyCertificateHandler.instance;
			yield return webRequest.SendWebRequest();
			while(!webRequest.isDone) {
				yield return null;
			}

			updateInfoWaiting= null;
			timeofLastUpdateCheck= DateTime.UtcNow.ToFileTimeUtc();

			if(webRequest.isNetworkError || webRequest.isHttpError) {
				if(!silently) EditorUtility.DisplayDialog(title, "Error checking for update\n\n" + webRequest.error, "OK");
			}else{
				var text= webRequest.downloadHandler.text;

				if(webRequest.downloadedBytes == 0) {
					if(!silently) EditorUtility.DisplayDialog(title, "The update info was blank, the server may be wrong or unavailable", "OK");
					yield break;
				}

				var data= ParseUpdateInfo(text);

				if(data == null) {
					if(!silently) EditorUtility.DisplayDialog(title, "Error parsing the update info, the server may be wrong or confused\n\n" + text.Substring(0, 512), "OK");
					yield break;
				}

				var selectedStream= SelectBestUpdate(data, EditorManager.settings.VersionSource);
				if(selectedStream == Manager.VersionSourceEnum.Undefined) {
					if(!silently) {
						EditorUtility.DisplayDialog(title, "Pantheon is up to date\n\n" + "v" + Manager.PantheonVersionString, "OK");
					}
				}else{
					int newBuildID= int.Parse(data[selectedStream]["BuildID"]);
					string newVersion= data[selectedStream]["Version"];

					EditorUtility.DisplayDialog(title, "Newer version available\n\n" + selectedStream.ToString() + ": v" + newVersion + " [" + newBuildID + "]", "OK");
					updateInfoWaiting= text;
				}
			}
		}

		public static Dictionary<Manager.VersionSourceEnum, Dictionary<string, string>> ParseUpdateInfo(string json) {
			var data= MiniJSON.Json.Deserialize(json) as Dictionary<string, object>;
			if(data == null) return null;
			if(!data.ContainsKey("Pantheon")) return null;
			if((string)data["Pantheon"] != "Pantheon_Version_Info") return null;

			var output= new Dictionary<Manager.VersionSourceEnum, Dictionary<string, string>>();
			
			foreach(var e_ in Enum.GetValues(typeof(Manager.VersionSourceEnum))) {
				var e= (Manager.VersionSourceEnum)e_;
				object values_;
				if(data.TryGetValue(e.ToString(), out values_)) {
					var item= new Dictionary<string, string>();
					var values= values_ as Dictionary<string, object>;

					foreach(var i in values) {
						item[i.Key]= i.Value.ToString();
					}

					output.Add(e, item);
				}
			}

			return output;
		}

		public static Manager.VersionSourceEnum SelectBestUpdate(Dictionary<Manager.VersionSourceEnum, Dictionary<string, string>> data, Manager.VersionSourceEnum stream) {
			Manager.VersionSourceEnum result= Manager.VersionSourceEnum.Undefined;
			int result_buildID= Manager.PantheonBuildID;

			foreach(var i in data) {
				if(i.Key > stream) continue;

				var buildID= int.Parse(i.Value["BuildID"]);
				if(buildID <= result_buildID) continue;
				
				result= i.Key;
				result_buildID= buildID;
			}

			return result;
		}
	}
}