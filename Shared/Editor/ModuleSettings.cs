﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;

using UnityEditor;
using UnityEngine;

namespace Pantheon {
	//[InitializeOnLoad]
	public class ModuleSettings:
		ScriptableObject
	{
		public static HashSet<ModuleSettings> All= new HashSet<ModuleSettings>();

		static GUIStyle defaultMargins_;
		public static GUIStyle defaultMargins {
			get {
				if(defaultMargins_ == null) {
					defaultMargins_= new GUIStyle(EditorStyles.inspectorDefaultMargins);
					//defaultMargins_.padding= new RectOffset(defaultMargins_.padding.left, defaultMargins_.padding.left, 0, defaultMargins_.padding.bottom);
				}
				return defaultMargins_;
			}
		}

		//[HideInInspector]
		public int PantheonBuildID= -1;
		public string assetPath { get { return AssetDatabase.GetAssetPath(this); } }
		public string GUID { get { return AssetDatabase.AssetPathToGUID(assetPath); } }

		public string displayName;
		private string keyName_;
		public string keyName {
			get {
				if(string.IsNullOrEmpty(keyName_)) keyName= displayName; //deliberately using this setter, not a typo
				return keyName_;
			}

			set {
				if(string.IsNullOrEmpty(value)) keyName_= null;
				else keyName_= value.Replace(':','_').Replace(' ','_');
			}
		}
		[NonSerialized] public int configPriority= 0;
		[NonSerialized] public bool showInConfigPanel= true;

		[NonSerialized] public string helpFile;

		[NonSerialized] public List<Tuple<SettingsFieldAttribute, MemberInfo>> configFields;

		#if UNITY_2018_3_OR_NEWER
    protected SettingsProvider InitializeSettingsProvider() {
			string path= "Project/Pantheon";
			if(displayName != null) path += "/" + displayName;

			var v= new SettingsProvider(path, SettingsScope.Project);
			v.label= displayName;
			v.guiHandler= (searchContext) => {
				DrawHeader();
				DrawContents(450);
			};
			return v;
		}
		#endif

		static ModuleSettings() {

		}

		public void ShowHelp() {
			if(string.IsNullOrEmpty(helpFile)) {
				Debug.LogWarning("No help file set for module '" + displayName + "'");
				return;
			}

			EditorManager.ShowHelpFile(helpFile);
		}

		new public void SetDirty() {
			EditorUtility.SetDirty(this);
		}

		public static void SetAllDirty() {
			foreach(var module in All) module.SetDirty();
		}

		protected virtual void OnEnable() {
			All.Add(this);

			if(string.IsNullOrEmpty(displayName)) displayName= GetType().Name;
			//EditorManager.Log("Adding module " + configName);
			configFields= new List<Tuple<SettingsFieldAttribute, MemberInfo>>(Manager.GetMemberAttributesOf<SettingsFieldAttribute>(GetType()));

			//if(SettingsWindow.instance != null) SettingsWindow.instance.Refresh();
			SettingsWindow.requestRefresh= true;
		}

		public void Display() {
			VulcanWindowManager.OpenWindow<SettingsWindow>().FocusOn(this);
		}

		public static T CreateTemporary<T>() where T:ModuleSettings	{
			var v= ScriptableObject.CreateInstance<T>();
			v.InitializeNewInstance();
			return v;
		}

		public static T LoadOrCreate<T>(string filename) where T:ModuleSettings	{
			if(!filename.EndsWith(".asset")) filename += ".asset";
			var dir= "Assets/Settings/Pantheon/";
			var path= dir + filename;

			var settings= AssetDatabase.LoadAssetAtPath<T>(path);

			if(settings == null) {
				EditorManager.Log("Creating settings asset: " + Path.GetFullPath(path));
				Directory.CreateDirectory(dir);
				settings= ScriptableObject.CreateInstance<T>();
				settings.InitializeNewInstance();
				AssetDatabase.CreateAsset(settings, path);

			}else	if(settings.PantheonBuildID < Manager.PantheonBuildID) {
				settings.UpgradeExistingInstance();
				settings.PantheonBuildID= Manager.PantheonBuildID;

			}else	if(settings.PantheonBuildID > Manager.PantheonBuildID) {
				throw new InvalidDataException("Settings file was from a newer build of Pantheon: " + filename);
			}

			return settings;
		}

		protected virtual void InitializeNewInstance() {
			PantheonBuildID= Manager.PantheonBuildID;
		}

		protected virtual void UpgradeExistingInstance() {

		}

		public virtual string ConfigHeaderInfo() {
			return null;
		}

		public virtual void DrawHeader() {
			GUILayout.BeginVertical(defaultMargins);

			if(!string.IsNullOrEmpty(helpFile)) {
				GUILayout.BeginHorizontal();
				GUILayout.FlexibleSpace();
				Styles.DrawLink(new GUIContent("Help", EditorGUIUtility.IconContent("_Help").image), ShowHelp);
				GUILayout.EndHorizontal();
			}

			string headerInfo= ConfigHeaderInfo();
			if(!string.IsNullOrEmpty(headerInfo)) {
				GUILayout.BeginHorizontal();
				//GUILayout.Button(EditorGUIUtility.IconContent("console.infoicon"), GUIStyle.none, GUILayout.Width(32));
				EditorGUILayout.TextArea(headerInfo, GUI.skin.GetStyle("HelpBox"));
				GUILayout.EndHorizontal();
			}

			GUILayout.EndVertical();
		}

		public virtual void DrawContents(float contentWidth) {
			EditorGUIUtility.labelWidth= 250;
			GUILayout.BeginVertical(defaultMargins);

			if(EditorManager.debugPantheon) {
				GUILayout.Label("Asset path: " + assetPath);
			}

			bool showAdvanced= SettingsWindow.showAdvanced;

			var fields= new List<Tuple<SettingsFieldAttribute, MemberInfo>>(configFields);
			fields.RemoveAll((v) => {
				var attrib= v.Item1;
				if(attrib.hidden) return true;
				if(attrib.advanced && !showAdvanced) return true;
				return false; 
			});

			fields.Sort((a, b)=> {
				int c= b.Item1.systemWide.CompareTo(a.Item1.systemWide);
				if(c == 0) c= a.Item1.ordering.CompareTo(b.Item1.ordering);
				return c;
			});

			GUILayout.BeginVertical();

			if(fields.Count == 0) { 
				GUILayout.Label("  No options currently available", GUILayout.Width(350));

			}else{
				SerializedObject serialization= null;

				int fieldCounter= -1;
				SettingsFieldAttribute prevAttrib= null;
				foreach(var fieldInfo in fields) {
					var attrib= fieldInfo.Item1;
					var member= fieldInfo.Item2;

					if(!string.IsNullOrEmpty(attrib.requiredModule)) {
						if(!EditorManager.GetModuleMode(attrib.requiredModule)) continue;
					}

					++fieldCounter;

					if((fieldCounter == 0) && attrib.systemWide) {
						GUILayout.Label("System wide settings", Styles.header);
					}

					if(!attrib.systemWide && (prevAttrib != null) && prevAttrib.systemWide) {
						GUILayout.EndVertical();
						GUILayout.BeginVertical();
						GUILayout.Space(10);
						GUILayout.Label("Project settings", Styles.header);
					}

					if(attrib.seperated) GUILayout.Space(10);

					var label= new GUIContent(string.IsNullOrEmpty(attrib.displayName) ? member.Name : attrib.displayName);
					if(!string.IsNullOrEmpty(attrib.tooltip)) label.tooltip= attrib.tooltip;
					if(attrib.systemWide) {
						if(string.IsNullOrEmpty(label.tooltip)) label.tooltip= ""; else label.tooltip += "\n\n";
						label.tooltip += "This option is applied across all Unity projects";
					}
					if(attrib.triggersScriptGeneration) {
						if(string.IsNullOrEmpty(label.tooltip)) label.tooltip= ""; else label.tooltip += "\n\n";
						if(PantheonSettings.instance.enableAutoScriptGeneration) {
							label.tooltip += "Changing this option will trigger a recompile of the project's code";
						}else{
							label.tooltip += "This option won't be automatically applied under current settings";
						}
					}

					if(attrib.advanced) label.text= "[Adv] "+label.text;

					GUILayout.BeginHorizontal();

					Type valueType;
					object value;
					Action<object> setter;
					
					bool wasChanged= GUI.changed;
					GUI.changed= false;

					if(member is FieldInfo) {
						var field= (member as FieldInfo);
						valueType= field.FieldType;
						value= field.GetValue(this);
						setter= (obj)=>field.SetValue(this, obj);

					}else if(member is PropertyInfo) {
						var property= (member as PropertyInfo);
						valueType= property.PropertyType;
						value= property.GetValue(this, null);
						setter= (obj)=>property.SetValue(this, obj, null);
					}else {
						throw new NotImplementedException(member.GetType().Name);
					}
					
					if(valueType == typeof(bool)) {
						EditorGUILayout.PrefixLabel(label);
						value= EditorGUILayout.Toggle((bool)value);
					}else if(valueType == typeof(int)) {
						EditorGUILayout.PrefixLabel(label);
						value= EditorGUILayout.DelayedIntField((int)value, GUILayout.Width(100));
					}else if(valueType == typeof(string)) {
						EditorGUILayout.PrefixLabel(label);
						value= EditorGUILayout.DelayedTextField((string)value, Styles.text);
					}else if(valueType.IsEnum) {
						EditorGUILayout.PrefixLabel(label);
						value= EditorGUILayout.EnumPopup((Enum)value, GUILayout.Width(100));

					}else{
						if(serialization == null) {
							serialization= new SerializedObject(this);
						}

						var property= serialization.FindProperty(member.Name);
						EditorGUILayout.PropertyField(property, label, true);
						//throw new NotImplementedException(value.GetType().FullName);
					}
					setter(value);

					if(GUI.changed && attrib.triggersScriptGeneration) {
						EditorManager.settings.testMenuScript= true;
					}
					GUI.changed |= wasChanged;

					GUILayout.EndHorizontal();
					prevAttrib= attrib;
				}

				if(serialization != null) {
					serialization.ApplyModifiedProperties();
				}
			}

			GUILayout.EndVertical();
			GUILayout.EndVertical();

			if(GUI.changed) SetDirty();
		}
	}
}