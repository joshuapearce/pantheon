﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

using UnityEditor;
using UnityEngine;

namespace Pantheon {
	[Serializable]
	public class HelpContents {
		public string helpFileURL; //http, https, www, file://, or Assets/yourfilehere.extension
		public List<HelpWindow.Page> helpPages= null;

		public bool hasHelpContents {
			get {
				if(!string.IsNullOrEmpty(helpFileURL)) return true;
				if((helpPages != null) && (helpPages.Count > 0)) return true;
				return false;
			}
		}

		public HelpContents() {

		}

		public HelpContents(string url) {
			helpFileURL= url;
		}

		public HelpContents(params HelpWindow.Page[] pages) {
			foreach(var page in pages) Add(page);
		}
		
		public void Add(HelpWindow.Page page) {
			if(helpPages == null) helpPages= new List<HelpWindow.Page>();
			helpPages.Add(page);
		}

		public void Show() {
			if(!string.IsNullOrEmpty(helpFileURL)) {
				if(
					helpFileURL.ToLower().StartsWith("http://") ||
					helpFileURL.ToLower().StartsWith("https://") ||
					helpFileURL.ToLower().StartsWith("www.") ||
					helpFileURL.ToLower().StartsWith("file://")
				) {
					Application.OpenURL(helpFileURL);
				}else{
					AssetDatabase.OpenAsset(AssetDatabase.LoadAssetAtPath(helpFileURL, typeof(UnityEngine.Object)));
				}
			}

			if((helpPages != null) && (helpPages.Count > 0)) {
				HelpWindow.Show("Help", helpPages, -1, -1, GetType());
			}
		}
	}
}