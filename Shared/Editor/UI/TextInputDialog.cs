﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

using UnityEngine;
using UnityEditor;

namespace Pantheon {
	public class TextInputDialog:
		EditorWindow
	{
		public static bool somethingIsModal { get { return instanceCount > 0; } } //used because Unity has a bug
		public static int instanceCount= 0;

		[NonSerialized] bool Valid= false;

		public delegate void onResult_delegate(string value);
		public delegate void onResult2_delegate(string value, string value2);
		onResult_delegate onResult;
		onResult2_delegate onResult2;

		EditorManager.VoidFunction DrawGUI;

		public static void Create(string title, EditorManager.VoidFunction DrawGUI, onResult_delegate onResult, int height= 0) {
			if(somethingIsModal) return;

			var v= Create(title);
			v.SetDimensions(0, height);

			v.DrawGUI= DrawGUI;
			v.onResult += onResult;
			v.value= "<success>";
		}

		public static void Create(string title, onResult_delegate onResult, string value=null, string value_name= null) {
			if(somethingIsModal) return;

			var v= Create(title);
			v.onResult += onResult;
			v.value= value;
			v.value_name= value_name;
		}

		public static void Create(string title, onResult2_delegate onResult, string value1=null, string value2=null, string value1_name=null, string value2_name=null) {
			if(somethingIsModal) return;

			var v= Create(title);
			v.onResult2 += onResult;
			v.value= value1;
			v.value2= value2;
			v.value_name= value1_name;
			v.value2_name= value2_name;
		}

		private static TextInputDialog Create(string title) {
			var v= EditorWindow.CreateInstance<TextInputDialog>();
			v.SetDimensions(450, 50);
			v.Valid= true;
			v.titleContent= new GUIContent(title);

			//v.onResult += Close;
			#if UNITY_2019_4_OR_NEWER
			#warning Test this to see if Unity 2019.4 fixes it
			//bugged from 2019.1 to at least 2019.3
			//It's not really modal, and the titlebar behaves about 20 pixels too large, blocking inputs near the top
			v.ShowModalUtility(); 
			#else
			v.ShowUtility();
			#endif

			return v;
		}

		void SetDimensions(int w, int h, bool recenter= true) {
			var rect= position;
			if(w > 0) rect.width= (float)w;
			if(h > 0) rect.height= (float)h;

			if(recenter) {
				rect.center= new Vector2(Screen.currentResolution.width / 2, Screen.currentResolution.height / 2);
			}

			position= rect;
			minSize= new Vector2(rect.width, rect.height);
			maxSize= new Vector2(rect.width, rect.height);
		}

		public string value;
		public string value2;
		public string value_name;
		public string value2_name;
		bool initialized;
		bool requestFocus= true;

		void OnGUI() {
			if(!Valid) Close();

			bool done= false;

			if(UnityEditorInternal.InternalEditorUtility.isApplicationActive) {
				this.Focus();
			}

			if(!initialized) {
				++instanceCount;
				initialized= true;
			}

			var rect= new Rect(10, 10, position.width - 20, position.height - 10);
			rect.height -= 40;
			GUILayout.BeginArea(rect);

			if(DrawGUI != null) {
				DrawGUI();
			}else{
				GUILayout.FlexibleSpace();

				GUI.SetNextControlName("TextInputDialog.input");

				GUILayout.BeginHorizontal();
				if(!string.IsNullOrEmpty(value_name)) EditorGUILayout.PrefixLabel(value_name);
				value= EditorGUILayout.TextField(value, GUILayout.ExpandWidth(true), GUILayout.Height(16));
				//if(newvalue != value) done= true;
				GUILayout.EndHorizontal();

				if(requestFocus) {
					EditorGUI.FocusTextInControl("TextInputDialog.input");
					requestFocus= false;
				}

				if(onResult2 != null) {
					GUILayout.BeginHorizontal();
					if(!string.IsNullOrEmpty(value2_name)) EditorGUILayout.PrefixLabel(value2_name);
					value2= EditorGUILayout.TextField(value2, GUILayout.ExpandWidth(true), GUILayout.Height(16));
					GUILayout.EndHorizontal();
				}

				GUILayout.FlexibleSpace();
			}
			GUILayout.EndArea();

			rect.yMin= rect.yMax + 10;
			rect.height= position.height - rect.yMax - 10;
			GUILayout.BeginArea(rect);

			if(GUILayout.Button("OK")) {
				done= true;
			}
			GUILayout.EndArea();

			if(GUI.changed) {
				Repaint(); 
			}

			if(done) {
				if(onResult != null) {
					onResult.Invoke(value);
					onResult= null;
				}

				if(onResult2 != null) {
					onResult2.Invoke(value, value2);
					onResult2= null;
				}
				Close();
			}
		}

		void OnDestroy() {
			instanceCount--;
			VulcanWindowManager.RepaintAll();

			if(onResult != null) {
				onResult.Invoke(null);
				onResult= null;
			}
		}
	}
}