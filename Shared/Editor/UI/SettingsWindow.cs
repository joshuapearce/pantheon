﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;

using UnityEditor;
using UnityEngine;

namespace Pantheon {
	public class SettingsWindow:
		VulcanWindow
	{
		public static SettingsWindow instance { get; private set; }
		public static bool showAdvanced;
		static ModuleSettings focusOn;

		[MenuItem("Tools/Pantheon/Settings", false, ToolsMenu.MenuPriority + 1005)]
		public static void Display() {
			Display(EditorManager.settings);
		}

		public static void Display(ModuleSettings module) {
			focusOn= module;
			var window= VulcanWindowManager.OpenWindow<SettingsWindow>();
			window.Show();
			window.Focus();
		}

		const float HeadingSize= 30;
		
		public static bool requestRefresh;

		static List<ModuleSettings> sections= new List<ModuleSettings>();
		[SerializeField] List<ModuleSettings> expandedSections= new List<ModuleSettings>();

		protected override void InitializePanel() {
			defaultTitle= "Pantheon";
			defaultIcon= EditorGUIUtility.IconContent("EditorSettings Icon").image;

			hasScrollingH= false;
			hasScrollingV= true;
			repaintOccasionallyInEdit= true;
			repaintOccasionallyInPlay= true;

			minSize= new Vector2(450, 500);
			maxSize= new Vector2(450, 4000);

			instance= this;
		}

		protected override void RefreshContents() {
			sections.Clear();
			//expandedSections.Clear();
			foreach(var module in ModuleSettings.All) {
				if(module.showInConfigPanel) sections.Add(module); 
			}

			sections.Sort((a, b) => {
				if(a.configPriority != b.configPriority) return b.configPriority.CompareTo(a.configPriority);
				return String.Compare(a.displayName, b.displayName, true);
			});
		}

		protected override void WindowUpdate() {
			if(focusOn) {
				FocusOn(focusOn);
				focusOn= null;
			}

			if(requestRefresh) RefreshContents();

			expandedSections.RemoveAll((v) => !sections.Contains(v));
		}

		protected override void DrawToolbar(Rect rect) {
			showAdvanced= GUILayout.Toggle(showAdvanced, "Advanced", Styles.toolbarButton);

			if(GUILayout.Button("Export", Styles.toolbarButton)) {
				EditorManager.QuickExportSettings();
			}

			EditorGUI.BeginDisabledGroup(!Directory.Exists(EditorManager.appDataPath + "ExportedSettings/"));
			if(GUILayout.Button("Import", Styles.toolbarButton)) {
				if(EditorUtility.DisplayDialog(
					"Confirm settings overwrite",
					"This will overwrite the current pantheon settings with the ones saved in your system appdata",
					"Confirm",
					"Cancel"
				)) {
					EditorManager.QuickImportSettings();
				}
			}
			EditorGUI.EndDisabledGroup();

			base.DrawToolbar(rect);
		}

		protected override Rect DrawContents(Rect rect) {
			DrawContents(sections, expandedSections, contentWidth, showAdvanced);
			rect.height= float.PositiveInfinity;
			return rect;
		}

		protected static void DrawContents(List<ModuleSettings> sections, List<ModuleSettings> expandedSections, float contentWidth, bool showAdvanced) {
			foreach(var module in sections) {
				bool expand= expandedSections.Contains(module);

				//var content= new GUIContent(module.displayName);
				//content.image= EditorGUIUtility.IconContent(expand ? "d_icon dropdown" : "d_forward").image;

				//if(GUILayout.Button(content, EditorStyles.foldoutHeader, GUILayout.ExpandWidth(true), GUILayout.Height(HeadingSize))) {
				//if(expand != EditorGUILayout.Foldout(expand, module.displayName, true, EditorStyles.foldoutHeader)) {
				if(expand != Styles.DrawFoldoutHeader(expand, new GUIContent(module.displayName))) {
					expand= !expand;
					if(!expand) expandedSections.Remove(module);
					else expandedSections.Add(module);
				}

				if(expand) {
					module.DrawHeader();
					module.DrawContents(contentWidth);
					GUILayout.Space(20);
				}
			}

			PantheonSettings.DrawCreditsBox();
		}

		protected override void AddItemsToMenu_(GenericMenu menu) {
			menu.AddItem(new GUIContent("Reload"), false, ()=> {
				expandedSections.Clear();
				InitializePanel();
			});

			menu.AddItem(new GUIContent("Open appdata folder"), false, ToolsMenu.OpenAppData);
			menu.AddItem(new GUIContent("Open project data folder"), false, ToolsMenu.OpenProjectData);

			base.AddItemsToMenu_(menu);
		}

		//----------------------------------
		public void FocusOn(ModuleSettings module) {
			if(module == null) return;

			expandedSections.Clear();
			expandedSections.Add(module);

			scrollPosition= new Vector2(0, HeadingSize * sections.IndexOf(module));
			Focus();
		}
	}
}