﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

using UnityEditor;
using UnityEngine;

namespace Pantheon {
	public abstract class VulcanWindow:
			EditorWindow, 
			IHasCustomMenu
		{
		public static Color backgroundColor {
			get {
				var L= EditorGUIUtility.isProSkin ? 0.22f : 0.76f;
				return new Color(L, L, L, 1.0f);
			}
		}

		//--------------------------------
		public int subindex; //per type

		[NonSerialized] public bool isSpecial;
		public virtual bool isDynamic { get { return false; } }
		[NonSerialized] public bool errorState= false;
		public string errorMessage;

		public bool closeOnRebuild;
		public long editorbuildIndex= long.MaxValue;

		protected bool hideToolbar;
		protected bool hideHelpButton;
		protected bool repaintInBackground;
		protected bool ignoreModalControls;
		public bool repaintOccasionallyInEdit;
		public bool repaintOccasionallyInPlay;
		public bool repaintConstantlyInEdit;
		public bool repaintConstantlyInPlay;

		public bool hasContextMenu;
		public bool hasSpecialCloseButton;

		public float contentWidth { get { return Mathf.Clamp(position.width, minSize.x, maxSize.x); } }

		public enum WindowContentsMode {
			Default,
			Settings,
			Help
		}

		public bool hasMouseFocus { get { return EditorWindow.mouseOverWindow == this; } }
		public bool hasMouseCursorInContent { get; private set; }
		public bool busy { get { return windowMode == WindowContentsMode.Default; } }
		[SerializeField] private WindowContentsMode windowMode;
		[SerializeField] private Vector2 scrollPosition_otherMode;

		protected virtual ModuleSettings settingsModule { get { return null; } }

		//User settings
		[NonSerialized] public bool settingsDirty= true;
		[VulcanWindowSetting] public bool showDirectories= true;
		[VulcanWindowSetting] public bool showOnlyFavorites;
		[VulcanWindowSetting] public bool showGridView;
		[VulcanWindowSetting] public bool showPictures= true;
		[VulcanWindowSetting] public bool largePictures;
		[VulcanWindowSetting] public bool showPictureOutlines= true;
		[VulcanWindowSetting] public bool naturalSorting;
		public Texture contentsBehind;

		//----------------------------------
		[VulcanWindowSetting] public bool filterEnabled;
		[VulcanWindowSetting] public int filterMask;
		[VulcanWindowSetting] public bool autoFilterRefresh;

		public string defaultTitle= "";
		[VulcanWindowSetting] public string userTitle= "";
		public Texture defaultIcon;
		public new string title {
			get {
				if(!string.IsNullOrEmpty(userTitle)) return userTitle;
				if(!string.IsNullOrEmpty(defaultTitle)) return defaultTitle;
				return GetType().Name;
			}
		}

		public new GUIContent titleContent {
			get { return base.titleContent; }
			//Use defaultTitle and defaultIcon instead
		}

		public bool GetFilterState(int index) {
			//if(!filterEnabled) return false;
			return (filterMask & (1 << index)) != 0;
		}

		public void SetFilterState(int index, bool state= true) {
			if(state) {
				filterMask |= (1 << index);
			}else{
				filterMask &= ~(1 << index);
			}
		}

		public void ToggleFilterState(int index) {
			filterMask ^= (1 << index);
		}

		protected bool initializedPanel { get; private set; }
		[NonSerialized] protected bool dirty;
		[NonSerialized] protected bool hasAttention;
		[NonSerialized] protected bool hasContentsAttention; //if the mouse is inside the content area, and wasn't dragged from elsewhere
		[NonSerialized] protected Rect positionPrevious;
		[NonSerialized] protected bool requestClickInteraction;
		[NonSerialized] protected bool requestContextMenu;
		protected float toolbarSize { get { return (hideToolbar && (windowMode == WindowContentsMode.Default)) ? 0 : 20; } }

		[NonSerialized] bool windowWorking_;
		protected bool windowWorking {
			get { return windowWorking_; }
			set {
				if(value == windowWorking_) return;

				windowWorking_= value;
				if(value) {
					icon_BeforeWorking= titleContent.image;
				}else{
					titleContent.image= icon_BeforeWorking;
					icon_BeforeWorking= null;
				}
			}
		}
		Texture icon_BeforeWorking;

		public bool hasScrollingH= true;
		public bool hasScrollingV= true;
		protected Vector2 scrollPosition {
			get { return scrollPosition_safe; }
			set { scrollPosition_immediate= value; }
		}

		protected Rect contentRect_absolute {
			get {
				var rect= contentRect;
				rect= new Rect(position.x + rect.x, position.y + rect.y, rect.width, rect.height);
				return rect;
			}
		}

		protected Rect contentRect {
			get {
				var rect= new Rect(0, 0, position.width, position.height);
				rect.yMin += toolbarSize;
				if(hasScrollingH) rect.yMax -= GUI.skin.horizontalScrollbar.fixedHeight;
				if(hasScrollingV) rect.xMax -= GUI.skin.verticalScrollbar.fixedWidth;
				return rect;
			}
		}

		[SerializeField] protected Vector2 scrollPosition_immediate;
		[SerializeField] protected Vector2 scrollPosition_safe; 

		[SerializeField] protected HelpContents helpConfig;
		public bool hasHelpContents { get { return (helpConfig != null) && helpConfig.hasHelpContents; } }

		GUIStyle helpButtonStyle;
		int RepaintCount;

		//--------------------------------
		/*
		static WindowType LoadOrCreate<WindowType>(string name= null) where WindowType: VulcanWindow {
			WindowType v;

			if(name == null) name= typeof(WindowType).Name;
			string assetPath= "Assets/Settings/Pantheon";
			assetPath += name;
			//if(index > 0) assetPath += "." + index;
			assetPath += ".settings.asset";

			if(!File.Exists(assetPath)) {
				v= ScriptableObject.CreateInstance<WindowType>();
				v.hideFlags &= ~(HideFlags.DontSaveInEditor);
				AssetDatabase.CreateAsset(v, assetPath);
			}else{
				v= AssetDatabase.LoadAssetAtPath<WindowType>(assetPath);
			}

			return v;
		}
		*/

		//--------------------------------
		public VulcanWindow() {
			initializedPanel= false;
			dirty= true;
		}

		void Awake() {
			initializedPanel= false;
		}

		void OnProjectChange() {
			initializedPanel= false;
		}

		void OnEnable() {
			initializedPanel= false;
		}

		protected virtual void OnDestroy() {
			SaveWindowSettings();

			VulcanWindowManager.AllWindows.Remove(this); 

			windowWorking= false;
			//EditorApplication.update -= _WindowUpdate;
			#if UNITY_2018_1_OR_NEWER
			EditorApplication.projectChanged -= RefreshAssets; 
			#else
			EditorApplication.projectWindowChanged -= RefreshAssets;
			#endif
		}

		protected abstract void InitializePanel(); 
		private void _InitializePanel() {
			if(initializedPanel) return;
			initializedPanel= true;

			LoadWindowSettings();

			if(closeOnRebuild && (editorbuildIndex < EditorManager.editorbuildIndex)) {
				Close();
				return;
			}
			editorbuildIndex= EditorManager.editorbuildIndex;

			dirty= true;

			VulcanWindowManager.AllWindows.Add(this);
			Styles.Initialize();

			autoRepaintOnSceneChange= true;
			wantsMouseEnterLeaveWindow= true;
			wantsMouseMove= true;

			//EditorApplication.update += _WindowUpdate;
			#if UNITY_2018_1_OR_NEWER 
			EditorApplication.projectChanged += RefreshAssets;
			#else
			EditorApplication.projectWindowChanged += RefreshAssets;
			#endif

			helpButtonStyle= new GUIStyle();
			helpButtonStyle.alignment= TextAnchor.MiddleRight;

			InitializePanel();
			RefreshAssets();
		}

		void LoadWindowSettings() {
			var json= VulcanWindowManager.getWindowSettings(GetType(), subindex);
			LoadWindowSettings(json);
		}

		void LoadWindowSettings(string json) {
			if(json == null) return;

			var type= GetType();

			var data= MiniJSON.Json.Deserialize(json) as Dictionary<string, object>;
			data.Remove("type");
			data.Remove("subindex");
			data.Remove("timeOfSave"); 

			foreach(var e in data) {
				LoadFieldSetting(e.Key, e.Value);
			}
		}

		public void SaveWindowSettings() {
			if(isDynamic) return;

			settingsDirty= false;
			VulcanWindowManager.saveWindowSettings(GetType(), subindex, GetWindowSettings());
		}

		void LoadFieldSetting(string fieldName, object value) {
			var field= GetField(fieldName);

			if(field == null) {
				if(EditorManager.debugPantheon) Debug.LogWarning(GetType().Name + "." + fieldName + " not present in class");

			}else{
				object context= (field.IsStatic ? null : this);
				var attribute= field.GetCustomAttribute<VulcanWindowSettingAttribute>();

				if(attribute == null) {
					Debug.LogError(GetType().Name + "." + fieldName + " is not a VulcanWindowSetting");

				}else{
					attribute.set(context, field, value);
				}
			}
		}

		FieldInfo GetField(string fieldName) {
			var type= GetType();

			while(true) {
				var field= GetType().GetField(fieldName, BindingFlags.GetField | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
				if(field != null) return field;

				if(type == typeof(VulcanWindow)) break;
				type= type.BaseType;
			}

			return null;
		}

		public string GetWindowSettings() {
			string str= "{";
			str += "\n\t";	str += "\"type\": \"" + GetType().FullName +"\"";
			str += ",\n\t";	str += "\"subindex\": " + subindex;
			str += ",\n\t";	str += "\"timeOfSave\": \"" + DateTime.Now.ToString() + "\"";

			foreach(var i in Manager.GetMemberAttributesOf<VulcanWindowSettingAttribute>(GetType())) {
				var attrib= i.Item1;
				var field= i.Item2 as FieldInfo;
				if(field == null) continue;

				str += ",\n\t";
				str += attrib.getJson(this, field);
			}

			str += "\n}";

			return str;
		}

		void Update() {
			if(!EditorManager.Initialized) return;

			if(!initializedPanel) {
				Repaint();
				return;
			}

			if(hasAttention) {
				//Repaint();
			}

			if(scrollPosition_safe != scrollPosition_immediate) {
				scrollPosition_safe= scrollPosition_immediate;
				Repaint();
			}

			if(!repaintInBackground && !EditorManager.hasEditorFocus) {
				return;
			}

			if(!errorState) {
				try {
					WindowUpdate();
					if(dirty) {
						dirty= false;
						RefreshContents();
					}
				}catch(Exception e) {
					errorState= true;
					errorMessage= e.ToString();
					Debug.LogError("An exception occured in VulcanWindow '" + titleContent.text + "'");
					Debug.LogException(e);
				}
			}
		}

		protected abstract void WindowUpdate();

		void OnInspectorUpdate() {
			UpdateTitle();
			if(windowWorking) Repaint();
		}

		void UpdateTitle() {
			if(windowWorking) {
				int index= Mathf.FloorToInt(((float)(EditorApplication.timeSinceStartup % 1.0)) * 12);
				titleContent.image= EditorGUIUtility.IconContent("WaitSpin" + index.ToString("D2")).image;
			} 
		}

		public void Refresh() {
			dirty= true;
			base.titleContent.text= title;
			base.titleContent.image= defaultIcon;
			Repaint();
		}

		protected virtual void ManualRefresh() {
			RefreshAssets();
			//RefreshContents(); //redundant
			Refresh();
		}

		protected virtual void RefreshAssets() {

		}

		protected virtual void RefreshContents() {

		}

		void IHasCustomMenu.AddItemsToMenu(GenericMenu menu) {
			AddItemsToMenu_(menu);

			if(hasHelpContents) {
				//if(menu.GetItemCount() > 0) menu.AddSeparator("");

				menu.AddItem(new GUIContent("Advanced/Hide help button"), hideHelpButton, ()=> {
					hideHelpButton= !hideHelpButton;
				});

				menu.AddItem(new GUIContent("Help"), false, ()=> {
					ShowHelp();
				});
			}

			menu.AddItem(new GUIContent("Advanced/Window inspector"), false, ()=> {
				Selection.activeObject= this;
				EditorGUIUtility.PingObject(this);
			});

			if(settingsModule != null) {
				bool isSettings= (windowMode == WindowContentsMode.Settings);
				menu.AddItem(new GUIContent("Settings"), isSettings, ()=> {
					windowMode= isSettings ? WindowContentsMode.Default : WindowContentsMode.Settings;
				});
			}
		}

		protected virtual void AddItemsToMenu_(GenericMenu menu) {

		}

		protected void OnGUI() {
			if(isSpecial) {
				var rect= new Rect(0, 0, position.width, position.height);

				if(contentsBehind != null) {
					GUI.DrawTexture(rect, contentsBehind, ScaleMode.ScaleToFit, true);
				}

				EditorGUI.BeginDisabledGroup(true);
				GUI.Button(rect, "");
				EditorGUI.EndDisabledGroup();
			}

			if(errorState) {
				ShowNotification(new GUIContent("Pantheon caught an error\nClick here to try again"));
				if(GUI.Button(new Rect(0,0,position.width, position.height), "", GUIStyle.none)) {
					errorState= false;
					RemoveNotification();
				}
				return;
			}

			try {
				if(!initializedPanel) _InitializePanel();

				EditorGUI.BeginDisabledGroup(!ignoreModalControls && TextInputDialog.somethingIsModal);

				if(
					dirty
					|| (positionPrevious.width != position.width)
					//|| (positionPrevious.height != position.height) //height is not stable when the panel is being moved, in older versions of Unity
				) {
					positionPrevious= position;
					dirty= false;
					RefreshContents();
				}

				ProcessEvents(Event.current);
				DrawGUI();

				if(
					GUI.changed
					#if !UNITY_2019_1_OR_NEWER //GUI.changed is set less frequently in newer versions (which is good for us)
					&& (scrollPosition_immediate == scrollPosition) //but in older versions, we can't afford to refresh constantly while scrolling
					#endif
				) {
					if(windowMode == WindowContentsMode.Default) {
						Refresh();
					}
					Repaint();
					EditorUtility.SetDirty(this);
				}

				EditorGUI.EndDisabledGroup();

			}catch(Exception e) {
				errorState= true;
				errorMessage= e.ToString();
				Debug.LogException(e);
				Debug.LogError("An exception occured in VulcanWindow '" + titleContent.text + "'");
			}
		}

		protected virtual void ProcessEvents(Event e) {
			hasContentsAttention &= hasAttention;

			switch(e.type) {
			case EventType.Repaint:
				UpdateTitle();
				
				hasMouseCursorInContent= hasMouseFocus && contentRect.Contains(e.mousePosition);

				++RepaintCount;
				if(requestContextMenu) {
					requestContextMenu= false;
					requestClickInteraction= false;
					if(hasContextMenu) {
						var menu= CreateContextMenu();
						if(menu != null) {
							Repaint();
							menu.ShowAsContext();
						}
					}

				}else if(requestClickInteraction) {
					requestClickInteraction= false;
					onClicked();
				}
				break;

			case EventType.KeyDown:
				Repaint();
				switch(e.keyCode) {
				case KeyCode.F1:
					if(hasHelpContents) ShowHelp();
					break;

				case KeyCode.F5:
					ManualRefresh();
					break;

				case KeyCode.UpArrow:
					scrollPosition_immediate += new Vector2(0, -100);
					break;

				case KeyCode.DownArrow:
					scrollPosition_immediate += new Vector2(0, 100);
					break;

				case KeyCode.Home:
					scrollPosition_immediate.y= 0;
					break;

				case KeyCode.End:
					scrollPosition_immediate.y= float.PositiveInfinity;
					break;

				case KeyCode.PageUp:
					scrollPosition_immediate += new Vector2(0, -contentRect.height);
					break;

				case KeyCode.PageDown:
					scrollPosition_immediate += new Vector2(0, contentRect.height);
					break;
				}
				break;

			case EventType.MouseMove:
				Repaint();
				break;

			case EventType.MouseEnterWindow:
				Repaint();
				hasAttention= true;
				break;

			case EventType.MouseLeaveWindow:
				Repaint();
				hasAttention= false;
				break;

			case EventType.MouseDown:
				hasContentsAttention= contentRect.Contains(e.mousePosition);
				if(hasContentsAttention) {
					if((e.clickCount == 2) && !e.control && !e.command && !e.shift) {
						//Delay the event, to avoid rapaint/layout not being in sync
						Manager.editorRunOnce += onDoubleClicked;
					}else{
						//Let MouseUp and ContextClick handle the selection (it avoids race conditions in the multiselect logic)
					}
				}
				break;

			case EventType.MouseUp:
				hasContentsAttention= contentRect.Contains(e.mousePosition);
				if(hasContentsAttention) {
					requestClickInteraction= true;
				}
				break;

			case EventType.ContextClick:
				if(hasContextMenu && contentRect.Contains(e.mousePosition)) {
					requestContextMenu= true;
				}
				break;
			}
		}

		private void DrawGUI() {
			Styles.Initialize();

			float contentWidth= this.contentWidth;
			float contentHeight= position.height;

			if(isSpecial) {
				float borderSize= 1;
				contentWidth -= borderSize * 2;
				contentHeight -= borderSize * 2;

				GUILayout.BeginArea(new Rect(borderSize, borderSize, contentWidth, contentHeight));

			}else	if(contentWidth != position.width) {
				//Unity 2019 introduced a bug where maxSize was not obeyed
				GUILayout.BeginArea(new Rect((position.width - contentWidth) / 2, 0, contentWidth, contentHeight));
			}else{
				GUILayout.BeginArea(new Rect(0,0, position.width, contentHeight));
			}

			switch(windowMode) {
			case WindowContentsMode.Default:
				if(!hideToolbar) _DrawToolbar(DrawToolbar);
				_DrawContents(new Rect(0, toolbarSize, contentWidth, contentHeight - toolbarSize));
				break;

			case WindowContentsMode.Settings:
				if(settingsModule == null) {
					windowMode= WindowContentsMode.Default;
					Debug.LogError("This window panel does not have a settings module");

				}else{
					_DrawToolbar((Rect rect_)=> {
						SettingsWindow.showAdvanced= GUILayout.Toggle(SettingsWindow.showAdvanced, "Advanced", EditorStyles.toolbarButton);

						GUILayout.FlexibleSpace();

						if(GUILayout.Button("<< Close options", EditorStyles.toolbarButton)) {
							windowMode= WindowContentsMode.Default;
						}
					});

					var rect= new Rect(0, toolbarSize, contentWidth, contentHeight - toolbarSize);
					GUILayout.BeginArea(rect);
					scrollPosition_otherMode= GUILayout.BeginScrollView(scrollPosition_otherMode, GUIStyle.none, GUI.skin.verticalScrollbar);
					GUILayout.BeginVertical();

					settingsModule.DrawHeader();
					settingsModule.DrawContents(contentWidth);

					GUILayout.EndVertical();
					GUILayout.EndScrollView();
					GUILayout.EndArea();
				}
				break;

			default:
				windowMode= WindowContentsMode.Default;
				throw new NotImplementedException();
			}

			GUILayout.EndArea();
		}

		private void _DrawToolbar(Action<Rect> contents) {
			var rect= new Rect(0, 0, contentWidth, toolbarSize);

			bool showHelp= hasHelpContents && !hideHelpButton;

			GUILayout.BeginArea(rect, EditorStyles.toolbar);
		  GUILayout.BeginHorizontal();
			if(showHelp) rect.xMax -= toolbarSize;
			if(hasSpecialCloseButton) rect.xMax -= toolbarSize;

			if(contents != null) contents(rect);

			if(showHelp) {
				rect.xMax += toolbarSize;
				GUILayout.Space(toolbarSize);
				if(GUI.Button(
					new Rect(rect.xMax - toolbarSize - 2, 0, toolbarSize, toolbarSize),
					EditorGUIUtility.IconContent("_Help"),
					helpButtonStyle
				)) {
					ShowHelp();
				}
			}

			if(hasSpecialCloseButton) {
				rect.xMax += toolbarSize;
				GUILayout.Space(toolbarSize);
				if(GUI.Button(
					new Rect(rect.xMax - toolbarSize - 2, 0, toolbarSize, toolbarSize),
					EditorGUIUtility.IconContent("winbtn_win_close"),
					helpButtonStyle
				)) {
					EditorApplication.delayCall += ()=> Close();
				}
			}

			GUILayout.EndHorizontal();
      GUILayout.EndArea();
		}

		protected virtual void DrawToolbar(Rect rect) {

		}

		protected bool DrawToolbarTab(string name, bool state) {
			return DrawToolbarTab(name, null, state);
		}

		protected bool DrawToolbarTab(string name, Texture icon, bool state) {
			return DrawToolbarTab(new GUIContent(name, icon), state);
		}

		protected bool DrawToolbarTab(GUIContent content, bool state) {
			return GUILayout.Toggle(state, content, Styles.toolbarButton);
		}

		private void _DrawContents(Rect rect) {
			GUILayout.BeginArea(rect, Styles.noPadding);
			rect= new Rect(0, 0, rect.width, rect.height);

			if(hasScrollingH || hasScrollingV) {
				scrollPosition_immediate= EditorGUILayout.BeginScrollView(
					scrollPosition_immediate,
					false,
					false,
					hasScrollingH ? GUI.skin.horizontalScrollbar : GUIStyle.none,
					hasScrollingV ? GUI.skin.verticalScrollbar : GUIStyle.none,
					GUIStyle.none
				);

				if(hasScrollingH) rect.yMax -= GUI.skin.verticalScrollbar.fixedHeight;
				else scrollPosition_immediate.x= 0;
				if(hasScrollingV) rect.xMax -= GUI.skin.horizontalScrollbar.fixedWidth;
				else scrollPosition_immediate.y= 0;
			}

			//Matching label width to what inspector panels use (as of 2019.3)
			float contextWidth= position.width;
			EditorGUIUtility.labelWidth= Mathf.Max((float)(contextWidth * 0.45f - 40.0f), 120f);
			var resultRect= DrawContents(rect);

			if(Event.current.type == EventType.Repaint) {
				resultRect.xMax= Mathf.Max(resultRect.xMax, GUILayoutUtility.GetLastRect().xMax);
				resultRect.yMax= Mathf.Max(resultRect.yMax, GUILayoutUtility.GetLastRect().yMax);
				scrollPosition_immediate.x= Mathf.Clamp(resultRect.xMax - rect.width, 0.0f, scrollPosition_immediate.x);
				scrollPosition_immediate.y= Mathf.Clamp(resultRect.yMax - rect.height, 0.0f, scrollPosition_immediate.y);
			}

			GUILayout.BeginVertical(EditorStyles.inspectorDefaultMargins);
			//This empty section is used to workaround a weird quirk where the margins change if that style is present in the layout.
			//I can't make it stop doing that, so instead I make sure it always does that.
			GUILayout.EndVertical();

			if(hasScrollingH || hasScrollingV) {
				GUILayout.EndScrollView();
			}

		  GUILayout.EndArea();

			if(EditorManager.debugPantheon) {
				GUILayout.BeginArea(new Rect(position.width - 200, position.height - 20, 200, 20));
				int ms= (int)Math.Floor((VulcanWindowManager.timeLastStaticUpdate - VulcanWindowManager.timePrevStaticUpdate) * 1000.0);

				GUILayout.Label("Repaint count: " + RepaintCount + " (" + ms + "ms)");
				GUILayout.EndArea();
			}
		}

		protected abstract Rect DrawContents(Rect rect);

		public void ShowHelp() {
			if(!hasHelpContents) return;
			helpConfig.Show();
		}

		protected virtual void onClicked() {

		}

		protected virtual void onDoubleClicked() {

		}

		protected virtual GenericMenu CreateContextMenu() {
			return null;
		}		
	}

}