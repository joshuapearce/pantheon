﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;

using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;

namespace Pantheon {
	public class ToolsMenu {
		public const int MenuPriority= 1000;
		public const int GeneralMenuPriority= 20;

		#if !UNITY_2018_1_OR_NEWER
		[MenuItem("Edit/Clear All PlayerPrefs")]
		#endif
		//[MenuItem("Tools/PlayerPrefs/Clear")]
		public static void ClearPlayerPrefs() {
			if(EditorUtility.DisplayDialog("Clear All PlayerPrefs", "Are you sure you want to clear all PlayerPrefs? This action cannot be undone.", "Yes", "No")) {
				PlayerPrefs.DeleteAll();
			}
		}

		[MenuItem("Tools/Folders/Assets", false, ToolsMenu.MenuPriority + 1)]
		static void OpenAssets() {
			System.Diagnostics.Process.Start(EditorManager.projectPath + "Assets");
		}

		[MenuItem("Tools/Folders/Editor logs", false, ToolsMenu.MenuPriority + 1)]
		static void OpenEditorLogFolder() {
			#if UNITY_EDITOR_WIN
			System.Diagnostics.Process.Start(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "Unity/Editor"));
			#elif UNITY_EDITOR_OSX
			System.Diagnostics.Process.Start(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), "Library/Logs/Unity"));
			#endif
		}

		[MenuItem("Tools/Folders/Project folder", false, ToolsMenu.MenuPriority + 1)]
		static void OpenProjectFolder() {
			System.Diagnostics.Process.Start(EditorManager.projectPath);
		}

		[MenuItem("Tools/Folders/Persistent data", false, ToolsMenu.MenuPriority + 1)]
		static void OpenPersistentData() {
			System.Diagnostics.Process.Start(Application.persistentDataPath);
		}

		[MenuItem("Tools/Folders/Unity.exe", false, ToolsMenu.MenuPriority + 1)]
		static void OpenUnityLocation() {
			System.Diagnostics.Process.Start(Path.GetDirectoryName(EditorApplication.applicationPath));
		}

		[MenuItem("Tools/Pantheon/Open appdata folder", false, ToolsMenu.MenuPriority + 60)]
		public static void OpenAppData() {
			System.Diagnostics.Process.Start(EditorManager.appDataPath);
		}

		[MenuItem("Tools/Pantheon/Open projectdata folder", false, ToolsMenu.MenuPriority + 60)]
		public static void OpenProjectData() {
			System.Diagnostics.Process.Start(EditorManager.projectPath + "Pantheon");
		}

		[MenuItem("Tools/Pantheon/Advanced/Uninstall", false, ToolsMenu.MenuPriority + 1000)]
		public static void ShowUninstallInfo() {
			EditorManager.ShowHelpFile("Uninstall.txt");
		}
	}
}