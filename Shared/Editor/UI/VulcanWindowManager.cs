﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;

using UnityEditor;
using UnityEngine;

namespace Pantheon {
	[InitializeOnLoad]
	public static class VulcanWindowManager {
		public static List<VulcanWindow> AllWindows= new List<VulcanWindow>();

		//------------------------------ 
		static VulcanWindowManager() { 
			Manager.editorUpdate += StaticUpdate;

			Manager.editorRunOnce += ()=> {
				//Debug.Log(EditorManager.projectPath + "Pantheon/WindowSettings/");
				Directory.CreateDirectory(EditorManager.projectPath + "Pantheon/WindowSettings/");
			};

			EditorManager.onEditorFocus += (state) => {
				if(!state) SaveAllWindows(false);
			};
		}

		public static double timePrevStaticUpdate { get; private set; }
		public static double timeLastStaticUpdate { get; private set; }

		static void StaticUpdate() {
			var t= EditorApplication.timeSinceStartup;
			bool occasional= (Math.Floor(timeLastStaticUpdate) != Math.Floor(t));
			timePrevStaticUpdate= timeLastStaticUpdate;
			timeLastStaticUpdate= t;

			foreach(var window in VulcanWindowManager.AllWindows) {
				if(Application.isPlaying) {
					if(window.repaintConstantlyInPlay) window.Repaint();
					else if(occasional && window.repaintOccasionallyInPlay) window.Repaint();
				}else{
					if(window.repaintConstantlyInEdit) window.Repaint();
					else if(occasional && window.repaintOccasionallyInEdit) window.Repaint();
				}
			}

			SaveAllWindows();
		}

		public static void RepaintAll() {
			foreach(var window in VulcanWindowManager.AllWindows) window.Repaint();
		}

		//------------------------------
		public static WindowType OpenWindow<WindowType>() where WindowType: VulcanWindow {
			WindowType v= GetWindow<WindowType>();

			v.Show();
			v.Focus();
			return v;
		}

		public static WindowType OpenMenuWindow<WindowType>(Vector2 position, Vector2 dimensions, float translucency= 0.0f, bool delayed= true) where WindowType: VulcanWindow {
			UnityEditorInternal.InternalEditorUtility.RepaintAllViews();

			var v= EditorWindow.CreateInstance<WindowType>();
			v.isSpecial= true;
			v.closeOnRebuild= true;
			//v.ShowPopup();

			if(EditorManager.settings.enableTransparentMenus && (translucency > 0.0f)) {
				int w= (int)dimensions.x;
				int h= (int)dimensions.y;

				var bg= new Texture2D(w, h, TextureFormat.RGBA32, false);
				var colors= UnityEditorInternal.InternalEditorUtility.ReadScreenPixel(position, w, h);

				int borderSize= 2;
				for(int x= borderSize; x < w - borderSize; x++) {
					for(int y= borderSize; y < h - borderSize; y++) {
						int i= x + y * w;
						colors[i].a= translucency;
					}
				}

				bg.SetPixels(colors);
				bg.Apply();
				v.contentsBehind= bg;
			}

			EditorManager.delayCall(delayed ? 3 : 0,
				()=> {
					v.ShowAsDropDown(new Rect(position.x, position.y, 0, 0), dimensions);
					v.Focus();
				}
			);

			return v;
		}

		public static WindowType OpenMenuWindow<WindowType>(Rect position, float translucency= 0.0f) where WindowType: VulcanWindow {
			return OpenMenuWindow<WindowType>(new Vector2(position.x, position.y), new Vector2(position.width, position.height), translucency);
		}

		public static WindowType GetWindow<WindowType>() where WindowType: VulcanWindow {
			WindowType v= null;
			foreach(var window in AllWindows) {
				if(window is WindowType) {
					v= window as WindowType;
					break;
				}
			}
			if(v == null) v= EditorWindow.GetWindow<WindowType>();

			return v;
		}

		//------------------------------
		public static void SaveAllWindows(bool ifDirty= true) {
			foreach(var window in AllWindows) {
				if(!ifDirty || (window.settingsDirty)) {
					window.SaveWindowSettings();
				}
			}
		}

		public static string getWindowSettingsFilePath(Type type, int index) {
			return EditorManager.projectPath + "Pantheon/WindowSettings/" + type.FullName + "#" + index + ".json";
		}

		public static string getWindowSettings(Type type, int index) {
			var filePath= getWindowSettingsFilePath(type, index);

			if(!File.Exists(filePath)) return null;
			return File.ReadAllText(filePath);
		}

		public static void saveWindowSettings(Type type, int index, string json) {
			if(EditorManager.debugPantheon) Debug.Log("Saving window settings for " + type.FullName + "#" + index);

			var filePath= getWindowSettingsFilePath(type, index);
			File.WriteAllText(filePath, json);
		}
	}
}