﻿using System;
using System.Reflection;

namespace Pantheon {
	[AttributeUsage(AttributeTargets.Field)]
	class VulcanWindowSettingAttribute:
		Attribute
	{
		public string getJson(object obj, FieldInfo field) {
			var value= field.GetValue(obj);

			var str= '"' + field.Name + "\": ";

			if(field.FieldType == typeof(bool)) {
				str += (bool)value ? "true" : "false";

			}else if(field.FieldType.IsPrimitive) {
				str += value.ToString();

			}else{
				str += '"' + value.ToString() + '"';
			}

			return str;
		}

		public void set(object obj, FieldInfo field, object value) {
			if(field.FieldType == value.GetType()) {
				field.SetValue(obj, value);

			}else{
				var str= value.ToString();

				if(field.FieldType == typeof(bool)) {
					bool v; bool.TryParse(str, out v); field.SetValue(obj, v);

				}else if(field.FieldType == typeof(int)) {
					int v; int.TryParse(str, out v); field.SetValue(obj, v);

				}else if(field.FieldType == typeof(float)) {
					float v; float.TryParse(str, out v); field.SetValue(obj, v);
					
				}else if(field.FieldType == typeof(double)) {
					double v; double.TryParse(str, out v); field.SetValue(obj, v);

				}else if(field.FieldType == typeof(string)) {
					field.SetValue(obj, str);
					
				}else if(field.FieldType == typeof(DateTime)) {
					DateTime v; DateTime.TryParse(str, out v); field.SetValue(obj, v);

				}else{
					throw new InvalidCastException(
						"Field name: " + field.Name + "; " +
						"Field type: " + field.FieldType.Name + "; " +
						"Value type: " + value.GetType().Name
					);
				}
			}
		}
	}
}
