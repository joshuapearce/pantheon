﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

using UnityEditor;
using UnityEngine;

namespace Pantheon {
	public static class Styles {
		//--------------------------------
		static bool Initialized= false;

		public static GUIStyle text;
		public static GUIStyle link;
		public static GUIStyle header;
		public static GUIStyle foldout;
		public static GUIStyle noPadding;

    public static GUIStyle boldPopupStyle;

		public static GUIStyle headingButton;
		public static GUIStyle standardButton;
		public static GUIStyle toolbarButton;
		public static GUIStyle toolbarToggleButton;
		public static GUIStyle toolbarTab;
		public static GUIStyle radioButton;
		public static GUIStyle checkBox;

		public static GUIContent favorite;
		public static GUIContent nonFavorite;
		public static GUIContent nonFavorite_over;
		public static Texture icon_Favorite;
		public static Texture icon_NonFavorite;
		public static Texture icon_NonFavorite_over;

		public static Color color_Link { get { return link.normal.textColor; } }
		public static Color color_Hline {	get { var c= text.normal.textColor; c.a= 0.44f; return c; } }


		//--------------------------------
		static Styles() {
			//It's important this class doesn't initialize early, since Unity won't be ready.
			//It also needs to initialize earlier than other components of Pantheon, so we can't rely on the EditorManager
			  //Note: The unity settings panel can be a good test, if no other pantheon windows are open
			//We rely on the basic static initializer to run on first use, instead of at program initialization.
			Initialize();
		}

		public static void Initialize() {
			//Called from static initializer, as well as some other classes for safety
			if(Initialized) return;
			Initialized= true;			

			text= new GUIStyle(EditorStyles.label);
			link= new GUIStyle(EditorStyles.label);
			//link.normal.textColor= new Color32(0x00, 0x78, 0xDA,255);
			link.normal.textColor= EditorGUIUtility.isProSkin ? new Color32(0x18, 0x8D, 0x9F,255) : new Color32(0x00, 0x00, 0xEE,255);
			header= EditorStyles.centeredGreyMiniLabel;

			foldout= new GUIStyle(EditorStyles.boldLabel);

			noPadding= new GUIStyle();
			noPadding.margin= new RectOffset(0,0,0,0);
			noPadding.padding= new RectOffset(0,0,0,0);

      boldPopupStyle= new GUIStyle(EditorStyles.popup);
      boldPopupStyle.fontStyle = FontStyle.Bold;

			headingButton= new GUIStyle("miniButton");
			standardButton= new GUIStyle();
			radioButton= EditorStyles.radioButton;
			checkBox= new GUIStyle();

			toolbarButton= EditorStyles.toolbarButton;
			toolbarToggleButton= EditorStyles.toolbarButton;

			icon_Favorite= EditorGUIUtility.IconContent("Favorite Icon").image;
			favorite= new GUIContent(icon_Favorite);
			icon_NonFavorite= null;
			nonFavorite= new GUIContent(icon_NonFavorite);
			icon_NonFavorite_over= EditorGUIUtility.IconContent("Favorite").image;
			nonFavorite_over= new GUIContent(icon_NonFavorite_over);
		}

		//--------------------------------
		public static void DrawLink(GUIContent content, EditorManager.VoidFunction f) {
			GUILayout.BeginHorizontal();
			if(content.image != null) {
				if(GUILayout.Button(content.image, link, GUILayout.Height(18), GUILayout.Width(18), GUILayout.ExpandHeight(false))) {
					f();
				}
			}

			if(GUILayout.Button(content.text, link, GUILayout.Height(18))) {
				f();
			}

			GUILayout.EndHorizontal();
		}

		public static void DrawLink(string url) {
			DrawLink(url, url);
		}

		public static void DrawLink(string text, string url) {
			var content= new GUIContent(text ?? url);

			if(url.StartsWith("http://") || url.StartsWith("https://") || url.StartsWith("www.")) {
				content.image= EditorGUIUtility.IconContent("BuildSettings.Web.Small").image;
			}else	if(url.EndsWith("/") || url.EndsWith("\\")) {
				content.image= EditorGUIUtility.IconContent("Folder Icon").image;
			}else{
				content.image= EditorGUIUtility.IconContent("DefaultAsset Icon").image;
			}

			if(content.text != url) content.tooltip= url;

			DrawLink(content, ()=> {
				Debug.Log("A: " + url);
				System.Diagnostics.Process.Start(url);
			});
		}

		public static void DrawLink(string text, EditorManager.VoidFunction f) {
			DrawLink(new GUIContent(text), f);
		}

		public static void DrawLink(string text, Texture icon, EditorManager.VoidFunction f) {
			DrawLink(new GUIContent(text, icon), f);
		}

		public static void DrawHorizontalLine(int thickness= 1, int padding= 0) {
			HorizontalLine(color_Hline, thickness, padding);
		}

		public static void HorizontalLine(Color color, int thickness= 1, int padding= 0) {
			Rect r= EditorGUILayout.GetControlRect(GUILayout.Height(thickness + padding));
			r.height= thickness;
			r.y += padding/2;
			r.x -= 4;
			r.width += 6;
			EditorGUI.DrawRect(r, color);
		}

		public static bool DrawFoldoutHeader(bool state, GUIContent content, bool enabled= true) {
			int h= 20;

			GUILayout.Label("", GUILayout.Height(h), GUILayout.ExpandWidth(true));
			var rect= GUILayoutUtility.GetLastRect();
			rect.xMin -= 4;
			rect.xMax += 4;
			rect.y++;
			rect.yMax++;
			if(rect.Contains(Event.current.mousePosition)) {
				EditorGUI.DrawRect(rect, new Color(1,1,1,0.27f));
			}else{
				EditorGUI.DrawRect(rect, new Color(1,1,1,0.09f));
			}

			var prevColor= GUI.color;
			if(!enabled) GUI.color= new Color(1,1,1,0.5f);

			var x= 2;
			state= EditorGUI.Foldout(new Rect(rect.xMin + 4, rect.yMin, rect.width - x, rect.height), state, new GUIContent("", content.tooltip), true);
			x += 16;

			if(content.image != null) {
				GUI.Label(new Rect(rect.xMin + x, rect.yMin, h, h), content.image);
				x += h + 4;
			}
			GUI.Label(new Rect(rect.xMin + x, rect.yMin, rect.width - x, rect.height), content.text, foldout);
			GUI.color= prevColor;

			EditorGUI.DrawRect(new Rect(rect.xMin, rect.yMin - 1, rect.width, 1), color_Hline);

			return state;
		}
	}
}