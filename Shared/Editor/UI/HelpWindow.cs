﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEditor;
using UnityEngine;


namespace Pantheon {
	[InitializeOnLoad]
	public class HelpWindow:
		EditorWindow
	{
		static HelpWindow() {
			EditorManager.RegisterModule("Help");
		}

		[Serializable]
		public class Page {
			public string title;
			public string text;
			public Texture2D image;
		}

		public static void Show(string title, List<Page> content, int widthPixels=-1, int heightPixels=-1, Type ID= null) {
			Show(title, content.ToArray(), widthPixels, heightPixels, ID);
		}

		public static HelpWindow Show(string title, Page[] content, int widthPixels=-1, int heightPixels=-1, Type ID= null) {
			if(ID != null) {
				foreach(var i in All) {
					if((i.ID == ID) && (i.title == title)) {
						i.Show();
						return i;
					}
				}
			}

			if(widthPixels <= 0) widthPixels= 500;
			if(heightPixels <= 0) heightPixels= 650;
			
			var window= EditorWindow.CreateInstance<HelpWindow>();
			window.title= title;
			window.content= new List<Page>(content);

			var rect= new Rect();
			rect.width= widthPixels;
			rect.height= heightPixels;
			rect.center= new Vector2(Screen.currentResolution.width / 2, Screen.currentResolution.height / 2);
			window.position= rect;
			window.currentPageIndex= -1; //hopefully paranoia

			window.ShowUtility();
			return window;
		}

		static List<HelpWindow> All= new List<HelpWindow>();

		//------------------------------
		public new string title;
		public Type ID;
		GUIStyle contentStyle;

		public List<Page> content= new List<Page>();
		public int currentPageIndex= -1;
		Page currentPage { get { return content[currentPageIndex]; } }

		void OnEnable() {
			All.Add(this);
		}

		void OnDisable() {
			All.Remove(this);
		}

		void OnGUI() {
			if(TextInputDialog.somethingIsModal) return;

			if(currentPageIndex < 0) {
				contentStyle= new GUIStyle(EditorStyles.label);
				contentStyle.richText= true;
				contentStyle.padding= new RectOffset(5,5,5,5);

				SetPage(0);
			}

			//------------------
			var page= currentPage;
			var e= Event.current;
			if(e.type == EventType.KeyDown) {
				switch(e.keyCode) {
					case KeyCode.LeftArrow: SetPage(currentPageIndex - 1); break;
					case KeyCode.UpArrow: SetPage(currentPageIndex - 1); break;
					case KeyCode.PageUp: SetPage(currentPageIndex - 1); break;
					case KeyCode.RightArrow: SetPage(currentPageIndex + 1); break;
					case KeyCode.DownArrow: SetPage(currentPageIndex + 1); break;
					case KeyCode.PageDown: SetPage(currentPageIndex + 1); break;
					case KeyCode.Home: SetPage(0); break;
					case KeyCode.End: SetPage(content.Count - 1); break;

					case KeyCode.Escape: Close(); break;
				}
				return;
			}

			//------------------
			var rect= new Rect(0, 0, position.width, position.height);
			GUILayout.BeginArea(rect);
			GUILayout.BeginVertical();
			if(page.image != null) GUILayout.Label(page.image, GUILayout.MaxWidth(position.width));
			if(page.text != null) GUILayout.Label(page.text, contentStyle, GUILayout.MaxWidth(position.width));
			GUILayout.EndVertical();
			GUILayout.EndArea();

			GUILayout.BeginArea(new Rect(0, rect.yMax - 40, rect.width, 40));
			GUILayout.BeginHorizontal();

			if(currentPageIndex > 0) {
				if(GUILayout.Button(EditorGUIUtility.IconContent("back@2x"), GUILayout.Height(40))) {
					SetPage(currentPageIndex - 1);
				}
			}

			GUILayout.FlexibleSpace();

			if(currentPageIndex < content.Count - 1) {
				if(GUILayout.Button(EditorGUIUtility.IconContent("forward@2x"), GUILayout.Height(40))) {
					SetPage(currentPageIndex + 1);
				}
			}

			GUILayout.EndHorizontal();
			GUILayout.EndArea();
		}

		void SetPage(int index) {
			if(index < 0) return;
			if(index >= content.Count) return;

			currentPageIndex= index;
			var title= this.title;
			if(!string.IsNullOrEmpty(currentPage.title)) title += " - " + currentPage.title;
			if(content.Count > 1) title += " - Page " + (currentPageIndex + 1) + " of " + content.Count;
			titleContent= new GUIContent(title, EditorGUIUtility.IconContent("_Help").image);

			Repaint();
		}
	}
}