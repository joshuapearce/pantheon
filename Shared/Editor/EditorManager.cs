﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;

using UnityEditor;
using UnityEngine;
using UnityEngine.Networking;

namespace Pantheon {
	[InitializeOnLoad]
	public static class EditorManager {
		#if UNITY_2020_1_OR_NEWER
		public const int UnityMajorVersion= 2020;
		#elif UNITY_2019_1_OR_NEWER
		public const int UnityMajorVersion= 2019;
		#elif UNITY_2018_1_OR_NEWER
		public const int UnityMajorVersion= 2018;
		#elif UNITY_2017_1_OR_NEWER
		public const int UnityMajorVersion= 2017;
		#endif

		///<summary>True when the editor has not recompiled code since Unity was first opened</summary>
		public static bool isFirstEditorInstance { get; private set; }

		static long editorbuildIndex_= -1;
		public static long editorbuildIndex {
			//Stored in a temp file because storing it in an asset creates git/svn noise
			get {
				if(editorbuildIndex_ < 0) {
					string path= EditorManager.tempDataPath_absolute + "editorbuildIndex";
					if(!File.Exists(path)) {
						editorbuildIndex= 0;
					}else {
						editorbuildIndex= long.Parse(File.ReadAllText(path));
					}
				}

				return editorbuildIndex_;
			}

			private set {
				string path= EditorManager.tempDataPath_absolute + "editorbuildIndex";
				File.WriteAllText(path, value.ToString());
				editorbuildIndex_= value;
			}
		}

		public static string projectPath {
			get {
				return Application.dataPath.Substring(0, Application.dataPath.Length - "Assets/".Length + 1);
			}
		}

		public static string tempDataPath_absolute {
			get {
				return projectPath + tempDataPath;
			}
		}

		public static string tempDataPath {
			get {
				return "Temp/Pantheon_" +  Manager.PantheonGUID + "/";
			}
		}

		public static string tempAssetPath {
			get {
				string path= "Assets/Settings/Pantheon/Temp/";
				if(!Directory.Exists(projectPath + path)) {
					Directory.CreateDirectory(projectPath + path);
					File.WriteAllText(projectPath + path + "/.gitignore", "*");
					File.WriteAllText(projectPath + path + "/.collabignore", "*");
					Log("Created directory '" + path + "', and an optional .gitignore/.collabignore files");
				}
				return path;
			}
		}

		public static string settingsPath {
			get {
				return "Assets/Settings/Pantheon/";
			}
		}

		//Returns the path for shared Pantheon settings
		static string appDataPath_;
		public static string appDataPath {
			get {
				if(appDataPath_ == null) {
					string path;
					if(settings.useRoamingAppdata) {
						path= Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "Unity.Pantheon/");
					}else{
						path= Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "Unity.Pantheon/");
						//path= Path.Combine(Application.persistentDataPath, "../Unity.Pantheon/");
					}

					path= Path.GetFullPath(path);

					if(!Directory.Exists(path)) {
						Debug.Log("Creating Unity.Pantheon directory: " + path);
						Directory.CreateDirectory(path);
					}
					appDataPath_= path;
				}
				return appDataPath_;
			}
		}

		public static long tick { get; private set; }

		public delegate void VoidFunction();
		public delegate void BoolFunction(bool b);
		public delegate void StringFunction(string str);

		public static BoolFunction onEditorFocus;
		static bool hadEditorFocus= false;

		//Works as of Unity 2019.2
		public static bool hasEditorFocus {	get {	return UnityEditorInternal.InternalEditorUtility.isApplicationActive;	}	}

		public static PantheonSettings settings { get { return PantheonSettings.instance; } }
		static HashSet<string> AvailableModules= new HashSet<string>();
		static Dictionary<Type, object> InterfaceInstances= new Dictionary<Type, object>();

		static Queue<string> logQueue= new Queue<string>();

		//--------------------------------
		public static void RegisterModule(string ID, BoolFunction ModuleActivated= null) {
			if(!HasModule(ID)) {
				AvailableModules.Add(ID);
			}

			if(ModuleActivated != null) {
				PantheonSettings.ModuleActivationCallbacks[ID]= ModuleActivated;
			}

			AvailableModules.Add(ID);
		}

		public static void RegisterInterface<T>(T obj) where T:class {
			InterfaceInstances.Add(typeof(T), obj);
		}

		public static T GetInterface<T>() where T:class {
			object obj= null;
			InterfaceInstances.TryGetValue(typeof(T), out obj);
			return obj as T;
		}

		public enum ModuleMode {
			Default,
			Activated,
			Disabled
		}

		public static bool GetModuleMode(string ID) {
			if(!HasModule(ID)) return false;

			ModuleMode mode;

			if(settings.moduleModes.TryGetValue(ID, out mode)) {
				switch(mode) {
					case ModuleMode.Default: break;
					case ModuleMode.Activated: return true;
					case ModuleMode.Disabled: return false;
					default: throw new NotImplementedException();
				}
			}

			mode= GetModuleModeGlobal(ID);

			switch(mode) {
				case ModuleMode.Default: return true;
				case ModuleMode.Activated: return true;
				case ModuleMode.Disabled: return false;
				default: throw new NotImplementedException();
			}
		}

		public static void SetModuleMode(string ID, ModuleMode mode) {
			if(!HasModule(ID)) throw new KeyNotFoundException("Module '" + ID + "' does not exist");

			var prevState= GetModuleMode(ID);
			settings.moduleModes[ID]= mode;

			var newState= GetModuleMode(ID);
			if((prevState != newState) && PantheonSettings.ModuleActivationCallbacks.ContainsKey(ID)) {
				PantheonSettings.ModuleActivationCallbacks[ID].Invoke(newState);
			}
		}

		public static void SetModuleModeGlobal(string ID, ModuleMode mode) {
			EditorPrefs.SetString("Pantheon.ModuleMode."+ID, mode.ToString());
		}

		public static bool HasModule(string ID) {
			return AvailableModules.Contains(ID);
		}

		public static ModuleMode GetModuleModeGlobal(string ID) {
			if(!HasModule(ID)) return ModuleMode.Disabled;

			return (ModuleMode)Enum.Parse(typeof(ModuleMode), EditorPrefs.GetString("Pantheon.ModuleMode."+ID, "Activated"));
		}

		//--------------------------------
		static EditorManager() {
			if(!File.Exists(tempDataPath + "EditorStarted")) {
				isFirstEditorInstance= true;
				Directory.CreateDirectory(tempDataPath);
				File.WriteAllText(tempDataPath + "EditorStarted", "");
			}else{
				isFirstEditorInstance= false;
			}

			Manager.RegisterAssembly(typeof(EditorManager).Assembly);
			Manager.editorRunOnce += Initialize; 
			Manager.editorUpdate += StaticUpdate;
		}

		public static void delayCall(VoidFunction function) {
			delayCall(1, function);
		}

		public static void delayCall(int counter, VoidFunction function) {
			if(counter <= 0) function();
			else EditorApplication.delayCall += () => delayCall(counter - 1, function);
		}

		static void StaticUpdate() {
			++tick;
			//Debug.Log("Tick: " + EditorManager.tick);

			UpdateCoroutines();

			if(hadEditorFocus != hasEditorFocus) {
				if(onEditorFocus != null) onEditorFocus.Invoke(hasEditorFocus);
				hadEditorFocus= hasEditorFocus;
			}

			int counter= 0;
			while(logQueue.Count > 0) {
				if(++counter > 100) {
					Log("Emptying the log queue of " + logQueue.Count + " items, instead of logging them");
					logQueue.Clear();
					break;
				}
				Log(logQueue.Dequeue());
			}
		}

		[SelfInitializing] public static bool debugPantheon {
			get { return EditorManager.Initialized && EditorPrefs.GetBool("Pantheon.debugPantheon", false); }
			set {
				UnityEditor.Menu.SetChecked("Tools/Pantheon/Advanced/Debug Pantheon", value);
				EditorPrefs.SetBool("Pantheon.debugPantheon", value);
				foreach(var window in VulcanWindowManager.AllWindows) window.Repaint();
			}
		}

		[MenuItem("Tools/Pantheon/Advanced/Debug Pantheon", false, ToolsMenu.MenuPriority + 1000)]
		static void ToggleDebug() {
			debugPantheon= !debugPantheon;
		}
		
		[MenuItem("Tools/Pantheon/Advanced/Open Pantheon log", false, ToolsMenu.MenuPriority + 1000)]
		static void OpenLog() {
			EditorUtility.OpenWithDefaultApp(projectPath + "Pantheon/log.txt");
		}

		public static bool Initialized { get; private set; }

		static void Initialize() {
			if(Initialized) return;
			//Debug.Log("Pantheon.EditorManager Initializing");
			Initialized= true;

			//Important to create these early
			if(!Directory.Exists(projectPath + "Pantheon")) {
				Directory.CreateDirectory(projectPath + "Pantheon");
				File.WriteAllText(projectPath + "Pantheon/.gitignore", "*");
				File.WriteAllText(projectPath + "Pantheon/.collabignore", "*");
				Debug.Log("Created project directory Pantheon, and an optional .gitignore/.collabignore file");
				Log("Created project directory Pantheon, and an optional .gitignore/.collabignore file");
			}

			if(!Directory.Exists(projectPath + EditorManager.settingsPath)) {
				Directory.CreateDirectory(projectPath + EditorManager.settingsPath);
				File.WriteAllText(projectPath + EditorManager.settingsPath +".gitignore", "*");
				File.WriteAllText(projectPath + EditorManager.settingsPath +".collabignore", "*");
				Log("Created assets directory '" + EditorManager.settingsPath + "', and an optional .gitignore/.collabignore file");
			}
			//Minimum initialization done

			if(isFirstEditorInstance) editorbuildIndex= 0;
			else ++editorbuildIndex;

			hadEditorFocus= hasEditorFocus;

			Log("Pantheon.EditorManager started" + (isFirstEditorInstance ? " (First instance)" : ""), debugPantheon);

			foreach(var item in PantheonSettings.ModuleActivationCallbacks) {
				if(!settings.moduleModes.ContainsKey(item.Key)) {
					settings.moduleModes.Add(item.Key, ModuleMode.Default);
				}
				item.Value(GetModuleMode(item.Key));
			}

			//Works as of Unity 2017.4.30f1
			//Works as of Unity 2019.1.7f1
			//Works as of Unity 2019.2.0f1
			{
				System.Reflection.FieldInfo field= typeof (EditorApplication).GetField("globalEventHandler", System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.NonPublic);
				if(field == null) {
					Debug.LogWarning("Pantheon was unable to hook into UnityEditor.EditorApplication.globalEventHandler. Hotkeys may not function.");

				}else{
					EditorApplication.CallbackFunction value= (EditorApplication.CallbackFunction)field.GetValue(null);

					value += ()=> {
						var e= Event.current;
						switch(e.type) {
							case EventType.KeyDown: break;
							case EventType.KeyUp: break;
							default: return;
						}
						if(globalKeyEventListener != null) globalKeyEventListener.Invoke(e);
					};

					field.SetValue(null, value);
				}
			}
		}

		//------------------------------
		public static EditorWindow FindWindow(Type t) {
			UnityEngine.Object[] windows= Resources.FindObjectsOfTypeAll(t);
			if(windows == null) return null;
			if(windows.Length == 0) return null;
			return windows[0] as EditorWindow;
		}

		public static T FindWindow<T>() where T:EditorWindow {
			return FindWindow(typeof(T)) as T;
		}

		//------------------------------
		public static void ShowHelpFile(string path) {
			if(path.StartsWith("Assets/")) {
				path= Application.dataPath + path.Substring(6);
			}else if(Path.GetDirectoryName(path) == "") {
				path= Application.dataPath + "/Pantheon/Help/" + path;
			}

			if(!File.Exists(path)) {
				Debug.LogError("Could not find help file '" + path + "'");
				return;
			}

			Debug.Log("Opening " + path);
			EditorUtility.OpenWithDefaultApp(path);
		}

		public static void Notify(string message, Texture icon= null) {
			Notify(new GUIContent(message, icon));
		}

		public static void Notify(GUIContent content) {
			if(!Manager.isUnityThread()) {
				Manager.editorRunOnce += () => Notify(content);
				return; 
			}

			var sceneView= SceneView.lastActiveSceneView;
			if(sceneView != null) sceneView.ShowNotification(content);

			var gameView= FindWindow(UnityReflection.GameView.t_GameView);
			if(gameView != null) gameView.ShowNotification(content);
		}

		public static void Log(string msg, bool toConsole= false) {
			if(debugPantheon || toConsole) {
				if(msg.StartsWith("pantheon", StringComparison.CurrentCultureIgnoreCase)) {
					Debug.Log(msg);
				}else{
					Debug.Log("Pantheon: " + msg);
				}
			}

			if(!Initialized) {
				logQueue.Enqueue(msg);
			}else{
				File.AppendAllText(
					projectPath + "Pantheon/log.txt", 
					DateTime.Now.ToString() +
					//"; " + Application.productName + 
					": " + msg + "\n"
				);
			}
		}

		//--------------------------------
		public static SettingsType GetSettings<SettingsType>(string settingsFileName) 
			where SettingsType: ScriptableObject
		{
			return GetSettings(settingsFileName, typeof(SettingsType)) as SettingsType;
		}
		 
		public static ScriptableObject GetSettings(string settingsFileName, Type settingsType) {
			if(!settingsFileName.EndsWith(".asset")) settingsFileName += ".asset";
			var dir= settingsPath;
			var path= dir + settingsFileName;
			var settings= AssetDatabase.LoadAssetAtPath(path, settingsType) as ScriptableObject;

			if(settings == null) {
				Debug.Log("Creating settings asset: " + path);
				Directory.CreateDirectory(dir);
				settings= ScriptableObject.CreateInstance(settingsType);
				//Debug.Log(settingsType + ": " + (settings != null));
				AssetDatabase.CreateAsset(settings, path);
			}

			return settings;
		}

		//--------------------------------
		public delegate void EventListener(Event e);
		public static EventListener globalKeyEventListener;

		//--------------------------------
		static Stack<IEnumerator> coroutines= new Stack<IEnumerator>();

		public static void StartCoroutine(IEnumerator routine) {
			coroutines.Push(routine);
		}

		static void UpdateCoroutines() {
			if((coroutines == null) || (coroutines.Count == 0)) return;

			var list= coroutines;
			coroutines= new Stack<IEnumerator>();
			foreach(var c in list) {
				if(c.MoveNext()) coroutines.Push(c);
			}
		}

		//--------------------------------
		[MenuItem("Tools/Pantheon/Advanced/Import settings", false, ToolsMenu.MenuPriority + 1002)]
		public static void QuickImportSettings() {
			var source= Path.GetFullPath(appDataPath + "ExportedSettings/");
			ImportSettings(source);
		}

		[MenuItem("Tools/Pantheon/Advanced/Export settings", false, ToolsMenu.MenuPriority + 1003)]
		public static void QuickExportSettings() {
			var destination= Path.GetFullPath(appDataPath + "ExportedSettings/");
			var backup= Path.GetFullPath(appDataPath + "ExportedSettings.previous/");

			if(Directory.Exists(destination)) {
				if(Directory.Exists(backup)) Directory.Delete(backup, true);
				Directory.Move(destination, backup);
			}

			ExportSettings(destination);
		}


		static void ImportSettings(string source) {
			var destination= Path.GetFullPath(settingsPath);
			var backup= Path.GetFullPath(appDataPath + "ImportedSettings.previous/");

			Debug.Log("Copying current settings for safety");
			ExportSettings(backup);

			foreach(var path in Directory.GetFiles(source, "*.settings.asset")) {
				var destfile= Path.GetFullPath(destination + Path.GetFileName(path));
				if(File.Exists(destfile)) File.Delete(destfile);
				File.Copy(path, destfile);

				var relativePath= Path.GetFullPath("Assets/");
				var assetPath= "Assets/"+ destfile.Substring(relativePath.Length);
				Debug.Log(assetPath);
				AssetDatabase.ImportAsset(assetPath);
			}

			//--------------------------------
			string str= File.ReadAllText(Path.Combine(source, "editorsettings.txt"));
			var values= new Dictionary<string, string>();
			foreach(var line in str.Split('\n')) {
				if((line == null) || (line.Trim() == "")) continue;

				int index= line.IndexOf(':');
				var fieldname= line.Substring(0, index).Trim();
				var fieldvalue= line.Substring(index + 1).Trim();
				values.Add(fieldname, fieldvalue);
			}

			foreach(var module in ModuleSettings.All) {
				foreach(var attribInfo in Manager.GetMemberAttributesOf<SettingsFieldAttribute>(module.GetType())) {
					var attrib= attribInfo.Item1;
					if(!attrib.systemWide) continue;
					var member= attribInfo.Item2;
					string fieldname= module.keyName + "/" + attrib.keyName;

					string value;
					if(!values.TryGetValue(fieldname, out value)) {
						Debug.LogWarning("Field '" + fieldname + "' was not found in the imported data");
						continue;
					}

					Type type;
					if(member is FieldInfo) type= (member as FieldInfo).FieldType;
					else if(member is PropertyInfo) type= (member as PropertyInfo).PropertyType;
					else {
						Debug.LogError("Could not import field due to unhandled member type" + module.displayName + "/" + attrib.displayName);
						continue;
					}

					object valueobj;
					if(type == typeof(bool)) valueobj= bool.Parse(value);
					else if(type == typeof(int)) valueobj= int.Parse(value);
					else if(type == typeof(float)) valueobj= float.Parse(value);
					else if(type == typeof(string)) valueobj= value;
					else{
						Debug.LogError("Unsupported field type '" + fieldname + "' = " + type.Name);
						continue;
					}

					Debug.Log(fieldname + " = " + valueobj.ToString());

					if(member is FieldInfo) (member as FieldInfo).SetValue(module, valueobj);
					else if(member is PropertyInfo) (member as PropertyInfo).SetValue(module, valueobj, null);
				}
			}

			//--------------------------------
			Debug.Log("Settings imported from " + source);
		}

		static void ExportSettings(string toDirectory, bool silently= false) {
			var destination= Path.GetFullPath(toDirectory);
			var source= Path.GetFullPath(settingsPath);

			ModuleSettings.SetAllDirty();
			AssetDatabase.SaveAssets();

			if(Directory.Exists(destination)) Directory.Delete(destination, true);
			Directory.CreateDirectory(destination);

			foreach(var path in Directory.GetFiles(source, "*.settings.asset")) {
				File.Copy(path, destination + Path.GetFileName(path));
			}

			//--------------------------------
			var list= new List<Tuple<string, string>>();

			foreach(var module in ModuleSettings.All) {
				foreach(var attribInfo in Manager.GetMemberAttributesOf<SettingsFieldAttribute>(module.GetType())) {
					var attrib= attribInfo.Item1;
					if(!attrib.systemWide) continue;
					var member= attribInfo.Item2;

					string value;
					if(member is FieldInfo) {
						value= (member as FieldInfo).GetValue(module).ToString();
					}else if(member is PropertyInfo) {
						value= (member as PropertyInfo).GetValue(module, null).ToString();
					}else{
						Debug.LogError("Could not export field due to unhandled member type" + module.displayName + "/" + attrib.displayName);
						continue;
					}

					string fieldname= module.keyName + "/" + attrib.keyName;

					if(value.Contains("\n")) {
						Debug.LogError("Settings field " + fieldname + " contains lines breaks, which are not currently supported");
						continue;
					}

					list.Add(new Tuple<string, string>(fieldname, value));
				}
			}

			list.Sort((a,b)=> a.Item1.CompareTo(b.Item1));
			string str= "";
			foreach(var i in list) str += i.Item1 + ": " + i.Item2 + "\n";
			File.WriteAllText(Path.Combine(destination, "editorsettings.txt"), str);
			
			//--------------------------------
			if(!silently) Debug.Log("Settings exported to " + destination + "\nWarning: This feature is experimental");
		}

		//----------------------------------
		public static void OpenInspector() {
			EditorApplication.ExecuteMenuItem("Window/General/Inspector");
		}

		public static bool OpenFolder(string path, bool Create= false) {
			path= path.Replace(Path.AltDirectorySeparatorChar, Path.DirectorySeparatorChar);
			path= path.TrimEnd(Path.DirectorySeparatorChar);

			if(EditorManager.debugPantheon) Debug.Log("Opening \"" + path + "\"");

			if(!Directory.Exists(path)) {
				if(!Create) {
					Debug.Log("Path not found: \"" + path + "\"");
					return false;
				}

				if(EditorManager.debugPantheon) Debug.Log("Creating \"" + path + "\"");
				Directory.CreateDirectory(path);
				if(!Directory.Exists(path)) return false;
			}

			System.Diagnostics.Process.Start(path);
			return true;
		}
	}
}