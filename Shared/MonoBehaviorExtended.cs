﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class MonoBehaviourExtended:
	MonoBehaviour
{
	protected MonoBehaviourExtended() {
		Pantheon.SelfInitializingAttribute.InitializeObject(this, false);
	}


	//public CrunchBug.LogWrapperStruct Log {
	//	get { return new CrunchBug.LogWrapperStruct { log= CrunchBug.Log.Default, messageType= CrunchBug.LogEventType.Info, context= this }; }
	//	set { /*this setter only exists for syntax reasons, for the += operator */ }
	//}

	//public static CrunchBug.LogWrapperStruct StaticLog {
	//	get { return new CrunchBug.LogWrapperStruct { log= CrunchBug.Log.Default, messageType= CrunchBug.LogEventType.Info, context= null }; }
	//	set { /*this setter only exists for syntax reasons, for the += operator */ }
	//}


	//These functions provide non string based overloads for sending messages. This provides security against refactoring or typos.
	//Usage: this.SendMessage(MemberFunction) and this.SendMessage(MemberFunction, argument)
	//this. is only shown for clarity, it's not needed when using this class
	public void SendMessage(MonoBehaviourExtensions.Function function) {
		MonoBehaviourExtensions.SendMessage(this, function);
	}

	public void SendMessage<A>(MonoBehaviourExtensions.Function_A<A> function, A argument) {
		MonoBehaviourExtensions.SendMessage(this, function, argument);
	}

	public void BroadcastMessage(MonoBehaviourExtensions.Function function) {
		MonoBehaviourExtensions.BroadcastMessage(this, function);
	}

	public void BroadcastMessage<A>(MonoBehaviourExtensions. Function_A<A> function, A argument) {
		MonoBehaviourExtensions.BroadcastMessage(this, function, argument);
	}

	public void SendMessageDownwards(MonoBehaviourExtensions.Function function) {
		MonoBehaviourExtensions.SendMessageDownwards(this, function);
	}

	public void SendMessageDownwards<A>(MonoBehaviourExtensions. Function_A<A> function, A argument) {
		MonoBehaviourExtensions.SendMessageDownwards(this, function, argument);
	}

	public void Dispose() {
		//GameBase.ReuseableObject.Dispose(gameObject);
	}
}
