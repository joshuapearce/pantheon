﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Reflection;
using UnityEngine;

namespace Pantheon {
	public static class BufferPool {
		/*
		 This class recycles objects such as List<> etc
		 Optionally, a GroupID can be used to automatically "scope out" all relevant objects
		 */

		static int groupID_counter= 0;
		static Stack<int> groupIDs= new Stack<int>();
		public static int GetGroupID() {
			if(groupIDs.Count > 0) return groupIDs.Pop();
			else return ++groupID_counter;
		}

		public static int DebugCounter= 0;

		public static T Create<T>() where T:class, new() {
			++DebugCounter;
			return CreateContainer<T>().Get();
		}

		public static T Create<T>(int GroupID) where T : class, new() {
			var v = Create<T>();
			entries.Add(new Entry { GroupID= GroupID, value= v });
			return v;
		}

		public static void Return<T>(T value) where T:class, new() {
			if(value == null) return;

			FindContainer(typeof(T)).Return(value);
			Forget(value);
		}

		public static void ReturnEntireGroup(int GroupID) {
			int i= 0;
			while(i < entries.Count) {
				var e= entries[i];
				if(e.GroupID != GroupID) {
					++i;
				}else{
					FindContainer(e.valueType).Return(e.value);
					entries[i]= entries[entries.Count - 1];
					entries.RemoveAt(entries.Count - 1);
					--DebugCounter;
				}
			}

			groupIDs.Push(GroupID);
		}

		public static void Forget<T>(T value) where T:class, new() {
			if(value == null) return;

			for(int i= 0; i<entries.Count; i++) {
				var e= entries[i];
				if(e.value == value) {
					--DebugCounter;
					entries[i]= entries[entries.Count - 1];
					entries.RemoveAt(entries.Count - 1);
					break;
				}
			}
		}

		//--------------------------------
		static Container FindContainer(Type t) {
			Container c;
			if(!containers.TryGetValue(t, out c)) {
				throw new KeyNotFoundException("Buffer container for '" + t.Name + "' not yet created");
			}
			return c;
		}

		static Container<T> CreateContainer<T>() where T:class, new() {
			Container c;
			if(!containers.TryGetValue(typeof(T), out c)) {
				c= new Container<T>();
				containers.Add(typeof(T), c);
			}

			return c as Container<T>;
		}

		static Dictionary<Type, Container> containers= new Dictionary<Type, Container>();

		public abstract class Container {
			public abstract void Return(object value);
		}

		public class Container<T>:
			Container
			where T: class, new()
		{
			public static Action<T> Clear; //This will be auto set for ICollection types, or can be set by the user

			private Stack<T> items= new Stack<T>(32);

			static Container() {
				if(typeof(ICollection).IsAssignableFrom(typeof(T))) {
					//There's no cleaner way to get the Clear() method from all the various containers
					var m= typeof(T).GetMethod("Clear", BindingFlags.Public | BindingFlags.Instance);
					Clear= (v)=> { m.Invoke(v, null); };
				}
			}

			public T Get() {
				if(items.Count > 0) return items.Pop() as T;
				else return new T();
			}

			public override void Return(object value) {
				Return(value as T);
			}

			public void Return(T value) {
				if(value == null) return;

				if(Clear != null) Clear(value);

				items.Push(value);
			}
		}

		//--------------------------------
		struct Entry {
			public int GroupID;
			public object value;
			public Type valueType { get { return value.GetType(); } }
		}

		static List<Entry> entries= new List<Entry>(256);
	}
}