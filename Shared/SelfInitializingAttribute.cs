﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

namespace Pantheon {
	[System.AttributeUsage(AttributeTargets.Property)]  
	#if UNITY_EDITOR
	[InitializeOnLoad]
	#endif
	public class SelfInitializingAttribute:
		Attribute
	{
		public static bool InitializingProperty { get; private set; }

		public bool delayed;

		public SelfInitializingAttribute(bool delayed= true) {
			this.delayed= delayed;
		}

		public void Apply(object obj, PropertyInfo property) {
			InitializingProperty= true;

			try {
				var v= property.GetValue(obj, null);
				if(property.CanWrite) property.SetValue(obj, v, null);

			}catch(Exception e) {
				Debug.LogError("There was an error in a SelfInitializingAttribute, it is being ignored so that Unity doesn't quit");
				Debug.LogException(e);
			}
			InitializingProperty= false;
		}

		//----------------------------------
		static SelfInitializingAttribute() {
			InitializeStatics(false);
			Manager.runOnce += ()=>	InitializeStatics(true);
		}

		[RuntimeInitializeOnLoadMethod]
		private static void InitializeNothing() {

		}

		private static void InitializeStatics(bool delayedInitializers) {
			//This does the actual work of finding all the initializers in the namespace
			foreach(var item in Manager.GetEveryMemberAttribute<SelfInitializingAttribute>(BindingFlags.Static | BindingFlags.SetProperty | BindingFlags.GetProperty)) {
				var property= item.Item2 as PropertyInfo;
				if(property == null) continue;

				var attribute= item.Item1;
				if(attribute.delayed != delayedInitializers) continue;

				attribute.Apply(null, property);
			}
		}

		//----------------------------------
		static Dictionary<Type, List<Tuple<SelfInitializingAttribute, MemberInfo>>> _cachedInfo_InitializeObject= new Dictionary<Type, List<Tuple<SelfInitializingAttribute, MemberInfo>>>();

		public static bool InitializeObject(object obj, bool doDelayed= false) {
			var type= obj.GetType();

			List<Tuple<SelfInitializingAttribute, MemberInfo>> list;
			if(!_cachedInfo_InitializeObject.TryGetValue(type, out list)) {
				list= new List<Tuple<SelfInitializingAttribute, MemberInfo>>(Manager.GetMemberAttributesOf<SelfInitializingAttribute>(type));
				_cachedInfo_InitializeObject[type]= list;
			}

			foreach(var info in list) {
				var attrib= info.Item1;
				if(attrib.delayed != doDelayed) continue;
				var property= info.Item2 as PropertyInfo;

				attrib.Apply(obj, property);
			}
			return true;
		}
	}
}