﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;

using UnityEngine;
using UnityEngine.Profiling;
using UnityEngine.SceneManagement;

namespace Pantheon {
	public class ManagerRuntime:
		MonoBehaviour
	{
		private static ManagerRuntime Instance_;
		public static ManagerRuntime Instance {
			get {
				if((Instance_ == null) && !Application.isPlaying) {
					Instantiate(); 
				}
				return Instance_; 
			}
			private set { Instance_= value; }
		}

		[TextArea] public string note;

		public static void Instantiate(string note= "") {
			if(Instance_ == null) {
				int counter= 0;
				for(int sceneIndex= 0; sceneIndex<SceneManager.sceneCount; sceneIndex++) {
					foreach(var obj in SceneManager.GetSceneAt(sceneIndex).GetRootGameObjects()) {
						++counter;
						var c= obj.GetComponent<ManagerRuntime>();
						if(c != null) {
							Instance= c;
							break;
						}
					}
					if(Instance_ != null) break;
				}

				if(Instance_ == null) {
					var obj= GameObject.Find("~Pantheon");

					if(obj == null) {
						obj= new GameObject("~Pantheon");
						note += "\nInstantiated new object";
					}else{
						note += "\nFound existing object";
					}

					Instance_= obj.GetComponent<ManagerRuntime>();
					if(Instance_ == null) {
						Instance_= obj.AddComponent<ManagerRuntime>();
						note += "\nInstantiated new component";
					}else{
						note += "\nFound existing component";
					}

					Instance.note= note.Trim();
				}

				GameObject.DontDestroyOnLoad(Instance.gameObject);
				Instance.hideFlags |= HideFlags.DontUnloadUnusedAsset;
				Instance.hideFlags |= HideFlags.DontSaveInEditor;
				Instance.hideFlags |= HideFlags.DontSaveInBuild;
				//Instance.hideFlags |= HideFlags.HideInHierarchy;
				//worker.hideFlags |= HideFlags.DontSave;
			}
		}

		void Update() {
			bool inEditor= false;
			#if UNITY_EDITOR
			inEditor= true; //doing it this way so that the code is always tested for validity
			#endif

			if(Instance_ == null) Instance= this;

			if(Instance_ == this) {
				if(!inEditor) {
					Manager.StaticUpdate();
				}

				if(Manager.updateWithGame != null) {
					Manager.updateWithGame.Invoke();
				}
			}
		}
	}
}