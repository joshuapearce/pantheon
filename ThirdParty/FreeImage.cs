﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;

using UnityEngine;

namespace Pantheon {
	public static class FreeImage {
		public enum FREE_IMAGE_FORMAT  {
			FIF_UNKNOWN = -1,
			FIF_BMP = 0,
			FIF_ICO = 1,
			FIF_JPEG = 2,
			FIF_PNG = 13
		}

		static bool? isAvailable_;
		public static bool isAvailable {
			get {
				if(!isAvailable_.HasValue) {
					try {
						isAvailable_= true;
						FreeImage_GetVersion();
					}catch(Exception e) {
						e.GetHashCode(); //just hiding a warning
						isAvailable_= false;
					}
				}
				return isAvailable_.Value;
			}
		}

		[DllImport("FreeImage.dll")]
    public static extern IntPtr FreeImage_GetVersion();

		[DllImport("FreeImage.dll")]
    public static extern IntPtr FreeImage_Load(FREE_IMAGE_FORMAT format, string filename, int flags=0);

		[DllImport("FreeImage.dll")]
    public static extern IntPtr FreeImage_LoadFromMemory(FREE_IMAGE_FORMAT format, byte[] memory, int flags=0);

		[DllImport("FreeImage.dll")]
    public static extern IntPtr FreeImage_ConvertFromRawBits(byte[] memory, int width, int height, int pitch, uint bpp, uint red_mask, uint green_mask, uint blue_mask, bool topdown);

		[DllImport("FreeImage.dll")]
		public static extern void FreeImage_Unload(IntPtr handle);

		[DllImport("FreeImage.dll")]
    public static extern bool FreeImage_Save(FREE_IMAGE_FORMAT format, IntPtr handle, string filename, int flags=0);

		[DllImport("FreeImage.dll")]
    public static extern bool FreeImage_SaveToMemory(FREE_IMAGE_FORMAT format, IntPtr handle, IntPtr memory, int flags=0);
	}
}