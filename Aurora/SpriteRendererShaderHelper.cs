﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#if UNITY_EDITOR
using UnityEditor;
#endif

using UnityEngine;
using UnityEngine.Profiling;

namespace GameBase {
	[RequireComponent(typeof(SpriteRenderer))]
	public class SpriteRendererShaderHelper:
		MonoBehaviourExtended
	{
		#if UNITY_EDITOR
		[MenuItem("GameObject/2D Object/Sprite (with ShaderHelper)", false, 1)]
		static void CreateSprite(MenuCommand menuCommand) {
			//We're "hijacking" the hidden EditorApplication.GOCreationCommands.CreateSprite function, to make this more future proof
			//All we want to do is add a single component anyways
			//Works as of Unity 2019.1.7f1
			
			var creatorType= typeof(UnityEditor.EditorApplication).Assembly.GetType("UnityEditor.GOCreationCommands", true);
			var method= creatorType.GetMethod("CreateSprite", System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.NonPublic);
			method.Invoke(null, new object[] { menuCommand });

			var v= Selection.activeGameObject; //This is the only way to get whatever new object was created. Hopefully the behavior doesn't change!
			v.AddComponent<SpriteRendererShaderHelper>();
		}
		#endif

		public new SpriteRenderer renderer { get { return GetComponent<SpriteRenderer>(); } }

		private void Start() {
			Apply();
		}

		private void OnValidate() {
			Apply();
		}

		public void Apply() {
			var renderer= this.renderer;
			var sprite= renderer.sprite;

			if(sprite == null) return;

			var properties= new MaterialPropertyBlock();
			renderer.GetPropertyBlock(properties);
			Vector4 xyxy;

			var texture= sprite.texture; 
			if(sprite.packed) {
				throw new NotSupportedException("SpriteRendererExtended doesn't currently support sprite atlases");

			}else{
				var textureRect= sprite.textureRect;
				xyxy= new Vector4(
					textureRect.xMin / (float)texture.width,
					textureRect.yMin / (float)texture.height,
					textureRect.xMax / (float)texture.width,
					textureRect.yMax / (float)texture.height
				);

				if(renderer.flipX) { var temp= xyxy.x; xyxy.x= xyxy.z; xyxy.z= temp; }
				if(renderer.flipY) { var temp= xyxy.y; xyxy.y= xyxy.w; xyxy.w= temp; }
			}

			properties.SetVector("__sprite_AtlasRect_xyxy", xyxy);
			renderer.SetPropertyBlock(properties);
		}
	}
}