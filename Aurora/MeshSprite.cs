﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

using UnityEngine;
using UnityEngine.Profiling;
using UnityEngine.Bindings;
using System.Linq;
using UnityEngine.UI;

namespace Pantheon {
	[ExecuteInEditMode]
  //[RequireComponent(typeof(MeshRenderer))]
  //[RequireComponent(typeof(MeshFilter))]
	public class MeshSprite:
		MonoBehaviour
	{
		static List<Color> buffer_colors= new List<Color>(1024);
		static Texture2D failTexture;

		public Texture texture_;
		public Texture texture {
			get {
				if(texture_ != null) return texture_;
				if(sprite != null) return sprite.texture;

				if(failTexture == null) {
					failTexture= new Texture2D(1,1);
					failTexture.SetPixel(0,0, Color.magenta);
					failTexture.Apply();
				}
				return failTexture;
			}

			set {
				texture_= value;
				sprite= null;
			}
		}
		public Sprite sprite;
		public Color color= Color.white;

		public bool flipX;
		public bool flipY;

		public Material material;

		public Bounds spriteBounds {
			get {
				if(sprite != null) return sprite.bounds;
				if(texture != null) return new Bounds(Vector3.zero, new Vector3((float)texture.width / 100.0f, (float)texture.height / 100.0f, 0.0f));
				return new Bounds(Vector3.zero, Vector3.one);
			}
		}

		Vector3 spriteVertexScale {
			get {
				var transform= this.transform as RectTransform;
				if(transform == null) return Vector3.one;

				var bounds= spriteBounds;
				return Vector3.one * Mathf.Min(
					transform.rect.width / bounds.size.x,
					transform.rect.height / bounds.size.y
				);
			}
		}

		Vector2 spriteVertexOffset {
			get {
				var transform= this.transform as RectTransform;
				if(transform == null) return Vector2.zero;

				var size= spriteBounds.size;
				size.Scale(spriteVertexScale);

				return new Vector3(
					(transform.pivot.x - 0.5f) * -size.x,
					(transform.pivot.y - 0.5f) * -size.y
				);
			}
		}

		public int Capacity {
			get { return (vertices == null) ? 0 : vertices.Capacity; }
			set {
				vertices.Capacity=  value;
				if(has_uv0) uv0.Capacity= value;
				if(has_uv1) uv1.Capacity= value;
				if(has_uv2) uv2.Capacity= value;
				if(has_uv3) uv3.Capacity= value;
				if(has_colors) colors.Capacity= value;
			}
		}

		public int vertexCount {
			get {	return (vertices == null) ? 0 : vertices.Count; }
		}

		bool inCanvas { get { return gameObject.GetComponentInParent<Canvas>() != null; } }
		[NonSerialized] new MeshRenderer renderer;
		[NonSerialized] MeshFilter filter;
		[NonSerialized] SpriteGraphic graphic;
		[NonSerialized] MaterialPropertyBlock properties;

		public Mesh mesh; //won't exist for canvas sprites

		Mesh filterMesh {
			get {
				if(filter == null) return null;

				#if UNITY_EDITOR
					if(!Application.isPlaying) return filter.sharedMesh;
				#endif
				return filter.mesh;
			}

			set {
				filter.sharedMesh= value;
			}
		}

		[NonSerialized] List<Vector3> vertices_;
		public List<Vector3> vertices {
			get { if(vertices_ == null) vertices_= new List<Vector3>(4); return vertices_; }
		}
		[NonSerialized] public List<int> triangles;

		[NonSerialized] List<Vector4> uv0_;
		public List<Vector4> uv0 {
			get { if(uv0_ == null) uv0_= new List<Vector4>(Capacity);	return uv0_; }
		}
		public bool has_uv0 { get { return true; } }

		[NonSerialized] List<Vector4> uv1_;
		public List<Vector4> uv1 { get { has_uv1= true; return uv1; } }
		public bool has_uv1 { 
			get {	return uv1_ != null; }
			set {	if(!value) uv1_= null; else if(uv1_ == null) uv1_= new List<Vector4>(Capacity); }
		}

		[NonSerialized] List<Vector4> uv2_;
		public List<Vector4> uv2 { get { has_uv2= true; return uv2; } }
		public bool has_uv2 { 
			get {	return uv2_ != null; }
			set {	if(!value) uv2_= null; else if(uv2_ == null) uv2_= new List<Vector4>(Capacity); }
		}

		[NonSerialized] List<Vector4> uv3_;
		public List<Vector4> uv3 { get { has_uv3= true; return uv3; } }
		public bool has_uv3 { 
			get {	return uv3_ != null; }
			set {	if(!value) uv3_= null; else if(uv3_ == null) uv3_= new List<Vector4>(Capacity); }
		}

		[NonSerialized] List<Color> colors_= null;
		public List<Color> colors { get { has_colors= true; return colors_; } }
		public bool has_colors { 
			get {	return colors_ != null; }
			set {	if(!value) colors_= null; else if(colors_ == null) colors_= new List<Color>(Capacity); }
		}

		//--------------------------------
		class SpriteGraphic:
			Graphic
		{
			MeshSprite owner;

			protected override void Start() {
				base.Start();

				owner= GetComponent<MeshSprite>();
			}

			protected override void OnPopulateMesh(VertexHelper vh) {
				vh.Clear();

				var sprite= owner.sprite;
				var spriteBounds= sprite.bounds;
				var transform= (RectTransform)this.transform;

				var spriteVertexScale= owner.spriteVertexScale;
				var spriteVertexOffset= owner.spriteVertexOffset;

				var rect= transform.rect;

				var vertices= owner.vertices;
				var uv0= (owner.has_uv0 && (owner.uv0.Count >= owner.vertexCount)) ? owner.uv0 : null;
				var uv1= (owner.has_uv1 && (owner.uv1.Count >= owner.vertexCount)) ? owner.uv1 : null;
				var uv2= (owner.has_uv2 && (owner.uv2.Count >= owner.vertexCount)) ? owner.uv2 : null;
				var uv3= (owner.has_uv3 && (owner.uv3.Count >= owner.vertexCount)) ? owner.uv3 : null;
				var colors= (owner.has_colors && (owner.colors.Count >= owner.vertexCount)) ? owner.colors : null;

				var v= new UIVertex();
				for(int i= 0; i<vertices.Count; i++) {
					v.position= vertices[i];
					v.position.Scale(spriteVertexScale);
					v.position += (Vector3)spriteVertexOffset;

					if(uv0 != null) v.uv0= uv0[i];
					if(uv1 != null) v.uv1= uv1[i];
					if(uv2 != null) v.uv2= uv2[i];
					if(uv3 != null) v.uv3= uv3[i];

					v.color= owner.color;
					if(colors != null) v.color *= colors[i];

					vh.AddVert(v);
				}

				var triangles= owner.triangles;
				for(int i= 0; i<owner.triangles.Count; i += 3) {
					vh.AddTriangle(triangles[i + 0], triangles[i + 1], triangles[i + 2]);
				}

			}

      protected override void UpdateMaterial() {
				base.UpdateMaterial();

				owner.RefreshMaterial();
				//material= owner.material;
				canvasRenderer.SetMaterial(owner.material, owner.sprite.texture);
			}
		}

		//--------------------------------
		protected void Update() {
			if((renderer == null) && (graphic == null)) {
				Reinitialize();
			}

			Refresh();
		}

		void OnValidate() {
			renderer= null;
			graphic= null;
		}

		void RefreshGeometry() {
			vertices.Clear();
			uv0.Clear();

			if(sprite != null) {
				foreach(var v in sprite.vertices) {
					vertices.Add(v);
				}

				foreach(var uv in sprite.uv) uv0.Add(uv);

				var spriteTriangles= sprite.triangles;
				if(triangles == null) triangles= new List<int>(spriteTriangles.Length);
				else triangles.Clear();
				foreach(var i in spriteTriangles) triangles.Add(i);

			}else{
				var bounds= spriteBounds;

				vertices.Add(new Vector3(spriteBounds.min.x, spriteBounds.max.y, 0)); uv0.Add(new Vector2(0,1));
				vertices.Add(new Vector3(spriteBounds.max.x, spriteBounds.min.y, 0)); uv0.Add(new Vector2(1,0));
				vertices.Add(new Vector3(spriteBounds.max.x, spriteBounds.max.y, 0)); uv0.Add(new Vector2(1,1));
				vertices.Add(new Vector3(spriteBounds.min.x, spriteBounds.min.y, 0)); uv0.Add(new Vector2(0,0));

				if(triangles == null) triangles= new List<int>(6);
				else triangles.Clear();
				triangles.AddRange(new int[] { 3,0,1, 2,1,0 });
			}

			if(flipX || flipY) {
				var center= spriteBounds.center;
				if(flipX) {
					for(int i= 0; i<vertices.Count; i++) {
						var v= vertices[i];
						v.x= center.x + (center.x - v.x);
						vertices[i]= v;
					}
				}

				if(flipY) {
					for(int i= 0; i<vertices.Count; i++) {
						var v= vertices[i];
						v.y= center.y + (center.y - v.y);
						vertices[i]= v;
					}
				}

				if(flipX ^ flipY) {
					for(int i= 0; i<triangles.Count; i+=3) {
						var temp= triangles[i+1];
						triangles[i+1]= triangles[i+2];
						triangles[i+2]= temp;
					}
				}
			}
		}

		[ContextMenu("Reinitialize")]
		public void Reinitialize() {
			RefreshGeometry();

			if(properties == null) properties= new MaterialPropertyBlock();
			else properties.Clear();

			if(!inCanvas) {
				renderer= GetComponent<MeshRenderer>();
				if(renderer == null) renderer= gameObject.AddComponent<MeshRenderer>();
				renderer.material= material;
				renderer.GetPropertyBlock(properties);

				filter= GetComponent<MeshFilter>();
				if(filter == null) filter= gameObject.AddComponent<MeshFilter>();
				if(mesh == null) mesh= new Mesh();
				else mesh.Clear();

				if(graphic != null) { GameObject.Destroy(graphic); graphic= null; }

			}else{
				if(mesh != null) {
					UnityEngine.Object.Destroy(mesh);
					mesh= null;
				}

				if(renderer != null) { GameObject.Destroy(renderer); renderer= null; }
				if(filter != null) { GameObject.Destroy(filter); filter= null; }

				graphic= GetComponent<SpriteGraphic>();
				if(graphic == null) graphic= gameObject.AddComponent<SpriteGraphic>();
			}

			SendMessage("SpriteReinitializing", SendMessageOptions.DontRequireReceiver);

			//Applying data
			Refresh();
		}

		Vector2 WorldToUV(Vector2 worldpos) {
			return LocalToUV(transform.InverseTransformPoint(worldpos));
		}

		Vector3 SpriteToLocal(Vector2 sprite) {
			throw new NotImplementedException();
		}

		Vector2 LocalToSprite(Vector2 localpos) {
			throw new NotImplementedException();
		}

		Vector2 LocalToUV(Vector3 localpos) {
			return SpriteToUV(LocalToSprite(localpos));
		}

		Vector2 SpriteToUV(Vector2 localpos) {
			throw new NotImplementedException();

			/*
			var texture= this.texture;

			if(texture == null) return Vector2.zero;

			var spriteRect= this.re;

			//inverse lerp without clamp
			var uv= new Vector2(
				(localpos.x - spriteRect.min.x) / spriteRect.width,
				(localpos.y - spriteRect.min.y) / spriteRect.height
			);

			return ConvertUV(uv);
			*/
		}

		Vector2 ConvertUV(Vector2 uv) {
			if((sprite == null) || !sprite.packed) return uv;

			#if UNITY_2018_1_OR_NEWER
			if((sprite.packingRotation & SpritePackingRotation.FlipHorizontal) != 0) {
				uv.x= 1.0f - uv.x;
			}

			if((sprite.packingRotation & SpritePackingRotation.FlipVertical) != 0) {
				uv.y= 1.0f - uv.y;
			}
			#endif

			var texture= this.texture;
			var rect= sprite.rect;

			//lerping without clamping
			return new Vector2(
				(rect.min.x + (rect.width * uv.x)) / texture.width, 
				(rect.min.y + (rect.height * uv.y)) / texture.height
			);
		}

		public void Refresh() {
			RefreshMaterial();

			SendMessage("SpriteRefreshing", SendMessageOptions.DontRequireReceiver);
			
			if(filter != null) {
				if(mesh == null) mesh= new Mesh();
			}

			if(mesh != null) {
				mesh.Clear();

				mesh.SetVertices(vertices);
				if(has_uv0) mesh.SetUVs(0, uv0);
				if(has_uv1) mesh.SetUVs(1, uv1);
				if(has_uv2) mesh.SetUVs(2, uv2);
				if(has_uv3) mesh.SetUVs(3, uv3);

				if(!has_colors) {
					buffer_colors.Clear();
					for(int i= 0; i<vertexCount; i++) buffer_colors.Add(color);
					mesh.SetColors(buffer_colors);

				}else if(color != Color.white) {
					buffer_colors.Clear();
					for(int i= 0; i<vertexCount; i++) buffer_colors.Add(color * colors[i]);
					mesh.SetColors(buffer_colors);

				}else{
					mesh.SetColors(colors);
				}

				mesh.SetTriangles(triangles, 0);

				mesh.RecalculateBounds();
				mesh.UploadMeshData(false);

				if(filter != null) {
					filterMesh= mesh;
				}
			}

			if(graphic != null) {
				graphic.SetVerticesDirty();
				graphic.SetMaterialDirty();
			}
		}

		public void RefreshMaterial() {
			properties.SetTexture("_MainTex", texture);
			if(renderer != null) {
				renderer.SetPropertyBlock(properties);
			}
		}
	}
}