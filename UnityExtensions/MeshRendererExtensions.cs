﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

using UnityEngine;
using UnityEngine.Profiling;

public static partial class MeshRendererExtensions {
	public static void ReinstantiateAllMaterials(this MeshRenderer v) {
		Material[] list= v.sharedMaterials;
		for(int i= 0; i<list.Length; i++) {
			if(list[i] != null) list[i]= new Material(list[i]);
		}
		v.sharedMaterials= list;
	}

	public static bool AddMaterial(this MeshRenderer v, Material material) {
		if(material == null) return false;

		Material[] list= v.sharedMaterials;

		int index= -1;
		for(int i= 0; i<list.Length; i++) {
			var m= v.sharedMaterials[i];
			if(m == material) return false;
			if((m == null) && (index < 0)) index= i;
		}

		if(index < 0) {
			index= list.Length;
			var temp= list;
			list= new Material[list.Length + 1];
			temp.CopyTo(list, 0);
		}

		list[index]= material;
		v.sharedMaterials= list;
		return true;
	}

	public static bool RemoveMaterial(this MeshRenderer v, Material material) {
		var output= new Material[v.sharedMaterials.Length - 1];

		int i= 0;
		foreach(var m in v.sharedMaterials) {
			if(m == material) continue;
			if(i >= output.Length) return false; //overflow, item presumably not found
			output[i++]= m;
		}

		v.sharedMaterials= output;
		return true;
	}

	public static bool ReplaceMaterial(this MeshRenderer v, Material existing, Material replacement) {
		for(int i= 0; i<v.sharedMaterials.Length; i++) {
			if(v.sharedMaterials[i] == existing) {
				v.sharedMaterials[i]= replacement;
				return true;
			}
		}

		return false;
	}
}

