﻿using System.Text;

using UnityEngine;

//Optional: Inherit the class MonoBehaviorExtended instead of MonoBehavior, for easy access to the members
//--------------------------------
public static partial class MonoBehaviourExtensions {
	public static string FullName(this MonoBehaviour v) {
		if(v.gameObject != null) return v.gameObject.FullName();
		return v.name;
	}

	//----------------------------------------------------------------
	public delegate void Function();
	public delegate void Function_A<A>(A argument);

	public static void SendMessage(this MonoBehaviour obj, Function function) {
		obj.SendMessage(function.Method.Name);
	}

	public static void SendMessage<A>(this MonoBehaviour obj, Function_A<A> function, A argument) {
		obj.SendMessage(function.Method.Name, argument);
	}

	public static void BroadcastMessage(this MonoBehaviour obj, Function function) {
		obj.BroadcastMessage(function.Method.Name);
	}
		
	public static void BroadcastMessage<A>(this MonoBehaviour obj, Function_A<A> function, A argument) {
		obj.BroadcastMessage(function.Method.Name, argument);
	}


	//SendMessageDownwards is just an alias for BroadcastMessage, for naming consistency
	public static void SendMessageDownwards(this MonoBehaviour obj, Function function) {
		BroadcastMessage(obj, function);
	}
		
	public static void SendMessageDownwards<A>(this MonoBehaviour obj, Function_A<A> function, A argument) {
		BroadcastMessage(obj, function, argument);
	}

	public static void SendMessageDownwards(this Component v, string methodname) {
		v.BroadcastMessage(methodname);
	}

	public static void SendMessageDownwards(this Component v, string methodname, object parameter) {
		v.BroadcastMessage(methodname, parameter);
	}

	public static void SendMessageDownwards(this Component v, string methodname, object parameter, SendMessageOptions options) {
		v.BroadcastMessage(methodname, parameter, options);
	}
}

