﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;

public static partial class GameObjectExtentions {
	public static void Dispose(this GameObject gameObject) {
		//GameBase.ReuseableObject.Dispose(gameObject);
	}

	//---------------------------------
	public static string FullName(this GameObject obj) {
		StringBuilder str= new StringBuilder(512);
		str.Append(obj.name);

		var t= obj.transform.parent;
		while(t != null) {
			str.Insert(0, "/");
			str.Insert(0, t.name);
			t= t.parent;
		}

		return str.ToString();
	}

	//---------------------------------
	public static Component GetOrAddComponent(this GameObject v, Type componentType) {
		var c= v.GetComponent(componentType);
		if(c == null) c= v.AddComponent(componentType);
		return c;
	}

	public static T GetOrAddComponent<T>(this GameObject v) where T:Component{
		var c= v.GetComponent<T>();
		if(c == null) c= v.AddComponent<T>();
		return c;
	}

	//---------------------------------
	public static IEnumerable<MeshRenderer> GetRenderers(this GameObject v, bool children= true) {
		if(!children) {
			var m= v.GetComponent<MeshRenderer>();
			if(m != null) yield return m;

		}else{
			foreach(var m in v.GetComponentsInChildren<MeshRenderer>()) {
				yield return m;
			}
		}
	}

	public static void AddMaterial(this GameObject v, Material material, bool children= false) {
		foreach(var r in v.GetRenderers(children)) {
			r.AddMaterial(material);
		}
	}

	public static void RemoveMaterial(this GameObject v, Material material, bool children= false) {
		foreach(var r in v.GetRenderers(children)) {
			r.RemoveMaterial(material);
		}
	}

	public static void ReplaceMaterial(this GameObject v, Material existing, Material replacement, bool children= false) {
		foreach(var r in v.GetRenderers(children)) {
			r.ReplaceMaterial(existing, replacement);
		}
	}

	//---------------------------------
	public static IEnumerable<GameObject> GetChildren(this GameObject parent) {
		int i= 0;
		while(i < parent.transform.childCount) {
			yield return parent.transform.GetChild(i++).gameObject;
		}
	}

	public static IEnumerable<GameObject> GetAllChildren(this GameObject parent) {
		int i= 0;
		while(i < parent.transform.childCount) {
			var child= parent.transform.GetChild(i++).gameObject;
			yield return child;
			foreach(var subchild in child.GetAllChildren()) yield return subchild;
		}
	}

	public static List<GameObject> GetChildren(this GameObject parent, List<GameObject> output) {
		if(output == null) output= new List<GameObject>(parent.transform.childCount);
		int i= 0;
		while(i < parent.transform.childCount) {
			output.Add(parent.transform.GetChild(i++).gameObject);
		}
		return output;
	}

	public static List<GameObject> GetAllChildren(this GameObject parent, List<GameObject> output) {
		if(output == null) output= new List<GameObject>(parent.transform.childCount);
		int i= 0;
		while(i < parent.transform.childCount) {
			var child= parent.transform.GetChild(i++).gameObject;
			output.Add(child);
			child.GetAllChildren(output);
		}
		return output;
	}

	//---------------------------------
}